  /**
 * Project Configuration
 * (sails.config.globals)
 *
 * Configure which global variables which will be exposed
 * automatically by Sails.
 *
 * For more information on configuration, check out:
 * http://sailsjs.org/#/documentation/reference/sails.config/sails.config.globals.html
 */
module.exports.project = {

  /***************************************************************************
   * Set the project name.  This can be utilized within email templates      *
   ***************************************************************************/
  name: 'Rendezvous',

  /***************************************************************************
   *                                                                          *
   * Session secret is utilized when creating JSON Web Tokens                 *
   *                                                                          *
   ***************************************************************************/
  secret: 'ewfn09qu43f09qfj94qf*&H#(R',
  tokenTTL: 7200000, // In seconds

  tippingDollarValue: 10,
  minTipTotal: 50,
  /***************************************************************************
   * Set the url of the consuming website.  This will be used for e-mails    *
   * and generated links. Ensure that the correct URL is defined for each    *
   * environment configuration, if it should differ.
   ***************************************************************************/

  giftAFriend: {
    template: 'giftAFriend',
    subject: 'Gift From Your Friend.',
    tokenTTL: 7200000 // In milliseconds
  },
  giftAFriendFreeShow: {
    template: 'giftAFriendFreeShow',
    subject: 'Gift From Your Friend.',
    tokenTTL: 7200000 // In milliseconds
  },
  getAccess: {
    template: 'getAccess',
    subject: 'Your Ticket',
    tokenTTL: 7200000 // In milliseconds
  },
  getFreeAccess: {
    template: 'getFreeAccess',
    subject: 'Your Ticket',
    tokenTTL: 7200000 // In milliseconds
  },
  signup: {
    uri: '/verify-account/',
    template: 'signup',
    subject: 'Rendezvous Account verification',
    tokenTTL: 7200000 // In milliseconds
  },
  signupStripe: {
    uri: '/verify-account/',
    template: 'signupStripe',
    subject: 'Rendezvous Account verification',
    tokenTTL: 7200000 // In milliseconds
  },
  signupFan: {
    uri: '/verify-account/',
    template: 'signupFan',
    subject: 'Rendezvous Account verification',
    tokenTTL: 7200000 // In milliseconds
  },
  passwordReset: {
    /***************************************************************************
     * The frontend URI for the password reset form                            *
     * A JSON Web Token will be appended to the end of this URI and linked     *
     * within the password reset e-mail.                                       *
     *                                                                         *
     * Email template: /assets/templates/email/forgotPassword                  *
     ***************************************************************************/
    uri: '/reset-password/',
    template: 'forgotPassword',
    subject: 'Password reset request received',
    tokenTTL: 7200000 // In milliseconds
  },
  passwordAdmin: {
    /***************************************************************************
     * The frontend URI for the password reset form                            *
     * A JSON Web Token will be appended to the end of this URI and linked     *
     * within the password reset e-mail.                                       *
     *                                                                         *
     * Email template: /assets/templates/email/forgotPassword                  *
     ***************************************************************************/
    uri: '/admin-password/',
    template: 'adminPassword',
    subject: 'Password set request received',
    tokenTTL: 7200000 // In milliseconds
  },
  ForgotPassword: {
    /***************************************************************************
     * The frontend URI for the password reset form                            *
     * A JSON Web Token will be appended to the end of this URI and linked     *
     * within the password reset e-mail.                                       *
     *                                                                         *
     * Email template: /assets/templates/email/forgotPassword                  *
     ***************************************************************************/
    uri: '/reset-password/',
    template: 'forgotPassword1',
    subject: 'New Password',
    tokenTTL: 7200000 // In milliseconds
  },

  accountActive: {
    template: 'accountActive',
    subject: 'Your Rendezvous Account Activated',
    tokenTTL: 7200000 // In milliseconds
  },

  finalizeList: {
    template: 'finalizeList',
    subject: 'Rendezvous | Finalized Session List',
    tokenTTL: 7200000 // In milliseconds
  },

  artistShow: {
    template: 'artistShow',
    subject: 'Rendezvous session has been created.',
    tokenTTL: 7200000 // In milliseconds
  },

  adminShow: {
    template: 'adminShow',
    subject: 'New session created.',
    tokenTTL: 7200000 // In milliseconds
  },

  followerShow: {
    template: 'followerShow',
    subject: 'Your favourite presenter added new session.',
    tokenTTL: 7200000 // In milliseconds
  },

  showReminder: {
    template: 'showReminder',
    subject: "It's session time.",
    tokenTTL: 7200000 // In milliseconds
  },

  setlistReminder: {
    template: 'setlistReminder',
    subject: "Set your song list.",
    tokenTTL: 7200000 // In milliseconds
  },

  topTipperEmail: {
    template: 'topTipperEmail',
    subject: 'Rendezvous | Top tipper list of your session',
    tokenTTL: 7200000 // In milliseconds
  },

};
