/**
* Policy Mappings
* (sails.config.policies)
*
* Policies are simple functions which run **before** your controllers.
* You can apply one or more policies to a given controller, or protect
* its actions individually.
*
* Any policy file (e.g. `api/policies/authenticated.js`) can be accessed
* below by its filename, minus the extension, (e.g. "authenticated")
*
* For more information on how policies work, see:
* http://sailsjs.org/#!/documentation/concepts/Policies
*
* For more information on configuring policies, check out:
* http://sailsjs.org/#!/documentation/reference/sails.config/sails.config.policies.html
*/


module.exports.policies = {

    /***************************************************************************
    *                                                                          *
    * Default policy for all controllers and actions (`true` allows public     *
    * access)                                                                  *
    *                                                                          *
    ***************************************************************************/

    // '*': true,

    /***************************************************************************
    *                                                                          *
    * Here's an example of mapping some policies to run before a controller    *
    * and its actions                                                          *
    *                                                                          *
    ***************************************************************************/
    // RabbitController: {

    // Apply the `false` policy as the default for all of RabbitController's actions
    // (`false` prevents all access, which ensures that nothing bad happens to our rabbits)
    // '*': false,

    // For the action `nurture`, apply the 'isRabbitMother' policy
    // (this overrides `false` above)
    // nurture	: 'isRabbitMother',

    // Apply the `isNiceToAnimals` AND `hasRabbitFood` policies
    // before letting any users feed our rabbits
    // feed : ['isNiceToAnimals', 'hasRabbitFood']
    // }

    AdminUserController: {
        getAdminUsers: ['isAuthenticated','isTokenValid', 'isVaildAdminUser'],
        getActiveAdminUsers: ['isAuthenticated','isTokenValid', 'isVaildAdminUser'],
        addAdminUser: ['isAuthenticated','isTokenValid', 'isVaildAdminUser'],
        updateAdminUser: ['isAuthenticated','isTokenValid', 'isVaildAdminUser'],
        updateAdminUserBulk: ['isAuthenticated','isTokenValid', 'isVaildAdminUser'],
        getAdminUser: ['isAuthenticated','isTokenValid', 'isVaildAdminUser'],
        deleteAdminUser: ['isAuthenticated','isTokenValid', 'isVaildAdminUser'],
        getActiveRoles: ['isAuthenticated','isTokenValid', 'isVaildAdminUser'],
        adminUserPasswordSet: true,
        routeAllow: ['isAuthenticated','isTokenValid', 'isVaildAdminUser'],
    },

    GenreController: {
        getGenreDetail: [],
        getGenreShows: [],
        grandPermission:['isAuthenticated', 'isTokenValid', 'isVaildFrontUser'],
        grandStripePermission:['isAuthenticated', 'isTokenValid', 'isVaildFrontUser']
    },

    PaypalController: {
        onboardingComplete: [],
        webhookOnboardingComplete: [],
        webhookPaymentComplete: [],
        setCronPayout: []
    },

    TierController: {
        getTiers: ['isAuthenticated','isTokenValid', 'isVaildAdminUser'],
        addTier: ['isAuthenticated','isTokenValid', 'isVaildAdminUser'],
        updateTier: ['isAuthenticated','isTokenValid', 'isVaildAdminUser'],
        updateTierBulk: ['isAuthenticated','isTokenValid', 'isVaildAdminUser'],
        getTier: ['isAuthenticated','isTokenValid', 'isVaildAdminUser'],
        deleteTier: ['isAuthenticated','isTokenValid', 'isVaildAdminUser'],
    },

    CmsController: {
        getCmses: ['isAuthenticated','isTokenValid', 'isVaildAdminUser'],
        //addCms: ['isAuthenticated','isTokenValid', 'isVaildAdminUser'],
        updateCms: ['isAuthenticated','isTokenValid', 'isVaildAdminUser'],
        //updateCmsBulk: ['isAuthenticated','isTokenValid', 'isVaildAdminUser'],
        getCms: ['isAuthenticated','isTokenValid', 'isVaildAdminUser'],
        //deleteCms: ['isAuthenticated','isTokenValid', 'isVaildAdminUser'],
    },

    FaqController: {
        getFaqs: ['isAuthenticated','isTokenValid', 'isVaildAdminUser'],
        addFaq: ['isAuthenticated','isTokenValid', 'isVaildAdminUser'],
        updateFaq: ['isAuthenticated','isTokenValid', 'isVaildAdminUser'],
        updateFaqBulk: ['isAuthenticated','isTokenValid', 'isVaildAdminUser'],
        getFaq: ['isAuthenticated','isTokenValid', 'isVaildAdminUser'],
        deleteFaq: ['isAuthenticated','isTokenValid', 'isVaildAdminUser'],
    },

    RoleController: {
        getRoleList: ['isAuthenticated','isTokenValid', 'isVaildAdminUser'],
        getSingleRole: ['isAuthenticated','isTokenValid', 'isVaildAdminUser'],
        updateRole: ['isAuthenticated','isTokenValid', 'isVaildAdminUser'],
        updateRoleBulk: ['isAuthenticated','isTokenValid', 'isVaildAdminUser'],
        updateRoleStatus: ['isAuthenticated','isTokenValid', 'isVaildAdminUser'],
        createRole: ['isAuthenticated','isTokenValid', 'isVaildAdminUser'],
        deleteRole: ['isAuthenticated','isTokenValid', 'isVaildAdminUser'],
        getModuleList: ['isAuthenticated','isTokenValid', 'isVaildAdminUser'],
        getModulePermission: ['isAuthenticated','isTokenValid', 'isVaildAdminUser'],
    },

    EmojiController: {
        getEmojis: ['isAuthenticated','isTokenValid', 'isVaildAdminUser'],
        addEmoji: ['isAuthenticated','isTokenValid', 'isVaildAdminUser'],
        updateEmoji: ['isAuthenticated','isTokenValid', 'isVaildAdminUser'],
        updateEmojiBulk: ['isAuthenticated','isTokenValid', 'isVaildAdminUser'],
        getEmoji: ['isAuthenticated','isTokenValid', 'isVaildAdminUser'],
        deleteEmoji: ['isAuthenticated','isTokenValid', 'isVaildAdminUser'],
    },

    GeneralController: {
        getCMSBySlug: [],
        getHomeData: [],
        getActiveFaqs: [],
        getActiveArtist: ['isAuthenticated', 'isTokenValid'],
        updateGeneralInfo: ['isAuthenticated','isTokenValid', 'isVaildAdminUser'],
        getGeneralInfoByKey: [],
        getTipPrize: []
    },

    OnDemandController: {
        getMyOnDemands: ['isAuthenticated', 'isTokenValid'],
        createOnDemand: ['isAuthenticated', 'isTokenValid'],
        deleteOnDemand: ['isAuthenticated', 'isTokenValid'],
        updateOnDemand: ['isAuthenticated', 'isTokenValid'],
        vodPayment: ['isAuthenticated', 'isTokenValid'],
        vodPaymentFinal: ['isAuthenticated', 'isTokenValid'],
        vodPaymentStripe: ['isAuthenticated', 'isTokenValid'],
        getFeatured: [],

        adminVods: ['isAuthenticated', 'isTokenValid'],
        addVod: ['isAuthenticated','isTokenValid', 'isVaildAdminUser'],
        updateVod: ['isAuthenticated','isTokenValid', 'isVaildAdminUser'],
        admingetVod: ['isAuthenticated','isTokenValid', 'isVaildAdminUser'],
        deleteVod: ['isAuthenticated','isTokenValid', 'isVaildAdminUser'],
        updateFeatured: ['isAuthenticated','isTokenValid', 'isVaildAdminUser'],
    },

    ShowController: {
        createShow: ['isAuthenticated', 'isTokenValid', 'isVaildFrontUser'],
        getShows: ['isAuthenticated', 'isTokenValid', 'isVaildFrontUser'],
        getFeaturedShows: [],
        searchShows: [],
        getShowList: ['isAuthenticated','isTokenValid', 'isVaildAdminUser'],
        addShow: ['isAuthenticated','isTokenValid', 'isVaildAdminUser'],
        updateShow: ['isAuthenticated','isTokenValid'],
        updateShowStatus: ['isAuthenticated','isTokenValid', 'isVaildAdminUser'],
        updateShowBulk: ['isAuthenticated','isTokenValid', 'isVaildAdminUser'],
        getShow: ['isAuthenticated','isTokenValid'],
        deleteShow: ['isAuthenticated','isTokenValid', 'isVaildAdminUser'],
        //getActiveClassificationCategory: ['isAuthenticated','isTokenValid'],
        getUserShows: ['isAuthenticated', 'isTokenValid', 'isVaildFrontUser'],
        getShowSongList: ['isAuthenticated', 'isTokenValid', 'isVaildFrontUser'],
        addShowSong: ['isAuthenticated', 'isTokenValid', 'isVaildFrontUser'],
        deleteShowSong: ['isAuthenticated', 'isTokenValid', 'isVaildFrontUser'],
        getShowDetail: [],
        getShowDetails: ['isAuthenticated','isTokenValid', 'isVaildAdminUser'],
        purchaseTip: ['isAuthenticated','isTokenValid', 'isVaildFrontUser'],
        getShowAccess: ['isAuthenticated','isTokenValid', 'isVaildFrontUser'],
        getShowAccessFinal: ['isAuthenticated','isTokenValid', 'isVaildFrontUser'],
        tipToArtist: ['isAuthenticated','isTokenValid', 'isVaildFrontUser'],
        tipToArtistByAdmin: ['isAuthenticated','isTokenValid', 'isVaildAdminUser'],
        giftAFriend: ['isAuthenticated','isTokenValid', 'isVaildFrontUser'],
        giftAFriendFreeShow: ['isAuthenticated','isTokenValid', 'isVaildFrontUser'],
        accountHistoryList: ['isAuthenticated','isTokenValid', 'isVaildFrontUser'],
        checkTicketStatus: [],
        shareFacebook: [],
        getUser: ['isAuthenticated','isTokenValid', 'isVaildFrontUser'],
        getShowData: ['isAuthenticated','isTokenValid', 'isVaildFrontUser'],
        finalizeList: ['isAuthenticated','isTokenValid', 'isVaildFrontUser'],
        checkShowOwner: ['isAuthenticated','isTokenValid', 'isVaildFrontUser'],
        getShowAccessStripe: ['isAuthenticated','isTokenValid', 'isVaildFrontUser'],
        tipToArtistStripe: ['isAuthenticated','isTokenValid', 'isVaildFrontUser'],
        giftAFriendStripe: ['isAuthenticated','isTokenValid', 'isVaildFrontUser'],
    },

    'customer/UserController': {
        getTimezone: [],
        signup: [],
        fbLogin: [],
        verifyAccount: [],
        checkFrontToken: [],
        uploadMedia:['isAuthenticated','isTokenValid','isVaildFrontUser'],
        updateProfile:['isAuthenticated','isTokenValid','isVaildFrontUser'],
        changePassword:['isAuthenticated','isTokenValid','isVaildFrontUser'],
        deleteMedia: ['isAuthenticated','isTokenValid','isVaildFrontUser'],
        getCustomerShows: ['isAuthenticated','isTokenValid','isVaildFrontUser'],
        getMyPurchases: ['isAuthenticated','isTokenValid','isVaildFrontUser'],
        getMyTickets: ['isAuthenticated','isTokenValid','isVaildFrontUser'],

        getUsers: ['isAuthenticated','isTokenValid','isVaildAdminUser'],
        getUser: ['isAuthenticated','isTokenValid','isVaildAdminUser'],
        updateUser: ['isAuthenticated','isTokenValid','isVaildAdminUser'],
        updateUserBulk: ['isAuthenticated','isTokenValid','isVaildAdminUser'],
        deleteUser: ['isAuthenticated','isTokenValid','isVaildAdminUser'],
        getPurchases: ['isAuthenticated','isTokenValid','isVaildAdminUser'],
        exportUser: ['isAuthenticated','isTokenValid','isVaildAdminUser'],

        getFollowers:['isAuthenticated','isTokenValid','isVaildFrontUser'],
        getFavorites:['isAuthenticated','isTokenValid','isVaildFrontUser'],
        followArtist:['isAuthenticated','isTokenValid','isVaildFrontUser'],
        getFollowStatus: ['isAuthenticated','isTokenValid','isVaildFrontUser'],
        soundCheckData: ['isAuthenticated','isTokenValid','isVaildFrontUser'],


        getPresenters: [],

    },

    YouTubeController: {
        addyouTube: ['isAuthenticated','isTokenValid', 'isVaildAdminUser'],
        youTubeList: ['isAuthenticated','isTokenValid', 'isVaildAdminUser'],
        getyouTube: ['isAuthenticated', 'isTokenValid', 'isVaildAdminUser'],
        deleteyouTube: ['isAuthenticated','isTokenValid', 'isVaildAdminUser'],
        updateyouTube: ['isAuthenticated', 'isTokenValid', 'isVaildAdminUser'],
    },
};
