/**
 * Production environment settings
 *
 * This file can include shared settings for a production environment,
 * such as API keys or remote database passwords.  If you're using
 * a version control solution for your Sails app, this file will
 * be committed to your repository unless you add it to your .gitignore
 * file.  If your repository will be publicly viewable, don't add
 * any private information to this file!
 *
 */

module.exports = {

  /***************************************************************************
   * Set the default database connection for models in the production        *
   * environment (see config/connections.js and config/models.js )           *
   ***************************************************************************/

  // models: {
  //   connection: 'someMysqlServer'
  // },

  /***************************************************************************
   * Set the port in the production environment to 80                        *
   ***************************************************************************/

  // port: 80,

  /***************************************************************************
   * Set the log level in production environment to "silent"                 *
   ***************************************************************************/

  // log: {
  //   level: "silent"
  // }

  models: {
     connection: 'liveMysqlServer'
  },

  // PayPal
  partner_id: 'ZQKR8BAZ9W9AW',
  partner_attribution_id: 'demosuni_SP',
  client_id: 'Ab-NH-RydR0y9JXDSiqKc288CVyzdmKgDpT-U4GHXAU4GSCJ9P8Pg2YpU_TyKtsEb3xUTeDPcvUP5zVr',
  secret_key: 'EDu8_ptkxVdMDYSaK_BMY4xpOSYTKPlNKVIS1qHx1D1eNqirX-PqOUZp1NiYldKZ6OPtpWScpk9EQlFw',
  api_url: 'https://api.paypal.com',
  paypal_partner_email: "jim@demosuni.com",

  // Stripe
  stripe_secret_key: 'sk_live_LzJbmxOEXH2l9tDxY7mIn80100BqtsTRWe',
  publishable_kye: 'pk_live_hXiKaSntkBe9pK9VjnPrs6Td00fbupI8at',
  stripe_connect_client:'ca_F40MEswyQSKyFXTFMEi71fzBFNYv4wA0',

  currency: 'AUD',

  fromEmail: 'info@rendezvous.live',
  fromName: 'Rendezvous',
  adminEmail: 'shows@rendezvous.live',
  setListEmail: 'setlist@rendezvous.live',
  //setListEmail: 'online@apra.com.au', // APRA Email for live
  wowza_server_ip: 'streaming.rendezvous.live',
  wowza_server_port: '1935',
  wowza_application: 'DemoSuni',
  //wowza_application: 'webrtc',
  wowza_sdp: 'wss://streaming.rendezvous.live/webrtc-session.json',
  webUrl: 'https://manage.rendezvous.live/index.html#',
  frontUrl: 'https://www.rendezvous.live/index.html#',
    frontUrlNoHash: 'https://www.rendezvous.live/index.html',
  apiUrl:  'https://api.rendezvous.live'

};
