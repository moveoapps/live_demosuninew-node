/**
 * Development environment settings
 *
 * This file can include shared settings for a development team,
 * such as API keys or remote database passwords.  If you're using
 * a version control solution for your Sails app, this file will
 * be committed to your repository unless you add it to your .gitignore
 * file.  If your repository will be publicly viewable, don't add
 * any private information to this file!
 *
 */

module.exports = {

  /***************************************************************************
   * Set the default database connection for models in the development       *
   * environment (see config/connections.js and config/models.js )           *
   ***************************************************************************/

  models: {
     connection: 'localMysqlServer'
  },

  // Paypal
  partner_id: '65JB2VUS3FH5A',
  partner_attribution_id: 'demosuni_SP',
  client_id: 'AQpudsUY_XbrhXV8BgGKxHWpvv0Yon_t5G34niD7mmTjRh9r1J3Xf6JPwsGE0coBlvzJlH1SBRoecHK-',
  secret_key: 'EMZ8GYkoQ1G9JPr9xegzHCroMUclEwVm5hfwRitPyApbUB547aqbI6cW_Viiht17VvACWNBZYzq0-02W',
  api_url: 'https://api.sandbox.paypal.com',
  paypal_partner_email: "jim_sandbox@demosuni.com",

  // Stripe
  stripe_secret_key: 'sk_test_8ziwlvsrARZVThcrGcpHPIac00he8vWFK4',
  publishable_kye: 'pk_test_RrOJnFzvgAtFFaBetzUB3w0a00PxH31NM0',
  stripe_connect_client:'ca_F40M6PhUHhMg5GueVzKWaeeC0bEmbhAB',

  currency: 'AUD',

  fromEmail: 'alastair.cook.3535@gmail.com',
  fromName: 'Alastair Cook',
  adminEmail: 'jason.franklin404@gmail.com',
  setListEmail: 'setlist@rendezvous.live',
  //setListEmail: 'online@apra.com.au', // APRA Email for live
  wowza_server_ip: 'streaming.rendezvous.live',
  wowza_server_port: '1935',
  wowza_application: 'DemoSuni',
  //wowza_application: 'webrtc',
  wowza_sdp: 'wss://streaming.rendezvous.live/webrtc-session.json',
  webUrl: 'http://lovebusk-admin.dev-applications.net/index.html#',
  frontUrl: 'https://demosuni.dev-applications.net/index.html#',
    frontUrlNoHash: 'https://demosuni.dev-applications.net/index.html',
  apiUrl: 'https://api-demosuni.dev-applications.net',
  hookTimeout: 400000,


};
