/**
* Route Mappings
* (sails.config.routes)
*
* Your routes map URLs to views and controllers.
*
* If Sails receives a URL that doesn't match any of the routes below,
* it will check for matching files (images, scripts, stylesheets, etc.)
* in your assets directory.  e.g. `http://localhost:1337/images/foo.jpg`
* might match an image file: `/assets/images/foo.jpg`
*
* Finally, if those don't match either, the default 404 handler is triggered.
* See `api/responses/notFound.js` to adjust your app's 404 logic.
*
* Note: Sails doesn't ACTUALLY serve stuff from `assets`-- the default Gruntfile in Sails copies
* flat files from `assets` to `.tmp/public`.  This allows you to do things like compile LESS or
* CoffeeScript for the front-end.
*
* For more information on configuring custom routes, check out:
* http://sailsjs.org/#!/documentation/concepts/Routes/RouteTargetSyntax.html
*/

module.exports.routes = {

    /***************************************************************************
    *                                                                          *
    * Make the view located at `views/homepage.ejs` (or `views/homepage.jade`, *
    * etc. depending on your default view engine) your home page.              *
    *                                                                          *
    * (Alternatively, remove this and add an `index.html` file in your         *
    * `assets` directory)                                                      *
    *                                                                          *
    ***************************************************************************/

    '/': {
        view: 'homepage'
    },

    /***************************************************************************
    *                                                                          *
    * Custom routes here...                                                    *
    *                                                                          *
    * If a request to a URL doesn't match any of the custom routes above, it   *
    * is matched against Sails route blueprints. See `config/blueprints.js`    *
    * for configuration options and examples.                                  *
    *                                                                          *
    ***************************************************************************/

    /***************************************************************************
    *                                                                          *
    * Password reset routes for users                                          *
    *                                                                          *
    ***************************************************************************/

    'POST /admin/login': 'AdminUser.adminLogin',
    'POST /admin/updateprofile': 'AdminUser.updateAdminProfile',
    'POST /admin/logout': 'AdminUser.logout',
    'POST /admin/reset': 'AdminUser.requestPasswordReset',
    'POST /admin/reset/:token': 'AdminUser.validatePasswordReset',

    'GET /admin/genre': 'Genre.getCategories',
    'GET /admin/activegenre': 'Genre.getActiveCategories',
    'POST /admin/genre/add': 'Genre.addGenre',
    'GET /admin/genre/:id': 'Genre.getGenre',
    'POST /admin/genre/update/:id': 'Genre.updateGenre',
    'POST /admin/genre/updatebulk': 'Genre.updateGenreBulk',
    'POST /admin/genre/delete': 'Genre.deleteGenre',

    'GET /admin/adminuser': 'AdminUser.getAdminUsers',
    'GET /admin/activeadminuser': 'AdminUser.getActiveAdminUsers',
    'POST /admin/adminuser/add': 'AdminUser.addAdminUser',
    'GET /admin/adminuser/:id': 'AdminUser.getAdminUser',
    'POST /admin/adminuser/update/:id': 'AdminUser.updateAdminUser',
    'POST /admin/adminuser/updatebulk': 'AdminUser.updateAdminUserBulk',
    'POST /admin/adminuser/delete': 'AdminUser.deleteAdminUser',
    'GET /admin/roles': 'AdminUser.getActiveRoles',
    'POST /admin/route/allow': 'AdminUser.routeAllow',
    'POST /adminusers/reset/:token': 'AdminUser.adminUserPasswordSet',

    'GET /admin/tier': 'Tier.getTiers',
    'POST /admin/tier/add': 'Tier.addTier',
    'GET /admin/tier/:id': 'Tier.getTier',
    'POST /admin/tier/update/:id': 'Tier.updateTier',
    'POST /admin/tier/updatebulk': 'Tier.updateTierBulk',
    'POST /admin/tier/delete': 'Tier.deleteTier',

    'GET /admin/cms': 'Cms.getCmses',
    //'POST /admin/cms/add': 'Cms.addCms',
    'GET /admin/cms/:id': 'Cms.getCms',
    'POST /admin/cms/update/:id': 'Cms.updateCms',
    //'POST /admin/cms/updatebulk': 'Cms.updateCmsBulk',
    //'POST /admin/cms/delete': 'Cms.deleteCms',

    'GET /admin/faq': 'Faq.getFaqs',
    'POST /admin/faq/add': 'Faq.addFaq',
    'GET /admin/faq/:id': 'Faq.getFaq',
    'POST /admin/faq/update/:id': 'Faq.updateFaq',
    'POST /admin/faq/updatebulk': 'Faq.updateFaqBulk',
    'POST /admin/faq/delete': 'Faq.deleteFaq',

    'GET /admin/user': 'customer/User.getUsers',
    'GET /admin/user/:id': 'customer/User.getUser',
    'POST /admin/user/update/:id': 'customer/User.updateUser',
    'POST /admin/user/updatebulk': 'customer/User.updateUserBulk',
    'POST /admin/user/delete': 'customer/User.deleteUser',
    'GET /admin/export': 'customer/User.exportUser',

    'GET /admin/presenters': 'customer/User.getPresenters',

    'GET /admin/show': 'Show.getShowList',
    'POST /admin/show/add': 'Show.addShow',
    'GET /admin/show/:id': 'Show.getShow',
    'POST /admin/show/update/:id': 'Show.updateShow',
    'POST /admin/show/updatestatus/:id': 'Show.updateShowStatus',
    'POST /admin/show/updatebulk': 'Show.updateShowBulk',
    'POST /admin/show/delete': 'Show.deleteShow',
    'GET /admin/show/details/:id': 'Show.getShowDetails',

    'GET /admin/role/list': 'Role.getRoleList',
    'POST /admin/role/create': 'Role.createRole',
    'POST /admin/role/edit/:id': 'Role.getSingleRole',
    'POST /admin/role/update/:id': 'Role.updateRole',
    'POST /admin/role/status/bulkupdate': 'Role.updateRoleBulk',
    'POST /admin/role/status/update/:id': 'Role.updateRoleStatus',
    'POST /admin/role/delete': 'Role.deleteRole',
    'GET /module/list': 'Role.getModuleList',
    'POST /admin/module/permission': 'Role.getModulePermission',

    'POST /admin/getgeneralinfobykey': 'General.getGeneralInfoByKey',
    'POST /admin/updategeneralinfo': 'General.updateGeneralInfo',

    /* ========== YouTube ========== */
    'GET /admin/youtube-videos': 'YouTube.youTubeList',
    'POST /admin/youtube-video/store': 'YouTube.addyouTube',
    'GET /admin/youtube-video/:id': 'YouTube.getyouTube',
    'POST /admin/youtube-video/delete': 'YouTube.deleteyouTube',
    'POST /admin/youtube-video/update/:id': 'YouTube.updateyouTube',

    'GET /admin/vods': 'OnDemand.adminVods',
    'GET /admin/vod/:id': 'OnDemand.admingetVod',
    'POST /admin/vod/add': 'OnDemand.addVod',
    'POST /admin/vod/update/:id': 'OnDemand.updateVod',
    'POST /admin/vod/delete': 'OnDemand.deleteVod',
    'POST /admin/vod/updatefeatured/:id': 'OnDemand.updateFeatured',

    'GET /sendshowemailsms/:id': 'Show.sendShowEmailSms',

    // ######### *****front routes**** #########
    'GET /front/timezone': 'customer/User.getTimezone',
    'GET /front/countries': 'General.getCountries',
    'POST /front/register': 'customer/User.signup',
    'POST /front/fblogin': 'customer/User.fbLogin',
    'POST /front/verify-account': 'customer/User.verifyAccount',
    'POST /front/check-front-token': 'customer/User.checkFrontToken',
    'PUT /front/show': 'Show.createShow',
    'POST /front/show/update/:id': 'Show.updateShow',
    'GET /front/shows': 'Show.getShows',
    'GET /front/show/featured': 'Show.getFeaturedShows',
    'GET /classificationcategory': 'Show.getActiveClassificationCategory',
    'GET /genre/:id': 'Genre.getGenreDetail',
    'GET /genreshow/:id': 'Genre.getGenreShows',
    'GET /genreyoutube/:id': 'Genre.getGenreYouTube',
    'GET /genreondemand/:id': 'Genre.getGenreOnDemand',
    'GET /front/show/:id': 'Show.getShowDetail',
    'POST /front/giftafriendfreeshow': 'Show.giftAFriendFreeShow',
    'POST /front/giftafriend': 'Show.giftAFriend',
    'POST /front/giftafriendfinal': 'Show.giftAFriendFinal',
    'POST /front/getfreeshowaccess': 'Show.getFreeShowAccess',
    'POST /front/giftafriendstripe': 'Show.giftAFriendStripe',
    'POST /front/purchasetip': 'Show.purchaseTip',
    'POST /front/getshowaccess': 'Show.getShowAccess',
    'POST /front/getshowaccessfinal': 'Show.getShowAccessFinal',
    'POST /front/getshowaccessstripe': 'Show.getShowAccessStripe',
    'POST /front/tiptoartist': 'Show.tipToArtist',
    'POST /front/tiptoartistfinal': 'Show.tipToArtistFinal',
    'POST /front/tiptoartiststripe': 'Show.tipToArtistStripe',
    'POST /admin/tiptoartist': 'Show.tipToArtistByAdmin',
    'GET /front/user_by_token': 'Show.getUser',
    'GET /show/ticketstatus/:id': 'Show.checkTicketStatus',
    'GET /front/searchshow': 'Show.searchShows',
    'GET /account/history/list': 'Show.accountHistoryList',
    'POST /front/check-show-owner': 'Show.checkShowOwner',
    'POST /front/session-password-check': 'Show.checkSessionPassword',


    'POST /front/getuserbyemail': 'customer/User.getUserByEmail',
    'GET /front/show/review/:id': 'Show.getShowData',

    'GET /admin/emoji': 'Emoji.getEmojis',
    'POST /admin/emoji/add': 'Emoji.addEmoji',
    'GET /admin/emoji/:id': 'Emoji.getEmoji',
    'POST /admin/emoji/update/:id': 'Emoji.updateEmoji',
    'POST /admin/emoji/updatebulk': 'Emoji.updateEmojiBulk',
    'POST /admin/emoji/delete': 'Emoji.deleteEmoji',

    'GET /admin/purchases': 'customer/User.getPurchases',
    'GET /front/mypurchases': 'customer/User.getMyPurchases',
    'GET /front/mytickets': 'customer/User.getMyTickets',

    // General route
    'GET /cms/:slug': 'General.getCMSBySlug',
    'GET /faqs': 'General.getActiveFaqs',
    'GET /artist': 'General.getActiveArtist',
    'GET /genre': 'General.getActiveGenre',
    'GET /tipprize': 'General.getTipPrize',
    'GET /home': 'General.getHomeData',
    'GET /grandpermission': 'General.grandPermission',
    'GET /grandstripepermission': 'General.grandStripePermission',
    'POST /front/addbank': 'General.addBank',


    'POST /front/login': 'customer/User.login',
    'POST /front/updateprofile': 'customer/User.updateProfile',
    'POST /front/updatepassword': 'customer/User.changePassword',
    'POST /front/uploadmedia': 'customer/User.uploadMedia',
    'POST /front/media/delete': 'customer/User.deleteMedia',
    'POST /front/logout': 'customer/User.logout',
    'POST /front/reset': 'customer/User.requestPasswordReset',
    'POST /front/reset/:token': 'customer/User.validatePasswordReset',
    'GET /front/getcustomershows': 'customer/User.getCustomerShows',

    'GET /front/getusershows': 'Show.getUserShows',
    'GET /front/getshowsongs/:id': 'Show.getShowSongList',
    'POST /front/addshowsong': 'Show.addShowSong',
    'POST /front/deleteshowsong': 'Show.deleteShowSong',
    'POST /front/finalizelist': 'Show.finalizeList',

    'GET /front/getfollowers': 'customer/User.getFollowers',
    'GET /front/getfavorites': 'customer/User.getFavorites',
    'GET /front/followartist/:id': 'customer/User.followArtist',
    'GET /front/getfollowstatus/:id': 'customer/User.getFollowStatus',
    'GET /front/user-profile/:id': 'customer/User.getUserProfile',
    'GET /front/sound-check/:token': 'customer/User.soundCheckData',
    'GET /front/go-live': 'customer/User.goliveShow',

    'GET /front/getvods/:genre_id?': 'OnDemand.getVods',
    'GET /front/getartistvods/:artist_id?': 'OnDemand.getArtistVods',

    'GET /front/ondemand': 'OnDemand.getMyOnDemands',
    'PUT /front/ondemand': 'OnDemand.createOnDemand',
    'POST /front/ondemand/delete': 'OnDemand.deleteOnDemand',
    'PUT /front/ondemand/update/:id': 'OnDemand.updateOnDemand',
    'GET /front/ondemand/featured': 'OnDemand.getFeatured',

    'POST /front/vodpayment': 'OnDemand.vodPayment',
    'POST /front/vodpaymentfinal': 'OnDemand.vodPaymentFinal',
    'POST /front/vodpaymentstripe': 'OnDemand.vodPaymentStripe',



    'GET /timezones': 'General.getTimezones',
    'POST /show/share/facebook': 'Show.shareFacebook',
    'GET /order/captureid/:order_id': 'Show.getCaptureID',
    'GET /getorderdetail/:order_id': 'Show.getOrderDetail',
    'GET /getpartnerrefereldetail/:partner_referral_id': 'Paypal.getPartnerReferelDetail',

    /* ========== YouTube ========== */
    'GET /front/youtube-videos': 'YouTube.allData',
    'POST /front/youtube-video/store': 'YouTube.createData',
    'GET /front/youtube-video/:id': 'YouTube.getData',

    // ***PayPal***
    'GET /paypal/onboarding_complete': 'Paypal.onboardingComplete',
    'POST /webhook/onboarding_complete': 'Paypal.webhookOnboardingComplete',
    'POST /webhook/payment_complete': 'Paypal.webhookPaymentCompletePost',

    // *****Stripe******
    'GET /stripe/onboarding_complete': 'General.stripeOnboarding',

    // CRON JOBS
    'GET /front/cron': 'General.setCron', // Every Hour (Reminder Email)
    'GET /front/cron/payout': 'Paypal.setCronPayout', // Every Six hours (Payout)
    'GET /front/cron/ondemand-payout': 'Paypal.setCronOnDemandPayout', // Every month (Ondemand Payout)
    'GET /front/cron/getcaptureid': 'Paypal.setCronCaptureID', // Every Hour (Get Order Capture ID)

};
