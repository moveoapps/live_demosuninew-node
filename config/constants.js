/**
* Project Configuration
* (sails.config.globals)
*
* Configure which global variables which will be exposed
* automatically by Sails.
*
* For more information on configuration, check out:
* http://sailsjs.org/#/documentation/reference/sails.config/sails.config.globals.html
*/

module.exports.constants = {

    artist_share: 70, // share in percentage.
    partner_share: 30, // share in percentage.

    // PayPal Charge
    paypal_pr_charge: 2.6, // Charge in percentage.
    paypal_fix_charge: 0.3, // Charge in amount.

    // Stripe Charge
    stripe_pr_charge: 2.9, // Charge in percentage.
    stripe_fix_charge: 0.3, // Charge in amount.

    algorithm: 'aes256',
    key: 'password',
};
