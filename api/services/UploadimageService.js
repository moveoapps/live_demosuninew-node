//UploadImageService.js
//var NotFoundException = require('../exceptions/NotFoundException');
var fs = require('fs');
var request = require('request');
var moment = require('moment');
var gm = require('gm').subClass({ imageMagick: true });
var ffmpeg = require('fluent-ffmpeg');
module.exports = {
	uploadImage: function (req, fieldname,dirname,uploadFile,callback) {
		req.file(fieldname).upload({
			adapter: require('skipper-better-s3'),
			key: sails.config.s3bucket.accesskey,
			secret: sails.config.s3bucket.secretkey,
			bucket: sails.config.s3bucket.bucketname,
			dirname: dirname,
			saveAs: uploadFile,
			maxBytes: 20*1000*1000,
			s3params:
			{
				ACL: 'public-read'
			}
		}, function (err, filesUploaded) {
			if (err){
				console.log("Error == ", err);
				return callback({message: "Fileupload fail"});
			}else {
				return callback(null,{file: filesUploaded[0]});
			}
		});
	},
	uploadVideo: function (req, fieldname,dirname, uploadFile ,callback) {
		req.file(fieldname).upload({
			adapter: require('skipper-better-s3'),
			key: sails.config.s3bucket.accesskey,
			secret: sails.config.s3bucket.secretkey,
			bucket: sails.config.s3bucket.bucketname,
			dirname: dirname,
			saveAs: uploadFile,
			maxBytes: 20*1000*1000,
			s3params:
			{
				ACL: 'public-read'
			}
		}, function (err, filesUploaded) {
			if (err){
				console.log('err', err);
				return callback({message: "Fileupload fail"});
			} else {
				return callback(null,{file: filesUploaded[0]});
			}
		});
	},

	uploadFbImage: function (social_media_id,fieldname,filename,callback) {
		// You can use all the supported options here
		const options ={
			key: sails.config.s3bucket.accesskey,
			secret: sails.config.s3bucket.secretkey,
			bucket: sails.config.s3bucket.bucketname,
			s3params:
			{
				ACL: 'public-read'
			}
		}

		// Create and configure an adapter
		const adapter = require('skipper-better-s3')(options)
		// Now, create a receiver - receiver is a writable stream which accepts
		// other streams that should be uploaded to S3
		// All files will be saved to this S3 directory (this configuration object is optional)
		const receiver = adapter.receive({dirname:fieldname,
			s3params:
			{
				ACL: 'public-read'
			}
		})

		var reqObj = request('https://graph.facebook.com/'+social_media_id+'/picture?width=339&height=514')
		reqObj.pipe(fs.createWriteStream('./.tmp/'+filename))

		reqObj.on('end', function () {
			const file = fs.createReadStream('./.tmp/'+filename)

			// And now, just send the file to the receiver!
			receiver.write(file, () => {
				// Upload complete! You can find some interesting info on the stream's
				// `extra` property
				fs.unlink('./.tmp/'+filename)
				return callback();
			})
		}, function (err, filesUploaded) {
			if (err)
			return callback({message: "Fileupload fail"});
			//return callback(null,{file: filesUploaded[0]});
		});
		// Suppose we want to upload a file which we temporarily saved to disk
		/*fs.readFile('105.jpg', function (err, data) {
			if (err) { throw err; }
			var base64data = new Buffer(data, 'binary');
			var s3 = new AWS.S3();
			s3.client.putObject({
				Bucket: 'mybucketname',
				Key: 'myarchive.tgz',
				Body: base64data
			}).done(function (resp) {
			console.log('Successfully uploaded package.');
		});*/
	},

	uploadImageMultiple: function (req, fieldname,directory, thumbdirectory ,callback) {
		var filename = req.file(fieldname)._files[0].stream.filename;
		var i = filename.lastIndexOf('.');
		var ext = filename.substr(i);
		uploadFileName = new moment().valueOf()+ext;
		req.file(fieldname).upload({
			adapter: require('skipper-better-s3'),
			key: sails.config.s3bucket.accesskey,
			secret: sails.config.s3bucket.secretkey,
			bucket: sails.config.s3bucket.bucketname,
			saveAs: uploadFileName,
			dirname: directory,
			maxBytes: 20*1000*1000,
			s3params:
			{
				ACL: 'public-read'
			}
		}, function (err, filesUploaded) {
			if (err)
			return callback({message: "Fileupload fail"});
			else
			{
				return callback(null,{file: filesUploaded});
			}
		});
	},
	updatePicture: function(data, fieldname, dirname, callback){
		let uploadFile;
		async.series([
			function (cBack) {
				if (typeof data._fileparser != 'undefined' && data._fileparser.upstreams.length > 0) {
					var filename = data.file(fieldname)._files[0].stream.filename;
					var i = filename.lastIndexOf('.');
					var ext = filename.substr(i);

					uploadFile = new moment().valueOf()+ext;
					UploadimageService.uploadImage(data, fieldname, dirname, uploadFile, function(err, image){
						if(err){
							console.log("error =1= ", err);
							return cBack(err.message, null);
						}
						else{
							return cBack(null);
						}
					});
				}
				else
				{
					uploadFile = '';
					return cBack(null);
				}
			}

		], function (err, data) {
			if (err) {
				return callback({message: err.message}, null);
			} else {
				return callback(null, uploadFile);
			}
		});
	},

	uploadVideoMultiple: function (req, fieldname,directory, thumbdirectory ,callback) {
		console.log("multiple video called");
		console.log(directory);
		filename = new moment()+".mp4";
		req.file(fieldname).upload({
			adapter: require('skipper-better-s3'),
			key: sails.config.s3bucket.accesskey,
			secret: sails.config.s3bucket.secretkey,
			bucket: sails.config.s3bucket.bucketname,
			saveAs: filename,
			dirname: directory,
			maxBytes: 20*1000*1000,
			s3params:
			{
				ACL: 'public-read'
			}
		}, function (err, filesUploaded) {
			if (err)
			return callback({message: "Fileupload fail"});
			else
			{
				console.log("uploaded");
				return callback(null,{file: filesUploaded});
			}
		});
	},

	resizeS3Image: function (dirname,filename,thumbdirectory,resizeWidth,resizeHeight,callback) {
		let uploadFile;
		async.series([
			function (secCallback) {
				if(dirname!='')
				var url = sails.config.s3bucket.bucketurl+dirname+'/'+filename;
				else
				var url = sails.config.s3bucket.bucketurl+filename;
				console.log(url);

					var thumbFileName = filename.split('.');
					var i = filename.lastIndexOf('.');
					var ext = filename.substr(i);
					uploadFile = thumbFileName[0]+ext;
					var writeStream = fs.createWriteStream('./.tmp/'+uploadFile);
					request(url).pipe(writeStream).on('finish', function () {
						console.log('resizeWidth',resizeWidth);
						// gm(request(url)).resize(resizeWidth, resizeHeight).stream(function (err, stdout, stderr) {
						// 	console.log('finish..');
						// 	return secCallback(null);
						// });

						var resizePhotos = gm('./.tmp/'+uploadFile)
						.resize(resizeWidth, resizeHeight)
						.write('./.tmp/'+uploadFile, function (err) {
							if (!err){
								console.log('done');
								return secCallback(null);
							}
							else {
								console.log(': error',err);
								return secCallback({message: "Image resize fail"});
							}
						});
					}, function (err, filesUploaded) {
						console.log(err);
						if (err)
						return secCallback({message: "Fileupload fail"});
						return secCallback(null,{file: filesUploaded[0]});
					});

			},
			function (thCallback) {
				const options =
				{ key: sails.config.s3bucket.accesskey
					, secret: sails.config.s3bucket.secretkey
					, bucket: sails.config.s3bucket.bucketname,
					s3params:
					{
						ACL: 'public-read'
					}
				}

				const adapter = require('skipper-better-s3')(options)
				const receiver = adapter.receive({
					dirname:thumbdirectory,
					s3params:
					{
						ACL: 'public-read'
					}
				})
				const file = fs.createReadStream('./.tmp/'+uploadFile);
				receiver.write(file, () => {
					console.log("inside");
					if (fs.existsSync('./.tmp/'+uploadFile)) {
						fs.unlink('./.tmp/'+uploadFile)
					}
					return thCallback(null);
				})
			}
		], function (err, data) {
			if (err) {
				return callback({message: err.message}, null);
			} else {
				return callback(null);
			}
		});

	},

	videoThumb: function (dirname,filename,thumbdirectory,callback) {
		let thumbname;
		async.series([
			function (secCallback) {
				if(dirname!='')
				var url = sails.config.s3bucket.bucketurl+dirname+'/'+filename;
				else
				var url = sails.config.s3bucket.bucketurl+filename;
				console.log(url);
				var thumbFileName = filename.split('.');
				thumbname = thumbFileName[0]+'.png';

				var proc = new ffmpeg(url)
				.on('end', function(err, res) {
					console.log('Screenshots ', err);
					return secCallback(null,{});
				})
				.takeScreenshots({
					count: 1,
					timemarks: [ '2' ], // number of seconds
					filename: thumbname
				}, './.tmp');

			},
			function (thCallback) {
				const options =
				{ key: sails.config.s3bucket.accesskey
					, secret: sails.config.s3bucket.secretkey
					, bucket: sails.config.s3bucket.bucketname,
					s3params:
					{
						ACL: 'public-read'
					}
				}

				const adapter = require('skipper-better-s3')(options)

				const receiver = adapter.receive({dirname:thumbdirectory,
					s3params:
					{
						ACL: 'public-read'
					}})

					const file = fs.createReadStream('./.tmp/'+thumbname)
					receiver.write(file, () => {
						console.log("thumb uploaded");
						fs.unlink('./.tmp/'+thumbname);
						return thCallback(null);
					});
				}
			], function (err, data) {
				if (err) {
					return callback({message: err.message}, null);
				} else {
					return callback(null);
				}
			});

		},

	};
