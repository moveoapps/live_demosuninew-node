// CmsService.js - in api/services
var moment = require('moment');
var _ = require('underscore');

module.exports = {

  /***************************************************************************
   * Do not utilize this method unless you are positive that the web token   *
   * has not expired.  To ensure that the web token is valid, add the        *
   * `isAuthorized` policy to any controllers which will utilize this.
   ***************************************************************************/

 //backend apis
  fetchData: function(data ,criteria ,callback) {
    Cms.find(criteria).sort('createdAt Desc').exec(function(err, foundCmss){
      if (err) {
        return callback({message: err.message}, null);
      } else {
        return callback(null,foundCmss);
      }
    });
  },
  addCms: function(req,callback) {
    let data = req.body;
    let cms;
    let cmsData;
    async.series([
        function (firstCallback) {
            Cms.findOne({'title':data.title}).exec(function(err, foundCms){
              if(err)
                return firstCallback(err, null);
              else
              {
                cms = foundCms;
                return firstCallback(null);
              }
            });
        },
        function (secCallback) {
          if(cms) {
            return secCallback({message: "Cms already exist with same title."}, null);
          } else {
            return secCallback(null);
          }
        },
        function (secondCallback) {
          Cms.create(data).exec(function (err, cmsData) {
            if(err)
              return secondCallback({message: err.message}, null);
            else
            {
              Cms.find({id:cmsData.id}).sort('createdAt').exec(function(err, foundCms){
                if (err) {
                  return secondCallback({message: err.message}, null);
                } else {
                  cmsData = foundCms;
                  return secondCallback(null,foundCms);
                }
              });
            }
          });
        },
    ], function (err, data) {
        if (err) {
            return callback({message: err.message}, null);
        } else {
            return callback(null, cmsData);
        }
    });
  },
  updateCms: function(req, updateArr, criteria, callback) {
    let dataObj;
    let cms;
    async.series([
        function (firstCallback) {
            Cms.findOne(criteria).exec(function(err, foundData){
              if(err)
                return firstCallback(err, null);
              else
              {
                dataObj = foundData;
                return firstCallback(null);
              }
            });
        },
        function (checkCallback) {
            Cms.findOne({id:{not: req.param('id')},'title':req.body.title}).exec(function(err, foundCms){
              if(err)
                return checkCallback(err, null);
              else
              {
                cms = foundCms;
                return checkCallback(null);
              }
            });
        },
        function (checksecCallback) {
          if(cms) {
            return checksecCallback({message: "Cms already exist with same title."}, null);
          } else {
            return checksecCallback(null);
          }
        },
        function (secCallback) {
          if(dataObj)
          {
            Cms.update(criteria, updateArr).exec(function(err, resObj){
              if(err)
                 return secCallback({message: err.message}, null);
              else{
                cmsObj = resObj;
                return secCallback(null, resObj);
              }
            });
          }
          else
          {
            return secCallback({message: "Cms not found."}, null);
          }
        }
    ], function (err, data) {
        if (err) {
            return callback({message: err.message}, null);
        } else {
            return callback(null, cmsObj);
        }
    });
  },
  deleteCms: function(req, criteria, callback) {
    Cms.destroy(criteria).exec(function (err, resData){
      if(err)
        return callback({message: err.message}, null);
      else
      {
        return callback(null,resData);
      }
    });
  },
}
