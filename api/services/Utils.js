
var cleanObj = function clean(obj) {
  for (var propName in obj) {
    if(obj[propName] == null || obj[propName] == 'NULL')
      obj[propName] = '';
    }

  return obj;

};

module.exports = {
    cleanObj: cleanObj,
};
