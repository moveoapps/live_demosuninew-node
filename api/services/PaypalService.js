// RoleService.js - in api/services
var moment = require('moment');
var moment_tz = require('moment-timezone');
var _ = require('underscore');
var request = require('request');

module.exports = {

    /***************************************************************************
    * Do not utilize this method unless you are positive that the web token   *
    * has not expired.  To ensure that the web token is valid, add the        *
    * `isAuthorized` policy to any controllers which will utilize this.
    ***************************************************************************/

    onboardingComplete: function(request, callback){
        //var data = {request:request}
        var postData = {merchantIdInPayPal:request.param('merchantIdInPayPal')};
        User.update({id:request.param('merchantId')}, postData).exec(function(error, data){
            if (error) {
                return callback({message: error.message}, null);
            } else {
                var createObj = {
                    merchantId:request.param('merchantId'),
                    merchantIdInPayPal:request.param('merchantIdInPayPal'),
                    isEmailConfirmed:request.param('isEmailConfirmed'),
                    permissionsGranted:request.param('permissionsGranted'),
                    accountStatus:request.param('accountStatus'),
                    productIntentID:request.param('productIntentID'),
                    consentStatus:request.param('consentStatus'),
                    returnMessage:request.param('returnMessage')
                };
                PaypalUserLog.create(createObj).exec(function(error, data){
                    if (error) {
                        return callback({message: error.message}, null);
                    } else {
                        return callback(null,{status: 'Done'});
                    }
                });
            }
        });
    },
    webhookOnboardingComplete: function(request, callback){
        //var data = {request:request}
        var data = request.body;
        console.log('data.resource.tracking_id',data.resource.tracking_id);
        console.log('data.resource.merchant_id',data.resource.merchant_id);
        User.update({id:data.resource.tracking_id}, {merchantIdInPayPal:data.resource.merchant_id}).exec(function(error, data){
            if (error) {
                return callback({message: error.message}, null);
            } else {
                return callback(null,{status: 'Done'});
            }
        });
    },
    webhookPaymentCompletePost: function(request, callback){
        var data = request.body;

        ShowOrder.update({payment_id:data.resource.parent_payment},{capture_id:data.resource.id,status:'COMPLETED'}).exec(function(err, res){
            if(err) {
                return callback({message: err.message}, null);
            } else {
                return callback(null,{status: 'Done'});
            }
        });
    },
    setCronOnDemandPayout: function(req, callback) {
        let vodPayouts;
        let paypal_token = '';
        let totalAmount;
            //var ticket_total = await ShowTicket.sum('amount').where({show:show.id});
            //var tip_total = await ArtistTip.sum('amount').where({show:show.id});
            async.series([
                function(firCallback) {
                    VodPayment.find({payout_done:0}).populate('order_id').exec(function(err, vodPayments){
                        if(err)
                        return firCallback(err, null);
                        else
                        {
                            vodPayouts = vodPayments;
                            //console.log('vodPayments',vodPayments);
                            return firCallback(null);
                        }
                    });
                },
                function(thCallback) {
                    if (paypal_token == '' || !paypal_token) {
                        var postData = {grant_type:'client_credentials'};
                        var token_url = sails.config.api_url+'/v1/oauth2/token';
                        var auth = "Basic " +new Buffer(sails.config.client_id+":"+sails.config.secret_key).toString('base64');

                        request.post({url:token_url, form: postData, headers : {"Authorization" : auth}}, function(err, httpResponse, paypalBody){
                            if(err) {
                                return thCallback(err.message, null);
                            }
                            else {
                                var parseBody = JSON.parse(paypalBody);
                                paypal_token = parseBody.access_token;
                                //console.log('paypal_token generate',paypal_token);
                                return thCallback(null);
                            }
                        });
                    } else {
                        return thCallback(null);
                    }
                },
                function(fourthCallback) {
                    async.eachSeries(vodPayouts,function(vodPayment,innerEachCallback) {
                        console.log('vodPayment.order_id.capture_id',vodPayment.order_id.capture_id);

                        if (vodPayment.order_id.capture_id && vodPayment.order_id != 'undefined') {
                            console.log('ticket if');
                            PaypalService.payoutPayPalOrder(req, paypal_token, vodPayment, function(err, body){
                                if(err){
                                    return innerEachCallback({message: err.message}, null);
                                } else {
                                    if (body && body != 'undefined') {
                                        vodPayment.update({id:vodPayment.id},{payout_done:1}).exec(function(err, res){
                                            if(err)
                                            return innerEachCallback({message: err.message}, null);
                                            else
                                            {
                                                return innerEachCallback(null);
                                            }
                                        });
                                    } else {
                                        return innerEachCallback(null);
                                    }
                                }
                            });
                        } else {
                            //console.log('ticket else');
                            return innerEachCallback(null);
                        }
                    },function(err){
                        if(err)
                        return fourthCallback(err, null);
                        else
                        return fourthCallback(null);
                    });
                },
            ], function(err) {
                if(err)
                return callback(err, null);
                else{
                    return callback(null);
                }
            });
    },
    setCronPayout: function(req, callback) {
        //let currDate = new Date();
        let currDate = moment_tz().utc().format('YYYY-MM-DD HH:mm:ss');
        let startTime = moment_tz().utc().subtract(7,"d").format('YYYY-MM-DD HH:mm:ss');
        //let endTime = moment_tz().utc().add(1.5,"hours").format('YYYY-MM-DD HH:mm:ss');
        //console.log('startTime',startTime);
        //console.log('endTime',endTime);
        let ticketPayouts;
        let tipPayouts;
        let paypal_token = '';
        let totalAmount;
        Show.find({show_end_date:{'<=':startTime}, status: 'Published', payout_done: 0}).populate(['artist','timezone']).sort('show_end_date ASC').exec(function(err, resShows){
            if(err)
            return callback(err, null);
            else
            {
                if(resShows) {

                    async.eachSeries(resShows,function(show,eachCallback) {

                        //var ticket_total = await ShowTicket.sum('amount').where({show:show.id});
                        //var tip_total = await ArtistTip.sum('amount').where({show:show.id});
                        async.series([
                            function(firCallback) {
                                ShowTicket.find({show:show.id, payout_done:0, is_free:0}).populate('order_id').exec(function(err, showTickets){
                                    if(err)
                                    return firCallback(err, null);
                                    else
                                    {
                                        ticketPayouts = showTickets;
                                        //console.log('ticketPayouts',ticketPayouts);
                                        return firCallback(null);
                                    }
                                });
                            },
                            function(secCallback) {
                                ArtistTip.find({show:show.id, payout_done:0}).populate('order_id').exec(function(err, artistTip){
                                    if(err)
                                    return secCallback(err, null);
                                    else
                                    {
                                        tipPayouts = artistTip;
                                        //console.log('tipPayouts',tipPayouts);
                                        return secCallback(null);
                                    }
                                });
                            },
                            function(thCallback) {


                                if (paypal_token == '' || !paypal_token) {
                                    var postData = {grant_type:'client_credentials'};
                                    var token_url = sails.config.api_url+'/v1/oauth2/token';
                                    var auth = "Basic " +new Buffer(sails.config.client_id+":"+sails.config.secret_key).toString('base64');

                                    request.post({url:token_url, form: postData, headers : {"Authorization" : auth}}, function(err, httpResponse, paypalBody){
                                        if(err) {
                                            return thCallback(err.message, null);
                                        }
                                        else {
                                            var parseBody = JSON.parse(paypalBody);
                                            paypal_token = parseBody.access_token;
                                            //console.log('paypal_token generate',paypal_token);

                                            return thCallback(null);
                                        }
                                    });
                                } else {
                                    return thCallback(null);
                                }
                            },
                            function(fourthCallback) {
                                async.eachSeries(ticketPayouts,function(showTicket,innerEachCallback) {
                                    if (showTicket.order_id.capture_id && showTicket.order_id != 'undefined') {
                                        //console.log('ticket if');
                                        PaypalService.payoutPayPalOrder(req, paypal_token, showTicket, function(err, body){
                                            if(err){
                                                return innerEachCallback({message: err.message}, null);
                                            } else {
                                                if (body && body != 'undefined') {
                                                    ShowTicket.update({id:showTicket.id},{payout_done:1}).exec(function(err, res){
                                                        if(err)
                                                        return innerEachCallback({message: err.message}, null);
                                                        else
                                                        {
                                                            return innerEachCallback(null);
                                                        }
                                                    });
                                                } else {
                                                    return innerEachCallback(null);
                                                }

                                            }
                                        });
                                    } else {
                                        //console.log('ticket else');
                                        return innerEachCallback(null);
                                    }
                                },function(err){
                                    if(err)
                                    return fourthCallback(err, null);
                                    else
                                    return fourthCallback(null);
                                });
                            },
                            function(fifthCallback) {
                                //console.log('fifthCallback');
                                async.eachSeries(tipPayouts,function(artistTip,innerEach2Callback) {
                                    if (artistTip.order_id.capture_id && artistTip.order_id != 'undefined') {
                                        //console.log('tip if');
                                        PaypalService.payoutPayPalOrder(req, paypal_token, artistTip, function(err, body){
                                            if(err){
                                                return innerEach2Callback({message: err.message}, null);
                                            } else {
                                                if (body && body != 'undefined') {
                                                    ArtistTip.update({id:artistTip.id},{payout_done:1}).exec(function(err, res){
                                                        if(err)
                                                        return innerEach2Callback({message: err.message}, null);
                                                        else
                                                        {
                                                            return innerEach2Callback(null);
                                                        }
                                                    });
                                                } else {
                                                    return innerEach2Callback(null);
                                                }
                                            }
                                        });
                                    } else {
                                        //console.log('tip else');
                                        return innerEach2Callback(null);
                                    }
                                },function(err){
                                    if(err)
                                    return fifthCallback(err, null);
                                    else
                                    return fifthCallback(null);
                                });
                            },
                        ], function(err) {
                            if(err)
                            return eachCallback(err, null);
                            else{
                                Show.update({id:show.id},{payout_done:1}).exec(function(err, res){
                                    if(err)
                                    return eachCallback({message: err.message}, null);
                                    else
                                    {
                                        return eachCallback(null);
                                    }
                                });
                            }
                        });
                    } ,function(err){
                        if(err)
                        return callback(err, null);
                        else
                        return callback(null);
                    });
                } else {
                    return callback(null,{});
                }
            }
        });
    },
    setCronPayout_OLD: function(req, callback) {
        //let currDate = new Date();
        let currDate = moment_tz().utc().format('YYYY-MM-DD HH:mm:ss');
        let startTime = moment_tz().utc().subtract(0,"hours").format('YYYY-MM-DD HH:mm:ss');
        //let endTime = moment_tz().utc().add(1.5,"hours").format('YYYY-MM-DD HH:mm:ss');
        //console.log('startTime',startTime);
        //console.log('endTime',endTime);
        let ticket_total;
        let tip_total;
        let paypal_token = '';
        let totalAmount;
        Show.find({show_end_date:{'<=':startTime}, status: 'Published', payout_done: 0}).populate(['artist','timezone']).sort('show_end_date ASC').exec(function(err, resShows){
            if(err)
            return callback(err, null);
            else
            {
                if(resShows) {

                    async.eachSeries(resShows,function(show,eachCallback) {

                        //var ticket_total = await ShowTicket.sum('amount').where({show:show.id});
                        //var tip_total = await ArtistTip.sum('amount').where({show:show.id});
                        async.series([
                            function(firCallback) {
                                ShowTicket.findOne({show:show.id}).sum('amount').exec(function(err, ticketTotal){
                                    if(err)
                                    return firCallback(err, null);
                                    else
                                    {
                                        ticket_total = ticketTotal.amount;
                                        return firCallback(null);
                                    }
                                });
                            },
                            function(secCallback) {
                                ArtistTip.findOne({show:show.id, tip_type:1}).sum('amount').exec(function(err, tipTotal){
                                    if(err)
                                    return secCallback(err, null);
                                    else
                                    {
                                        tip_total = tipTotal.amount;
                                        return secCallback(null);
                                    }
                                });
                            },
                            function(thCallback) {
                                //console.log('show_id',show.id);
                                //console.log('ticket_total'+show.id,ticket_total);
                                //console.log('tip_total'+show.id,tip_total);
                                var total = Number(ticket_total) + Number(tip_total);
                                totalAmount = total/sails.config.project.tippingDollarValue;
                                //console.log('total'+show.id,totalAmount);

                                if (paypal_token == '' || !paypal_token) {
                                    var postData = {grant_type:'client_credentials'};
                                    var token_url = sails.config.api_url+'/v1/oauth2/token';
                                    var auth = "Basic " +new Buffer(sails.config.client_id+":"+sails.config.secret_key).toString('base64');

                                    request.post({url:token_url, form: postData, headers : {"Authorization" : auth}}, function(err, httpResponse, paypalBody){
                                        if(err) {
                                            return thCallback(err.message, null);
                                        }
                                        else {
                                            var parseBody = JSON.parse(paypalBody);
                                            paypal_token = parseBody.access_token;

                                            return thCallback(null);
                                        }
                                    });
                                } else {
                                    return thCallback(null);
                                }

                            },
                            function(fourCallback) {
                                if (totalAmount > 0) {
                                    var artistShare = (totalAmount*0.7).toFixed(2);
                                    var charityShare = (totalAmount*0.05).toFixed(2);

                                    //console.log('artistShare'+show.id,artistShare);
                                    //console.log('charityShare'+show.id,charityShare);
                                    var payout_url = sails.config.api_url+'/v1/payments/payouts';
                                    var auth = "Bearer "+paypal_token;
                                    var CurrentDate = moment().unix();
                                    var sender_batch_id = show.id+show.artist.id+CurrentDate;

                                    var postData  = {
                                        "sender_batch_header": {
                                            "sender_batch_id": sender_batch_id,
                                            "email_subject": "You have a payout!",
                                            "email_message": "You have received a payout for "+show.title+" show! Thanks for using our service!",
                                            "recipient_type": "EMAIL"
                                        },
                                        "items": [
                                            {
                                                "amount": {
                                                    "value": charityShare,
                                                    "currency": "AUD"
                                                },
                                                "note": "Thanks for your patronage!",
                                                "sender_item_id": show.id+0001,
                                                "receiver": sails.config.paypal.charity_paypal_email
                                            },
                                            {
                                                "amount": {
                                                    "value": artistShare,
                                                    "currency": "AUD"
                                                },
                                                "note": "Thanks for your support!",
                                                "sender_item_id":  show.id+0002,
                                                "receiver": show.artist.email
                                            }
                                        ]
                                    };

                                    postDataJson = JSON.stringify(postData);
                                    var contentLength = postDataJson.length;
                                    request({
                                        headers: {
                                            'Content-Length': contentLength,
                                            'Content-Type': 'application/json',
                                            "Authorization" : auth
                                        },
                                        uri: payout_url,
                                        body: postDataJson,
                                        method: 'POST'
                                    }, function (err, res, body) {
                                        if(err)
                                        return fourCallback({message: err.message}, null);
                                        else
                                        {
                                            PayoutHistory.create({show:show.id,response:body}).exec(function(err, payoutData){

                                            });
                                            Show.update({id:show.id},{payout_done:1}).exec(function(err, payoutData){

                                            });
                                            //console.log('body'+show.id,body);
                                            fourCallback(null);
                                        }
                                    });
                                } else {
                                    fourCallback(null);
                                }
                            },
                        ], function(err) {
                            if(err)
                            return eachCallback(err, null);
                            else
                            return eachCallback(null);
                        });


                    } ,function(err){
                        if(err)
                        return callback(err, null);
                        else
                        return callback(null);
                    });
                } else {
                    return callback(null,{});
                }
            }
        });
    },
    setCronCaptureID: function(req, callback) {
        let paypal_token;
        let allOrder;
        async.series([
            function (firstCall) {
                var postData = {grant_type:'client_credentials'};
                var token_url = sails.config.api_url+'/v1/oauth2/token';
                var auth = "Basic " +new Buffer(sails.config.client_id+":"+sails.config.secret_key).toString('base64');

                request.post({url:token_url, form: postData, headers : {"Authorization" : auth}}, function(err, httpResponse, paypalBody){
                    if(err){
                        return firstCall({message: err.message}, null);
                    }
                    else{
                        parseBody = JSON.parse(paypalBody);
                        paypal_token = parseBody.access_token;
                        //console.log('tokeon==',parseBody.access_token);
                        return firstCall(null);
                    }
                });
            },
            function(secCallback) {
                let sqlQuery = "SELECT SO.*,AT.id as at_id, ST.id as st_id, VP.id as vp_id FROM show_order AS SO \
                LEFT JOIN artist_tip AS AT ON AT.order_id = SO.id \
                LEFT JOIN show_ticket ST ON ST.order_id = SO.id \
                LEFT JOIN vod_payment VP ON VP.order_id = SO.id \
                WHERE SO.status != 'COMPLETED' AND (SO.capture_id IS NULL OR SO.capture_id = '') HAVING (st_id != '' OR at_id != '' OR vp_id != '')";
                ShowOrder.query(sqlQuery ,function(err, orders) {
                    if(err)
                    return secCallback(err, null);
                    else
                    {
                        //console.log('orders',orders);
                        allOrder = orders;
                        return secCallback(null);
                    }
                });
            },
            function (thCallback) {
                async.eachSeries(allOrder,function(order,innerEachCallback) {
                    if (order.order_id && order.order_id != 'undefined') {
                        PaypalService.detailPayPalOrder(order.order_id, paypal_token, function(err, orderData){
                            if(err) {
                                return innerEachCallback({message: err.message}, null);
                            } else {
                                if (orderData && orderData != 'undefined') {
                                    if (orderData.purchase_units[0].payment_summary.captures && orderData.purchase_units[0].payment_summary.captures != 'undefined') {
                                        capture_id = orderData.purchase_units[0].payment_summary.captures[0].id;
                                        ShowOrder.update({order_id:order.order_id},{capture_id:capture_id,status:orderData.status}).exec(function(err, res){
                                            if(err) {
                                                return innerEachCallback({message: err.message}, null);
                                            } else {
                                                return innerEachCallback(null);
                                            }
                                        });
                                    } else {
                                        return innerEachCallback(null);
                                    }
                                } else {
                                    return innerEachCallback(null);
                                }

                            }
                        });
                    } else {
                        //console.log('tip else');
                        return innerEachCallback(null);
                    }
                },function(err){
                    if(err)
                    return thCallback(err, null);
                    else
                    return thCallback(null);
                });
            }
        ], function (err, data) {
            if (err) {
                return callback({message: err.message}, null);
            } else {
                return callback(null,allOrder);
            }
        });
    },
    createPayPalOrder: function(req, parseBody, showData, userObj, orderType, thCallback) {
        var order_url = sails.config.api_url+'/v1/checkout/orders';
        var auth = "Bearer "+parseBody.access_token;
        var subTotal = 0;
        var emoji_id;
        var description, item_name, payment_descriptor, return_url;
        if (orderType == 1 || orderType == 4) {
            subTotal = showData.price;
            description = "Get access of the show - "+showData.title;
            item_name  = "Show Ticket";
            payment_descriptor  = "Show Ticket Payment";
            if (orderType == 1) {
                return_url = sails.config.frontUrl+'/show/'+showData.id;
            } else {
                return_url = sails.config.frontUrl+'/show/'+showData.id+'/gift/1';
            }
        } else if(orderType == 2 || orderType == 3) {
            subTotal = showData.tipping_amount;
            emoji_id = req.body.emoji_id;
            description = "Give tip to the presenter - "+showData.artist.name;
            item_name  = "Presenter Tip";
            payment_descriptor  = "Tip Payment";
            if (orderType == 2) {
                return_url = sails.config.frontUrl+'/show/'+showData.id+'/tip/1';
            } else {
                return_url = sails.config.frontUrl+'/show/'+showData.id+'/tip/2';
            }
        } else if (orderType == 5 || orderType == 6) {
            description = "On-demand Payment";
            item_name  = "On-demand Payment";
            payment_descriptor  = "On-demand Payment";
            if (orderType == 5) {
                subTotal = showData.price;
                return_url = sails.config.frontUrl+'/vod/'+showData.id+'/payment-success/1';
            } else {
                subTotal = showData.ondemand_price;
                return_url = sails.config.frontUrl+'/vod/'+showData.id+'/payment-success/2';
            }
        }

        let tax = (((subTotal*sails.config.constants.paypal_pr_charge)/100)+sails.config.constants.paypal_fix_charge).toFixed(2);
        let artistShare = (((subTotal*sails.config.constants.artist_share)/100) + Number(tax)).toFixed(2);
        let partnerShare = ((subTotal*sails.config.constants.partner_share)/100).toFixed(2);
        let grandTotal = (Number(subTotal) + Number(tax)).toFixed(2);
        let timestamp = Math.floor(Date.now());
        let orderObj;
        console.log('showData.artist.merchantIdInPayPal',showData.artist.merchantIdInPayPal);
        var postData  = {
        // "application_context":{
        //   "landing_page":"billing",
        // },
        "purchase_units": [
            {
                "reference_id": 'show_artist_'+timestamp,
                "description": description,
                "amount": {
                    "currency": sails.config.paypal.currency_code,
                    "details": {
                        "subtotal": grandTotal,
                        "shipping": "0.00",
                        "tax": "0.00"
                    },
                    "total": grandTotal
                },
                "payee": {
                    "merchant_id": showData.artist.merchantIdInPayPal
                },
                "items": [
                    {
                        "name": item_name,
                        //"sku": "",
                        "price": subTotal,
                        "currency": sails.config.paypal.currency_code,
                        "quantity": "1"
                    },
                    {
                        "name": "PayPal Charge",
                        //"sku": "",
                        "price": tax,
                        "currency": sails.config.paypal.currency_code,
                        "quantity": "1"
                    }
                ],
                //   "shipping_address": {
                    //   "recipient_name": "Pat Brandt",
                    //   "line1": "2211 N First Street",
                    //   "line2": "Building 17",
                    //   "city": "San Jose",
                    //   "country_code": "US",
                    //   "postal_code": "95131",
                    //   "state": "CA",
                    //   "phone": "4085551901"
                    // },
                    // "shipping_method": "United Postal Service",
                    "partner_fee_details": {
                        "receiver": {
                            "email": sails.config.paypal_partner_email
                        },
                        "amount": {
                            "value": partnerShare,
                            "currency": sails.config.paypal.currency_code
                        }
                    },
                    "payment_linked_group": 1,
                    "custom": "custom_value_"+timestamp,
                    "invoice_number": "invoice_number_"+timestamp,
                    "payment_descriptor": payment_descriptor
                }
            ],
            "redirect_urls": {
                "return_url": return_url,
                "cancel_url": sails.config.frontUrl+'/show/'+showData.id
            }
        };

        postDataJson = JSON.stringify(postData);
        //console.log('postDataJson==',postDataJson);
        request({
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
                "Authorization" : auth,
                "PayPal-Request-Id" : timestamp,
                "PayPal-Partner-Attribution-Id": sails.config.partner_attribution_id
            },
            uri: order_url,
            body: postDataJson,
            method: 'POST'
        }, function (err, res, body) {
            if(err)
            return thCallback({message: err.message}, null);
            else
            {
                var body = JSON.parse(body);
                console.log(body);
                //console.log('status code',res.statusCode);
                if (res.statusCode != 404 && res.statusCode != 400) {
                    async.eachSeries(body.links,function(linkObj, eachCallback) {
                        if (linkObj.rel == 'approval_url') {
                            var data = {order_id:body.id,amount:subTotal,
                                total_amount:body.gross_total_amount.value,
                                tax:tax,
                                show:showData.id,
                                user:userObj.id,
                                order_type:orderType,
                                approval_url:linkObj.href,
                                status:body.status,
                                emoji:emoji_id};
                                if (orderType == 4) {
                                    data.gift_email = req.body.email;
                                }
                                if (orderType == 6) {
                                    data.ondemand = showData.id;
                                    data.show = null;
                                }
                                ShowOrder.create(data).exec(function (err, orderData){
                                    if(err)
                                    return eachCallback({message: err.message}, null);
                                    else
                                    {
                                        orderObj = orderData;
                                        return eachCallback(null);
                                    }
                                });
                            } else {
                                return eachCallback(null);
                            }
                            //return eachCallback(null);
                        } ,function(err, res){
                            if(err)
                            return thCallback(err, null);
                            else
                            return thCallback(null, orderObj);
                        });
                    } else {
                        return thCallback({message: 'Something went wrong. Please try again later.'}, null);
                    }

                }
            });
    },
    payPayPalOrder:function(req, parseBody, order_id, callback){
        var api_url = sails.config.api_url+'/v1/checkout/orders/'+order_id+'/pay';
        var auth = "Bearer "+parseBody.access_token;
        let timestamp = Math.floor(Date.now());

        var postData  = {
            "disbursement_mode": "DELAYED"
        };
        postDataJson = JSON.stringify(postData);
        //ostDataJson = JSON.stringify(postData);
        request({
            headers: {
                'Content-Type': 'application/json',
                "Authorization" : auth,
                "PayPal-Request-Id" : timestamp,
                "PayPal-Partner-Attribution-Id": sails.config.partner_attribution_id
            },
            uri: api_url,
            body: postDataJson,
            method: 'POST'
        }, function (err, res, body) {
            if(err)
            return callback({message: err.message}, null);
            else
            {
                var body = JSON.parse(body);
                //console.log('pay order',body);
                //console.log('status code',res.statusCode);
                if (res.statusCode != 404 && res.statusCode != 400) {

                    //send mail to artist and followers api call
                    var exec = require('child_process').exec;
                    var child;
                    url = sails.config.apiUrl+'/getorderdetail/'+order_id;
                    //url = 'http://localhost:1337/sendEmailSms/'+token;

                    child = exec('wget ' + url+' -q -b',
                    function (error, stdout, stderr) {
                        //console.log('stdout: ' + stdout);
                        //console.log('stderr: ' + stderr);
                        if (error !== null) {
                            console.log('exec error: ' + error);
                        }
                    }).unref();

                    return callback(null, body);
                } else {
                    return callback({message: 'Something went wrong in payment process. Please try again.'}, null);
                }
            }
        });
    },
    payoutPayPalOrder:function(req, paypal_token, showTicket, callback) {

        var api_url = sails.config.api_url+'/v1/payments/referenced-payouts-items';
        var auth = "Bearer "+paypal_token;
        let timestamp = Math.floor(Date.now());
        //console.log('paypal_token',paypal_token);
        var postData  = {
            "reference_type": "TRANSACTION_ID",
            "reference_id": showTicket.order_id.capture_id
        };
        postDataJson = JSON.stringify(postData);
        //ostDataJson = JSON.stringify(postData);
        request({
            headers: {
                'Content-Type': 'application/json',
                "Authorization" : auth,
                "PayPal-Request-Id" : timestamp,
                "PayPal-Partner-Attribution-Id": sails.config.partner_attribution_id
            },
            uri: api_url,
            body: postDataJson,
            method: 'POST'
        }, function (err, res, body) {
            if(err)
            return callback({message: err.message}, null);
            else
            {
                var body = JSON.parse(body);
                //console.log('payout order',body);
                //console.log('status code',res.statusCode);
                if (res.statusCode != 404 && res.statusCode != 400 && res.statusCode != 403) {
                    return callback(null, body);
                } else { //
                    //return callback({message: 'Something went wrong in payout process. Please try again.'}, null);
                    return callback(null);
                }
            }
        });
    },
    detailPayPalOrder:function(order_id, paypal_token, callback){
        var api_url = sails.config.api_url+'/v1/checkout/orders/'+order_id;
        var auth = "Bearer "+paypal_token;
        let timestamp = Math.floor(Date.now());

        request({
            headers: {
                'Content-Type': 'application/json',
                "Authorization" : auth,
                "PayPal-Request-Id" : timestamp,
                "PayPal-Partner-Attribution-Id": sails.config.partner_attribution_id
            },
            uri: api_url,
            method: 'GET'
        }, function (err, res, body) {
            if(err)
            return callback({message: err.message}, null);
            else
            {
                var body = JSON.parse(body);
                //console.log('detail order',body);
                //console.log('status code',res.statusCode);
                if (res.statusCode != 404 && res.statusCode != 400) {
                    return callback(null, body);
                } else { //
                    //return callback({message: 'Something went wrong. Please try again.'}, null);
                    return callback(null);
                }
            }
        });
    },
    detailPayPalPartnerReferel:function(partner_referral_id, paypal_token, callback){
        var api_url = sails.config.api_url+'/v1/customer/partner-referrals/'+partner_referral_id;
        var auth = "Bearer "+paypal_token;
        let timestamp = Math.floor(Date.now());

        request({
            headers: {
                'Content-Type': 'application/json',
                "Authorization" : auth,
                "PayPal-Request-Id" : timestamp,
                "PayPal-Partner-Attribution-Id": sails.config.partner_attribution_id
            },
            uri: api_url,
            method: 'GET'
        }, function (err, res, body) {
            if(err)
            return callback({message: err.message}, null);
            else
            {
                var body = JSON.parse(body);
                //console.log('detail order',body);
                //console.log('status code',res.statusCode);
                if (res.statusCode != 404 && res.statusCode != 400) {
                    return callback(null, body);
                } else { //
                    //return callback({message: 'Something went wrong. Please try again.'}, null);
                    return callback(null);
                }
            }
        });
    },
    getPartnerReferelDetail: function(partner_referral_id, callback) {
        async.series([
            function (firstCall) {
                var postData = {grant_type:'client_credentials'};
                var token_url = sails.config.api_url+'/v1/oauth2/token';
                var auth = "Basic " +new Buffer(sails.config.client_id+":"+sails.config.secret_key).toString('base64');

                request.post({url:token_url, form: postData, headers : {"Authorization" : auth}}, function(err, httpResponse, paypalBody){
                    if(err){
                        return firstCall({message: err.message}, null);
                    }
                    else{
                        parseBody = JSON.parse(paypalBody);
                        paypal_token = parseBody.access_token;
                        //console.log('tokeon==',parseBody.access_token);
                        return firstCall(null);
                    }
                });
            },
            function (thCallback) {
                PaypalService.detailPayPalPartnerReferel(partner_referral_id, paypal_token, function(err, partnerData){
                    if(err) {
                        return thCallback({message: err.message}, null);
                    } else {
                        if (partnerData.referral_data.requested_capabilities && partnerData.referral_data.requested_capabilities != 'undefined') {
                            partner_client_id = partnerData.referral_data.requested_capabilities[0].api_integration_preference.rest_third_party_details.partner_client_id;
                            User.update({partner_referral_id:partner_referral_id},{partner_client_id:partner_client_id}).exec(function(err, res){
                                if(err) {
                                    return thCallback({message: err.message}, null);
                                } else {
                                    return thCallback(null);
                                }
                            });
                        } else {
                            return thCallback(null);
                        }
                    }
                });
            }
        ], function (err, data) {
            if (err) {
                return callback({message: err.message}, null);
            } else {
                return callback(null);
            }
        });
    },
}
