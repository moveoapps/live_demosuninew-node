// UserService.js - in api/services
var moment_tz = require('moment-timezone');
var moment = require('moment');
var Hashids = require('hashids');


module.exports = {

    /***************************************************************************
        * Do not utilize this method unless you are positive that the web token   *
        * has not expired.  To ensure that the web token is valid, add the        *
        * `isAuthorized` policy to any controllers which will utilize this.
    ***************************************************************************/

    getCMS: function(data, criteria, callback) {
        Cms.find(criteria).sort('createdAt DESC').exec(function(err, foundCategories){
            if (err) {
                return callback({message: err.message}, null);
            } else {
                return callback(null,foundCategories);
            }
        });
    },

    getHomeData: function(req, callback) {
        //let currDate = new Date();
        let currDate = moment_tz().utc().format('YYYY-MM-DD HH:mm:ss');
        let showTrendigDate = moment(currDate).subtract(3, 'months').format('YYYY-MM-DD HH:mm:ss'); // trendingshows of last three months
        var userData = TokenService.decode(req.headers.authorization);
        let vod_start_date = '2019-02-20 12:00:00'; // Implemented VOD on this date
        let deleteUser = [0];
        let resData;

        async.series([
            function (callback1) {
                UserService.deleteuserID(req, function(err, resData){
                    if(err)
                        return callback1(err, null);
                    else
                    {
                        if(resData.length > 0){
                            deleteUser = resData;
                        }
                        return callback1(null, deleteUser);
                    }
                });
            },
            function (callback2) {
                async.parallel({
                    nextShows: function(callback) {
                        Show.find({show_end_date:{'>':currDate}, status: 'Published', is_featured:1}).where({artist:{'!':deleteUser}}).populate(['artist','timezone']).sort('publish_date ASC').limit(3).exec(function(err, resShows){
                            if(err)
                                return callback(err, null);
                            else
                            {
                                return callback(null, resShows);
                            }
                        });
                    },
                    featuredShows: function(callback) {
                        // Show.find({show_end_date:{'>':currDate}, status: 'Published', is_featured: 1}).populate(['artist', 'genres', 'timezone']).sort('publish_date ASC').limit(5).exec(function(err, fullShows){
                        Show.find({show_end_date:{'>':currDate}, status: 'Published', is_featured: 1}).where({artist:{'!':deleteUser}}).populate(['artist', 'genres', 'timezone']).sort('publish_date ASC').exec(function(err, fullShows){
                            if(err)
                                return callback(err, null);
                            else
                            {
                                // Show.find({show_end_date:{'>':currDate}, status: 'Published', is_featured: 1}).populate(['artist', 'genres','timezone']).sort('createdAt DESC').limit(5).exec(function(err, recentShows){
                                Show.find({show_end_date:{'>':currDate}, status: 'Published', is_featured: 1}).where({artist:{'!':deleteUser}}).populate(['artist', 'genres','timezone']).sort('createdAt DESC').exec(function(err, recentShows){
                                    if(err)
                                        return callback(err, null);
                                    else
                                    {
                                        resShows = {full_schedule:fullShows, recently_added: recentShows};
                                        return callback(null, resShows);
                                    }
                                });
                            }
                        });
                    },
                    featuredOndemand: function(callback) {
                        OnDemandService.getFeaturedVods(req, 5, function(err, resData){
                            if(err)
                                return callback(err, null);
                            else
                            {
                                return callback(null, resData);
                            }
                        });
                    },
                    vods: function(callback) {
                        OnDemandService.getVods(req, 5, function(err, resData){
                            if(err)
                                return callback(err, null);
                            else
                            {
                                return callback(null, resData);
                            }
                        });
                    },
                    genres: function(callback) {
                        Genre.find({is_active:1}).exec(function(err, resgenre){
                            if(err)
                                return callback(err, null);
                            else
                            {
                                return callback(null, resgenre);
                            }
                        });
                    },
                    trendingshows: function (secCall) {
                        sqlQuery = 'SELECT S.*,U.name, U.profile_picture,U.id as user_id,count(ST.id) as ticket_count FROM shows as S \
                        INNER JOIN users as U ON U.id = S.artist \
                        INNER JOIN show_ticket as ST ON ST.show = S.id \
                        WHERE S.publish_date >= "'+showTrendigDate+'" AND S.status = "Published"';

                        if(deleteUser.length > 0){
                            sqlQuery += 'AND S.artist NOT IN ('+deleteUser+')';
                        }
                        sqlQuery += 'GROUP BY S.id ORDER BY ticket_count desc';
                        // GROUP BY S.id ORDER BY ticket_count desc LIMIT 5';
                        // console.log(sqlQuery);
                        Show.query(sqlQuery ,function(err, trendindShows) {
                            //ArtistTip.find({artist:showData.artist.id}).sum('amount').sort('amount', 'DESC').limit(5).exec(function(err, resTip){
                            if (err)
                            {
                                return secCall({message: err.message}, null);
                            }
                            else
                            {
                                if (trendindShows.length > 0 && trendindShows != 'undefined')
                                {
                                    var item = 0;
                                    _.each(trendindShows,function(show, i){
                                        Show.findOne({'id':show.id}).populate(['genres','artist']).exec(function(err, showgenre){
                                            if (err)
                                            {
                                                //return callback({message: err.message}, null);
                                            }
                                            else
                                            {
                                                trendindShows[i].genres = showgenre.genres;
                                                trendindShows[i].artist = showgenre.artist;
                                                item++;
                                                if (item == trendindShows.length) {
                                                    return secCall(null, trendindShows);
                                                }
                                            }
                                        });
                                    });
                                }
                                else
                                {
                                    return secCall(null,[]);
                                }
                            }
                        });
                    },
                    youtube: function(callback6) {
                        YoutubeVideo.find({user:{'!':deleteUser}}).populate(['user','genre']).limit(5).sort('createdAt DESC').exec(function(err, foundVideo){
                            if (err)
                            {
                                return callback6({message: err.message}, null);
                            }
                            else
                            {
                                return callback6(null,foundVideo);
                            }
                        });
                    }
                }, function(err, results) {
                    if(err) {
                        callback2({message: err.message}, null);
                    }else {
                        resData = results;
                        callback2(null,results);
                    }
                });
            }
        ], function(err, res) {
            if (err) {
                return callback({message: err.message}, null);
            } else {
                return callback(null,resData);
            }
        });
        /*async.parallel({
            nextShows: function(callback) {
                Show.find({show_end_date:{'>':currDate}, status: 'Published', is_featured:1}).populate(['artist','timezone']).sort('publish_date ASC').limit(3).exec(function(err, resShows){
                    if(err)
                        return callback(err, null);
                    else
                    {
                        return callback(null, resShows);
                    }
                });
            },
            featuredShows: function(callback) {
                Show.find({show_end_date:{'>':currDate}, status: 'Published', is_featured: 1}).populate(['artist', 'genres', 'timezone']).sort('publish_date ASC').exec(function(err, fullShows){
                    if(err)
                        return callback(err, null);
                    else
                    {
                        Show.find({show_end_date:{'>':currDate}, status: 'Published', is_featured: 1}).populate(['artist', 'genres','timezone']).sort('createdAt DESC').exec(function(err, recentShows){
                            if(err)
                                return callback(err, null);
                            else
                            {
                                resShows = {full_schedule:fullShows, recently_added: recentShows};
                                return callback(null, resShows);
                            }
                        });
                    }
                });
            },
            vods: function(callback) {
                OnDemandService.getVods(req, 5, function(err, resData){
                    if(err)
                        return callback(err, null);
                    else
                    {
                        return callback(null, resData);
                    }
                });
            },
            genres: function(callback) {
                Genre.find({is_active:1}).exec(function(err, resgenre){
                    if(err)
                        return callback(err, null);
                    else
                    {
                        return callback(null, resgenre);
                    }
                });
            },
            trendingshows: function (secCall) {
                sqlQuery = 'SELECT S.*,U.name, U.profile_picture,U.id as user_id,count(ST.id) as ticket_count FROM shows as S \
                INNER JOIN users as U ON U.id = S.artist \
                INNER JOIN show_ticket as ST ON ST.show = S.id \
                WHERE S.publish_date >= "'+showTrendigDate+'" AND S.status = "Published"\
                GROUP BY S.id ORDER BY ticket_count desc';

                Show.query(sqlQuery ,function(err, trendindShows) {
                    if (err)
                    {
                        return secCall({message: err.message}, null);
                    }
                    else
                    {

                        if (trendindShows.length > 0 && trendindShows != 'undefined'){
                            var item = 0;
                            _.each(trendindShows,function(show, i){
                                Show.findOne({'id':show.id}).populate(['genres','artist']).exec(function(err, showgenre){
                                    if (err){
                                        //return callback({message: err.message}, null);
                                    }else {
                                        trendindShows[i].genres = showgenre.genres;
                                        trendindShows[i].artist = showgenre.artist;
                                        item++;
                                        if (item == trendindShows.length) {
                                            return secCall(null, trendindShows);
                                        }
                                    }
                                });
                            });
                        }else {
                            return secCall(null,[]);
                        }
                    }
                });
            },
            youtube: function(callback6) {
                YoutubeVideo.find().populate(['user','genre']).limit(5).sort('createdAt DESC').exec(function(err, foundVideo){
                    if (err)
                    {
                        return callback6({message: err.message}, null);
                    }
                    else
                    {
                        return callback6(null,foundVideo);
                    }
                });
            }
        }, function(err, results) {
            if(err) {
                callback({message: err.message}, null);
            }else {
                callback(null,results);
            }
        });*/
    },

    getTimezones: function(req,callback) {
        req.setTimeout(0);
        let zones = [];
        async.series([
            function (callback1) {
                Timezone.find().sort('order_number ASC').exec(function(err, resData){
                    if(err) {
                        return callback1(err, null);
                    }else {
                        zones = resData;
                        return callback1(null,zones);
                    }
                });
            },
            function (callback2) {
                if(zones){
                    async.forEachOf(zones,function(indvZone,keyZone, eachCallback) {
                        var code = '';
                        async.waterfall([
                            function findcode(codeCallback) {
                                if(indvZone.zone_name != ''){
                                    code = moment().tz(indvZone.zone_name).format('z');
                                    return codeCallback(null);
                                }else{
                                    return codeCallback(null);
                                }
                            },
                            function updatecode(updatecodeCallback) {
                                console.log(code);
                                Timezone.update({id: indvZone.id}, {code: code}).exec(function(err, resData){
                                    if(err){
                                        return updatecodeCallback(err.message, null);
                                    }
                                    else{
                                        return updatecodeCallback(null);
                                    }
                                });
                            },
                        ],function (err, results) {
                                return eachCallback(null, {});
                        });
                    } ,function(err){
                        if(err){
                            return callback2(err, null);
                        }
                        else{
                            return callback2(null,{});
                        }
                    });
                }else {
                    return callback2(null,{});
                }
            },
        ], function (err, data) {
            if (err) {
                callback({message: err.message}, null);
            } else {
                callback(null, {});
            }
        });
    },

    updateGeneralInfo: function(req,callback) {
        let optiondata = req.body;
        async.series([
            function (listCallback) {
                if(optiondata){
                    async.forEachOf(optiondata,function(indvOpt,keyOpt, eachCallback) {
                        let option;
                        async.waterfall([
                            function checkOption(checkCallback) {
                                GeneralInfo.findOne({option:keyOpt}).exec(function(err, foundOption){
                                    if(err){
                                        return checkCallback(err, null);
                                    }else {
                                        option = foundOption;
                                        return checkCallback(null);
                                    }
                                });
                            },
                            function setOption(setCallback) {
                                if(option){
                                    GeneralInfo.update({option: keyOpt}, {value: indvOpt}).exec(function(err, resOption){
                                        if (err) {
                                            return setCallback({message: err.message}, null);
                                        } else {
                                            return setCallback(null);
                                        }
                                    });
                                }else{
                                    insertData = {option: keyOpt, value: indvOpt};
                                    GeneralInfo.create(insertData).exec(function(err, resOption){
                                        if (err) {
                                            return setCallback({message: err.message}, null);
                                        } else {
                                            return setCallback(null);
                                        }
                                    });
                                }
                            },
                        ],function (err, results) {
                                return eachCallback(null, {});
                        });
                    },function(err){
                        if(err){
                            return listCallback(err, null);
                        }else {
                            return listCallback(null,{});
                        }
                    });
                }else{
                    return listCallback(null,{});
                }
            },
        ], function (err, data) {
            if (err) {
                return callback({message: err.message}, null);
            } else {
                return callback(null, {});
            }
        });
    },

    getGeneralInfoByKey: function(req, callback) {
        GeneralInfo.find({option: req.body.key}).exec(function(err, foundOptions){
            if (err) {
                return callback({message: err.message}, null);
            } else {
                return callback(null,foundOptions);
            }
        });
    },

    setCron: function(req, callback) {
        async.parallel({
            sendEmailReminder:function (cBack) {
                let currDate = moment_tz().utc().format('YYYY-MM-DD HH:mm:ss');
                let startTime = moment_tz().utc().add(0.5,"hours").format('YYYY-MM-DD HH:mm:ss');
                let endTime = moment_tz().utc().add(1.5,"hours").format('YYYY-MM-DD HH:mm:ss');

                Show.find({publish_date_utc:{'>=':startTime, '<':endTime}, status: 'Published', is_reminder: 1}).populate(['artist','timezone']).sort('publish_date_utc ASC').exec(function(err, resShows){
                    if(err){
                        return cBack(err, null);
                    }else {
                        if(resShows) {
                            async.eachSeries(resShows,function(show, eachCallback) {
                                var profile_picture = show.artist.profile_picture?sails.config.s3bucket.bucketurl+sails.config.s3bucket.userprofile+show.artist.profile_picture:sails.config.apiUrl+'/images/user-image.png';
                                var published_date = moment(show.publish_date).format('DD MMMM Y h:mm A')+' '+show.timezone.code;
                                var templateData = {
                                    to: show.artist.email,
                                    from: sails.config.fromName+' <'+sails.config.fromEmail+'>',
                                    subject: sails.config.project.showReminder.subject,
                                    userEmail: show.artist.email,
                                    siteName: sails.config.project.name,
                                    show: show,
                                    published_date:published_date,
                                    profile_picture:profile_picture,
                                    base_url: sails.config.apiUrl,
                                    name: show.artist.name,
                                    curr_year: (new Date()).getFullYear()
                                };

                                EmailService.sendMail(sails.config.project.showReminder.template, templateData, function(err, message) {
                                    if (err) {
                                        console.log("error mail");
                                        return eachCallback({message: err.message}, null);
                                    }else {
                                        return eachCallback(null, {});
                                    }
                                });
                            } ,function(err){
                                if(err){
                                    return cBack(err, null);
                                }else {
                                    return cBack(null,resShows);
                                }
                            });
                        }else {
                            return cBack(null,{});
                        }
                    }
                });
            },
            setlistReminder:function (secCall) {
                let currDate = moment_tz().utc().format('YYYY-MM-DD HH:mm:ss');
                Show.find({show_end_date:{'<':currDate}, status: 'Published', setlistemail_done: 0}).populate(['artist','timezone']).sort('publish_date_utc ASC').exec(function(err, resShows){
                    if(err){
                        return secCall(err, null);
                    }else {
                        if(resShows) {
                            async.eachSeries(resShows,function(show, eachCallback) {
                                var profile_picture = show.artist.profile_picture?sails.config.s3bucket.bucketurl+sails.config.s3bucket.userprofile+show.artist.profile_picture:sails.config.apiUrl+'/images/user-image.png';
                                var published_date = moment(show.publish_date).format('DD MMMM Y h:mm A')+' '+show.timezone.code;
                                var templateData = {
                                    to: show.artist.email,
                                    from: sails.config.fromName+' <'+sails.config.fromEmail+'>',
                                    subject: sails.config.project.setlistReminder.subject,
                                    userEmail: show.artist.email,
                                    siteName: sails.config.project.name,
                                    show: show,
                                    published_date:published_date,
                                    profile_picture:profile_picture,
                                    base_url: sails.config.apiUrl,
                                    url: sails.config.frontUrl +'/setlist/'+show.id,
                                    name: show.artist.name,
                                    curr_year: (new Date()).getFullYear()
                                };

                                EmailService.sendMail(sails.config.project.setlistReminder.template, templateData, function(err, message) {
                                    if (err) {
                                        return eachCallback(null);
                                    } else {
                                        Show.update({id:show.id},{setlistemail_done:1}).exec(function(err,response){
                                            return eachCallback(null);
                                        });
                                    }
                                });
                            },function(err){
                                if(err){
                                    return secCall(err, null);
                                }else {
                                    return secCall(null,resShows);
                                }
                            });
                        }else {
                            return secCall(null,{});
                        }
                    }
                });
            },
            topTipperEmail:function (secCall) {
                let currDate = moment_tz().utc().format('YYYY-MM-DD HH:mm:ss');
                Show.find({show_end_date:{'<':currDate}, status: 'Published', toptipperemail_done: 0}).populate(['artist','timezone']).sort('publish_date_utc ASC').exec(function(err, resShows){
                    if(err){
                        return secCall(err, null);
                    }else {
                        //console.log('topTipperEmail',resShows);
                        if(resShows) {
                            async.eachSeries(resShows,function(show, eachCallback) {

                                sqlQuery = 'SELECT SUM(amount) as totaltip, U.name, U.email, U.profile_picture,U.id as user_id FROM artist_tip as AT \
                                    INNER JOIN users as U ON U.id = AT.user \
                                    WHERE AT.show = '+show.id+' AND AT.tip_type = 1\
                                    GROUP BY AT.user HAVING totaltip >= '+sails.config.project.minTipTotal+' ORDER BY totaltip desc ';
                                    //console.log(sqlQuery);
                                ArtistTip.query(sqlQuery ,function(err, resTip) {
                                    //ArtistTip.find({artist:showData.artist.id}).sum('amount').sort('amount', 'DESC').limit(5).exec(function(err, resTip){
                                    if (err) {
                                        return eachCallback({message: err.message}, null);
                                    } else {
                                        if (resTip.length > 0 && typeof resTip != 'undefined') {
                                            let str = '';
                                            async.forEachOf(resTip,function(tipObj,keyShw, innereachCallback) {
                                                str = str + '<tr>\
                                                  <td align="left" style="padding:10px; color:#000; font-size:14px; font-weight:400; font-family:Arial, Helvetica, sans-serif; border-bottom:1px solid #ddd; border-right:1px solid #ddd;">'+tipObj.name+'</td>\
                                                  <td align="left" style="padding:10px; color:#000; font-size:14px; font-weight:400; font-family:Arial, Helvetica, sans-serif; border-bottom:1px solid #ddd; border-right:1px solid #ddd;">'+tipObj.email+'</td>\
                                                  <td align="left" style="padding:10px; color:#000; font-size:14px; font-weight:400; font-family:Arial, Helvetica, sans-serif; border-bottom:1px solid #ddd; border-right:1px solid #ddd;">$'+(tipObj.totaltip).toFixed(2)+'</td>\
                                                </tr>';
                                                return innereachCallback(null);
                                            } ,function(err){
                                                if(err){
                                                    return eachCallback(null);
                                                }else {
                                                    published_date = moment(show.publish_date).format('DD MMMM Y h:mm A')+' '+show.timezone.code;
                                                    var profile_picture = show.artist.profile_picture?sails.config.s3bucket.bucketurl+sails.config.s3bucket.userprofile+show.artist.profile_picture:sails.config.apiUrl+'/images/user-image.png';
                                                    console.log(published_date);
                                                    var templateData = {
                                                        to: show.artist.email,
                                                        from: sails.config.fromName+' <'+sails.config.fromEmail+'>',
                                                        subject: sails.config.project.topTipperEmail.subject,
                                                        siteName: sails.config.project.name,
                                                        userlist_str: str,
                                                        show: show,
                                                        published_date:published_date,
                                                        profile_picture:profile_picture,
                                                        base_url: sails.config.apiUrl,
                                                        curr_year: (new Date()).getFullYear()
                                                    };
                                                    EmailService.sendMail(sails.config.project.topTipperEmail.template, templateData, function(err, message) {
                                                        if (err) {
                                                            return eachCallback(null);
                                                        }else {
                                                            Show.update({id:show.id}, {toptipperemail_done: 1}).exec(function(err, ShowUpdate){
                                                                return eachCallback(null);
                                                            });
                                                        }
                                                    });
                                                }
                                            });
                                        } else {
                                            return eachCallback(null);
                                        }
                                    }
                                });
                            } ,function(err){
                                if(err){
                                    return secCall(err, null);
                                }else {
                                    return secCall(null,resShows);
                                }
                            });
                        }else {
                            return secCall(null,{});
                        }
                    }
                });
            }
        },function (err, res) {
            if (err) {
                return callback({message: err.message}, null);
            } else {
                return callback(null, res)
            }
        })
    },

    grandPermission: function(req, callback) {
        let data = req.body;
        var userObj  = TokenService.decode(req.headers.authorization);
        let action_url;
        User.findOne({id: userObj.id}).exec(function(err, userData){
            if (err) {
                return callback({message: err.message}, null);
            } else {
                if (userData) {
                    if (userData.paypal_action_url && userData.paypal_action_url != null) {
                        var resJson = {paypal_action_url:userData.paypal_action_url};
                        return callback(null,resJson);
                    } else {
                        UserService.createPartnerReferrals(req, userData, function(err, body){
                            if(err){
                                return ServerError(err.message,res);
                            }else {
                                console.log(body);
                                async.eachSeries(body.links,function(linkObj, eachCallback) {
                                    if (linkObj.rel == 'action_url') {
                                        var action_link = linkObj.href;
                                        var linkArr = action_link.split('&context_token');
                                        var linkArr2 = linkArr[0].split('token=');
                                        var partner_referral_id = linkArr2[1];

                                        //*******Get partner refrel detail****
                                        /*var exec = require('child_process').exec;
                                        var child;
                                        url = sails.config.apiUrl+'/getpartnerrefereldetail/'+partner_referral_id;
                                        console.log(url);

                                        child = exec('wget ' + url+' -q -b',
                                        function (error, stdout, stderr) {
                                            console.log('stdout: ' + stdout);
                                            console.log('stderr: ' + stderr);
                                            if (error !== null) {
                                                console.log('exec error: ' + error);
                                            }
                                        }).unref();*/

                                        User.update({id:userData.id}, {paypal_action_url:linkObj.href, partner_referral_id:partner_referral_id}).exec(function (err, userRes){
                                            if(err){
                                                return eachCallback({message: err.message}, null);
                                            }else {
                                                action_url = linkObj.href;
                                                return eachCallback(null);
                                            }
                                        });
                                    } else {
                                        return eachCallback(null);
                                    }
                                    //return eachCallback(null);
                                } ,function(err){
                                    if(err){
                                        return callback(err, null);
                                    }else {
                                        var resJson = {paypal_action_url:action_url};
                                        return callback(null,resJson);
                                    }
                                });
                            }
                        });
                    }
                }else {
                    return callback({message: 'Something went wrong. Please try again later.'}, null);
                }
            }
        });
    },

    grandStripePermission: function(req, callback) {
        let data = req.body;
        var userObj  = TokenService.decode(req.headers.authorization);
        let action_url;
        User.findOne({id: userObj.id}).exec(function(err, userData){
            if (err) {
                return callback({message: err.message}, null);
            } else {
                if (userData) {
                    var hashids = new Hashids(sails.config.project.name,10);
                    var encodedId = hashids.encode(userData.id);

                    let stripe_connect_client = sails.config.stripe_connect_client;
                    let redirect_uri = sails.config.frontUrlNoHash+'/stripe/onboarding_complete';
                    let stripe_link = 'https://connect.stripe.com/oauth/authorize?response_type=code&state='+encodedId+'&client_id='+stripe_connect_client+'&scope=read_write';

                    var resJson = {stripe_action_url:stripe_link};
                    return callback(null,resJson);
                } else {
                    return callback({message: 'Something went wrong. Please try again later.'}, null);
                }
            }
        });
    },
}
