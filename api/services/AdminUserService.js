// AdminUserService.js - in api/services
var moment = require('moment');
var _ = require('underscore');
var bcrypt = require('bcrypt');
var options = {
  provider: 'google',
  formatter: null         // 'gpx', 'string', ...
};

module.exports = {

  /***************************************************************************
   * Do not utilize this method unless you are positive that the web token   *
   * has not expired.  To ensure that the web token is valid, add the        *
   * `isAuthorized` policy to any controllers which will utilize this.
   ***************************************************************************/

  addUser: function(req,callback) {
    let data = req.body;
    let user;
    let isMobileExist;

    // Remove leading zero
    //data.mobile = data.mobile.replace(/^0+/, '');
    async.series([
        function (cBack) {
            User.findOne({'country_code':data.country_code,'mobile':data.mobile}).exec(function(err, foundUser){
              if(err)
                return cBack(err, null);
              else
              {
                isMobileExist = foundUser;
                return cBack(null);
              }
            });
        },
        function (firstCallback) {
            User.findOne({'email':data.email}).exec(function(err, foundUser){
              if(err)
                return firstCallback(err, null);
              else
              {
                user = foundUser;
                return firstCallback(null);
              }
            });
        },
        function (secondCallback) {
          if (isMobileExist) {
            return secondCallback({message: "Mobile number already exist."}, null);
          } else if (user) {
            return secondCallback({message: "Email already exist."}, null);
          } else {
            var num = Math.floor(Math.random() * 900000) + 100000;
            var num_email = Math.floor(Math.random() * 900000) + 100000;
            //var num = '000000';
            data.verification_code = num;
            data.email_verification_code = num_email;
            if (data.social_media_id) {
              var fileName = Math.floor(Date.now())+'.jpg';
              UploadimageService.uploadFbImage(data.social_media_id,'avatar',fileName, function(err, image){
                  if(err)
                  {
                    return secondCallback({message: err.message}, null);
                  }
                  else
                  {
                      data.avatar = fileName;
                      if (data.device_token!="undefined" && data.device_token!=null)
                      {
                          data.device_token = data.device_token;
                      }
                      AdminUserService.getAddressArr(data,function(err,addressArr){
                          AdminUserService.getLatLong(addressArr,function(err,latlongDetails){

                          //  console.log(latlongDetails);
                              data.latitude = latlongDetails.latitude;
                              data.longitude = latlongDetails.longitude;
                              data.is_verified = 0;
                              User.create(data, function (err, userData) {
                                  if(err)
                                    return secondCallback({message: err.message}, null);
                                  else
                                  {
                                    var userDataFinal = userData;
                                    AdminUserService.sendVerificationCode(userData.mobile,userData.email,userData.verification_code,userData.email_verification_code,userData,function(err,userData){
                                      if(err)
                                        return secondCallback({message:err.message},null);
                                      else
                                      {
                                        user = userDataFinal;
                                        return secondCallback(null, userDataFinal);
                                      }
                                    });
                                  }
                              });
                          });
                      });

                  }
              });
            } else {
                      AdminUserService.getAddressArr(data,function(err,addressArr){
                          AdminUserService.getLatLong(addressArr,function(err,latlongDetails){
                              data.latitude = latlongDetails.latitude;
                              data.longitude = latlongDetails.longitude;
                              data.is_verified = 0;
                              User.create(data).exec(function (err, userData){
                                //console.log(err,userData);
                                if(err)
                                  return secondCallback({message: err.message}, null);
                                else
                                {
                                  var userDataFinal = userData;
                                  AdminUserService.sendVerificationCode(userData.mobile,userData.email,userData.verification_code,userData.email_verification_code,userData,function(err,userData){
                                    //console.log(err,userData);
                                      if(err)
                                        return secondCallback({message:err.message},null);
                                      else
                                      {
                                        user = userDataFinal;
                                        return secondCallback(null, userDataFinal);
                                      }
                                    });
                                }
                              });
                          });
                      });
            }

          }
        }
    ], function (err, data) {
        if (err) {
            return callback({message: err.message}, null);
        } else {
            return callback(null, user);
        }
    });
  },

  changePassword: function(req, callback) {
    let data = req.body;
    var userData  = TokenService.decode(req.headers.authorization);
    let isMatch = false;
    var updateData = {};
    let token;
    let resData;
    let userObj;
    var crieteria = {id: userData.id};
    async.series([
        function (cBack) {
            User.find(crieteria).exec(function(err, foundUser){
              if(err)
                return cBack(err, null);
              else
              {
                userObj = foundUser[0];
                return cBack(null);
              }
            });
        },
        function (firstCallback) {
          if (userObj) {
            User.isValidPassword(data.old_password, userObj, function(err, match) {
              if (match) {
                  isMatch = true;
                  return firstCallback(null);
              } else {
                //res.unauthorized();
                //console.log(err);
                return firstCallback({message: "Old password does not match with your account."}, null);
              }
            });
          } else {
            return firstCallback({message: "Old password does not match with your account."}, null);
          }
        },
        function (secondCallback) {
          if (isMatch) {
            User.encryptPassword(data.new_password, function(err, encryptedPassword) {
                updateData.password = encryptedPassword;

                User.update(crieteria, updateData).exec(function(err, user){
                  if(err)
                      return secondCallback({message: err.message}, null);
                  else{
                    //resData = user[0];
                    //token = TokenService.sign(resData, sails.config.project.passwordReset.tokenTTL);
                    return secondCallback(null, user[0]);
                  }
              });
            });
          } else {
            return secondCallback({message: "Old password does not match with your account."}, null);
          }
        }
    ], function (err, data) {
        if (err) {
            callback({message: err.message}, null);
        } else {
            callback(null);
        }
    });
  },

  updateUser: function(updateArr, criteria, callback) {
    let dataObj;
    let responseObj;
    async.series([
        function (firstCallback) {
          //  console.log(criteria);
            User.findOne(criteria).exec(function(err, foundData){
              if(err)
                return firstCallback(err, null);
              else
              {
                dataObj = foundData;
                return firstCallback(null);
              }
            });
        },
        function (secCallback) {
          if(dataObj)
          {
            User.update(criteria, updateArr).exec(function(err, resObj){
              if(err)
                 return secCallback({message: err.message}, null);
              else{
                responseObj = resObj;
                return secCallback(null, resObj);
              }
            });
          }
          else
          {
            return secCallback({message: "User not found."}, null);
          }
        }
    ], function (err, data) {
        if (err) {
            return callback({message: err.message}, null);
        } else {
            return callback(null, responseObj);
        }
    });
  },
  logout: function (req,callback) {
    let dataObj;
    async.series([
        function (firstCallback) {
            Invalidtoken.findOne({'token': req.headers.authorization}).exec(function(err, foundData){
              if(err)
                return firstCallback(err.message, null);
              else
              {
                dataObj = foundData;
                return firstCallback(null);
              }
            });
        },
        function (secCallback) {
          if(dataObj)
          {
            return secCallback(null);
          }
          else
          {
            Invalidtoken.create({'token': req.headers.authorization}).exec( function (err,user) {
              if(err)
                return secCallback(err.message, null);
              else{
                return secCallback(null);
              }
            });
          }
        }
    ], function (err, data) {
        if (err) {
            return callback({message: err.message}, null);
        } else {
            return callback(null);
        }
    });

  },
  getUser: function (req) {
      if(req.headers.hasOwnProperty('authorization')) {
          return TokenService.decode(req.headers.authorization, sails.config.session.secret);
      } else {
          return false;
      }
  },
  updatePicture: function(data,callback){
    let uploadFile;
    async.series([
        function (cBack) {
            if (typeof data._fileparser != 'undefined' && data._fileparser.upstreams.length > 0){
              var filename = data.file('avatar')._files[0].stream.filename;
              var i = filename.lastIndexOf('.');
              var ext = filename.substr(i);

              uploadFile = new moment().valueOf()+ext;
              UploadimageService.uploadImage(data,"avatar","avatar",uploadFile, function(err, image){
                if(err)
                  return cBack(err.message, null);
                else{
                  return cBack(null);
                }
              });
            }
            else
            {
              uploadFile = '';
              return cBack(null);
            }
        }
    ], function (err, data) {
        if (err) {
            return callback({message: err.message}, null);
        } else {
            return callback(null, uploadFile);
        }
    });
  },
  updateAdminProfile: function(data, callback) {
    var users  = TokenService.decode(data.headers.authorization);
    var crieteria = {id: users.id};

    var updateData = {first_name:data.body.first_name,last_name:data.body.last_name, email:data.body.email};

    async.series([
        function (secCallback) {
            AdminUserService.updatePicture(data, function(err,res){
              if (err) {
                return secCallback({message: err.message},null);
              } else {
                updateData.avatar = '';
                if (res) {
                  updateData.avatar = res;
                  //console.log('==res=='+res);
                }else{
                  AdminUser.find({'id':users.id}).exec(function(err, res){
                    if(err){
                      updateData.avatar = '';
                    }else{
                      if (res) {
                        updateData.avatar = res[0]['avatar'];
                      }
                    }
                  });
                }
                return secCallback(null);
              }
            });
        },
        function (thCallback) {
          //console.log(data.body.password);
          if (data.body.password) {
            AdminUser.encryptPassword(data.body.password, function(err, encryptedPassword) {
                updateData.password = encryptedPassword;

                AdminUser.update(crieteria, updateData).exec(function(err, user){
                //  console.log("update",err);
                //  console.log("update user",user[0]);
                  if(err)
                      thCallback({message: err.message}, null);
                  else{
                    userData = user[0];
                    thCallback(null, user[0]);
                  }
              });
            });
          } else {

            AdminUser.update(crieteria, updateData).exec(function(err, user){
                //console.log("update",err);
                //console.log("update user",user[0]);
                if(err)
                    thCallback({message: err.message}, null);
                else{
                  userData = user[0];
                  thCallback(null, user[0]);
                }
            });

          }
        }
    ], function (err, data) {
        if (err) {
            return callback({message: err.message}, null);
        } else {
          console.log(userData);
            return callback(null, userData);
        }
    });
  },
  updateUserAdminUser: function(req,callback) {
   let data = req.body;
   let isMobileExist;
   let user;
   //var userData  = TokenService.decode(req.headers.authorization);
   let user_id = req.params.id;
   async.series([
       function (cBack) {
           User.find({'country_code':data.country_code,'mobile':data.mobile, id: { '!': user_id }}).exec(function(err, foundUser){
             if(err)
               return cBack(err, null);
             else
             {
               if (foundUser.length > 0) {
                 isMobileExist = foundUser;
               }
               return cBack(null);
             }
           });
       },
       function (firstCallback) {
           User.find({'email':data.email, id: { '!': user_id }}).exec(function(err, foundUser){
             if(err)
               return firstCallback(err, null);
             else
             {
               if (foundUser.length > 0) {
                 user = foundUser;
               }
               return firstCallback(null);
             }
           });
       },
       function (secCallback) {
           UserService.updatePicture(req, function(err,res){
             if (err) {
               return secCallback(err,null);
             } else {
               data.avatar = '';
               if (res) {
                 data.avatar = res;
                 //console.log('==res=='+res);
               }else{
                 User.find({'email':data.email}).exec(function(err, res){
                   if(err){
                     data.avatar = '';
                   }else{
                     data.avatar = res[0]['avatar'];
                   }
                 });
               }
               return secCallback(null);
             }
           });
       },
       function (secondCallback) {
         if (isMobileExist) {
           return secondCallback({message: "Mobile number already exist."}, null);
         } else if (user) {
           return secondCallback({message: "Email already exist."}, null);
         } else {
           let criteria = {'id': user_id};
           if(typeof data.zip_code!='undefined' || typeof data.country_id!='undefined' || typeof data.state_id!='undefined' || typeof city!='undefined')
           {
             UserService.getAddressArr(data,function(err,addressArr){
               UserService.getLatLong(addressArr,function(err,latlongDetails){
                   data.latitude = latlongDetails.latitude;
                   data.longitude = latlongDetails.longitude;
                   UserService.updateUser(data, criteria, function(err,userObj){
                     if (err) {
                       return secondCallback(err,null);
                     } else {
                       user = userObj;
                       return secondCallback(null,user);
                     }
                   });
                 });
             });
           }
           else
           {
             UserService.updateUser(data, criteria, function(err,userObj){
                     if (err) {
                       return secondCallback(err,null);
                     } else {
                       user = userObj;
                       return secondCallback(null,user);
                     }
                   });
           }
         }
       }
   ], function (err, data) {
       if (err) {
           return callback({message: err.message}, null);
       } else {
           return callback(null, user);
       }
   });
 },
 deleteUser: function(req, criteria, callback) {
   let userData;
   async.parallel(
   [
     function deleteUserPost(firstCallback) {
       postcriteria = {user: req.body.userArray};
       Post.destroy(postcriteria).exec(function (err, resData){
         if(err)
           return firstCallback({message: err.message}, null);
         else
         {
           return firstCallback(null,resData);
         }
       });
     },
     function deleteUserSuggestion(secondCallback) {
       suggcriteria = {user: req.body.userArray};
       Suggestion.destroy(suggcriteria).exec(function (err, resData){
         if(err)
           return secondCallback({message: err.message}, null);
         else
         {
           return secondCallback(null,resData);
         }
       });
     },
     function deleteSuggestionComment(thirdCallback) {
       suggcriteria = {user: req.body.userArray};
       Suggestioncomment.destroy(suggcriteria).exec(function (err, resData){
         if(err)
           return thirdCallback({message: err.message}, null);
         else
         {
           return thirdCallback(null,resData);
         }
       });
     },
     function deleteUserCategory(fourthCallback) {
       user_cat_criteria = {user: req.body.userArray};
       Usercategory.destroy(user_cat_criteria).exec(function (err, resData){
         if(err)
           return fourthCallback({message: err.message}, null);
         else
         {
           return fourthCallback(null,resData);
         }
       });
     },
     function deleteUser(lastCallback) {
       User.destroy(criteria).exec(function (err, resData){
         if(err)
           return lastCallback({message: err.message}, null);
         else
         {
           userData = resData;
           return lastCallback(null,resData);
         }
       });
     }
   ],
   function (err, results) {
       return callback(null, userData);
   }); // async.parallel
 },

 //backend apis
  fetchData: function(data, criteria, callback) {
    AdminUser.find(criteria).populate(['role']).sort('createdAt Desc').exec(function(err, foundUsers){
      if (err) {
        return callback({message: err.message}, null);
      } else {
        return callback(null,foundUsers);
      }
    });
  },
  getActiveRoles: function(data, criteria, callback) {
    Role.find(criteria).sort('createdAt asc').exec(function(err, foundRoles){
      if (err) {
        return callback({message: err.message}, null);
      } else {
        return callback(null,foundRoles);
      }
    });
  },
  addAdminUser: function(req,callback) {
    let data = req.body;
    let user;
    let adminuser;
    async.series([
        function (firstCallback) {
            AdminUser.findOne({'email':data.email}).exec(function(err, foundUser){
              if(err)
                return firstCallback(err, null);
              else
              {
                user = foundUser;
                return firstCallback(null);
              }
            });
        },
        function (secCallback) {
          if(user) {
            return secCallback({message: "Email already exist."}, null);
          } else {
            UploadimageService.updatePicture(req, 'avatar', 'avatar', function(err,res){
              if (err) {
                return secCallback(err,null);
              } else {
                data.avatar = '';
                if (res) {
                  data.avatar = res;
                }
                return secCallback(null);
              }
            });
          }
        },
        // function (Callback2) {
        //   AdminUser.encryptPassword(data.password, function(err, encryptedPassword) {
        //     if(err)
        //         return Callback2({message: err.message}, null);
        //     else{
        //       data.password = encryptedPassword;
        //       return Callback2(null, data);
        //     }
        //   });
        // },
        function (secondCallback) {
          AdminUser.create(data).exec(function (err, userData) {
            if(err)
              return secondCallback({message: err.message}, null);
            else
            {
              AdminUser.find({id:userData.id}).populate(['role']).sort('createdAt').exec(function(err, foundUser){
                if (err) {
                  return secondCallback({message: err.message}, null);
                } else {
                  adminuser = foundUser;
                  return secondCallback(null,foundUser);
                }
              });
            }
          });
        },
        function (Callback3) {
          // Create a verification token, which will expire in 2 hours
          token = TokenService.sign({id: adminuser[0].id,user_type: 5}, sails.config.project.passwordAdmin.tokenTTL);

          var templateData = {
            to: adminuser[0].email,
            from: sails.config.fromName+' <'+sails.config.fromEmail+'>',
            subject: sails.config.project.passwordAdmin.subject,
            userEmail: adminuser[0].email,
            siteName: sails.config.project.name,
            url: sails.config.webUrl + sails.config.project.passwordAdmin.uri + token,
            base_url: sails.config.apiUrl,
            name: adminuser[0].first_name+' '+adminuser[0].last_name,
            curr_year: (new Date()).getFullYear()
          };
          console.log('url ='+sails.config.webUrl + sails.config.project.passwordAdmin.uri + token);
          EmailService.sendMail(sails.config.project.passwordAdmin.template, templateData, function(err, message) {
            if (err) {
              console.log("error mail");
              return Callback3({message: err.message}, null);
            }else {
              return Callback3(null,adminuser);
            }
            // return sails.log.error('> error sending password reset email', err);
            // sails.log.verbose('> sending password reset email to:', user.email);
          });
        },
    ], function (err, data) {
        if (err) {
            return callback({message: err.message}, null);
        } else {
            return callback(null, adminuser);
        }
    });
  },
  updateAdminUser: function(req, updateArr, criteria, callback) {
    let dataObj;
    async.series([
        function (firstCallback) {
            AdminUser.findOne(criteria).exec(function(err, foundData){
              if(err)
                return firstCallback(err, null);
              else
              {
                dataObj = foundData;
                return firstCallback(null);
              }
            });
        },
        function (Callback2) {
          UploadimageService.updatePicture(req, 'avatar', 'avatar', function(err,res){
            if (err) {
              return Callback2(err,null);
            } else {
                //updateArr.avatar = '';
              if (res) {
                updateArr.avatar = res;
              }
              return Callback2(null);
            }
          });
        },
        // function (Callback3) {
        //   if(req.body.password){
        //     AdminUser.encryptPassword(req.body.password, function(err, encryptedPassword) {
        //       if(err)
        //           return Callback3({message: err.message}, null);
        //       else{
        //         updateArr.password = encryptedPassword;
        //         return Callback3(null, updateArr);
        //       }
        //     });
        //   }else{
        //     return Callback3(null, updateArr);
        //   }
        // },
        function (secCallback) {
          if(dataObj)
          {
            AdminUser.update(criteria, updateArr).exec(function(err, resObj){
              if(err)
                 return secCallback({message: err.message}, null);
              else{
                userObj = resObj;
                return secCallback(null, resObj);
              }
            });
          }
          else
          {
            return secCallback({message: "Admin User not found."}, null);
          }
        }
    ], function (err, data) {
        if (err) {
            return callback({message: err.message}, null);
        } else {
            return callback(null, userObj);
        }
    });
  },
  deleteAdminUser: function(req, criteria, callback) {
    AdminUser.destroy(criteria).exec(function (err, resData){
      if(err)
        return callback({message: err.message}, null);
      else
      {
        return callback(null,resData);
      }
    });
  },
  routeAllow: function(req, userData, callback) {
      let reqData = req.body;
      let permissionDataFinal;
      async.waterfall([
          function(callback_A) {
              AdminUser.findOne({id: userData.id}).exec(function (err, adminuser) {
                  if (err){
                      callback_A({message: err.message}, null)
                  } else {
                      if (typeof adminuser != 'undefined') {
                          callback_A(null, adminuser)
                      } else {
                          callback_A({message: "User not found!"}, null)
                      }
                  }
              });
          },
          function(adminuser, callback_B) {
              let criteria = {
                  name: reqData.section,
              }
              Module.findOne(criteria).exec(function(err, moduleData){
                  if (err) {
                      return callback_B({message: err.message}, null);
                  } else {
                      return callback_B(null, adminuser, moduleData);
                  }
              });
          },
          function(adminuser, moduleData, callback_C) {
              let criteria = {
                  role: adminuser.role,
                  module: moduleData.id
              }
              Permission.findOne(criteria).exec(function(error, permissionData){
                  if (error) {
                      callback_C({message: error.message}, null);
                  } else {
                      permissionDataFinal = permissionData[reqData.mode];
                      callback_C(null, permissionData);
                  }
              });
          },
      ], function (err, result) {
          if(err){
              return callback({message: err.message}, null);
          } else {
            return callback(null, permissionDataFinal);
          }
      });

  },
}
