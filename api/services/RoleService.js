// RoleService.js - in api/services
var moment = require('moment');
var _ = require('underscore');

module.exports = {

    /***************************************************************************
    * Do not utilize this method unless you are positive that the web token   *
    * has not expired.  To ensure that the web token is valid, add the        *
    * `isAuthorized` policy to any controllers which will utilize this.
    ***************************************************************************/
    getList: function(request, responses, criteria, callback){
        Role.find(criteria).sort('createdAt Desc').exec(function(error, data){
            if (!error) {
                return callback(null, data);
            } else {
                return callback({message: error.message}, null);
            }
        });
    },
    getRole: function(request, responses, criteria, callback){
        let finalData;
        async.waterfall([
            function(callback_one){
                Role.findOne(criteria).populate('modules', {
                    where : {
                        is_active: 1
                    }
                }).exec(function(error, data){
                    if (error) {
                        return callback_one({message: error.message}, null);
                    } else {
                        finalData = data;
                        return callback_one(null, data);
                    }
                });
            },
            function(roleData, callback_two){
                async.forEachOf(roleData.modules, function(moduleName, key, eachCallback) {
                    Permission.findOne({role: parseInt(criteria.id), module: moduleName.id}).exec(function(error, permissionData){
                        if (error) {
                            eachCallback({message: error.message}, null);
                        } else {
                            finalData.modules[key].permission = permissionData;
                            eachCallback(null, finalData);
                        }
                    });
                } ,function(err){
                    if(err) {
                        return callback_two(err, null);
                    } else {
                        return callback_two(null, []);
                    }
                });
            },
        ], function(error, data){
            if (error) {
                return callback({message: error.message}, null);
            } else {
                return callback(null, finalData);
            }
        });
    },
    createRole: function(request, responses, data, callback){

        let roleFinalData, permissionFinalData;
        let moduleList = ['genre', 'tier', 'emoji', 'show', 'cms', 'faq'];
        async.waterfall([
            function(callback_one){
                Role.create(data).exec(function(error, role){
                    if (error) {
                        return callback_one({message: error.message}, null);
                    } else {
                        roleFinalData = role;
                        return callback_one(null, role);
                    }
                });
            },
            function(role, callback_two){
                Module.find({is_active: 1}).exec(function(error, modules){
                    if (error) {
                        return callback_two({message: error.message}, null);
                    } else {
                        return callback_two(null, role, modules);
                    }
                });
            },
            function(role, modules, callback_three){
                async.forEachOf(modules, function(moduleName, key, eachCallback) {
                    let row = {
                        role: roleFinalData.id,
                        module: moduleName.id,
                        add: (request.body[moduleName.name].add)?1:0,
                        edit: (request.body[moduleName.name].edit)?1:0,
                        delete: (request.body[moduleName.name].delete)?1:0,
                    }

                    Permission.create(row).exec(function(error, permissionData){
                        if (error) {
                            eachCallback({message: error.message}, null);
                        } else {
                            permissionFinalData = permissionData;
                            eachCallback(null, permissionData);
                        }
                    });
                } ,function(err){
                    if(err) {
                        return callback_three(err, null);
                    } else {
                        return callback_three(null, []);
                    }
                });
            }
        ], function(error, data){
            if (error) {
                return callback({message: error.message}, null);
            } else {
                return callback(null, roleFinalData);
            }
        });
    },
    bulkUpdateRole: function(request, responses, criteria, data, callback){
        let ids;
        let resObje;
        async.series([
            function(callback_one){
                Role.find(criteria).exec(function(error, data){
                    if (error) {
                        return callback_one(error, null);
                    } else {
                        ids = data.filter(role => role.id).map(role => role.id);
                        return callback_one(null);
                    }
                });
            },
            function(callback_two){
                if(ids.length) {
                    Role.update({'id': ids}, data).exec(function(error, data){
                        if(error) {
                            return callback_two({message: error.message}, null);
                        } else {
                            resObje = data;
                            return callback_two(null, data);
                        }
                    });
                } else {
                    return callback_two({message: "Role not found."}, null);
                }
            }
        ], function(error, data){
            if (error) {
                return callback({message: error.message}, null);
            } else {
                return callback(null, resObje);
            }
        });
    },
    updateRole: function(request, responses, criteria, data, callback){
        let roleId = request.params.id;
        let roleFinalData, permissionFinalData;

        async.waterfall([
            function (callback_one) {
                Role.update(criteria, data).exec(function(error, roleData){
                    if(error) {
                        return callback_one({message: error.message}, null);
                    } else {
                        if (typeof roleData == 'undefined') {
                            return callback_one({message: "Role not found!"}, null);
                        } else {
                            roleFinalData = roleData;
                            return callback_one(null, roleData);
                        }
                    }
                });
            },
            function(role, callback_two){
                Module.find({is_active: 1}).exec(function(error, modules){
                    if (error) {
                        return callback_two({message: error.message}, null);
                    } else {
                        return callback_two(null, role, modules);
                    }
                });
            },
            function (roleData, modules, callback_three) {
                async.forEachOf(modules, function(moduleName, key, eachCallback) {
                    let row = {
                        add: (request.body[moduleName.name].add)?1:0,
                        edit: (request.body[moduleName.name].edit)?1:0,
                        delete: (request.body[moduleName.name].delete)?1:0,
                    }

                    let criterias = {
                        role: parseInt(roleId),
                        module: moduleName.id,
                    }

                    Permission.update(criterias, row).exec(function(error, permissionData){
                        if (error) {
                            eachCallback({message: error.message}, null);
                        } else {
                            permissionFinalData = permissionData;
                            eachCallback(null, permissionData);
                        }
                    });
                } ,function(err){
                    if(err) {
                        return callback_three(err, null);
                    } else {
                        return callback_three(null, []);
                    }
                });
            },
        ], function(error, data){
            if (error) {
                return callback({message: error.message}, null);
            } else {
                return callback(null, roleFinalData);
            }
        });
    },
    deleteRole: function(request, responses, criteria, callback){
        let ids;
        let resObje;
        async.series([
            function (callback_one) {
                Role.find(criteria).exec(function(error, data){
                    if (error) {
                        return callback_one(error, null);
                    } else {
                        ids = data.filter(role => role.id).map(role => role.id);
                        return callback_one(null);
                    }
                });
            },
            function (callback_two) {
                if(ids.length) {
                    Role.destroy({'id': ids}).exec(function (error, data){
                        if(error) {
                            return callback_two({message: error.message}, null);
                        } else {
                            resObje = data;
                            return callback_two(null,data);
                        }
                    });
                } else {
                    return callback_two({message: "Role not found."}, null);
                }
            }
        ], function(error, data){
            if (error) {
                return callback({message: error.message}, null);
            } else {
                return callback(null, resObje);
            }
        });
    },
    getModuleList: function(request, responses, callback){
        Module.find({is_active: 1}).exec(function(error, modules){
            if (error) {
                return callback({message: error.message}, null);
            } else {
                return callback(null, modules);
            }
        });
    },
    getModulePermission: function(request, userData, callback){
        let reqData = request.body;
        let permissionDataFinal;
        async.waterfall([
            function(callback_A) {
                AdminUser.findOne({id: userData.id}).exec(function (err, adminuser) {
                    if (err){
                        callback_A({message: err.message}, null)
                    } else {
                        if (typeof adminuser != 'undefined') {
                            callback_A(null, adminuser)
                        } else {
                            callback_A({message: "User not found!"}, null)
                        }
                    }
                });
            },
            function(adminuser, callback_B) {
                let criteria = {
                    name: reqData.name,
                }
                Module.findOne(criteria).exec(function(err, moduleData){
                    if (err) {
                        return callback_B({message: err.message}, null);
                    } else {
                        return callback_B(null, adminuser, moduleData);
                    }
                });
            },
            function(adminuser, moduleData, callback_C) {
                let criteria = {
                    role: adminuser.role,
                    module: moduleData.id
                }
                Permission.findOne(criteria).exec(function(error, permissionData){
                    if (error) {
                        callback_C({message: error.message}, null);
                    } else {
                        permissionDataFinal = permissionData;
                        callback_C(null, permissionData);
                    }
                });
            },
        ], function (err, result) {
            if(err){
                return callback({message: err.message}, null);
            } else {
              return callback(null, permissionDataFinal);
            }
        });
    },
}
