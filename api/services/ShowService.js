
// UserService.js - in api/services
var moment = require('moment');
var Hashids = require('hashids');
var moment_tz = require('moment-timezone');
var generator = require('generate-password');
var request = require('request');
var crypto = require('crypto');

module.exports = {

    /***************************************************************************
    * Do not utilize this method unless you are positive that the web token   *
    * has not expired.  To ensure that the web token is valid, add the        *
    * `isAuthorized` policy to any controllers which will utilize this.
    ***************************************************************************/


    createShow: function (req, callback) {
        let data = req.body;
        var userData = TokenService.decode(req.headers.authorization);
        let uploadFile = '';
        let zone_name = '';
        let showData;
        let artistData;
        // var algorithm = 'aes256';
        // var key = 'password';
        var algorithm = sails.config.constants.algorithm;
        var key = sails.config.constants.key;
        var text = data.password;
        async.series([
            function (firstCall) {

                User.findOne({ id: userData.id }).populate('tier').exec(function (err, userObj) {
                    if (err) {
                        return firstCall({ message: err.message }, null);
                    } else {
                        artistData = userObj;
                        return firstCall(null, userObj);
                    }
                });
            },
            function (cBack) {
                //if (Number(artistData.tier.max_ppv) >= Number(data.price)) {
                if (typeof req._fileparser != 'undefined' && req._fileparser.upstreams.length > 0) {
                    var filename = req.file('media_name')._files[0].stream.filename;
                    var i = filename.lastIndexOf('.');
                    var ext = filename.substr(i);
                    uploadFile = new moment().valueOf() + ext;
                    UploadimageService.uploadVideo(req, "media_name", sails.config.s3bucket.showmedia, uploadFile, function (err, video) {
                        if (err) {
                            uploadFile = '';
                            return cBack(err.message, null);
                        }
                        else {
                            return cBack(null);
                        }
                    });
                }
                else {
                    uploadFile = '';
                    return cBack(null);
                }
                // } else {
                //     return cBack({ message: 'You can not set price more than $' + artistData.tier.max_ppv }, null);
                // }

            },
            function (midCall) {
                Timezone.findOne({ id: data.timezone }).exec(function (err, resZone) {
                    if (err) {
                        return midCall({ message: err.message }, null);
                    } else {
                        zone_name = resZone.zone_name;
                        return midCall(null, resZone);
                    }
                });
            },
            function (secCall) {
                data.media_name = uploadFile;
                data.artist = userData.id;
                var fmt = 'YYYY-MM-DD HH:mm:00';
                let date_picker = data.publish_date.split(' ');
                let new_date = date_picker[0] + ' ' + date_picker[1] + ' ' + date_picker[2] + ' ' + date_picker[3] + ' ' + date_picker[4];
                var p_date = moment_tz(new_date).format(fmt);
                data.publish_date = p_date;
                data.publish_date_utc = moment_tz.tz(p_date, fmt, zone_name).utc().format(fmt);
                show_end_date = moment(data.publish_date_utc).add(data.duration_hour, 'hours').format(fmt);
                data.show_end_date = moment(show_end_date).add(data.duration_minute, 'm').format(fmt);

                stream_name = data.title.replace(/\s/g, '');
                stream_name = stream_name.substring(0, 5);
                stream_name = stream_name + '' + new moment().valueOf();
                data.stream_name = stream_name;
                if (data.is_password == 'true') {
                    if (text != '' && text != null) {
                        var cipher = crypto.createCipher(algorithm, key);
                        var encrypted = cipher.update(text, 'utf8', 'hex') + cipher.final('hex');
                        data.password = encrypted;
                    }
                } else {
                    data.password = null;
                }
                Show.create(data).exec(function (err, show) {
                    if (err) {
                        return secCall({ message: err.message }, null);
                    } else {
                        showData = show;
                        genreArray = data.genre.split(',');
                        let pc = 0;
                        _.each(genreArray, function (genre) {
                            ShowGenre.create({ 'show': show.id, 'genre': genre }).exec(function (err, showgenre) {
                                pc++;
                                if (pc == genreArray.length) {
                                    return secCall(null, show);
                                }
                            });
                        });
                    }
                });
            },
            function (lastCall) {
                tipPrizeArray = data.tipprize.split(',');
                let pc = 0;
                _.each(tipPrizeArray, function (tipprize) {
                    ShowTipPrize.create({ 'show': showData.id, 'tipprize': tipprize }).exec(function (err, showtipprize) {
                        pc++;
                        if (pc == tipPrizeArray.length) {
                            return lastCall(null, showData);
                        }
                    });
                });
            },
        ], function (err, res) {
            if (err) {
                return callback({ message: err.message }, null);
            } else {
                //send mail to artist and followers api call
                var hashids = new Hashids(sails.config.project.name, 10);
                var encodedId = hashids.encode(showData.id);
                var exec = require('child_process').exec;
                var child;
                url = sails.config.apiUrl + '/sendshowemailsms/' + encodedId;
                //url = 'http://localhost:1337/sendEmailSms/'+token;

                child = exec('wget ' + url + ' -q -b',
                    function (error, stdout, stderr) {
                        if (error !== null) {
                            //console.log('exec error: ' + error);
                        }
                    }).unref();
                return callback(null, showData)
            }
        });
    },

    getShows: function (req, callback) {
        let currDate = moment_tz().utc().format('YYYY-MM-DD HH:mm:ss');
        var userObj = TokenService.decode(req.headers.authorization);
        let previousShow;
        let upcomingShow;
        async.series([
            function (cBack) {
                Show.find().where({ show_end_date: { '<=': currDate }, artist: userObj.id }).populate('showtickets').sort('createdAt DESC').exec(function (err, foundShows) {
                    if (err) {
                        return cBack({ message: err.message }, null);
                    } else {
                        previousShow = foundShows;
                        return cBack(null, foundShows);
                    }
                });
            },
            function (secCall) {
                Show.find().where({ show_end_date: { '>': currDate }, artist: userObj.id }).populate('showtickets').sort('createdAt DESC').exec(function (err, foundShows) {
                    if (err) {
                        return secCall({ message: err.message }, null);
                    } else {
                        upcomingShow = foundShows;
                        return secCall(null, foundShows);
                    }
                });
            }
        ], function (err, res) {
            if (err) {
                return callback({ message: err.message }, null);
            } else {
                let res = { previousShow: previousShow, upcomingShow: upcomingShow };
                return callback(null, res)
            }
        });
    },

    getOrderDetail: function (order_id, callback) {
        async.series([
            function (firstCall) {
                var postData = { grant_type: 'client_credentials' };
                var token_url = sails.config.api_url + '/v1/oauth2/token';
                var auth = "Basic " + new Buffer(sails.config.client_id + ":" + sails.config.secret_key).toString('base64');

                request.post({ url: token_url, form: postData, headers: { "Authorization": auth } }, function (err, httpResponse, paypalBody) {
                    if (err) {
                        return firstCall({ message: err.message }, null);
                    }
                    else {
                        parseBody = JSON.parse(paypalBody);
                        paypal_token = parseBody.access_token;
                        return firstCall(null);
                    }
                });
            },
            function (thCallback) {
                PaypalService.detailPayPalOrder(order_id, paypal_token, function (err, orderData) {
                    if (err) {
                        return thCallback({ message: err.message }, null);
                    } else {
                        if (orderData.payment_details && orderData.payment_details != 'undefined') {
                            payment_id = orderData.payment_details.payment_id;
                            ShowOrder.update({ order_id: order_id }, { payment_id: payment_id }).exec(function (err, res) {
                                if (err) {
                                    return thCallback({ message: err.message }, null);
                                } else {
                                    return thCallback(null);
                                }
                            });
                        } else {
                            return thCallback(null);
                        }
                    }
                });
            }
        ], function (err, data) {
            if (err) {
                return callback({ message: err.message }, null);
            } else {
                return callback(null);
            }
        });
    },

    getFeaturedShows: function (req, callback) {
        var ss = req.param('ss');
        let deleteUser = [0];
        let res = [];
        let resData = [];
        var sort = req.param('sort');
        let order_by = '';
        if (sort) {
            if (sort == '1') {
                order_by = 'ORDER BY S.title ASC';
            } else if (sort == '2') {
                order_by = 'ORDER BY S.title DESC';
            } else if (sort == '3') {
                order_by = 'ORDER BY S.publish_date_utc ASC';
            } else {
                order_by = 'ORDER BY S.id DESC';
            }
        }
        let currDate = moment_tz().utc().format('YYYY-MM-DD HH:mm:ss');
        async.series([
            function (callback1) {
                UserService.deleteuserID(req, function (err, resData) {
                    if (err)
                        return callback1(err, null);
                    else {
                        if (resData.length > 0) {
                            deleteUser = resData;
                        }
                        return callback1(null, deleteUser);
                    }
                });
            },
            function (callback2) {
                let sqlQuery = 'SELECT S.*, U.name, U.profile_picture from shows as S \
                    LEFT JOIN showgenre as SG ON S.id = SG.show \
                    LEFT JOIN genre as G ON G.id = SG.genre \
                    INNER JOIN users as U ON U.id = S.artist \
                    INNER JOIN timezone as T ON T.id = S.timezone \
                    WHERE show_end_date >= "'+ currDate + '" \
                    AND S.status = "Published" \
                    AND S.is_featured = 1 \
                    AND S.artist NOT IN ('+ deleteUser + ')\
                    AND (S.title LIKE "%'+ ss + '%" OR G.name LIKE "%' + ss + '%" OR S.tags LIKE "%' + ss + '%") \
                    GROUP BY S.id \
                    '+ order_by;
                Show.query(sqlQuery, function (err, foundShows) {
                    if (err) {
                        return callback2({ message: err.message }, null);
                    } else {
                        if (foundShows.length > 0 && foundShows != 'undefined') {
                            var item = 0;
                            _.each(foundShows, function (show, i) {
                                Show.findOne({ 'id': show.id }).populate(['genres', 'artist']).exec(function (err, showgenre) {
                                    if (err) {
                                        //return callback({message: err.message}, null);
                                    } else {
                                        foundShows[i].genres = showgenre.genres;
                                        foundShows[i].artist = showgenre.artist;
                                        show.genres = showgenre.genres;
                                        res.push(show);
                                        item++;
                                        if (item == foundShows.length) {
                                            resData = foundShows;
                                            return callback2(null, foundShows);
                                        }
                                    }
                                });
                            });
                        } else {
                            return callback2(null, []);
                        }
                    }
                });
            }
        ], function (err, res) {
            if (err) {
                return callback({ message: err.message }, null);
            } else {
                return callback(null, resData);
            }
        });

        /*let sqlQuery = 'SELECT S.*, U.name, U.profile_picture from shows as S \
            LEFT JOIN showgenre as SG ON S.id = SG.show \
            LEFT JOIN genre as G ON G.id = SG.genre \
            INNER JOIN users as U ON U.id = S.artist \
            INNER JOIN timezone as T ON T.id = S.timezone \
            WHERE show_end_date >= "'+ currDate + '" \
            AND S.status = "Published" \
            AND S.is_featured = 1 \
            AND (S.title LIKE "%'+ ss + '%" OR G.name LIKE "%' + ss + '%" OR S.tags LIKE "%' + ss + '%") \
            GROUP BY S.id \
            '+ order_by;

        Show.query(sqlQuery, function (err, foundShows) {
            if (err) {
                return callback({ message: err.message }, null);
            } else {

                if (foundShows.length > 0 && foundShows != 'undefined') {
                    var item = 0;
                    _.each(foundShows, function (show, i) {
                        Show.findOne({ 'id': show.id }).populate(['genres', 'artist']).exec(function (err, showgenre) {
                            if (err) {
                                return callback({message: err.message}, null);
                            } else {
                                foundShows[i].genres = showgenre.genres;
                                foundShows[i].artist = showgenre.artist;
                                show.genres = showgenre.genres;
                                res.push(show);
                                item++;
                                if (item == foundShows.length) {
                                    return callback(null, foundShows);
                                }
                            }
                        });
                    });
                } else {
                    return callback(null, []);
                }
            }
        });*/
    },

    searchShows: function (req, callback) {
        let res = [];
        let resData;
        let deleteUser = [0];
        var ss = req.param('ss');
        let currDate = moment_tz().utc().format('YYYY-MM-DD HH:mm:ss');
        async.series([
            function (callback1) {
                UserService.deleteuserID(req, function (err, resData) {
                    if (err)
                        return callback1(err, null);
                    else {
                        if (resData.length > 0) {
                            deleteUser = resData;
                        }
                        return callback1(null, deleteUser);
                    }
                });
            },
            function (callback2) {
                let sqlQuery = 'SELECT S.*, U.name, U.profile_picture from shows as S \
                    INNER JOIN showgenre as SG ON S.id = SG.show \
                    INNER JOIN genre as G ON G.id = SG.genre \
                    INNER JOIN users as U ON U.id = S.artist \
                    INNER JOIN timezone as T ON T.id = S.timezone \
                    WHERE S.status = "Published" \
                    AND S.artist NOT IN ('+ deleteUser + ')\
                    AND (S.title LIKE "%'+ ss + '%" OR G.name LIKE "%' + ss + '%" OR S.tags LIKE "%' + ss + '%") GROUP BY S.id';

                Show.query(sqlQuery, function (err, foundShows) {
                    if (err) {
                        return callback2({ message: err.message }, null);
                    } else {
                        if (foundShows.length > 0 && foundShows != 'undefined') {
                            var item = 0;
                            _.each(foundShows, function (show, i) {
                                Show.findOne({ 'id': show.id }).populate(['genres', 'artist']).exec(function (err, showgenre) {
                                    if (err) {
                                        //return callback({message: err.message}, null);
                                    } else {
                                        show.genres = showgenre.genres;
                                        show.artist = showgenre.artist;
                                        res.push(show);
                                        item++;
                                        if (item == foundShows.length) {
                                            resData = res;
                                            return callback2(null, res);
                                        }
                                    }
                                });
                            });
                        } else {
                            return callback2(null, []);
                        }

                    }
                });
            }
        ], function (err, res) {
            if (err) {
                return callback({ message: err.message }, null);
            } else {
                return callback(null, resData);
            }
        });
    },

    getShowDetail: function (req, callback) {
        let currDate = moment_tz().utc().format('YYYY-MM-DD HH:mm:ss');
        let show_id = req.param('id');
        let upcomingShow;
        var userObj = TokenService.decode(req.headers.authorization);
        let showData;
        let response;

        async.series([
            //Load user
            function (firstcallback) {
                Show.findOne({ id: show_id }).populate(['artist', 'genres', 'timezone', 'tipprizes']).sort('createdAt DESC').exec(function (err, foundShows) {
                    if (err) {
                        return firstcallback({ message: err.message }, null);
                    } else {
                        if (foundShows != 'undefined' && foundShows) {
                            showData = foundShows;
                            return firstcallback(null, foundShows)
                        } else {
                            return firstcallback({ message: 'Show does not exist!' }, null);
                        }
                    }
                });
            },
            function (parallelCallback) {
                async.parallel({
                    artistfollowers_count: function (cBack) {
                        Artistfollowers.count({ artist: showData.artist.id }).exec(function (err, foundShows) {
                            if (err) {
                                return cBack({ message: err.message }, null);
                            } else {
                                return cBack(null, foundShows);
                            }
                        });
                    },
                    emojis: function (secCall) {
                        Emoji.find({ is_active: 1 }).sort('value ASC').exec(function (err, foundShows) {
                            if (err) {
                                return secCall({ message: err.message }, null);
                            } else {
                                return secCall(null, foundShows);
                            }
                        });
                    },
                    gotticket: function (secCall) {
                        if (userObj) {
                            ShowTicket.findOne({ user: userObj.id, show: show_id }).exec(function (err, foundShows) {
                                if (err) {
                                    return secCall({ message: err.message }, null);
                                } else {
                                    if (foundShows && typeof foundShows != 'undefined') {
                                        return secCall(null, true);
                                    } else {
                                        if (showData.artist.id == userObj.id) {
                                            return secCall(null, true);
                                        } else {
                                            return secCall(null, false);
                                        }
                                    }
                                }
                            });
                        } else {
                            return secCall(null, false);
                        }
                    },
                    vod_pay: function (secCall) {
                        if (userObj) {
                            //ShowTicket.findOne({ user: userObj.id, show: show_id }).exec(function (err, foundShows) {
                            VodPayment.findOne({ show: show_id, user: userObj.id }).exec(function (err, vodPay) {
                                if (err) {
                                    return secCall({ message: err.message }, null);
                                } else {
                                    if (vodPay && typeof vodPay != 'undefined') {
                                        return secCall(null, true);
                                    } else {
                                        if (showData.artist.id == userObj.id) {
                                            return secCall(null, true);
                                        } else {
                                            if (showData.is_paid == 0) {
                                                return secCall(null, true);
                                            } else {
                                                return secCall(null, false);
                                            }
                                        }
                                    }
                                }
                            });
                        } else {
                            return secCall(null, false);
                        }
                    },
                    topfans: function (secCall) {
                        sqlQuery = 'SELECT SUM(amount) as totaltip,U.name, U.profile_picture,U.id as user_id FROM artist_tip as AT \
                        INNER JOIN users as U ON U.id = AT.user \
                        WHERE AT.show = '+ showData.id + ' AND AT.tip_type = 1\
                        GROUP BY AT.user ORDER BY totaltip desc LIMIT 5';

                        ArtistTip.query(sqlQuery, function (err, resTip) {
                            //ArtistTip.find({artist:showData.artist.id}).sum('amount').sort('amount', 'DESC').limit(5).exec(function(err, resTip){
                            if (err) {
                                return secCall({ message: err.message }, null);
                            } else {

                                return secCall(null, resTip);
                            }
                        });
                    }
                }, function (err, res) {
                    if (err) {
                        return callback({ message: err.message }, null);
                    } else {
                        var pr_charge = 0;
                        var fix_charge = 0;
                        if (showData.artist.payment_type == 1) {
                            pr_charge = sails.config.constants.paypal_pr_charge;
                            fix_charge = sails.config.constants.paypal_fix_charge;
                        } else {
                            pr_charge = sails.config.constants.stripe_pr_charge;
                            fix_charge = sails.config.constants.stripe_fix_charge;
                        }
                        response = res;
                        response.show = showData;
                        response.show.wowza_sdp = sails.config.wowza_sdp;
                        response.show.wowza_application = sails.config.wowza_application;
                        var fmt = 'YYYY-MM-DD HH:mm:ss';
                        var show_duration_min = (res.show.duration_hour * 60) + res.show.duration_minute;

                        response.show.to_date = moment(res.show.publish_date_utc, fmt).add(res.show.duration_hour, 'hours').format(fmt);
                        response.show.to_date = moment(response.show.to_date, fmt).add(res.show.duration_minute, 'm').format(fmt);
                        response.show.tax = ((showData.price * pr_charge) / 100) + fix_charge;

                        return parallelCallback(null, res);
                    }
                })
            }
        ], function (err, resdata) {
            if (err) {
                return callback({ message: err.message }, null);
            } else {
                return callback(null, response)
            }
        });
    },

    giftAFriend_OLD: function (req, callback) {
        let data = req.body;
        var userObj = TokenService.decode(req.headers.authorization);
        let userData;
        let fromUser;
        async.series([
            function (cBack) {
                User.findOne({ email: data.email }).sort('createdAt DESC').exec(function (err, foundUser) {
                    if (err) {
                        return cBack({ message: err.message }, null);
                    } else {
                        userData = foundUser;
                        return cBack(null, userData);
                    }
                });
            },
            function (firstCallbackBack) {
                User.findOne({ id: userObj.id }).exec(function (err, foundUser) {
                    if (err) {
                        return firstCallbackBack({ message: err.message }, null);
                    } else {
                        fromUser = foundUser;
                        return firstCallbackBack(null, userData);
                    }
                });
            },
            function (secCall) {
                if (userData) {
                    if (data.amount > fromUser.balance) {
                        return secCall({ message: 'You do not have sufficient balance to gift.' }, null);
                    } else if (userData.id == fromUser.id) {
                        return secCall({ message: 'You can not gift your self.' }, null);
                    } else {
                        data.to_user = userData.id;
                        data.user = fromUser.id;
                        Giftafriend.create(data).exec(function (err, show) {
                            if (err) {
                                return secCall({ message: err.message }, null);
                            } else {
                                let balance_data = {
                                    user: userData.id,
                                    amount: data.amount,
                                    balance_type: 1,
                                    current_balance: userData.balance + data.amount,
                                    description: 'Gift from friend'
                                };
                                ArtistBalance.create(balance_data).exec(function (err, artistbalancehistory) {
                                    if (err) {
                                        return secCall({ message: err.message }, null);
                                    } else {
                                        let balancedata = {
                                            user: fromUser.id,
                                            amount: data.amount,
                                            balance_type: 2,
                                            current_balance: Number(fromUser.balance) - Number(data.amount),
                                            description: 'Gift to friend'
                                        };
                                        ArtistBalance.create(balancedata).exec(function (err, artistbalancehistory) {
                                            if (err) {
                                                return secCall({ message: err.message }, null);
                                            } else {
                                                return secCall(null);
                                            }
                                        });
                                    }
                                });
                            }
                        });
                    }
                } else {
                    return secCall({ message: 'This user does not exist!' }, null);
                }
            },
            function (thCallback) {
                User.update({ id: userData.id }, { balance: userData.balance + data.amount }).exec(function (err, resUser) {
                    if (err) {
                        return thCallback({ message: err.message }, null);
                    } else {

                        User.update({ id: fromUser.id }, { balance: fromUser.balance - data.amount }).exec(function (err, resUser) {
                            if (err) {
                                return thCallback({ message: err.message }, null);
                            } else {

                                var templateData = {
                                    to: userData.email,
                                    from: sails.config.fromName + ' <' + sails.config.fromEmail + '>',
                                    subject: sails.config.project.giftAFriend.subject,
                                    siteName: sails.config.project.name,
                                    base_url: sails.config.apiUrl,
                                    name: userData.name,
                                    from_name: fromUser.name,
                                    amount: data.amount,
                                    curr_year: (new Date()).getFullYear()
                                };
                                EmailService.sendMail(sails.config.project.giftAFriend.template, templateData, function (err, message) {
                                    if (err) {
                                        return sails.log.error('> error sending email', err);
                                    }
                                    sails.log.verbose('> sending email to:', userData.email);
                                });
                                return thCallback(null);
                            }
                        });

                    }
                });

            }
        ], function (err, res) {
            if (err) {
                return callback({ message: err.message }, null);
            } else {
                return callback(null, res)
            }
        });
    },

    giftAFriendFreeShow: function (req, callback) {
        let data = req.body;
        var userObj = TokenService.decode(req.headers.authorization);
        let userData;
        let fromUser;
        let order;
        async.series([
            function (cBack) {
                User.findOne({ email: data.email }).sort('createdAt DESC').exec(function (err, foundUser) {
                    if (err) {
                        return cBack({ message: err.message }, null);
                    } else {
                        userData = foundUser;
                        return cBack(null, userData);
                    }
                });
            },
            function (firstCallbackBack) {
                Show.findOne({ id: data.show_id }).populate('artist').exec(function (err, showObj) {
                    if (err) {
                        return firstCallbackBack({ message: err.message }, null);
                    } else {
                        showData = showObj;
                        return firstCallbackBack(null);
                    }
                });
            },
            function (secCall) {
                if (userData) {
                    if (userData.id == userObj.id) {
                        return secCall({ message: 'You can not gift your self.' }, null);
                    } else if (showData.artist.id == userData.id) {
                        return secCall({ message: 'You can not gift to presenter of the session.' }, null);
                    } else {
                        return secCall(null);
                    }
                } else {
                    User.create({ email: data.email, gifted_user: 1 }).exec(function (err, foundUser) {
                        if (err) {
                            return secCall({ message: err.message }, null);
                        } else {
                            userData = foundUser;
                            return secCall(null);
                        }
                    });
                }
            },
            function (firstCallbackBack) {
                User.findOne({ id: userObj.id }).exec(function (err, foundUser) {
                    if (err) {
                        return firstCallbackBack({ message: err.message }, null);
                    } else {
                        fromUser = foundUser;
                        return firstCallbackBack(null, userData);
                    }
                });
            },

            function (thCallback) {
                var templateData = {
                    to: userData.email,
                    from: sails.config.fromName + ' <' + sails.config.fromEmail + '>',
                    subject: sails.config.project.giftAFriendFreeShow.subject,
                    siteName: sails.config.project.name,
                    base_url: sails.config.apiUrl,
                    url: sails.config.frontUrl,
                    show_url: sails.config.frontUrl + '/show/' + data.show_id,
                    name: userData.name,
                    from_name: fromUser.name,
                    amount: data.amount,
                    curr_year: (new Date()).getFullYear()
                };
                EmailService.sendMail(sails.config.project.giftAFriendFreeShow.template, templateData, function (err, message) {
                    if (err) {
                        return sails.log.error('> error sending email', err);
                    }
                    sails.log.verbose('> sending email to:', userData.email);
                });
                return thCallback(null);
            },
            function (secCall) {
                var p_data = { to_user: userData.id, user: userObj.id, show_id: data.show_id }
                Giftafriend.create(p_data).exec(function (err, show) {
                    if (err) {
                        return secCall({ message: err.message }, null);
                    } else {
                        return secCall(null);
                    }
                });
            },
        ], function (err, res) {
            if (err) {
                return callback({ message: err.message }, null);
            } else {
                return callback(null)
            }
        });
    },

    giftAFriend: function (req, callback) {
        let data = req.body;
        var userObj = TokenService.decode(req.headers.authorization);
        let userData;
        let fromUser;
        let orderObj;
        let showData;
        async.series([
            function (cBack) {
                User.findOne({ email: data.email }).sort('createdAt DESC').exec(function (err, foundUser) {
                    if (err) {
                        return cBack({ message: err.message }, null);
                    } else {
                        userData = foundUser;
                        return cBack(null, userData);
                    }
                });
            },
            function (firstCallbackBack) {
                Show.findOne({ id: data.show_id }).populate('artist').exec(function (err, showObj) {
                    if (err) {
                        return firstCallbackBack({ message: err.message }, null);
                    } else {
                        showData = showObj;
                        return firstCallbackBack(null);
                    }
                });
            },
            function (secCall) {
                if (userData) {
                    if (userData.id == userObj.id) {
                        return secCall({ message: 'You can not gift your self.' }, null);
                    } else if (showData.artist.id == userData.id) {
                        return secCall({ message: 'You can not gift to presenter of the session.' }, null);
                    } else {
                        req.body.giftafriend = 1;
                        req.body.touser = userData.id;
                        ShowService.getShowAccess(req, function (err, orderData) {
                            if (err) {
                                return secCall({ message: err.message }, null);
                            } else {
                                orderObj = orderData;
                                return secCall(null)
                            }
                        });
                    }
                } else {
                    User.create({ email: data.email, gifted_user: 1 }).exec(function (err, foundUser) {
                        if (err) {
                            return secCall({ message: err.message }, null);
                        } else {
                            req.body.giftafriend = 1;
                            req.body.touser = foundUser.id;
                            ShowService.getShowAccess(req, function (err, orderData) {
                                if (err) {
                                    return secCall({ message: err.message }, null);
                                } else {
                                    orderObj = orderData;
                                    return secCall(null)
                                }
                            });
                        }
                    });


                }
            }

        ], function (err, res) {
            if (err) {
                return callback({ message: err.message }, null);
            } else {
                return callback(null, orderObj)
            }
        });
    },

    giftAFriendFinal: function (req, callback) {
        let data = req.body;
        var userObj = TokenService.decode(req.headers.authorization);
        let userData;
        let fromUser;
        let order;
        async.series([
            function (firstCallbackBack) {
                ShowOrder.findOne({ show: data.show_id, order_id: data.order_id }).exec(function (err, orderObj) {
                    if (err) {
                        return firstCallbackBack({ message: err.message }, null);
                    } else {
                        if (!orderObj || orderObj == 'undefined') {
                            return firstCallbackBack({ message: 'Something went wrong. Please try again later.' }, null); // Stop the process  and directly return to the main function
                        } else {
                            order = orderObj;
                            return firstCallbackBack(null);
                        }
                    }
                });
            },
            function (cBack) {
                User.findOne({ email: order.gift_email }).sort('createdAt DESC').exec(function (err, foundUser) {
                    if (err) {
                        return cBack({ message: err.message }, null);
                    } else {
                        userData = foundUser;
                        return cBack(null, userData);
                    }
                });
            },
            function (firstCallbackBack) {
                User.findOne({ id: userObj.id }).exec(function (err, foundUser) {
                    if (err) {
                        return firstCallbackBack({ message: err.message }, null);
                    } else {
                        fromUser = foundUser;
                        return firstCallbackBack(null, userData);
                    }
                });
            },
            function (thCallback) {
                var templateData = {
                    to: userData.email,
                    from: sails.config.fromName + ' <' + sails.config.fromEmail + '>',
                    subject: sails.config.project.giftAFriend.subject,
                    siteName: sails.config.project.name,
                    base_url: sails.config.apiUrl,
                    url: sails.config.frontUrl,
                    name: userData.name,
                    from_name: fromUser.name,
                    amount: data.amount,
                    curr_year: (new Date()).getFullYear()
                };
                EmailService.sendMail(sails.config.project.giftAFriend.template, templateData, function (err, message) {
                    if (err) {
                        return sails.log.error('> error sending email', err);
                    }
                    sails.log.verbose('> sending email to:', userData.email);
                });
                return thCallback(null);
            },
            function (secCall) {
                if (userData) {
                    req.body.giftafriend = 1;
                    req.body.touser = userData.id;
                    ShowService.getShowAccessFinal(req, function (err, orderData) {
                        if (err) {
                            return secCall({ message: err.message }, null);
                        } else {
                            return secCall(null)
                        }
                    });
                } else {
                    return secCall({ message: 'This user does not exist!' }, null);
                }
            },
            function (secCall) {
                var p_data = { to_user: userData.id, user: userObj.id, show_id: data.show_id }
                Giftafriend.create(p_data).exec(function (err, show) {
                    if (err) {
                        return secCall({ message: err.message }, null);
                    } else {
                        return secCall(null);
                    }
                });
            },
        ], function (err, res) {
            if (err) {
                return callback({ message: err.message }, null);
            } else {
                return callback(null)
            }
        });
    },

    giftAFriendStripe: function (req, callback) {
        let data = req.body;
        var userObj = TokenService.decode(req.headers.authorization);
        let userData;
        let fromUser;
        let order;
        async.series([
            function (cBack) {
                User.findOne({ email: data.email }).sort('createdAt DESC').exec(function (err, foundUser) {
                    if (err) {
                        return cBack({ message: err.message }, null);
                    } else {
                        userData = foundUser;
                        return cBack(null, userData);
                    }
                });
            },
            function (firstCallbackBack) {
                Show.findOne({ id: data.show_id }).populate('artist').exec(function (err, showObj) {
                    if (err) {
                        return firstCallbackBack({ message: err.message }, null);
                    } else {
                        showData = showObj;
                        return firstCallbackBack(null);
                    }
                });
            },
            function (secCall) {
                if (userData) {
                    if (userData.id == userObj.id) {
                        return secCall({ message: 'You can not gift your self.' }, null);
                    } else if (showData.artist.id == userData.id) {
                        return secCall({ message: 'You can not gift to presenter of the session.' }, null);
                    } else {
                        return secCall(null);
                    }
                } else {
                    User.create({ email: data.email, gifted_user: 1 }).exec(function (err, foundUser) {
                        if (err) {
                            return secCall({ message: err.message }, null);
                        } else {
                            userData = foundUser;
                            return secCall(null);
                        }
                    });
                }
            },
            function (firstCallbackBack) {
                User.findOne({ id: userObj.id }).exec(function (err, foundUser) {
                    if (err) {
                        return firstCallbackBack({ message: err.message }, null);
                    } else {
                        fromUser = foundUser;
                        return firstCallbackBack(null, userData);
                    }
                });
            },

            function (secCall) {
                if (userData) {
                    req.body.giftafriend = 1;
                    req.body.touser = userData.id;
                    ShowService.getShowAccessStripe(req, function (err, orderData) {
                        if (err) {
                            return secCall({ message: err.message }, null);
                        } else {
                            return secCall(null)
                        }
                    });
                } else {
                    return secCall({ message: 'This user does not exist!' }, null);
                }
            },
            function (thCallback) {
                var templateData = {
                    to: userData.email,
                    from: sails.config.fromName + ' <' + sails.config.fromEmail + '>',
                    subject: sails.config.project.giftAFriend.subject,
                    siteName: sails.config.project.name,
                    base_url: sails.config.apiUrl,
                    url: sails.config.frontUrl,
                    name: userData.name,
                    from_name: fromUser.name,
                    amount: data.amount,
                    curr_year: (new Date()).getFullYear()
                };
                EmailService.sendMail(sails.config.project.giftAFriend.template, templateData, function (err, message) {
                    if (err) {
                        return sails.log.error('> error sending email', err);
                    }
                    sails.log.verbose('> sending email to:', userData.email);
                });
                return thCallback(null);
            },
            function (secCall) {
                var p_data = { to_user: userData.id, user: userObj.id, show_id: data.show_id }
                Giftafriend.create(p_data).exec(function (err, show) {
                    if (err) {
                        return secCall({ message: err.message }, null);
                    } else {
                        return secCall(null);
                    }
                });
            },
        ], function (err, res) {
            if (err) {
                return callback({ message: err.message }, null);
            } else {
                return callback(null)
            }
        });
    },

    purchaseTip: function (req, callback) {
        let data = req.body;
        var userObj = TokenService.decode(req.headers.authorization);
        tipping_dolar = data.amount * sails.config.project.tippingDollarValue;
        let userData;
        let fromUser;
        let finalBalance;
        async.series([

            function (firstCallbackBack) {
                User.findOne({ id: userObj.id }).exec(function (err, foundUser) {
                    if (err) {
                        return firstCallbackBack({ message: err.message }, null);
                    } else {
                        userData = foundUser;
                        return firstCallbackBack(null, userData);
                    }
                });
            },
            function (secCall) {
                var post_data = { user: userData.id, amount: data.amount, order_id: data.order_id, status: data.status, payment_date: data.payment_date, payment_type: 1 };
                UserPurchaseHistory.create(post_data).exec(function (err, show) {
                    if (err) {
                        return secCall({ message: err.message }, null);
                    } else {
                        let balance_data = {
                            user: userData.id,
                            amount: tipping_dolar,
                            balance_type: 1,
                            current_balance: userData.balance + tipping_dolar,
                            description: 'Purshase tipping dollar'
                        };
                        ArtistBalance.create(balance_data).exec(function (err, artistbalancehistory) {
                            if (err) {
                                return secCall({ message: err.message }, null);
                            } else {
                                return secCall(null);
                            }
                        });
                    }
                });
            },
            function (thCallback) {
                User.update({ id: userData.id }, { balance: userData.balance + tipping_dolar }).exec(function (err, resUser) {
                    if (err) {
                        return thCallback({ message: err.message }, null);
                    } else {
                        finalBalance = resUser[0].balance;
                        return thCallback(null);
                    }
                });
            }
        ], function (err, res) {
            if (err) {
                return callback({ message: err.message }, null);
            } else {

                var data = { balance: finalBalance };
                return callback(null, data)
            }
        });
    },

    getShowAccess_old: function (req, callback) {
        let data = req.body;
        var userObj = TokenService.decode(req.headers.authorization);
        let userData;
        let showData;
        let tipping_amount;
        async.series([
            function (firstCallbackBack) {
                User.findOne({ id: userObj.id }).exec(function (err, foundUser) {
                    if (err) {
                        return firstCallbackBack({ message: err.message }, null);
                    } else {
                        userData = foundUser;
                        return firstCallbackBack(null);
                    }
                });
            },
            function (firstCallbackBack) {
                Show.findOne({ id: data.show_id }).populate(['timezone', 'artist']).exec(function (err, showObj) {
                    if (err) {
                        return firstCallbackBack({ message: err.message }, null);
                    } else {
                        showData = showObj;
                        return firstCallbackBack(null);
                    }
                });
            },
            // function (secCall) {
            //   var post_data = {user: userData.id, amount: showData.price, payment_id: data.payment_id, status: data.status, payment_date: data.payment_date, payment_type: 2};
            //   UserPurchaseHistory.create(post_data).exec(function(err, show){
            //     if (err) {
            //       return secCall({message: err.message}, null);
            //     } else {
            //       return secCall(null);
            //     }
            //   });
            // },
            function (thCallback) {
                tipping_amount = showData.price * sails.config.project.tippingDollarValue;
                if (tipping_amount > userData.balance) {
                    return thCallback({ message: 'You do not have sufficient balance to get access.' }, null);
                } else {

                    var book_id = generator.generate({
                        length: 10,
                        numbers: true
                    });
                    var booking_id = book_id.toUpperCase();
                    var post_data = { show: data.show_id, user: userData.id, booking_id: booking_id, amount: tipping_amount };
                    ShowTicket.create(post_data).exec(function (err, showTicket) {
                        if (err) {
                            return thCallback({ message: err.message }, null);
                        } else {
                            let current_balance = Number(userData.balance) - Number(tipping_amount);
                            let balancedata = {
                                user: userData.id,
                                amount: tipping_amount,
                                balance_type: 2,
                                current_balance: current_balance,
                                description: 'Get access of the show.'
                            };
                            ArtistBalance.create(balancedata).exec(function (err, artistbalancehistory) {
                                if (err) {
                                    return thCallback({ message: err.message }, null);
                                } else {
                                    User.update({ id: userData.id }, { balance: current_balance }).exec(function (err, resUser) {
                                        if (err) {
                                            return thCallback({ message: err.message }, null);
                                        } else {

                                            published_date = moment(showData.publish_date).format('DD MMMM Y h:mm A') + ' ' + showData.timezone.code;
                                            created_date = moment(showTicket.createdAt).format('DD MMM, Y | h:mm A') + ' ' + showData.timezone.code;
                                            confirmation_no = showTicket.id;
                                            var profile_picture = showData.artist.profile_picture ? sails.config.s3bucket.bucketurl + sails.config.s3bucket.userprofile + showData.artist.profile_picture : sails.config.apiUrl + '/images/user-image.png';

                                            var templateData = {
                                                to: userData.email,
                                                from: sails.config.fromName + ' <' + sails.config.fromEmail + '>',
                                                subject: sails.config.project.getAccess.subject,
                                                siteName: sails.config.project.name,
                                                base_url: sails.config.apiUrl,
                                                name: userData.name,
                                                show: showData,
                                                tipping_amount: tipping_amount,
                                                booking_id: booking_id,
                                                published_date: published_date,
                                                confirmation_no: confirmation_no,
                                                created_date: created_date,
                                                profile_picture: profile_picture,
                                                curr_year: (new Date()).getFullYear()
                                            };
                                            EmailService.sendMail(sails.config.project.getAccess.template, templateData, function (err, message) {
                                                if (err) {
                                                    return sails.log.error('> error sending email', err);
                                                }
                                                sails.log.verbose('> sending email to:', userData.email);
                                            });

                                            return thCallback(null);
                                        }
                                    });
                                }
                            });


                        }
                    });
                }
            }
        ], function (err, res) {
            if (err) {
                return callback({ message: err.message }, null);
            } else {
                return callback(null, res)
            }
        });
    },


    checkSessionPassword: function (req, callback) {
        Show.findOne({ id: req.body.show_id }).exec(function (err, foundShow) {
            if (err) {
                return callback({ message: err.message }, null);
            } else {
                if (foundShow && foundShow != 'undefined') {
                    // var algorithm = 'aes256';
                    // var key = 'password';
                    var algorithm = sails.config.constants.algorithm;
                    var key = sails.config.constants.key;

                    if (foundShow.is_password == true) {
                        var text = foundShow.password;
                        if (text != '' && text != null) {
                            var decipher = crypto.createDecipher(algorithm, key);
                            var decrypted = decipher.update(text, 'hex', 'utf8') + decipher.final('utf8');
                            if (decrypted == req.body.password) {
                                return callback(null, true);
                            } else {
                                return callback(null, false);
                            }
                        } else {
                            return callback(null, false);
                        }
                    } else {
                        return callback(null, true);
                    }
                } else {
                    return callback(null, false);
                }
            }
        });
    },

    getFreeShowAccess: function (req, callback) {
        let data = req.body;

        var userObj = TokenService.decode(req.headers.authorization);

        let userData;
        let showData;
        let tipping_amount;
        let parseBody;
        let orderData;

        async.series([

            function (firstCallbackBack) {
                ShowTicket.findOne({ show: data.show_id, user: userObj.id }).exec(function (err, ticketObj) {
                    if (err) {
                        return firstCallbackBack({ message: err.message }, null);
                    } else {
                        if (!ticketObj || ticketObj == 'undefined') {
                            return firstCallbackBack(null);
                        } else {
                            return callback({ message: 'Already have access of this free session.' }, null); // Stop the process  and directly return to the main function
                        }
                    }
                });
            },
            function (secCallbackBack) {
                User.findOne({ id: userObj.id }).exec(function (err, foundUser) {
                    if (err) {
                        return secCallbackBack({ message: err.message }, null);
                    } else {
                        userData = foundUser;
                        return secCallbackBack(null);
                    }
                });
            },
            function (thCallbackBack) {
                Show.findOne({ id: data.show_id }).populate(['timezone', 'artist']).exec(function (err, showObj) {
                    if (err) {
                        return thCallbackBack({ message: err.message }, null);
                    } else {
                        showData = showObj;
                        return thCallbackBack(null);
                    }
                });
            },

            function (thirdCallbackBack) {

                if (showData && showData != 'undefined') {
                    // var algorithm = 'aes256';
                    // var key = 'password';
                    var algorithm = sails.config.constants.algorithm;
                    var key = sails.config.constants.key;

                    if (showData.is_password == true) {
                        var text = showData.password;
                        if (text != '' && text != null) {
                            var decipher = crypto.createDecipher(algorithm, key);
                            var decrypted = decipher.update(text, 'hex', 'utf8') + decipher.final('utf8');
                            if (decrypted == req.body.password) {
                                return thirdCallbackBack(null);
                            } else {
                                return callback({ message: 'Password does not match.' }, null);
                            }
                        } else {
                            return callback({ message: 'Password does not match.' }, null);
                        }
                    } else {
                        return thirdCallbackBack(null);
                    }
                } else {
                    return callback({ message: 'Session does not exist.' }, null);
                }

            },

            function (frCallbackBack) {
                var book_id = generator.generate({
                    length: 10,
                    numbers: true
                });
                var booking_id = book_id.toUpperCase();
                var post_data = { show: data.show_id, user: userData.id, is_free: 1, booking_id: booking_id, amount: 0 };
                ShowTicket.create(post_data).exec(function (err, showTicket) {
                    if (err) {
                        return frCallbackBack({ message: err.message }, null);
                    } else {
                        published_date = moment(showData.publish_date).format('DD MMMM Y h:mm A') + ' ' + showData.timezone.code;
                        created_date = moment(showTicket.createdAt).format('DD MMM, Y | h:mm A') + ' ' + showData.timezone.code;
                        confirmation_no = showTicket.id;
                        var profile_picture = showData.artist.profile_picture ? sails.config.s3bucket.bucketurl + sails.config.s3bucket.userprofile + showData.artist.profile_picture : sails.config.apiUrl + '/images/user-image.png';
                        var templateData = {
                            to: userData.email,
                            from: sails.config.fromName + ' <' + sails.config.fromEmail + '>',
                            subject: sails.config.project.getAccess.subject,
                            siteName: sails.config.project.name,
                            base_url: sails.config.apiUrl,
                            site_url: sails.config.frontUrl,
                            name: userData.name,
                            show: showData,
                            subTotal: 0.00,
                            tax: 0.00,
                            grandTotal: 0.00,
                            tipping_amount: tipping_amount,
                            booking_id: booking_id,
                            published_date: published_date,
                            confirmation_no: confirmation_no,
                            created_date: created_date,
                            profile_picture: profile_picture,
                            curr_year: (new Date()).getFullYear()
                        };
                        EmailService.sendMail(sails.config.project.getFreeAccess.template, templateData, function (err, message) {
                            if (err) {
                                return frCallbackBack(null);
                            } else {
                                return frCallbackBack(null);
                            }
                        });
                    }
                });
            },
        ], function (err, res) {
            if (err) {
                return callback({ message: err.message }, null);
            } else {
                return callback(null, orderData)
            }
        });
    },

    getShowAccessStripe: function (req, callback) {
        let data = req.body;
        if (data.giftafriend == 1) {
            var userObj = { id: data.touser };
            var userObj_tmp = TokenService.decode(req.headers.authorization);
            order_type = 4;
            payment_type = 3;
        } else {
            var userObj = TokenService.decode(req.headers.authorization);
            order_type = 1;
            payment_type = 2;
        }
        let userData;
        let showData;
        let tipping_amount;
        let parseBody;
        let orderData;

        async.series([

            function (firstCallbackBack) {
                ShowTicket.findOne({ show: data.show_id, user: userObj.id }).exec(function (err, ticketObj) {
                    if (err) {
                        return firstCallbackBack({ message: err.message }, null);
                    } else {
                        if (!ticketObj || ticketObj == 'undefined') {
                            return firstCallbackBack(null);
                        } else {
                            return callback({ message: 'Something went wrong. Please try again later.' }, null); // Stop the process  and directly return to the main function
                        }
                    }
                });
            },
            function (firstCallbackBack) {
                User.findOne({ id: userObj.id }).exec(function (err, foundUser) {
                    if (err) {
                        return firstCallbackBack({ message: err.message }, null);
                    } else {
                        userData = foundUser;
                        return firstCallbackBack(null);
                    }
                });
            },
            function (firstCallbackBack) {
                Show.findOne({ id: data.show_id }).populate(['timezone', 'artist']).exec(function (err, showObj) {
                    if (err) {
                        return firstCallbackBack({ message: err.message }, null);
                    } else {
                        showData = showObj;
                        return firstCallbackBack(null);
                    }
                });
            },
            function (thCallback) {
                let subTotal = showData.price;
                let tax = (((subTotal * sails.config.constants.stripe_pr_charge) / 100) + sails.config.constants.stripe_fix_charge).toFixed(2);
                let artistShare = (((subTotal * sails.config.constants.artist_share) / 100) + Number(tax)).toFixed(2);
                let partnerShare = ((subTotal * sails.config.constants.partner_share) / 100).toFixed(2);
                let grandTotal = (Number(subTotal) + Number(tax)).toFixed(2);

                StripeService.createCharge(req, order_type, showData, function (err, res) {
                    if (err) {
                        return thCallback({ message: err.message }, null);
                    } else {
                        orderData = res;
                        return thCallback(null);
                    }
                });
            },
            function (thCallback) {

                var post_data = { amount: orderData.total_amount, order_id: data.order_id, status: orderData.status, payment_date: orderData.createdAt, payment_type: payment_type };
                if (data.giftafriend == 1) {
                    post_data.user = userObj_tmp.id;
                } else {
                    post_data.user = userObj.id;
                }
                UserPurchaseHistory.create(post_data).exec(function (err, show) {
                    if (err) {
                        return thCallback({ message: err.message }, null);
                    } else {
                        return thCallback(null);
                    }
                });
            },
            function (frCallbackBack) {
                var book_id = generator.generate({
                    length: 10,
                    numbers: true
                });
                var booking_id = book_id.toUpperCase();
                var post_data = { show: data.show_id, user: userData.id, order_id: orderData.id, booking_id: booking_id, amount: orderData.total_amount };
                ShowTicket.create(post_data).exec(function (err, showTicket) {
                    if (err) {
                        return frCallbackBack({ message: err.message }, null);
                    } else {
                        published_date = moment(showData.publish_date).format('DD MMMM Y h:mm A') + ' ' + showData.timezone.code;
                        created_date = moment(showTicket.createdAt).format('DD MMM, Y | h:mm A') + ' ' + showData.timezone.code;
                        confirmation_no = showTicket.id;
                        var profile_picture = showData.artist.profile_picture ? sails.config.s3bucket.bucketurl + sails.config.s3bucket.userprofile + showData.artist.profile_picture : sails.config.apiUrl + '/images/user-image.png';
                        var templateData = {
                            to: userData.email,
                            from: sails.config.fromName + ' <' + sails.config.fromEmail + '>',
                            subject: sails.config.project.getAccess.subject,
                            siteName: sails.config.project.name,
                            base_url: sails.config.apiUrl,
                            site_url: sails.config.frontUrl,
                            name: userData.name,
                            show: showData,
                            subTotal: (showData.price).toFixed(2),
                            tax: orderData.tax,
                            grandTotal: orderData.total_amount,
                            tipping_amount: tipping_amount,
                            booking_id: booking_id,
                            published_date: published_date,
                            confirmation_no: confirmation_no,
                            created_date: created_date,
                            profile_picture: profile_picture,
                            curr_year: (new Date()).getFullYear()
                        };
                        EmailService.sendMail(sails.config.project.getAccess.template, templateData, function (err, message) {
                            if (err) {
                                return frCallbackBack(null);
                            } else {
                                return frCallbackBack(null);
                            }
                        });
                    }
                });
            },
        ], function (err, res) {
            if (err) {
                return callback({ message: err.message }, null);
            } else {
                return callback(null, orderData)
            }
        });
    },

    getShowAccessFinal: function (req, callback) {
        let data = req.body;
        if (data.giftafriend == 1) {
            var userObj = { id: data.touser };
            var userObj_tmp = TokenService.decode(req.headers.authorization);
            order_type = 4;
            payment_type = 3;
        } else {
            var userObj = TokenService.decode(req.headers.authorization);
            order_type = 1;
            payment_type = 2;
        }
        let userData;
        let showData;
        let tipping_amount;
        let parseBody;
        let orderData;

        async.series([
            function (firstCallbackBack) {
                ShowOrder.findOne({ show: data.show_id, order_id: data.order_id, user: userObj.id, order_type: order_type }).exec(function (err, orderObj) {
                    if (err) {
                        return firstCallbackBack({ message: err.message }, null);
                    } else {
                        if (!orderObj || orderObj == 'undefined') {
                            return callback({ message: 'Something went wrong. Please try again later.' }, null); // Stop the process  and directly return to the main function
                        } else {
                            orderData = orderObj;
                            return firstCallbackBack(null);
                        }
                    }
                });
            },
            function (firstCallbackBack) {
                ShowTicket.findOne({ show: data.show_id, order_id: orderData.id, user: userObj.id }).exec(function (err, ticketObj) {
                    if (err) {
                        return firstCallbackBack({ message: err.message }, null);
                    } else {
                        if (!ticketObj || ticketObj == 'undefined') {
                            return firstCallbackBack(null);
                        } else {
                            return callback({ message: 'Something went wrong. Please try again later.' }, null); // Stop the process  and directly return to the main function
                        }
                    }
                });
            },
            function (firstCallbackBack) {
                User.findOne({ id: userObj.id }).exec(function (err, foundUser) {
                    if (err) {
                        return firstCallbackBack({ message: err.message }, null);
                    } else {
                        userData = foundUser;
                        return firstCallbackBack(null);
                    }
                });
            },
            function (firstCallbackBack) {
                Show.findOne({ id: data.show_id }).populate(['timezone', 'artist']).exec(function (err, showObj) {
                    if (err) {
                        return firstCallbackBack({ message: err.message }, null);
                    } else {
                        showData = showObj;
                        return firstCallbackBack(null);
                    }
                });
            },
            function (secCall) {
                var postData = { grant_type: 'client_credentials' };
                var token_url = sails.config.api_url + '/v1/oauth2/token';
                var auth = "Basic " + new Buffer(sails.config.client_id + ":" + sails.config.secret_key).toString('base64');

                request.post({ url: token_url, form: postData, headers: { "Authorization": auth } }, function (err, httpResponse, paypalBody) {
                    if (err) {
                        return secCall({ message: err.message }, null);
                    }
                    else {
                        parseBody = JSON.parse(paypalBody);
                        return secCall(null);
                    }
                });
            },
            function (thCallback) {
                PaypalService.payPayPalOrder(req, parseBody, data.order_id, function (err, body) {
                    if (err) {
                        return thCallback({ message: err.message }, null);
                    } else {
                        var book_id = generator.generate({
                            length: 10,
                            numbers: true
                        });
                        var booking_id = book_id.toUpperCase();
                        var post_data = { amount: orderData.total_amount, order_id: data.order_id, status: body.status, payment_date: body.create_time, payment_type: payment_type };
                        if (data.giftafriend == 1) {
                            post_data.user = userObj_tmp.id;
                        } else {
                            post_data.user = userObj.id;
                        }
                        UserPurchaseHistory.create(post_data).exec(function (err, show) {
                        });

                        var post_data = { show: data.show_id, user: userData.id, order_id: orderData.id, booking_id: booking_id, amount: orderData.total_amount };
                        ShowTicket.create(post_data).exec(function (err, showTicket) {
                            if (err) {
                                return thCallback({ message: err.message }, null);
                            } else {
                                published_date = moment(showData.publish_date).format('DD MMMM Y h:mm A') + ' ' + showData.timezone.code;
                                created_date = moment(showTicket.createdAt).format('DD MMM, Y | h:mm A') + ' ' + showData.timezone.code;
                                confirmation_no = showTicket.id;
                                var profile_picture = showData.artist.profile_picture ? sails.config.s3bucket.bucketurl + sails.config.s3bucket.userprofile + showData.artist.profile_picture : sails.config.apiUrl + '/images/user-image.png';
                                var templateData = {
                                    to: userData.email,
                                    from: sails.config.fromName + ' <' + sails.config.fromEmail + '>',
                                    subject: sails.config.project.getAccess.subject,
                                    siteName: sails.config.project.name,
                                    base_url: sails.config.apiUrl,
                                    site_url: sails.config.frontUrl,
                                    name: userData.name,
                                    show: showData,
                                    subTotal: (showData.price).toFixed(2),
                                    tax: orderData.tax,
                                    grandTotal: orderData.total_amount,
                                    tipping_amount: tipping_amount,
                                    booking_id: booking_id,
                                    published_date: published_date,
                                    confirmation_no: confirmation_no,
                                    created_date: created_date,
                                    profile_picture: profile_picture,
                                    curr_year: (new Date()).getFullYear()
                                };
                                EmailService.sendMail(sails.config.project.getAccess.template, templateData, function (err, message) {
                                    if (err) {
                                        return thCallback(null);
                                    } else {
                                        return thCallback(null);
                                    }
                                });
                            }
                        });
                    }
                });
            }
        ], function (err, res) {
            if (err) {
                return callback({ message: err.message }, null);
            } else {
                return callback(null, orderData)
            }
        });
    },

    getShowAccess: function (req, callback) {
        let data = req.body;

        if (data.giftafriend == 1) {
            var userObj = { id: data.touser };
            order_type = 4;
        } else {
            var userObj = TokenService.decode(req.headers.authorization);
            order_type = 1;
        }
        let userData;
        let showData;
        let tipping_amount;
        let parseBody;
        let orderObj;
        async.series([
            /*function (firstCallbackBack) {
              User.findOne({id:userObj.id}).exec(function(err, foundUser){
                if (err) {
                  return firstCallbackBack({message: err.message}, null);
                } else {
                  userData = foundUser;
                  return firstCallbackBack(null);
                }
              });
            },*/
            function (firstCallbackBack) {
                Show.findOne({ id: data.show_id }).populate(['timezone', 'artist']).exec(function (err, showObj) {
                    if (err) {
                        return firstCallbackBack({ message: err.message }, null);
                    } else {
                        showData = showObj;
                        return firstCallbackBack(null);
                    }
                });
            },
            function (secCall) {
                var postData = { grant_type: 'client_credentials' };
                var token_url = sails.config.api_url + '/v1/oauth2/token';
                var auth = "Basic " + new Buffer(sails.config.client_id + ":" + sails.config.secret_key).toString('base64');

                request.post({ url: token_url, form: postData, headers: { "Authorization": auth } }, function (err, httpResponse, paypalBody) {
                    if (err) {
                        return secCall({ message: err.message }, null);
                    }
                    else {
                        parseBody = JSON.parse(paypalBody);
                        return secCall(null);
                    }
                });
            },
            function (thCallback) {
                PaypalService.createPayPalOrder(req, parseBody, showData, userObj, order_type, function (err, orderData) {
                    if (err) {
                        return thCallback({ message: err.message }, null);
                    } else {
                        orderObj = orderData;
                        return thCallback(null, orderObj);
                    }
                });
            }
        ], function (err, res) {
            if (err) {
                return callback({ message: err.message }, null);
            } else {
                return callback(null, orderObj)
            }
        });
    },

    tipToArtist: function (req, callback) {
        let data = req.body;
        var userObj = TokenService.decode(req.headers.authorization);
        let userData;
        let showData;
        let order_type;
        if (data.show_user == userObj.id) {
            return callback({ message: "You can't give tip to yourself." }, null);
        } else {
            async.series([
                function (firstCallbackBack) {
                    User.findOne({ id: userObj.id }).exec(function (err, foundUser) {
                        if (err) {
                            return firstCallbackBack({ message: err.message }, null);
                        } else {
                            userData = foundUser;
                            return firstCallbackBack(null, userData);
                        }
                    });
                },
                function (firstCallbackBack) {
                    Show.findOne({ id: data.show_id }).populate(['timezone', 'artist']).exec(function (err, showObj) {
                        if (err) {
                            return firstCallbackBack({ message: err.message }, null);
                        } else {
                            showData = showObj;
                            return firstCallbackBack(null);
                        }
                    });
                },
                function (secCall) {
                    var postData = { grant_type: 'client_credentials' };
                    var token_url = sails.config.api_url + '/v1/oauth2/token';
                    var auth = "Basic " + new Buffer(sails.config.client_id + ":" + sails.config.secret_key).toString('base64');

                    request.post({ url: token_url, form: postData, headers: { "Authorization": auth } }, function (err, httpResponse, paypalBody) {
                        if (err) {
                            return secCall({ message: err.message }, null);
                        }
                        else {
                            parseBody = JSON.parse(paypalBody);
                            return secCall(null);
                        }
                    });
                },
                function (thCallback) {
                    if (userData) {
                        showData.tipping_amount = data.tipping_amount;
                        if (data.tip_type == 1) {
                            order_type = 2;
                        } else if (data.tip_type == 2) {
                            order_type = 3;
                        }
                        PaypalService.createPayPalOrder(req, parseBody, showData, userObj, order_type, function (err, orderData) {
                            if (err) {
                                return thCallback({ message: err.message }, null);
                            } else {
                                orderObj = orderData;
                                return thCallback(null, orderObj);
                            }
                        });
                    } else {
                        return thCallback({ message: 'This user does not exist!' }, null);
                    }
                },
            ], function (err, res) {
                if (err) {
                    return callback({ message: err.message }, null);
                } else {
                    return callback(null, orderObj)
                }
            });
        }
    },

    tipToArtistStripe: function (req, callback) {
        let data = req.body;
        var userObj = TokenService.decode(req.headers.authorization);
        let showData;
        let orderData;
        let orderObj;
        let artistTipData;
        let order_type;
        if (data.tip_type == 1) {
            order_type = 2;
        } else if (data.tip_type == 2) {
            order_type = 3;
        }

        async.series([
            function (thCallbackBack) {
                Show.findOne({ id: data.show_id }).populate(['timezone', 'artist']).exec(function (err, showObj) {
                    if (err) {
                        return thCallbackBack({ message: err.message }, null);
                    } else {
                        showData = showObj;
                        if (showData.artist.id == userObj.id) {
                            return callback({ message: "You can't give tip to yourself." }, null);
                        } else {
                            return thCallbackBack(null);
                        }
                    }
                });
            },
            function (thCallback) {
                // let subTotal = showData.price;
                // let tax = (((subTotal*sails.config.constants.stripe_pr_charge)/100)+sails.config.constants.stripe_fix_charge).toFixed(2);
                // let artistShare = (((subTotal*sails.config.constants.artist_share)/100) + Number(tax)).toFixed(2);
                // let partnerShare = ((subTotal*sails.config.constants.partner_share)/100).toFixed(2);
                // let grandTotal = (Number(subTotal) + Number(tax)).toFixed(2);
                showData.tipping_amount = data.tipping_amount;
                StripeService.createCharge(req, order_type, showData, function (err, res) {
                    if (err) {
                        return thCallback({ message: err.message }, null);
                    } else {
                        orderObj = res;
                        return thCallback(null);
                    }
                });
            },
            function (firstCallbackBack) {
                ShowOrder.findOne({ id: orderObj.id }).populate('emoji').exec(function (err, resOrder) {
                    if (err) {
                        return firstCallbackBack({ message: err.message }, null);
                    } else {
                        orderData = resOrder;
                        return firstCallbackBack(null);
                    }
                });
            },
            function (thCallback) {
                var post_data = { user: userObj.id, amount: orderData.total_amount, order_id: data.order_id, status: orderData.status, payment_date: orderData.create_time, payment_type: 1 };
                UserPurchaseHistory.create(post_data).exec(function (err, show) {
                    if (err) {
                        return thCallback({ message: err.message }, null);
                    } else {
                        return thCallback(null);
                    }
                });
            },
            function (fifthCallback) {
                var post_data = { artist: showData.artist.id, show: data.show_id, order_id: orderData.id, tip_type: data.tip_type, user: userObj.id, amount: orderData.total_amount };
                if (data.tip_type == 2) {
                    post_data.emoji_id = orderData.emoji.id;
                }
                ArtistTip.create(post_data).exec(function (err, artistTipObj) {
                    if (err) {
                        return fifthCallback({ message: err.message }, null);
                    } else {
                        return fifthCallback(null);
                    }
                });
            },
        ], function (err, res) {
            if (err) {
                return callback({ message: err.message }, null);
            } else {
                return callback(null, orderData)
            }
        });
    },

    tipToArtistFinal: function (req, callback) {
        let data = req.body;
        var userObj = TokenService.decode(req.headers.authorization);
        let showData;
        let orderData;
        let artistTipData;
        let order_type;
        if (data.tip_type == 1) {
            order_type = 2;
        } else if (data.tip_type == 2) {
            order_type = 3;
        }

        async.series([
            function (thCallbackBack) {
                Show.findOne({ id: data.show_id }).populate(['timezone', 'artist']).exec(function (err, showObj) {
                    if (err) {
                        return thCallbackBack({ message: err.message }, null);
                    } else {
                        showData = showObj;
                        if (showData.artist.id == userObj.id) {
                            return callback({ message: "You can't give tip to yourself." }, null);
                        } else {
                            return thCallbackBack(null);
                        }
                    }
                });
            },
            function (firstCallbackBack) {
                ShowOrder.findOne({ show: data.show_id, order_id: data.order_id, user: userObj.id, order_type: order_type }).populate('emoji').exec(function (err, orderObj) {
                    if (err) {
                        return firstCallbackBack({ message: err.message }, null);
                    } else {
                        if (!orderObj || orderObj == 'undefined') {
                            callback({ message: "Something went wrong. Please try again later." }); // Stop the process  and directly return to the main function
                        } else {
                            orderData = orderObj;
                            return firstCallbackBack(null);
                        }
                    }
                });
            },
            function (secCall) {
                ArtistTip.findOne({ show: data.show_id, order_id: orderData.id, user: userObj.id }).exec(function (err, tipObj) {
                    if (err) {
                        return secCall({ message: err.message }, null);
                    } else {
                        if (!tipObj || tipObj == 'undefined') {
                            return secCall(null);
                        } else {
                            callback({ message: "Something went wrong. Please try again later." }); // Stop the process  and directly return to the main function
                        }
                    }
                });
            },

            function (frCall) {
                var postData = { grant_type: 'client_credentials' };
                var token_url = sails.config.api_url + '/v1/oauth2/token';
                var auth = "Basic " + new Buffer(sails.config.client_id + ":" + sails.config.secret_key).toString('base64');

                request.post({ url: token_url, form: postData, headers: { "Authorization": auth } }, function (err, httpResponse, paypalBody) {
                    if (err) {
                        return frCall({ message: err.message }, null);
                    }
                    else {
                        parseBody = JSON.parse(paypalBody);
                        return frCall(null);
                    }
                });
            },
            function (fifthCallback) {

                PaypalService.payPayPalOrder(req, parseBody, data.order_id, function (err, body) {
                    if (err) {
                        return fifthCallback({ message: err.message }, null);
                    } else {

                        var post_data = { user: userObj.id, amount: orderData.total_amount, order_id: data.order_id, status: body.status, payment_date: body.create_time, payment_type: 1 };
                        UserPurchaseHistory.create(post_data).exec(function (err, show) {

                        });

                        var post_data = { artist: showData.artist.id, show: data.show_id, order_id: orderData.id, tip_type: data.tip_type, user: userObj.id, amount: orderData.total_amount };
                        if (data.tip_type == 2) {
                            post_data.emoji_id = orderData.emoji.id;
                        }
                        ArtistTip.create(post_data).exec(function (err, artistTipObj) {
                            if (err) {
                                return fifthCallback({ message: err.message }, null);
                            } else {
                                /*ShowOrder.findOne({id:data.order_id}).populate('emoji').exec(function(err, resArtistTip){
                                    if (err) {
                                        return fifthCallback({message: err.message}, null);
                                    } else {
                                        artistTipData = resArtistTip;
                                        return fifthCallback(null);
                                    }
                                });*/
                                return fifthCallback(null);
                            }
                        });
                    }
                });
            }
        ], function (err, res) {
            if (err) {
                return callback({ message: err.message }, null);
            } else {
                return callback(null, orderData)
            }
        });
    },

    tipEmojiToArtist_OLD: function (req, callback) {
        let data = req.body;
        var userObj = TokenService.decode(req.headers.authorization);
        let userData;
        let toUser;
        if (data.show_user == userObj.id) {
            return callback({ message: "You can't give tip to yourself." }, null);
        } else {
            async.series([
                function (firstCallbackBack) {
                    User.findOne({ id: data.show_user }).exec(function (err, foundUser) {
                        if (err) {
                            return firstCallbackBack({ message: err.message }, null);
                        } else {
                            toUser = foundUser;
                            return firstCallbackBack(null, userData);
                        }
                    });
                },
                function (firstCallbackBack) {
                    User.findOne({ id: userObj.id }).exec(function (err, foundUser) {
                        if (err) {
                            return firstCallbackBack({ message: err.message }, null);
                        } else {
                            userData = foundUser;
                            return firstCallbackBack(null, userData);
                        }
                    });
                },
                function (secCall) {
                    if (userData) {
                        if (data.tipping_amount > userData.balance) {
                            return secCall({ message: 'You do not have sufficient balance to tip.' }, null);
                        } else {
                            // let balance_data = {user: data.show_user, amount: data.tipping_amount, balance_type: 1, description: 'Get emoji tip from user'};
                            // ArtistBalance.create(balance_data).exec(function(err, artistbalancehistory){
                            //   if (err) {
                            //     return secCall({message: err.message}, null);
                            //   } else {
                            let balancedata = {
                                user: userData.id,
                                amount: data.tipping_amount,
                                balance_type: 2,
                                current_balance: Number(userData.balance) - Number(data.tipping_amount),
                                description: 'Tip emoji to artist'
                            };
                            ArtistBalance.create(balancedata).exec(function (err, artistbalancehistory) {
                                if (err) {
                                    return secCall({ message: err.message }, null);
                                } else {
                                    return secCall(null);
                                }
                            });
                            //}
                            //});
                        }

                    } else {
                        return secCall({ message: 'This user does not exist!' }, null);
                    }
                },
                function (thCallback) {
                    var post_data = { artist: toUser.id, show: data.show_id, emoji_id: data.emoji_id, tip_type: data.tip_type, user: userData.id, amount: data.tipping_amount };
                    ArtistTip.create(post_data).exec(function (err, resUser) {
                        if (err) {
                            return thCallback({ message: err.message }, null);
                        } else {
                            return thCallback(null);
                        }
                    });
                },
                function (frCallback) {
                    User.update({ id: userData.id }, { balance: Number(userData.balance) - Number(data.tipping_amount) }).exec(function (err, resUser) {
                        if (err) {
                            return frCallback({ message: err.message }, null);
                        } else {

                            // User.update({id: data.show_user}, {balance: Number(toUser.balance) + Number(data.tipping_amount)}).exec(function(err, resUser){
                            //   if (err) {
                            //     return frCallback({message: err.message}, null);
                            //   } else {
                            //     return frCallback(null);
                            //   }
                            // });
                            return frCallback(null);
                        }
                    });
                }
            ], function (err, res) {
                if (err) {
                    return callback({ message: err.message }, null);
                } else {
                    return callback(null)
                }
            });
        }
    },

    tipToArtist_OLD: function (req, callback) {
        let data = req.body;
        var userObj = TokenService.decode(req.headers.authorization);
        let userData;
        let toUser;
        if (data.show_user == userObj.id) {
            return callback({ message: "You can't give tip to yourself." }, null);
        } else {
            async.series([
                function (firstCallbackBack) {
                    User.findOne({ id: data.show_user }).exec(function (err, foundUser) {
                        if (err) {
                            return firstCallbackBack({ message: err.message }, null);
                        } else {
                            toUser = foundUser;
                            return firstCallbackBack(null, userData);
                        }
                    });
                },
                function (firstCallbackBack) {
                    User.findOne({ id: userObj.id }).exec(function (err, foundUser) {
                        if (err) {
                            return firstCallbackBack({ message: err.message }, null);
                        } else {
                            userData = foundUser;
                            return firstCallbackBack(null, userData);
                        }
                    });
                },
                function (secCall) {
                    if (userData) {
                        if (data.tipping_amount > userData.balance) {
                            return secCall({ message: 'You do not have sufficient balance to tip.' }, null);
                        } else {
                            // let balance_data = {
                            //     user: data.show_user,
                            //     amount: data.tipping_amount,
                            //     balance_type: 1,
                            //     current_balance: Number(toUser.balance) + Number(data.tipping_amount),
                            //     description: 'Get tip from user'
                            // };
                            // ArtistBalance.create(balance_data).exec(function(err, artistbalancehistory){
                            //     if (err) {
                            //         return secCall({message: err.message}, null);
                            //     } else {
                            let balancedata = {
                                user: userData.id,
                                amount: data.tipping_amount,
                                balance_type: 2,
                                current_balance: Number(userData.balance) - Number(data.tipping_amount),
                                description: 'Tip to artist'
                            };
                            ArtistBalance.create(balancedata).exec(function (err, artistbalancehistory) {
                                if (err) {
                                    return secCall({ message: err.message }, null);
                                } else {
                                    return secCall(null);
                                }
                            });
                            //     }
                            // });
                        }

                    } else {
                        return secCall({ message: 'This user does not exist!' }, null);
                    }
                },
                function (thCallback) {
                    var post_data = { artist: toUser.id, show: data.show_id, emoji_id: data.emoji_id, tip_type: data.tip_type, user: userData.id, amount: data.tipping_amount };
                    ArtistTip.create(post_data).exec(function (err, resUser) {
                        if (err) {
                            return thCallback({ message: err.message }, null);
                        } else {
                            return thCallback(null);
                        }
                    });
                },
                function (frCallback) {
                    User.update({ id: userData.id }, { balance: Number(userData.balance) - Number(data.tipping_amount) }).exec(function (err, resUser) {
                        if (err) {
                            return frCallback({ message: err.message }, null);
                        } else {

                            // [0035 Artist will get amount of money instead of tip]
                            // User.update({id: data.show_user}, {balance: Number(toUser.balance) + Number(data.tipping_amount)}).exec(function(err, resUser){
                            //   if (err) {
                            //     return frCallback({message: err.message}, null);
                            //   } else {
                            //     return frCallback(null);
                            //   }
                            // });
                            return frCallback(null);
                        }
                    });
                }
            ], function (err, res) {
                if (err) {
                    return callback({ message: err.message }, null);
                } else {
                    return callback(null)
                }
            });
        }
    },

    tipToArtistByAdmin: function (req, callback) {
        let data = req.body;
        var userObj = TokenService.decode(req.headers.authorization);
        let toUser;

        async.series([
            function (firstCallbackBack) {
                User.findOne({ id: data.show_user }).exec(function (err, foundUser) {
                    if (err) {
                        return firstCallbackBack({ message: err.message }, null);
                    } else {
                        toUser = foundUser;
                        return firstCallbackBack(null, foundUser);
                    }
                });
            },
            function (secCall) {
                let balance_data = {
                    user: data.show_user,
                    amount: data.tipping_amount,
                    balance_type: 1,
                    current_balance: Number(toUser.balance) + Number(data.tipping_amount),
                    description: 'Get tip from Admin'
                };
                ArtistBalance.create(balance_data).exec(function (err, artistbalancehistory) {
                    if (err) {
                        return secCall({ message: err.message }, null);
                    } else {
                        return secCall(null);
                    }
                });
            },
            function (thCallback) {
                var post_data = { artist: toUser.id, emoji_id: data.emoji_id, tip_type: data.tip_type, user: userObj.id, amount: data.tipping_amount };
                ArtistTip.create(post_data).exec(function (err, resUser) {
                    if (err) {
                        return thCallback({ message: err.message }, null);
                    } else {
                        return thCallback(null);
                    }
                });
            },
            function (frCallback) {
                User.update({ id: data.show_user }, { balance: Number(toUser.balance) + Number(data.tipping_amount) }).exec(function (err, resUser) {
                    if (err) {
                        return frCallback({ message: err.message }, null);
                    } else {
                        return frCallback(null);
                    }
                });
            }
        ], function (err, res) {
            if (err) {
                return callback({ message: err.message }, null);
            } else {
                return callback(null)
            }
        });
    },

    checkTicketStatus: function (req, callback) {
        let data = req.body;
        let show_id = req.param('id');
        let ticket_count;
        let showData;
        async.series([
            function (firstCallbackBack) {
                Show.findOne({ id: show_id }).exec(function (err, foundShow) {
                    if (err) {
                        return firstCallbackBack({ message: err.message }, null);
                    } else {
                        showData = foundShow;
                        return firstCallbackBack(null);
                    }
                });
            },
            function (firstCallbackBack) {
                ShowTicket.count({ show: show_id }).exec(function (err, count) {
                    if (err) {
                        return firstCallbackBack({ message: err.message }, null);
                    } else {
                        ticket_count = count;
                        return firstCallbackBack(null);
                    }
                });
            }
        ], function (err, res) {
            if (err) {
                return callback({ message: err.message }, null);
            } else {
                var resData = { sold_out: true };
                if (showData.audiance_capacity > ticket_count) {
                    resData.sold_out = false;
                }
                return callback(null, resData)
            }
        });
    },

    checkShowOwner: function (req, callback) {
        var userObj = TokenService.decode(req.headers.authorization);
        Show.findOne({ artist: userObj.id, id: req.body.show_id }).exec(function (err, foundShow) {
            if (err) {
                return callback({ message: err.message }, null);
            } else {
                if (foundShow && foundShow != 'undefined') {
                    return callback(null, true);
                } else {
                    return callback(null, false);
                }
            }
        });
    },


    /*========== backend apis ==========*/
    fetchData: function (data, criteria, callback) {
        let deleteUser = [0];
        let resData;
        async.series([
            function (callback1) {
                UserService.deleteuserID(data, function (err, resData) {
                    if (err)
                        return callback1(err, null);
                    else {
                        if (resData.length > 0) {
                            deleteUser = resData;
                        }
                        return callback1(null, deleteUser);
                    }
                });
            },
            function (callback2) {
                Show.find(criteria).where({ artist: { '!': deleteUser } }).populate(['artist', 'showtickets']).sort('createdAt Desc').exec(function (err, foundShows) {
                    if (err) {
                        return callback2({ message: err.message }, null);
                    } else {
                        resData = foundShows;
                        return callback2(null, foundShows);
                    }
                });
            }
        ], function (err, res) {
            if (err) {
                return callback({ message: err.message }, null);
            } else {
                return callback(null, resData);
            }
        });

    },

    getShow: function (data, criteria, callback) {
        // var algorithm = 'aes256';
        // var key = 'password';
        var algorithm = sails.config.constants.algorithm;
        var key = sails.config.constants.key;
        Show.find(criteria).populate(['genres', 'tipprizes']).sort('createdAt Desc').exec(function (err, foundShows) {
            if (err) {
                return callback({ message: err.message }, null);
            } else {
                if (foundShows[0].is_password == true) {
                    var text = foundShows[0].password;
                    if (text != '' && text != null) {
                        var decipher = crypto.createDecipher(algorithm, key);
                        var decrypted = decipher.update(text, 'hex', 'utf8') + decipher.final('utf8');
                        foundShows[0].password = decrypted;
                    }
                }
                return callback(null, foundShows);
            }
        });
    },

    getShowDetails: function (data, criteria, callback) {
        // var algorithm = 'aes256';
        // var key = 'password';
        var algorithm = sails.config.constants.algorithm;
        var key = sails.config.constants.key;
        Show.find(criteria).populate(['genres', 'artist', 'classification_category', 'timezone', 'showlist']).sort('createdAt Desc').exec(function (err, foundShows) {
            if (err) {
                return callback({ message: err.message }, null);
            } else {
                if (foundShows[0].is_password == true) {
                    var text = foundShows[0].password;
                    if (text != '' && text != null) {
                        var decipher = crypto.createDecipher(algorithm, key);
                        var decrypted = decipher.update(text, 'hex', 'utf8') + decipher.final('utf8');
                        foundShows[0].password = decrypted;
                    }
                }
                return callback(null, foundShows);
            }
        });
    },

    getShowData: function (data, criteria, callback) {
        var userData = TokenService.decode(data.headers.authorization);
        // var algorithm = 'aes256';
        // var key = 'password';
        var algorithm = sails.config.constants.algorithm;
        var key = sails.config.constants.key;
        Show.findOne(criteria).populate(['genres', 'artist', 'classification_category', 'timezone', 'showlist', 'tipprizes']).sort('createdAt Desc').exec(function (err, showObj) {
            if (err) {
                return callback({ message: err.message }, null);
            } else {
                if (userData.id == showObj.artist.id) {
                    if (showObj.is_password == true) {
                        var text = showObj.password;
                        if (text != '' && text != null) {
                            var decipher = crypto.createDecipher(algorithm, key);
                            var decrypted = decipher.update(text, 'hex', 'utf8') + decipher.final('utf8');
                            showObj.password = decrypted;
                        }
                    }
                    return callback(null, showObj);
                } else {
                    return callback({ message: 'You do not have a access to review this show.' }, null);
                }
            }
        });
    },

    updateShow: function (req, updateArr, criteria, callback) {
        let dataObj;
        let uploadFile = '';
        let zone_name = '';
        // var algorithm = 'aes256';
        // var key = 'password';
        var algorithm = sails.config.constants.algorithm;
        var key = sails.config.constants.key;
        var text = updateArr.password;

        async.series([
            function (firstCallback) {
                Show.findOne(criteria).exec(function (err, foundData) {
                    if (err)
                        return firstCallback(err, null);
                    else {
                        dataObj = foundData;
                        return firstCallback(null);
                    }
                });
            },
            function (cBack) {
                if (typeof req._fileparser != 'undefined' && req._fileparser.upstreams.length > 0) {
                    var filename = req.file('media_name')._files[0].stream.filename;
                    var i = filename.lastIndexOf('.');
                    var ext = filename.substr(i);
                    uploadFile = new moment().valueOf() + ext;
                    UploadimageService.uploadVideo(req, "media_name", sails.config.s3bucket.showmedia, uploadFile, function (err, video) {
                        if (err) {
                            return cBack(err.message, null);
                        }
                        else {
                            updateArr.media_name = uploadFile;
                            return cBack(null);
                        }
                    });
                }
                else {
                    return cBack(null);
                }
            },
            function (removeCall) {
                if (dataObj) {
                    ShowGenre.destroy({ show: req.param('id') }).exec(function (err, resData) {
                        if (err)
                            return removeCall({ message: err.message }, null);
                        else {
                            return removeCall(null, resData);
                        }
                    });
                }
                else {
                    return removeCall(null, {});
                }
            },
            function (removeTipCall) {
                if (dataObj) {
                    ShowTipPrize.destroy({ show: req.param('id') }).exec(function (err, resData) {
                        if (err)
                            return removeTipCall({ message: err.message }, null);
                        else {
                            return removeTipCall(null, resData);
                        }
                    });
                }
                else {
                    return removeTipCall(null, {});
                }
            },
            function (midCall) {
                Timezone.findOne({ id: updateArr.timezone }).exec(function (err, resZone) {
                    if (err) {
                        return midCall({ message: err.message }, null);
                    } else {
                        zone_name = resZone.zone_name;
                        return midCall(null, resZone);
                    }
                });
            },
            function (secCall) {
                if (dataObj) {
                    var fmt = 'YYYY-MM-DD HH:mm:00';
                    let date_picker = updateArr.publish_date.split(' ');
                    let new_date = date_picker[0] + ' ' + date_picker[1] + ' ' + date_picker[2] + ' ' + date_picker[3] + ' ' + date_picker[4];
                    var p_date = moment(new_date).format(fmt);
                    updateArr.publish_date = p_date;
                    updateArr.publish_date_utc = moment_tz.tz(p_date, fmt, zone_name).utc().format(fmt);
                    show_end_date = moment(updateArr.publish_date_utc).add(updateArr.duration_hour, 'hours').format(fmt);
                    updateArr.show_end_date = moment(show_end_date).add(updateArr.duration_minute, 'm').format(fmt);
                    if (updateArr.is_password == 'true') {
                        if (text != '' && text != null) {
                            var cipher = crypto.createCipher(algorithm, key);
                            var encrypted = cipher.update(text, 'utf8', 'hex') + cipher.final('hex');
                            updateArr.password = encrypted;
                        }
                    } else {
                        updateArr.password = null;
                    }

                    Show.update(criteria, updateArr).exec(function (err, show) {
                        if (err) {
                            return secCall({ message: err.message }, null);
                        } else {
                            genreArray = req.body.genre.split(',');
                            let pc = 0;
                            showObj = show[0];
                            _.each(genreArray, function (genre) {
                                ShowGenre.create({ 'show': req.param('id'), 'genre': genre }).exec(function (err, showgenre) {
                                    pc++;
                                    if (pc == genreArray.length) {
                                        return secCall(null, show);
                                    }
                                });
                            });
                        }
                    });
                }
                else {
                    return secCall({ message: "Show not found." }, null);
                }
            },
            function (lastCall) {
                tipprizeArray = req.body.tipprize.split(',');
                let pc = 0;
                _.each(tipprizeArray, function (tipprize) {
                    ShowTipPrize.create({ 'show': req.param('id'), 'tipprize': tipprize }).exec(function (err, showtipprize) {
                        pc++;
                        if (pc == tipprizeArray.length) {
                            return lastCall(null);
                        }
                    });
                });
            },
        ], function (err, data) {
            if (err) {
                return callback({ message: err.message }, null);
            } else {
                return callback(null, showObj);
            }
        });
    },

    updateShowStatus: function (req, updateArr, criteria, callback) {
        Show.update(criteria, updateArr).exec(function (err, resObj) {
            if (err)
                return callback({ message: err.message }, null);
            else {
                return callback(null, resObj);
            }
        });
    },

    updateShowBulk: function (req, updateArr, criteria, callback) {
        let dataObj;
        async.series([
            function (firstCallback) {
                Show.findOne(criteria).exec(function (err, foundData) {
                    if (err)
                        return firstCallback(err, null);
                    else {
                        dataObj = foundData;
                        return firstCallback(null);
                    }
                });
            },
            function (secCallback) {
                if (dataObj) {
                    Show.update(criteria, updateArr).exec(function (err, resObj) {
                        if (err)
                            return secCallback({ message: err.message }, null);
                        else {
                            showObj = resObj;
                            return secCallback(null, resObj);
                        }
                    });
                }
                else {
                    return secCallback({ message: "Show not found." }, null);
                }
            }
        ], function (err, data) {
            if (err) {
                return callback({ message: err.message }, null);
            } else {
                return callback(null, showObj);
            }
        });
    },

    deleteShow: function (req, criteria, callback) {
        Show.destroy(criteria).exec(function (err, resData) {
            if (err) {
                return callback({ message: err.message }, null);
            }
            else {
                return callback(null, resData);
            }
        });
    },

    getActiveClassificationCategory: function (data, criteria, callback) {
        ClassificationCategory.find(criteria).sort('createdAt Desc').exec(function (err, foundCategories) {
            if (err) {
                return callback({ message: err.message }, null);
            } else {
                return callback(null, foundCategories);
            }
        });
    },

    getUserShows: function (data, criteria, callback) {
        Show.find(criteria).sort('createdAt Desc').exec(function (err, foundShows) {
            if (err) {
                return callback({ message: err.message }, null);
            } else {
                return callback(null, foundShows);
            }
        });
    },

    getShowSongList: function (data, criteria, callback) {
        ShowList.find(criteria).sort('createdAt Asc').exec(function (err, foundShows) {
            if (err) {
                return callback({ message: err.message }, null);
            } else {
                return callback(null, foundShows);
            }
        });
    },

    createShowSong: function (data, callback) {
        ShowList.create(data.body).exec(function (err, song) {
            if (err) {
                return callback({ message: err.message }, null);
            } else {
                ShowList.find({ show: song.show }).sort('createdAt Asc').exec(function (err, foundShows) {
                    if (err) {
                        return callback({ message: err.message }, null);
                    } else {
                        return callback(null, foundShows);
                    }
                });
            }
        });
    },

    deleteShowSong: function (req, criteria, callback) {
        ShowList.destroy(criteria).exec(function (err, resData) {
            if (err)
                return callback({ message: err.message }, null);
            else {
                return callback(null, resData);
            }
        });
    },

    finalizeList: function (data, criteria, callback) {
        var user = TokenService.decode(data.headers.authorization);
        let showlist = [];
        let userData;
        let show;
        let showlist_str = '';
        let shows;
        let str = '';
        async.series([
            function (firstCallback) {
                data.body.show = data.body.show_id;
                SetList.create(data.body).exec(function (err, setList) {
                    if (err)
                        return firstCallback(err, null);
                    else {
                        return firstCallback(null);
                    }
                });
            },
            function (firstCallback) {
                ShowList.find(criteria).exec(function (err, foundSong) {
                    if (err)
                        return firstCallback(err, null);
                    else {
                        showlist = foundSong;
                        return firstCallback(null);
                    }
                });
            },
            function (secondCallback) {
                User.find({ id: user.id }).exec(function (err, foundUser) {
                    if (err)
                        return secondCallback(err, null);
                    else {
                        userData = foundUser[0];
                        return secondCallback(null);
                    }
                });
            },
            function (thCallback) {
                Show.findOne({ id: criteria.show }).populate(['timezone', 'setlist']).exec(function (err, foundShow) {
                    if (err)
                        return thCallback(err, null);
                    else {
                        show = foundShow;
                        return thCallback(null);
                    }
                });
            },
            function (listCallback) {
                if (showlist) {
                    async.forEachOf(showlist, function (indvShw, keyShw, eachCallback) {
                        if (indvShw.song_name && indvShw.song_writers) {
                            str = str + '<tr>\
                <td align="left" style="padding:10px; color:#000; font-size:14px; font-weight:400; font-family:Arial, Helvetica, sans-serif; border-bottom:1px solid #ddd; border-right:1px solid #ddd;">'+ indvShw.song_name + '</td>\
                <td align="left" style="padding:10px; color:#000; font-size:14px; font-weight:400; font-family:Arial, Helvetica, sans-serif; border-bottom:1px solid #ddd; border-right:1px solid #ddd;">'+ indvShw.song_writers + '</td>\
                <td align="left" style="padding:10px; color:#000; font-size:14px; font-weight:400; font-family:Arial, Helvetica, sans-serif; border-bottom:1px solid #ddd; border-right:1px solid #ddd;">'+ indvShw.ori_perfomer + '</td>\
                <td align="left" style="padding:10px; color:#000; font-size:14px; font-weight:400; font-family:Arial, Helvetica, sans-serif; border-bottom:1px solid #ddd; border-right:1px solid #ddd;">'+ indvShw.record_label + '</td>\
                <td align="left" style="padding:10px; color:#000; font-size:14px; font-weight:400; font-family:Arial, Helvetica, sans-serif; border-bottom:1px solid #ddd;">'+ indvShw.duration_minute + ':' + indvShw.duration_second + '</td>\
              </tr>';
                        }
                        return eachCallback(null);
                    }, function (err) {
                        if (err)
                            return listCallback(err, null);
                        else
                            showlist_str = str;
                        return listCallback(null, showlist);
                    });
                } else {
                    return listCallback(null, showlist);
                }
            },
            function (MailCallback) {
                if (userData && showlist) {
                    //send mail to admin for finalize song list
                    // var dateUtc = moment.utc(show.publish_date_utc);
                    // var localDate = moment(dateUtc).local();
                    // var published_date = localDate.format("DD MMMM Y h:mm A");
                    var show_url = show.setlist[0].show_url;
                    var licence = show.setlist[0].licence;
                    var qtr_end = show.setlist[0].qtr_end;
                    var music_artist = show.setlist[0].music_artist;

                    published_date = moment(show.publish_date).format('DD MMMM Y h:mm A') + ' ' + show.timezone.code;
                    var profile_picture = userData.profile_picture ? sails.config.s3bucket.bucketurl + sails.config.s3bucket.userprofile + userData.profile_picture : sails.config.apiUrl + '/images/user-image.png';

                    var templateData = {
                        to: sails.config.setListEmail,
                        from: userData.email,
                        subject: sails.config.project.finalizeList.subject,
                        userEmail: sails.config.setListEmail,
                        siteName: sails.config.project.name,
                        showlist_str: showlist_str,
                        show: show,
                        show_url: show_url,
                        licence: licence,
                        qtr_end: qtr_end,
                        music_artist: music_artist,
                        published_date: published_date,
                        user: userData,
                        profile_picture: profile_picture,
                        base_url: sails.config.apiUrl,
                        name: userData.name,
                        curr_year: (new Date()).getFullYear()
                    };
                    EmailService.sendMail(sails.config.project.finalizeList.template, templateData, function (err, message) {
                        if (err) {
                            return MailCallback({ message: err.message }, null);
                        } else {
                            Show.update({ id: criteria.show }, { is_finalize_list: 1 }).exec(function (err, ShowUpdate) {
                                if (err)
                                    return MailCallback(err, null);
                                else {
                                    return MailCallback(null, {});
                                }
                            });
                        }
                    });
                } else {
                    return MailCallback(null, {});
                }
            },
            function (finalCallback) {
                //let currDate = new Date();
                let currDate = moment_tz().utc().format('YYYY-MM-DD HH:mm:ss');
                criteria = { artist: user.id, status: 'Published', show_end_date: { '>': currDate }, or: [{ is_finalize_list: null }, { is_finalize_list: { not: 1 } }] };
                Show.find(criteria).sort('createdAt Desc').exec(function (err, foundShows) {
                    if (err) {
                        return finalCallback({ message: err.message }, null);
                    } else {
                        shows = foundShows;
                        return finalCallback(null, foundShows);
                    }
                });
            },
        ], function (err, data) {
            if (err) {
                return callback({ message: err.message }, null);
            } else {
                return callback(null, shows);
            }
        });
    },

    sendShowEmailSms: function (req, callback) {
        var id = req.param('id');
        var hashids = new Hashids(sails.config.project.name, 10);
        let showId = hashids.decode(id);
        let show;
        var followers = [];
        let published_date;
        // var algorithm = 'aes256';
        // var key = 'password';
        var algorithm = sails.config.constants.algorithm;
        var key = sails.config.constants.key;
        async.series([
            function (firstCallback) {
                Show.findOne({ id: showId }).populate(['artist', 'timezone']).exec(function (err, foundShow) {
                    if (err)
                        return firstCallback(err, null);
                    else {
                        show = foundShow;
                        if (show.is_password == true) {
                            var text = show.password;

                            if (text != '' && text != null) {
                                var decipher = crypto.createDecipher(algorithm, key);
                                var decrypted = decipher.update(text, 'hex', 'utf8') + decipher.final('utf8');
                                show.password = decrypted;
                            }
                        } else {
                            show.password = '-';
                        }
                        return firstCallback(null);
                    }
                });
            },
            function (MailCallback) {
                if (show) {
                    //send mail to artist for show
                    published_date = moment(show.publish_date).format('DD MMMM Y h:mm A') + ' ' + show.timezone.code;
                    var profile_picture = show.artist.profile_picture ? sails.config.s3bucket.bucketurl + sails.config.s3bucket.userprofile + show.artist.profile_picture : sails.config.apiUrl + '/images/user-image.png';

                    var templateData = {
                        to: show.artist.email,
                        from: sails.config.fromName + ' <' + sails.config.fromEmail + '>',
                        subject: sails.config.project.artistShow.subject,
                        userEmail: show.artist.email,
                        siteName: sails.config.project.name,
                        show: show,
                        published_date: published_date,
                        profile_picture: profile_picture,
                        base_url: sails.config.apiUrl,
                        site_url: sails.config.frontUrl,
                        name: show.artist.name,
                        session_password: show.password,
                        wowza_RTMP: sails.config.wowza_server_ip,
                        wowza_port: sails.config.wowza_server_port,
                        wowza_applicatoion: sails.config.wowza_application,
                        wowza_stream_name: show.stream_name,
                        wowza_rtmp_url: 'rtmp://' + sails.config.wowza_server_ip + ':' + sails.config.wowza_server_port + '/' + sails.config.wowza_application,
                        curr_year: (new Date()).getFullYear()
                    };
                    EmailService.sendMail(sails.config.project.artistShow.template, templateData, function (err, message) {
                        if (err) {
                            return MailCallback({ message: err.message }, null);
                        } else {
                            return MailCallback(null, {});
                        }
                    });
                } else {
                    return MailCallback(null, {});
                }
            },
            function (adminMailCallback) {
                if (show) {
                    //send mail to Admin about new show
                    published_date = moment(show.publish_date).format('DD MMMM Y h:mm A') + ' ' + show.timezone.code;
                    var profile_picture = show.artist.profile_picture ? sails.config.s3bucket.bucketurl + sails.config.s3bucket.userprofile + show.artist.profile_picture : sails.config.apiUrl + '/images/user-image.png';
                    //var artist_type = (show.artist.artist_type == 1) ? 'Emerging Artist' : 'Established Artist';
                    var templateData = {
                        to: sails.config.adminEmail,
                        from: sails.config.fromName + ' <' + sails.config.fromEmail + '>',
                        subject: sails.config.project.adminShow.subject,
                        userEmail: show.artist.email,
                        siteName: sails.config.project.name,
                        show: show,
                        //artist_type: artist_type,
                        published_date: published_date,
                        profile_picture: profile_picture,
                        base_url: sails.config.apiUrl,
                        site_url: sails.config.frontUrl,
                        name: show.artist.name,
                        curr_year: (new Date()).getFullYear()
                    };
                    EmailService.sendMail(sails.config.project.adminShow.template, templateData, function (err, message) {
                        if (err) {
                            return adminMailCallback({ message: err.message }, null);
                        } else {
                            return adminMailCallback(null, {});
                        }
                    });
                } else {
                    return adminMailCallback(null, {});
                }
            },
            function (followersCallback) {
                Artistfollowers.find({ artist: show.artist.id }).populate(['follower']).sort('id Asc').exec(function (err, foundUsers) {
                    if (err)
                        return followersCallback(err, null);
                    else {
                        followers = foundUsers;
                        return followersCallback(null);
                    }
                });
            },
            function (secCallback) {
                if (followers) {
                    async.forEachOf(followers, function (indvUsr, keyUsr, eachCallback) {

                        var profile_picture = show.artist.profile_picture ? sails.config.s3bucket.bucketurl + sails.config.s3bucket.userprofile + show.artist.profile_picture : sails.config.apiUrl + '/images/user-image.png';

                        var templateData = {
                            to: indvUsr.follower.email,
                            from: sails.config.fromName + ' <' + sails.config.fromEmail + '>',
                            subject: sails.config.project.followerShow.subject,
                            userEmail: indvUsr.follower.email,
                            siteName: sails.config.project.name,
                            show: show,
                            published_date: published_date,
                            profile_picture: profile_picture,
                            base_url: sails.config.apiUrl,
                            site_url: sails.config.frontUrl,
                            name: indvUsr.follower.name,
                            curr_year: (new Date()).getFullYear()
                        };
                        EmailService.sendMail(sails.config.project.followerShow.template, templateData, function (err, message) {
                            if (err) {
                                return eachCallback({ message: err.message }, null);
                            } else {
                                return eachCallback(null, {});
                            }
                        });
                    }, function (err) {
                        if (err)
                            return secCallback(err, null);
                        else
                            return secCallback(null, {});
                    });
                } else {
                    return secCallback(null, {});
                }
            },
        ], function (err, data) {
            if (err) {
                return callback({ message: err.message }, null);
            } else {
                return callback(null, {});
            }
        });
    },

    accountHistoryList: function (req, user, callback) {
        ArtistBalance.find({ user: user.id }).sort('id DESC').exec(function (err, artistbalancehistory) {
            if (err) {
                return callback({ message: err.message }, null);
            } else {
                return callback(null, artistbalancehistory);
            }
        });
    },

    shareFacebook: function (req, callback) {

        Show.findOne({ select: ['title', 'description', 'media_name'], where: req.body }).exec(function (err, show) {
            if (err) {
                return callback({ message: err.message }, null);
            } else {
                return callback(null, show);
            }
        });
    },

    getCaptureID: function (order_id, callback) {
        let paypal_token;
        async.series([
            function (firstCall) {
                var postData = { grant_type: 'client_credentials' };
                var token_url = sails.config.api_url + '/v1/oauth2/token';
                var auth = "Basic " + new Buffer(sails.config.client_id + ":" + sails.config.secret_key).toString('base64');

                request.post({ url: token_url, form: postData, headers: { "Authorization": auth } }, function (err, httpResponse, paypalBody) {
                    if (err) {
                        return firstCall({ message: err.message }, null);
                    }
                    else {
                        parseBody = JSON.parse(paypalBody);
                        paypal_token = parseBody.access_token;
                        return firstCall(null);
                    }
                });
            },
            function (secCallback) {
                PaypalService.detailPayPalOrder(order_id, paypal_token, function (err, orderData) {
                    if (err) {
                        return secCallback({ message: err.message }, null);
                    } else {

                        if (orderData.purchase_units[0].payment_summary.captures && orderData.purchase_units[0].payment_summary.captures != 'undefined') {
                            capture_id = orderData.purchase_units[0].payment_summary.captures[0].id;
                            ShowOrder.update({ order_id: order_id }, { capture_id: capture_id, status: orderData.status }).exec(function (err, res) {
                                if (err) {
                                    return secCallback({ message: err.message }, null);
                                } else {
                                    return secCallback(null);
                                }
                            });
                        } else {
                            return secCallback(null);
                        }
                    }
                });
            }
        ], function (err, data) {
            if (err) {
                return callback({ message: err.message }, null);
            } else {
                return callback(null);
            }
        });
    },
}
