
// YouTubeService.js - in api/services
var moment = require('moment');
var Hashids = require('hashids');
var moment_tz = require('moment-timezone');
var generator = require('generate-password');
var request = require('request');

module.exports = {

    /***************************************************************************
    * Do not utilize this method unless you are positive that the web token   *
    * has not expired.  To ensure that the web token is valid, add the        *
    * `isAuthorized` policy to any controllers which will utilize this.
    ***************************************************************************/
    // Front User Function
        allData: function (req, criteria, callback)
        {
            let resData;
            let deleteUser = [0];
            async.series([
                function (callback1) {
                    UserService.deleteuserID(req, function(err, resData){
                        if(err)
                            return callback1(err, null);
                        else
                        {
                            if(resData.length > 0){
                                deleteUser = resData;
                            }
                            return callback1(null, deleteUser);
                        }
                    });
                },
                function (callback2) {
                    YoutubeVideo.find(criteria).where({user:{'!':deleteUser}}).populate(['user','genre']).sort('createdAt DESC').exec(function(err, foundVideo){
                        if (err){
                            return callback2({message: err.message}, null);
                        }else {
                            resData = foundVideo;
                            return callback2(null,resData);
                        }
                    });
                }
            ], function(err, res) {
                if (err) {
                    return callback({message: err.message}, null);
                } else {
                    return callback(null,resData);
                }
            });
        },

        getData: function (req, criteria, callback)
        {
            var userData = TokenService.decode(req.headers.authorization);
            let data = req.body;
            let resData;

            YoutubeVideo.findOne(criteria).sort('createdAt DESC').exec(function(err, foundVideo){
                if (err)
                {
                    return callback({message: err.message}, null);
                }
                else
                {
                    resData = foundVideo;
                    return callback(null,resData);
                }
            });
        },

        createData: function (req, callback)
        {
            var userData = TokenService.decode(req.headers.authorization);
            var data = {
                user: userData.id,
                title: req.body.title,
                genre: req.body.genre,
    			youtube_link: req.body.youtube_link
            };
            YoutubeVideo.create(data).exec(function (err, createVideo){
                if (err)
                {
                    return callback({ message: err.message }, null);
                }
                else
                {
                    return callback(null, createVideo);
                }
            });
        },
    // Admin User Function

        youTubeList: function (req, criteria, callback)
        {
            let resData;
            let deleteUser = [0];
            async.series([
                function (callback1) {
                    UserService.deleteuserID(req, function(err, resData){
                        if(err)
                            return callback1(err, null);
                        else
                        {
                            if(resData.length > 0){
                                deleteUser = resData;
                            }
                            return callback1(null, deleteUser);
                        }
                    });
                },
                function (callback2) {
                    YoutubeVideo.find(criteria).where({user:{'!':deleteUser}}).populate(['user','genre']).sort('createdAt DESC').exec(function(err, foundVideo){
                        if (err){
                            return callback2({message: err.message}, null);
                        }else {
                            resData = foundVideo;
                            return callback2(null,resData);
                        }
                    });
                }
            ], function(err, res) {
                if (err) {
                    return callback({message: err.message}, null);
                } else {
                    return callback(null,resData);
                }
            });
        },

        addyouTube: function (req, callback)
        {
            var userData = TokenService.decode(req.headers.authorization);
            var data = {
                title: req.body.title,
                user: req.body.user,
                genre: req.body.genre,
    			youtube_link: req.body.youtube_link
            };
            YoutubeVideo.create(data).exec(function (err, createVideo){
                if (err)
                {
                    return callback({ message: err.message }, null);
                }
                else
                {
                    return callback(null, createVideo);
                }
            });
        },

        updateyouTube: function (req, criteria, callback)
        {
            var data = {
                title: req.body.title,
                user: req.body.user,
                genre: req.body.genre,
    			youtube_link: req.body.youtube_link
            };

            YoutubeVideo.update(criteria,data).exec(function (err, createVideo){
                if (err)
                {
                    return callback({ message: err.message }, null);
                }
                else
                {
                    return callback(null, createVideo);
                }
            });
        },

        getyouTube: function (req, criteria, callback)
        {
            var userData = TokenService.decode(req.headers.authorization);
            let data = req.body;
            let resData;

            YoutubeVideo.findOne(criteria).sort('createdAt DESC').exec(function(err, foundVideo){
                if (err)
                {
                    return callback({message: err.message}, null);
                }
                else
                {
                    resData = foundVideo;
                    return callback(null,resData);
                }
            });
        },

        deleteyouTube: function (req, criteria, callback) {
            console.log('come');
            YoutubeVideo.destroy(criteria).exec(function (err, resData) {
                if (err) {
                    console.log('1111');
                    return callback({ message: err.message }, null);
                }
                else {
                    console.log('222');
                    return callback(null, resData);
                }
            });
        },
}
