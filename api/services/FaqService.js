// FaqService.js - in api/services
var moment = require('moment');
var _ = require('underscore');

module.exports = {

  /***************************************************************************
   * Do not utilize this method unless you are positive that the web token   *
   * has not expired.  To ensure that the web token is valid, add the        *
   * `isAuthorized` policy to any controllers which will utilize this.
   ***************************************************************************/

 //backend apis
  fetchData: function(data ,criteria ,callback) {
    Faq.find(criteria).sort('createdAt Desc').exec(function(err, foundFaqs){
      if (err) {
        return callback({message: err.message}, null);
      } else {
        return callback(null,foundFaqs);
      }
    });
  },
  addFaq: function(req,callback) {
    let data = req.body;
    let faqData;
    async.series([
        function (firstCallback) {
          Faq.create(data).exec(function (err, faqData) {
            if(err)
              return firstCallback({message: err.message}, null);
            else
            {
              Faq.find({id:faqData.id}).sort('createdAt').exec(function(err, foundFaq){
                if (err) {
                  return firstCallback({message: err.message}, null);
                } else {
                  faqData = foundFaq;
                  return firstCallback(null,foundFaq);
                }
              });
            }
          });
        },
    ], function (err, data) {
        if (err) {
            return callback({message: err.message}, null);
        } else {
            return callback(null, faqData);
        }
    });
  },
  updateFaq: function(req, updateArr, criteria, callback) {
    let dataObj;
    async.series([
        function (firstCallback) {
            Faq.findOne(criteria).exec(function(err, foundData){
              if(err)
                return firstCallback(err, null);
              else
              {
                dataObj = foundData;
                return firstCallback(null);
              }
            });
        },
        function (secCallback) {
          if(dataObj)
          {
            Faq.update(criteria, updateArr).exec(function(err, resObj){
              if(err)
                 return secCallback({message: err.message}, null);
              else{
                faqObj = resObj;
                return secCallback(null, resObj);
              }
            });
          }
          else
          {
            return secCallback({message: "Faq not found."}, null);
          }
        }
    ], function (err, data) {
        if (err) {
            return callback({message: err.message}, null);
        } else {
            return callback(null, faqObj);
        }
    });
  },
  deleteFaq: function(req, criteria, callback) {
    Faq.destroy(criteria).exec(function (err, resData){
      if(err)
        return callback({message: err.message}, null);
      else
      {
        return callback(null,resData);
      }
    });
  },
}
