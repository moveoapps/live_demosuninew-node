// UserService.js - in api/services
var moment_tz = require('moment-timezone');
var moment = require('moment');

module.exports = {

    /***************************************************************************
        * Do not utilize this method unless you are positive that the web token   *
        * has not expired.  To ensure that the web token is valid, add the        *
        * `isAuthorized` policy to any controllers which will utilize this.
    ***************************************************************************/

    fetchData: function(data, criteria, callback) {
        Genre.find(criteria).sort('createdAt DESC').exec(function(err, foundCategories){
            if (err){
                return callback({message: err.message}, null);
            }else{
                return callback(null,foundCategories);
            }
        });
    },

    getGenreDetail: function(req, criteria, callback) {
        let currDate = moment_tz().utc().format('YYYY-MM-DD HH:mm:ss');
        let showTrendigDate = moment(currDate).subtract(3, 'months').format('YYYY-MM-DD HH:mm:ss'); // trendingshows of last three months
        let genre_id = req.param('id');
        // let genre_id;
        let deleteUser = [0];
        let resData;
        async.series([
            /*function (callback1) {
                Genre.findOne(criteria).exec(function(err, slugGenre){
                    if (err) {
                        return callback1({message: err.message}, null);
                    } else {
                        genre_id = slugGenre.id;
                        return callback1(null,genre_id);
                    }
                });
            },*/
            function (callback2) {
                UserService.deleteuserID(req, function(err, resData){
                    if(err)
                        return callback2(err, null);
                    else
                    {
                        if(resData.length > 0){
                            deleteUser = resData;
                        }
                        return callback2(null, deleteUser);
                    }
                });
            },
            function (callback3) {
                async.parallel({
                    genre: function(call1) {
                        Genre.findOne(criteria).exec(function(err, foundCategories){
                            if (err) {
                                return call1({message: err.message}, null);
                            } else {
                                return call1(null,foundCategories);
                            }
                        });
                    },
                    trendingshows: function (call2) {
                        sqlQuery = 'SELECT S.*,U.name, U.profile_picture,U.id as user_id,count(ST.id) as ticket_count FROM shows as S \
                        INNER JOIN users as U ON U.id = S.artist \
                        INNER JOIN show_ticket as ST ON ST.show = S.id \
                        INNER JOIN showgenre as SG ON SG.show = S.id \
                        INNER JOIN genre as G ON G.id = SG.genre \
                        WHERE S.publish_date >= "'+showTrendigDate+'" AND S.status = "Published" AND G.slug = "'+genre_id+'"\
                        AND S.artist NOT IN ('+deleteUser+')\
                        GROUP BY S.id ORDER BY ticket_count desc LIMIT 5';

                        Show.query(sqlQuery ,function(err, trendindShows) {
                            //ArtistTip.find({artist:showData.artist.id}).sum('amount').sort('amount', 'DESC').limit(5).exec(function(err, resTip){
                            if (err) {
                              return call2({message: err.message}, null);
                            }
                            else
                            {
                                if (trendindShows.length > 0 && trendindShows != 'undefined')
                                {
                                    var item = 0;
                                    _.each(trendindShows,function(show, i){
                                        Show.findOne({'id':show.id}).populate(['genres','artist']).exec(function(err, showgenre){
                                            if (err) {
                                                //return callback({message: err.message}, null);
                                            }
                                            else
                                            {
                                                trendindShows[i].genres = showgenre.genres;
                                                trendindShows[i].artist = showgenre.artist;
                                                item++;
                                                if (item == trendindShows.length){
                                                    return call2(null, trendindShows);
                                                }
                                            }
                                        });
                                    });
                                }
                                else
                                {
                                    return call2(null,[]);
                                }
                            }
                        });
                    }

                }, function(err, results) {
                    if (err) {
                        return callback3({message: err.message}, null);
                    } else {
                        resData = results;
                        return callback3(null,resData);
                    }
                });
            }
        ], function(err, res) {
            if (err) {
                return callback({message: err.message}, null);
            } else {
                return callback(null,resData);
            }
        });
    },

    getGenreShows: function(req, callback) {
        let res = [];
        let resData = [];
        let deleteUser = [0];
        let ss = req.param('ss');
        var sort = req.param('sort');
        let genre_id = req.param('id');
        let currDate = moment_tz().utc().format('YYYY-MM-DD HH:mm:ss');
        let showDisplayDate = moment(currDate).subtract(3, 'months').format('YYYY-MM-DD HH:mm:ss');

        async.series([
            /*function (callback1) {
                Genre.findOne({slug: req.param('id')}).exec(function(err, slugGenre){
                    if (err) {
                        return callback1({message: err.message}, null);
                    } else {
                        genre_id = slugGenre.id;
                        return callback1(null,genre_id);
                    }
                });
            },*/
            function (callback2) {
                UserService.deleteuserID(req, function(err, resData){
                    if(err)
                        return callback2(err, null);
                    else
                    {
                        if(resData.length > 0){
                            deleteUser = resData;
                        }
                        return callback2(null, deleteUser);
                    }
                });
            },
            function (callback3) {
                let order_by = '';
                if(sort)
                {
                    if(sort == '1'){
                        order_by = 'ORDER BY S.title ASC';
                    }else if(sort == '2'){
                        order_by = 'ORDER BY S.title DESC';
                    }else if(sort == '3'){
                        order_by = 'ORDER BY S.publish_date_utc ASC';
                    }else{
                        order_by = 'ORDER BY S.id DESC';
                    }
                }
                //WHERE show_end_date > "'+currDate+'" \ //0035 Condition change now, show will display till 3 month of show publish_date.
                let sqlQuery = 'SELECT S.*, U.name, U.profile_picture from shows as S \
                    INNER JOIN showgenre as SG ON S.id = SG.show \
                    INNER JOIN genre as G ON G.id = SG.genre \
                    INNER JOIN users as U ON U.id = S.artist \
                    INNER JOIN timezone as T ON T.id = S.timezone \
                    WHERE publish_date > "'+showDisplayDate+'" \
                    AND status = "Published" \
                    AND G.slug = "'+genre_id+'" \
                    AND (S.title LIKE "%'+ss+'%" OR S.tags LIKE "%'+ss+'%") \
                    AND S.artist NOT IN ('+deleteUser+') \
                    '+order_by;

                /*let sqlQuery = 'SELECT S.*, U.name, U.profile_picture from shows as S \
                    INNER JOIN showgenre as SG ON S.id = SG.show \
                    INNER JOIN users as U ON U.id = S.artist \
                    INNER JOIN timezone as T ON T.id = S.timezone \
                    WHERE publish_date > "'+showDisplayDate+'" \
                    AND status = "Published" \
                    AND SG.genre = "'+genre_id+'" \
                    AND (S.title LIKE "%'+ss+'%" OR S.tags LIKE "%'+ss+'%") \
                    AND S.artist NOT IN ('+deleteUser+') \
                    '+order_by;*/
                //console.log(sqlQuery);
                //Show.query('SELECT pet.name FROM pet WHERE pet.name = $1', [ 'dog' ] ,function(err, rawResult) {
                Show.query(sqlQuery ,function(err, fullShows) {
                    if(err)
                        return callback3(err, null);
                    else
                    {
                        if (fullShows.length > 0 && fullShows != 'undefined') {
                            var item = 0;
                            _.each(fullShows,function(show, i){
                                Show.findOne({'id':show.id}).populate(['genres','artist']).exec(function(err, showgenre){
                                    if (err) {
                                        //return callback({message: err.message}, null);
                                    } else {
                                        fullShows[i].genres = showgenre.genres;
                                        fullShows[i].artist = showgenre.artist;
                                        show.genres = showgenre.genres;
                                        res.push(show);
                                        item++;
                                        if (item == fullShows.length) {
                                            resData = fullShows;
                                            return callback3(null, resData);
                                        }
                                    }
                                });
                            });
                        } else {
                           return callback3(null,[]);
                        }
                    }
                });
            }
        ], function (err, res) {
            if (err) {
                return callback({ message: err.message }, null);
            } else {
                return callback(null, resData);
            }
        });
    },

    getGenreOnDemand: function(req, data_limit = null, callback){
        var userData  = TokenService.decode(req.headers.authorization);
        let currDate = moment_tz().utc().format('YYYY-MM-DD HH:mm:ss');
        let vod_start_date = '2019-02-20 12:00:00'; // Implemented VOD on this date
        let showvod;
        let staticvod;
        let show_vod_len;
        let genre_id;
        let artist_id = "";
        let deleteUser = [0];

        let ss = req.param('ss');
        var sort = req.param('sort');

        let order_by = '';
        let order_by_show = '';
        if(sort)
        {
            if(sort == '1'){
                order_by_show = 'ORDER BY S.title ASC';
                order_by = { title: 'ASC' };
            }else if(sort == '2'){
                order_by_show = 'ORDER BY S.title DESC';
                order_by = { title: 'DESC' };
            }else if(sort == '3'){
                order_by_show = 'ORDER BY S.publish_date_utc ASC';
                order_by = { id: 'DESC' };
            }else{
                order_by_show = 'ORDER BY S.id DESC';
                order_by = { id: 'DESC' };
            }
        }

        async.series([
            function (callback1) {
                Genre.findOne({slug: req.param('id')}).exec(function(err, slugGenre){
                    if (err) {
                        return callback1({message: err.message}, null);
                    } else {
                        genre_id = slugGenre.id;
                        return callback1(null,genre_id);
                    }
                });
            },
            function (call1){
                UserService.deleteuserID(req, function(err, resData){
                    if(err)
                        return call1(err, null);
                    else
                    {
                        if(resData.length > 0){
                            deleteUser = resData;
                        }
                        return call1(null, deleteUser);
                    }
                });
            },
            function (cBack) {
                let criteria = {show_end_date:{'<':currDate},publish_date:{'>':vod_start_date}, status: 'Published'};

                sqlQuery = 'SELECT S.*,U.name as artist_name, U.profile_picture,U.id as user_id FROM shows as S \
                INNER JOIN users as U ON U.id = S.artist \
                INNER JOIN showgenre as SG ON SG.show = S.id \
                WHERE S.show_end_date < "'+currDate+'" AND S.publish_date > "'+vod_start_date+'" AND S.status = "Published"';

                if (genre_id) {
                    sqlQuery +=' AND SG.genre = "'+genre_id+'"';
                }
                if (artist_id) {
                    sqlQuery +=' AND S.artist = "'+artist_id+'"';
                }

                sqlQuery +='AND (S.title LIKE "%'+ss+'%" OR S.tags LIKE "%'+ss+'%") \ ';
                sqlQuery +='AND S.artist NOT IN ('+deleteUser+')';
                sqlQuery +=' GROUP BY S.id \ '; // New Added without Order by.
                sqlQuery += order_by_show;


                if (data_limit > 0) {
                    sqlQuery +=' LIMIT ' + data_limit;
                } else {
                    //var query = Show.find(criteria).populate(['artist']).sort('publish_date ASC');
                }

                Show.query(sqlQuery ,function(err, resShows) {
                    if(err)
                        return cBack(err, null);
                    else
                    {
                        async.forEachOf(resShows,function(indvShw,keyShw, eachCallback) {
                            let vod_pay = 0;
                            let vod_free = 1;
                            if (userData) {
                                VodPayment.findOne({show:indvShw.id, user:userData.id}).exec(function(err, vodPay){
                                    if (err) {
                                        return eachCallback({message: err.message}, null);
                                    } else {

                                        if (vodPay) {
                                            vod_pay = 1;
                                        }

                                        if(indvShw.artist == userData.id)
                                        {
                                            indvShw.vod_pay = vod_free;
                                        }else{
                                            if(indvShw.is_paid == 1){
                                                indvShw.vod_pay = vod_pay;
                                            }else {
                                                indvShw.vod_pay = vod_free;
                                            }
                                            // indvShw.vod_pay = vod_pay;
                                        }

                                        indvShw.uid = 'show_'+indvShw.id;
                                        User.findOne({id:indvShw.artist}).exec(function(err, userObj){
                                            if (err) {
                                                return eachCallback({message: err.message}, null);
                                            } else {
                                                indvShw.artist = userObj;
                                                return eachCallback(null);
                                            }
                                        });
                                    }
                                });
                            } else {
                                indvShw.vod_pay = vod_pay;
                                return eachCallback(null);
                            }
                            indvShw.vod_type = 1;
                            indvShw.vod_price = indvShw.price;

                        } ,function(err){
                            if(err){
                                return cBack(err, null);
                            } else {
                                showvod = resShows;
                                show_vod_len = showvod.length;
                                return cBack(null);
                            }
                        });

                    }
                });
            },
            function (thumbCallback) {
                criteria = {artist:{'!':deleteUser}};
                if (genre_id) {
                    criteria.genre = genre_id;
                }
                if (artist_id) {
                    criteria.artist = artist_id;
                }

                var query = OnDemand.find(criteria).where({ 'title' : { contains : ss } }).sort(order_by).populate(['artist']);
                if (show_vod_len < data_limit) {
                    var len = data_limit - show_vod_len;
                    var query = OnDemand.find(criteria).where({ 'title' : { contains : ss } }).sort(order_by).populate(['artist']).limit(len);
                }

                query.exec(function(err, foundOndemands){
                    if (err) {
                        return thumbCallback({message: err.message}, null);
                    } else {
                        async.forEachOf(foundOndemands,function(ondemand,keyondemand, eachCallback) {
                            let vod_pay = 0;
                            let vod_free = 1;
                            ondemand.vod_type = 2;
                            ondemand.vod_price = ondemand.ondemand_price;
                            if (userData) {
                                VodPayment.findOne({ondemand:ondemand.id, user:userData.id}).exec(function(err, vodPay){
                                    if (err) {
                                        return eachCallback({message: err.message}, null);
                                    } else {
                                        if (vodPay) {
                                            vod_pay = 1;
                                        }

                                        if(ondemand.artist.id == userData.id)
                                        {
                                            ondemand.vod_pay = vod_free;
                                        }else {
                                            if(ondemand.is_paid == 1){
                                                ondemand.vod_pay = vod_pay;
                                            }else {
                                                ondemand.vod_pay = vod_free;
                                            }
                                        }

                                        ondemand.artist_name = ondemand.artist.name;
                                        ondemand.uid = 'ondemand_'+ondemand.id;
                                        return eachCallback(null);
                                    }
                                });
                            } else {
                                if(ondemand.is_paid == 1){
                                    ondemand.vod_pay = vod_pay;
                                }else {
                                    ondemand.vod_pay = vod_free;
                                }
                                return eachCallback(null);
                            }
                        } ,function(err){
                            if(err){
                                return thumbCallback(err, null);
                            } else {
                                staticvod = foundOndemands;
                                return thumbCallback(null);
                            }
                        });
                    }
                });
            },
        ], function (err, res) {
            if (err) {
                return callback({message: err.message}, null);
            } else {
                var vod_link = 'https://streaming.rendezvous.live/Demosuni_VOD';
                if (data_limit > 0) {
                    if (show_vod_len == data_limit) {
                        var finalArr = showvod;
                    } else {
                        var finalArr = showvod.concat(staticvod);
                    }
                } else {
                    var finalArr = showvod.concat(staticvod);
                }

                var fullArry = {'shows':finalArr, 'vod_link':vod_link};

                return callback(null, fullArry);
            }
        });
    },

    getGenreYouTube: function(req, callback) {
        let genre_id;
        let ss = req.param('ss');
        var sort = req.param('sort');
        let res = [];
        let resData;

        let order_by = '';
        if(sort)
        {
            if(sort == '1'){
                order_by = { title: 'ASC' };
            }else if(sort == '2'){
                order_by = { title: 'DESC' };
            }else{
                order_by = { id: 'DESC' };
            }
        }
        async.series([
            function (callback1) {
                if(req.param('id')){
                    Genre.findOne({slug: req.param('id')}).exec(function(err, slugGenre){
                        if (err) {
                            return callback1({message: err.message}, null);
                        } else {
                            genre_id = slugGenre.id;
                            return callback1(null,genre_id);
                        }
                    });
                }else{
                    return callback1(null,genre_id);
                }
            },
            function (callback2) {
                YoutubeVideo.find({ genre: genre_id }).where({ 'title' : { contains : ss } }).populate(['user','genre']).sort(order_by).exec(function(err, youTubeVideos){
                    if(err){
                        return callback(err, null);
                    }else {
                        return callback(null,youTubeVideos);
                    }
                });
            }
        ], function (err, res) {
            if(err){
                return callback(err, null);
            }else {
                return callback(null,youTubeVideos);
            }
        });
    },

    updateGenre: function(req, updateArr, criteria, callback) {
        let lower = updateArr.name;
        let str = lower.toLowerCase();
        let slug = str.replace(/[&\/\\#, +()$~%.'":*?<>{}]/g, '-');
        let dataObj;
        let category;
        async.series([
            function (callback1) {
                Genre.findOne(criteria).exec(function(err, foundData){
                    if(err)
                        return callback1(err, null);
                    else
                    {
                        dataObj = foundData;
                        return callback1(null);
                    }
                });
            },
            function (callback2) {
                Genre.findOne({'slug':slug}).exec(function(err, foundCategory){
                    if(err)
                        return callback2(err, null);
                    else
                    {
                        category = foundCategory;
                        if(category){
                            if(category.id != dataObj.id){
                                return callback({message: "Category already exist with same name."}, null);
                            }else{
                                return callback2(null);
                            }
                        }else{
                            return callback2(null);
                        }
                    }
                });
            },
            function (secCallback) {
                if(dataObj)
                {
                    if (typeof req._fileparser != 'undefined' && req._fileparser.upstreams.length > 0)
                    {
                        UploadimageService.updatePicture(req, 'cover_image', sails.config.s3bucket.genremedia , function(err,res){
                            if (err) {
                                return secCallback(err,null);
                            } else {
                                updateArr.cover_image = '';
                                if (res) {
                                    updateArr.cover_image = res;
                                }
                                return secCallback(null);
                            }
                        });
                    }
                    else
                    {
                        updateArr.cover_image = '';
                        return secCallback(null);
                    }
                }
                else
                {
                    return secCallback({message: "Genre not found."}, null);
                }
            },
            function (thCallback) {
                updateArr.slug = slug;
                Genre.update(criteria, updateArr).exec(function(err, resObj){
                    if(err)
                        return thCallback({message: err.message}, null);
                    else
                    {
                        genreObj = resObj;
                        return thCallback(null, resObj);
                    }
                });
            }
        ], function (err, data) {
            if (err) {
                return callback({message: err.message}, null);
            } else {
                return callback(null, genreObj);
            }
        });
    },

    updateBulkGenre: function(updateArr, criteria, callback) {
        Genre.update(criteria, updateArr).exec(function(err, resObj){
            if(err)
                return callback({message: err.message}, null);
            else
            {
                genreObj = resObj;
                return callback(null, resObj);
            }
        });
    },

    addGenre: function(req,callback) {
        let data = req.body;
        let lower = req.body.name;
        let str = lower.toLowerCase();
        let slug = str.replace(/[&\/\\#, +()$~%.'":*?<>{}]/g, '-');
        let genre;
        let category;
        async.series([
            function (callback1) {
                Genre.findOne({'slug':slug}).exec(function(err, foundCategory){
                    if(err)
                        return callback1(err, null);
                    else
                    {
                        category = foundCategory;
                        if(category){
                            return callback({message: "Category already exist with same name."}, null);
                        }else{
                            return callback1(null)
                        }
                    }
                });
            },
            function (callback2) {
                if (typeof req._fileparser != 'undefined' && req._fileparser.upstreams.length > 0){
                    UploadimageService.updatePicture(req, 'cover_image', sails.config.s3bucket.genremedia , function(err,res){
                        if (err) {
                            return callback2(err,null);
                        } else {
                            data.cover_image = '';
                            if (res) {
                                data.cover_image = res;
                            }
                            return callback2(null);
                        }
                    });
                }else{
                    data.cover_image = '';
                    return callback2(null);
                }
            },
            function (callback3) {
                data.slug = slug;
                Genre.create(data).exec(function (err, resData){
                    if(err){
                        return callback3({message: err.message}, null);
                    }else {
                        genre = resData;
                        return callback3(null,resData);
                    }
                });
            }
        ], function (err, data) {
            if (err) {
                return callback({message: err.message}, null);
            } else {
                return callback(null, genre);
            }
        });
    },

    deleteGenre: function(req, criteria, callback) {
        async.parallel([
            function deleteGenreShow(addReportShowCallback) {
                showcriteria = {genre: req.body.genreArray};
                Show.destroy(showcriteria).exec(function (err, resData){
                    if(err)
                        return addReportShowCallback({message: err.message}, null);
                    else
                    {
                        return addReportShowCallback(null,resData);
                    }
                });
            },
            function deleteGenre(sendMailToAdminCallback) {
                Genre.destroy(criteria).exec(function (err, resData){
                    if(err)
                        return sendMailToAdminCallback({message: err.message}, null);
                    else
                    {
                        return sendMailToAdminCallback(null,resData);
                    }
                });
            }
        ],function (err, results) {
            return callback(null, results[1]);
        }); // async.parallel
    },
}
