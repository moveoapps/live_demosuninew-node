// EmojiService.js - in api/services
var moment = require('moment');
var _ = require('underscore');

module.exports = {

  /***************************************************************************
   * Do not utilize this method unless you are positive that the web token   *
   * has not expired.  To ensure that the web token is valid, add the        *
   * `isAuthorized` policy to any controllers which will utilize this.
   ***************************************************************************/

  //backend apis
  fetchData: function(data ,criteria ,callback) {
    Emoji.find(criteria).sort('createdAt Desc').exec(function(err, foundEmojis){
      if (err) {
        return callback({message: err.message}, null);
      } else {
        return callback(null,foundEmojis);
      }
    });
  },
  addEmoji: function(req,callback) {
    Emoji.create(data).exec(function (err, emojiData) {
      if (err) {
        return callback({message: err.message}, null);
      } else {
        return callback(null,emojiData);
      }
    });
  },
  updateEmoji: function(req, updateArr, criteria, callback) {
    let dataObj;
    async.series([
        function (firstCallback) {
            Emoji.findOne(criteria).exec(function(err, foundData){
              if(err)
                return firstCallback(err, null);
              else
              {
                dataObj = foundData;
                return firstCallback(null);
              }
            });
        },
        function (secCallback) {
          if(dataObj)
          {
            Emoji.update(criteria, updateArr).exec(function(err, resObj){
              if(err)
                 return secCallback({message: err.message}, null);
              else{
                emojiObj = resObj;
                return secCallback(null, resObj);
              }
            });
          }
          else
          {
            return secCallback({message: "Emoji not found."}, null);
          }
        }
    ], function (err, data) {
        if (err) {
            return callback({message: err.message}, null);
        } else {
            return callback(null, emojiObj);
        }
    });
  },
  deleteEmoji: function(req, criteria, callback) {
    Emoji.destroy(criteria).exec(function (err, resData){
      if(err)
        return callback({message: err.message}, null);
      else
      {
        return callback(null,resData);
      }
    });
  },
}
