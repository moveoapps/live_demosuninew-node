// TierService.js - in api/services
var moment = require('moment');
var _ = require('underscore');

module.exports = {

  /***************************************************************************
   * Do not utilize this method unless you are positive that the web token   *
   * has not expired.  To ensure that the web token is valid, add the        *
   * `isAuthorized` policy to any controllers which will utilize this.
   ***************************************************************************/

 //backend apis
  fetchData: function(data ,criteria ,callback) {
    Tier.find(criteria).sort('createdAt Desc').exec(function(err, foundTiers){
      if (err) {
        return callback({message: err.message}, null);
      } else {
        return callback(null,foundTiers);
      }
    });
  },
  addTier: function(req,callback) {
    let data = req.body;
    let tier;
    let tierData;
    async.series([
        function (firstCallback) {
            Tier.findOne({'name':data.name}).exec(function(err, foundTier){
              if(err)
                return firstCallback(err, null);
              else
              {
                tier = foundTier;
                return firstCallback(null);
              }
            });
        },
        function (secCallback) {
          if(tier) {
            return secCallback({message: "Tier already exist with same name."}, null);
          } else {
            return secCallback(null);
          }
        },
        function (secondCallback) {
          Tier.create(data).exec(function (err, tierData) {
            if(err)
              return secondCallback({message: err.message}, null);
            else
            {
              Tier.find({id:tierData.id}).sort('createdAt').exec(function(err, foundTier){
                if (err) {
                  return secondCallback({message: err.message}, null);
                } else {
                  tierData = foundTier;
                  return secondCallback(null,foundTier);
                }
              });
            }
          });
        },
    ], function (err, data) {
        if (err) {
            return callback({message: err.message}, null);
        } else {
            return callback(null, tierData);
        }
    });
  },
  updateTier: function(req, updateArr, criteria, callback) {
    let dataObj;
    async.series([
        function (firstCallback) {
            Tier.findOne(criteria).exec(function(err, foundData){
              if(err)
                return firstCallback(err, null);
              else
              {
                dataObj = foundData;
                return firstCallback(null);
              }
            });
        },
        function (secCallback) {
          if(dataObj)
          {
            Tier.update(criteria, updateArr).exec(function(err, resObj){
              if(err)
                 return secCallback({message: err.message}, null);
              else{
                tierObj = resObj;
                return secCallback(null, resObj);
              }
            });
          }
          else
          {
            return secCallback({message: "Tier not found."}, null);
          }
        }
    ], function (err, data) {
        if (err) {
            return callback({message: err.message}, null);
        } else {
            return callback(null, tierObj);
        }
    });
  },
  deleteTier: function(req, criteria, callback) {
    Tier.destroy(criteria).exec(function (err, resData){
      if(err)
        return callback({message: err.message}, null);
      else
      {
        return callback(null,resData);
      }
    });
  },
}
