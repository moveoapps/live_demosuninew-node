// UserService.js - in api/services
var moment = require('moment');
var Hashids = require('hashids');
var request = require('request');
var moment_tz = require('moment-timezone');

module.exports = {

  /***************************************************************************
  * Do not utilize this method unless you are positive that the web token   *
  * has not expired.  To ensure that the web token is valid, add the        *
  * `isAuthorized` policy to any controllers which will utilize this.
  ***************************************************************************/
    changePassword: function(req, callback) {
        let data = req.body;
        var userData  = TokenService.decode(req.headers.authorization);
        let isMatch = false;
        var updateData = {};
        let token;
        let resData;
        let userObj;
        var crieteria = {id: userData.id};
        async.series([
            function (cBack) {
                User.find(crieteria).exec(function(err, foundUser){
                    if(err)
                        return cBack(err, null);
                    else
                    {
                        userObj = foundUser[0];
                        return cBack(null);
                    }
                });
            },
            function (firstCallback) {
                if (userObj && data.old_password) {
                    User.isValidPassword(data.old_password, userObj, function(err, match) {
                        if (match) {
                            isMatch = true;
                            return firstCallback(null);
                        } else {
                            //res.unauthorized();
                            return firstCallback({message: "Current password does not match with your account."}, null);
                        }
                    });
                } else {
                    //return firstCallback({message: "Current password does not match with your account."}, null);
                    isMatch = true;
                    return firstCallback(null, {});
                }
            },
            function (secondCallback) {
                if (isMatch) {
                    User.encryptPassword(data.new_password, function(err, encryptedPassword) {
                        updateData.password = encryptedPassword;

                        User.update(crieteria, updateData).exec(function(err, user){
                            if(err)
                            return secondCallback({message: err.message}, null);
                            else{
                                //resData = user[0];
                                //token = TokenService.sign(resData, sails.config.project.passwordReset.tokenTTL);
                                return secondCallback(null, user[0]);
                            }
                        });
                    });
                } else {
                    return secondCallback({message: "Current password does not match with your account."}, null);
                }
            }
        ], function (err, data) {
            if (err) {
                callback({message: err.message}, null);
            } else {
                callback(null);
            }
        });
    },

    logout: function (req,callback) {
        let dataObj;
        async.series([
            function (firstCallback) {
                Invalidtoken.findOne({'token': req.headers.authorization}).exec(function(err, foundData){
                    if(err)
                    return firstCallback(err.message, null);
                    else
                    {
                        dataObj = foundData;
                        return firstCallback(null);
                    }
                });
            },
            function (secCallback) {
                if(dataObj)
                {
                    return secCallback(null);
                }
                else
                {
                    Invalidtoken.create({'token': req.headers.authorization}).exec( function (err,user) {
                        if(err)
                        return secCallback(err.message, null);
                        else{
                            return secCallback(null);
                        }
                    });
                }
            }
        ], function (err, data) {
            if (err) {
                return callback({message: err.message}, null);
            } else {
                return callback(null);
            }
        });
    },

    getUser: function (req) {
        if(req.headers.hasOwnProperty('authorization')) {
            return TokenService.decode(req.headers.authorization, sails.config.session.secret);
        } else {
            return false;
        }
    },

    updateProfile: function(data, callback) {
        var users  = TokenService.decode(data.headers.authorization);
        var crieteria = {id: users.id};
        let updateData = data.body;
        async.series([
            function (secCallback) {
                UserService.updatePicture(data, function(err,res){
                    if (err) {
                        return secCallback({message: err.message},null);
                    } else {
                        if (res) {
                            updateData.profile_picture = res;
                        }else{
                            User.find({'id':users.id}).exec(function(err, res){
                                if(err){
                                    updateData.profile_picture = '';
                                }else{
                                    if (res) {
                                        updateData.profile_picture = res[0]['profile_picture'];
                                    }
                                }
                            });
                        }
                        return secCallback(null);
                    }
                });
            },
            function (thCallback) {
                User.update(crieteria, updateData).exec(function(err, user){
                    if(err)
                    thCallback({message: err.message}, null);
                    else{
                        userData = user[0];
                        thCallback(null, user[0]);
                    }
                });
            }
        ], function (err, data) {
            if (err) {
                return callback({message: err.message}, null);
            } else {
                return callback(null, userData);
            }
        });
    },

    updatePicture: function(data,callback){
        let uploadFile;
        async.series([
            function (cBack) {
                if (typeof data._fileparser != 'undefined' && data._fileparser.upstreams.length > 0){
                    var filename = data.file('profile_picture')._files[0].stream.filename;
                    var i = filename.lastIndexOf('.');
                    var ext = filename.substr(i);
                    uploadFile = new moment().valueOf()+ext;
                    UploadimageService.uploadImage(data,"profile_picture","avatar",uploadFile, function(err, image){
                        if(err)
                            return cBack(err.message, null);
                        else{
                            //return cBack(null);
                            console.log('resize start');
                            UploadimageService.resizeS3Image("avatar",uploadFile,sails.config.s3bucket.userProfileThumb,339,514,function(err,thumbname){
                                if (err)
                                return cBack({message: "File thumb fail"});
                                else
                                {
                                    console.log('resize end');
                                    console.log(thumbname);
                                    return cBack(null);
                                }
                            });
                        }
                    });
                }
                else
                {
                    uploadFile = '';
                    return cBack(null);
                }
            }
        ], function (err, data) {
            if (err) {
                return callback({message: err.message}, null);
            } else {
                return callback(null, uploadFile);
            }
        });
    },

    registration: function(req, callback) {
        let data = req.body;
        if (data.artist_type == 2) {
            data.tier = 2;
        }
        data.is_active = 1;
        let user;
        // Remove leading zero
        //data.mobile = data.mobile.replace(/^0+/, '');
        async.series([
            function (firstCallback) {
                User.findOne({'email':data.email}).exec(function(err, foundUser){
                    if(err)
                        return firstCallback(err, null);
                    else
                    {
                        user = foundUser;
                        return firstCallback(null);
                    }
                });
            },

            function (secondCallback) {
                if (user) {
                    console.log('if');
                    if (user.gifted_user || user.is_delete == 1) {
                        data.gifted_user = 0;
                        User.encryptPassword(data.password, function(err, encryptedPassword) {
                            data.password = encryptedPassword;
                            data.is_delete = 0;
                            User.update({id:user.id},data).exec(function (err, userObj){
                                if(err)
                                    return secondCallback({message: err.message}, null);
                                else
                                {
                                    userData = userObj[0];
                                    UserService.afterCreateUser(req, userData, function(err, body){
                                        if(err){
                                            return secondCallback({message: err.message}, null);
                                        } else {
                                            return secondCallback(null);
                                        }
                                    });
                                }
                            });
                        });
                    }
                    else {
                        return secondCallback({message: "Email already exist."}, null);
                    }
                } else {
                    console.log('else');
                    User.create(data).exec(function (err, userData){
                        if(err){
                            return secondCallback({message: err.message}, null);
                        } else {
                            UserService.afterCreateUser(req, userData, function(err, body){
                                if(err){
                                    return secondCallback({message: err.message}, null);
                                } else {
                                    return secondCallback(null);
                                }
                            });
                        }
                    });
                }
            }
        ], function (err, data) {
            if (err) {
                return callback({message: err.message}, null);
            } else {
                return callback(null);
            }
        });
    },

    afterCreateUser: function(req, userData, secondMidCallback) {
        var hashids = new Hashids(sails.config.project.name,10);
        var encodedId = hashids.encode(userData.id);

        // ****PayPal Integration****
        if (userData.user_type == 1) {
            if (userData.payment_type == 1) {
                UserService.createPartnerReferrals(req, userData, function(err, body){
                    if(err){
                        return ServerError(err.message,res);
                    }
                    else {
                        console.log('paypal partner response',body);
                        async.eachSeries(body.links,function(linkObj, eachCallback) {
                            if (linkObj.rel == 'action_url') {

                                var action_link = linkObj.href;
                                var linkArr = action_link.split('&context_token');
                                var linkArr2 = linkArr[0].split('referralToken=');
                                var partner_referral_id = linkArr2[1];
                                //console.log('partner_referral_id==',linkArr[1]);

                                // ********Get partner refrel detail********

                                // var exec = require('child_process').exec;
                                // var child;
                                // url = sails.config.apiUrl+'/getpartnerrefereldetail/'+partner_referral_id;
                                // console.log(url);
                                //
                                // child = exec('wget ' + url+' -q -b',
                                // function (error, stdout, stderr) {
                                    //   console.log('stdout: ' + stdout);
                                    //   console.log('stderr: ' + stderr);
                                    //   if (error !== null) {
                                    //     console.log('exec error: ' + error);
                                    //   }
                                // }).unref();

                                User.update({id:userData.id}, {paypal_action_url:linkObj.href,partner_referral_id:partner_referral_id}).exec(function (err, userRes){
                                    if(err)
                                        return eachCallback({message: err.message}, null);
                                    else
                                    {
                                        var templateData = {
                                            to: userData.email,
                                            from: sails.config.fromName+' <'+sails.config.fromEmail+'>',
                                            subject: sails.config.project.signup.subject,
                                            userEmail: userData.email,
                                            siteName: sails.config.project.name,
                                            url: sails.config.frontUrl + sails.config.project.signup.uri + encodedId,
                                            base_url: sails.config.apiUrl,
                                            name: userData.name,
                                            payment_type: userData.payment_type,
                                            paypal_action_url:linkObj.href,
                                            curr_year: (new Date()).getFullYear()
                                        };

                                        EmailService.sendMail(sails.config.project.signup.template, templateData, function(err, message) {
                                            if (err){
                                                //return sails.log.error('> error sending artist registration email', err);
                                                console.log('error',err);
                                                return eachCallback(null);
                                            } else {
                                                //sails.log.verbose('> sending artist registration email to:', user.email);
                                                return eachCallback(null);
                                            }
                                        });
                                    }
                                });
                            } else {
                                return eachCallback(null);
                            }
                            //return eachCallback(null);
                        } ,function(err){
                            if(err)
                                return secondMidCallback(err, null);
                            else
                                return secondMidCallback(null);
                        });
                    }
                });
            } else {
                //  ****************0035 Create Stripe custom account **************
                /*StripeService.createAccount(req, userData, function(err, response){
                    if(err){
                        return secondMidCallback({message: err.message}, null);
                    } else {
                        var templateData = {
                            to: userData.email,
                            from: sails.config.fromName+' <'+sails.config.fromEmail+'>',
                            subject: sails.config.project.signup.subject,
                            userEmail: userData.email,
                            siteName: sails.config.project.name,
                            url: sails.config.frontUrl + sails.config.project.signup.uri + encodedId,
                            base_url: sails.config.apiUrl,
                            name: userData.name,
                            payment_type: userData.payment_type,
                            paypal_action_url:'',
                            curr_year: (new Date()).getFullYear()
                        };

                        EmailService.sendMail(sails.config.project.signup.template, templateData, function(err, message) {
                            if (err){
                                //return sails.log.error('> error sending artist registration email', err);
                                console.log('error',err);
                                return secondMidCallback(null);
                            } else {
                                //sails.log.verbose('> sending artist registration email to:', user.email);
                                return secondMidCallback(null);
                            }
                        });
                    }
                });*/

                //  ****************0035 Create Stripe Standard account **************
                let stripe_connect_client = sails.config.stripe_connect_client;
                let redirect_uri = sails.config.frontUrlNoHash+'/stripe/onboarding_complete';
                let stripe_link = 'https://connect.stripe.com/oauth/authorize?response_type=code&state='+encodedId+'&client_id='+stripe_connect_client+'&scope=read_write';
                // console.log('stripe_link',stripe_link);
                var templateData = {
                    to: userData.email,
                    from: sails.config.fromName+' <'+sails.config.fromEmail+'>',
                    subject: sails.config.project.signupStripe.subject,
                    userEmail: userData.email,
                    siteName: sails.config.project.name,
                    url: sails.config.frontUrl + sails.config.project.signupStripe.uri + encodedId,
                    base_url: sails.config.apiUrl,
                    name: userData.name,
                    payment_type: userData.payment_type,
                    stripe_link:stripe_link,
                    curr_year: (new Date()).getFullYear()
                };

                EmailService.sendMail(sails.config.project.signupStripe.template, templateData, function(err, message) {
                    if (err){
                        //return sails.log.error('> error sending artist registration email', err);
                        console.log('error',err);
                        return secondMidCallback(null);
                    } else {
                        //sails.log.verbose('> sending artist registration email to:', user.email);
                        return secondMidCallback(null);
                    }
                });
            }
        } else {
            var templateData = {
                to: userData.email,
                from: sails.config.fromName+' <'+sails.config.fromEmail+'>',
                subject: sails.config.project.signupFan.subject,
                userEmail: userData.email,
                siteName: sails.config.project.name,
                url: sails.config.frontUrl + sails.config.project.signupFan.uri + encodedId,
                base_url: sails.config.apiUrl,
                name: userData.name,
                curr_year: (new Date()).getFullYear()
            };

            EmailService.sendMail(sails.config.project.signupFan.template, templateData, function(err, message) {
                if (err){
                    return sails.log.error('> error sending in fan registration email', err);
                    console.log('error',err);
                    return secondMidCallback(null);
                } else {
                    console.log('message',message);
                    //sails.log.verbose('> sending fan registration email to:', user.email);
                    return secondMidCallback(null);
                }
            });

        }
    },

    createPartnerReferrals: function(req, userData, secondCallback) {
        let data = req.body;
        // Remove leading zero
        //data.mobile = data.mobile.replace(/^0+/, '');
        var postData = {grant_type:'client_credentials'};
        var token_url = sails.config.api_url+'/v1/oauth2/token';
        var auth = "Basic " +new Buffer(sails.config.client_id+":"+sails.config.secret_key).toString('base64');
        request.post({url:token_url, form: postData, headers : {"Authorization" : auth}}, function(err, httpResponse, paypalBody){
            if(err){
                return secondCallback(err.message, res);
            }
            else{
                var parseBody = JSON.parse(paypalBody);
                var merchant_url = sails.config.api_url+'/v1/customer/partner-referrals';
                var auth = "Bearer "+parseBody.access_token;

                var postData  = {
                    "customer_data": {
                        "customer_type": "MERCHANT",
                        "person_details": {
                            "email_address": userData.email,
                            "name": {
                                "prefix": "",
                                "given_name": userData.name,
                                "surname": "",
                                "middle_name": ""
                            }
                        },
                        "preferred_language_code":"en_AU",
                        "primary_currency_code": "AUD",
                        "referral_user_payer_id": {
                            "type": "PAYER_ID",
                            "value": sails.config.partner_id
                        },
                        "partner_specific_identifiers": [
                            {
                                "type": "TRACKING_ID",
                                "value": userData.id
                            }
                        ]
                    },
                    "requested_capabilities": [{
                        "capability": "API_INTEGRATION",
                        "api_integration_preference": {
                            "partner_id": sails.config.partner_id,
                            "rest_api_integration": {
                                "integration_method": "PAYPAL",
                                "integration_type": "THIRD_PARTY"
                            },
                            "rest_third_party_details": {
                                "partner_client_id": sails.config.client_id,
                                "feature_list": [
                                    "PAYMENT",
                                    "REFUND",
                                    "PARTNER_FEE",
                                    "DELAY_FUNDS_DISBURSEMENT"
                                    //"ADVANCED_TRANSACTIONS_SEARCH"
                                ]
                            }
                        }
                    }],
                    "web_experience_preference": {
                        "partner_logo_url": sails.config.apiUrl+'/images/logo.png',
                        "return_url": sails.config.frontUrl+'/paypal/onboarding_complete',
                        "action_renewal_url": sails.config.frontUrl
                    },
                    "collected_consents": [
                        {
                          "type": "SHARE_DATA_CONSENT",
                          "granted": true
                        }
                    ],
                    "products": [
                        "EXPRESS_CHECKOUT"
                    ]
                };
                postDataJson = JSON.stringify(postData);
                // console.log('postDataJson',postDataJson);
                var contentLength = postDataJson.length;
                request({
                    headers: {
                        'Content-Length': contentLength,
                        'Content-Type': 'application/json',
                        "Authorization" : auth,
                        "PayPal-Partner-Attribution-Id": sails.config.partner_attribution_id
                    },
                    uri: merchant_url,
                    body: postDataJson,
                    method: 'POST'
                }, function (err, res, body) {
                    if(err)
                        return secondCallback({message: err.message}, null);
                    else
                    {
                        var body = JSON.parse(body);
                        return secondCallback(null, body);
                    }
                });
            }
        });
    },

    fbLogin: function(req, callback) {
        let data = req.body;
        let user;
        let userObj;
        let res;
        let fileName;
        // Remove leading zero
        //data.mobile = data.mobile.replace(/^0+/, '');

        async.series([
            function (firstCallback) {
                User.findOne({or: [{social_id:data.social_id}, {'email': data.email}]}).exec(function(err, foundUser){
                    if(err)
                        return firstCallback(err, null);
                    else
                    {
                        user = foundUser;
                        return firstCallback(null);
                    }
                });
            },
            function (secondCallback) {
                if (user) {
                    User.update({id:user.id},data).exec(function (err, userData){
                        if(err)
                            return secondCallback({message: err.message}, null);
                        else
                        {
                            var token = TokenService.sign({id: user.id,user_type: user.user_type}, sails.config.project.tokenTTL);

                            if (userData[0].user_type == 2 && userData[0].name && userData[0].name != null && userData[0].email && userData[0].email != null && userData[0].country && userData[0].country != null) {
                                is_profile_complete = 1;
                            } else if (userData[0].user_type == 1 && userData[0].name && userData[0].name != null && userData[0].email && userData[0].email != null && userData[0].country && userData[0].country != null && userData[0].bio && userData[0].bio != null) {
                                is_profile_complete = 1;
                            } else {
                                is_profile_complete = 0;
                            }
                            userData[0].is_profile_complete = is_profile_complete;
                            userObj = userData[0];
                            res = {user: userData[0], token: token};
                            return secondCallback(null);
                        }
                    });
                } else {
                    fileName = Math.floor(Date.now())+'.jpg';
                    UploadimageService.uploadFbImage(data.social_id,'avatar',fileName, function(err, image){
                        if(err)
                        {
                            return callback({message: err.message}, null);
                        }
                        else
                        {
                            data.profile_picture=fileName;
                            User.create(data).exec(function (err, userData){
                                if(err)
                                    return secondCallback({message: err.message}, null);
                                else
                                {
                                    var token = TokenService.sign({id: userData.id,user_type: userData.user_type}, sails.config.project.tokenTTL);
                                    if (userData.user_type == 2 && userData.name && userData.name != null && userData.email && userData.email != null && userData.country && userData.country != null) {
                                        is_profile_complete = 1;
                                    } else if (userData.user_type == 1 && userData.name && userData.name != null && userData.email && userData.email != null && userData.country && userData.country != null && userData.bio && userData.bio != null) {
                                        is_profile_complete = 1;
                                    } else {
                                        is_profile_complete = 0;
                                    }
                                    userData.is_profile_complete = is_profile_complete;
                                    userObj = userData;

                                    res = {user: userData, token: token};

                                    UploadimageService.resizeS3Image("avatar",fileName,sails.config.s3bucket.userProfileThumb,339,514,function(err,thumbname){
                                        if (err)
                                            return secondCallback({message: "File thumb fail"});
                                        else
                                        {
                                            return secondCallback(null);
                                        }
                                    });
                                    //return secondCallback(null);
                                }
                            });
                        }
                    });
                }
            },
            function (thCallback) {
                User.update({id:userObj.id},{firsttime_login:0}).exec(function(err, userData){
                    return thCallback(null);
                });
            },
        ], function (err, data) {
            if (err) {
                return callback({message: err.message}, null);
            } else {
                return callback(null, res);
            }
        });
    },

    verifyAccount: function(req, callback) {
        let data = req.body;
        let message;
        let res = {};
        var hashids = new Hashids(sails.config.project.name,10);
        let userId = hashids.decode(data.id);
        // Remove leading zero
        //data.mobile = data.mobile.replace(/^0+/, '');
        async.series([
            function (firstCallback) {
                User.find({id:userId}).exec(function(err, foundUser){
                    if(err)
                    return firstCallback(err, null);
                    else
                    {
                        user = foundUser[0];
                        return firstCallback(null);
                    }
                });
            },
            function (secondCallback) {
                if (user) {
                    if (user.is_verified) {
                        message = 'Your account already verified. Please login.';
                        return secondCallback(null);
                    } else {
                        User.update({id:userId},{'is_verified':1}).exec(function (err, userData){
                            if(err)
                                return secondCallback({message: err.message}, null);
                            else
                            {
                                if (userData[0].user_type == 2) {
                                    message = 'Your account has been verified.';
                                } else {
                                    // message = 'Your account has been verified. We will activate your account within 24hrs.';
                                    message = 'Your account has been verified and you’re ready to go!\nJump onto the site and start streaming or browse our on-demand videos.';
                                }
                                return secondCallback(null);
                            }
                        });
                    }
                } else {
                    return secondCallback({message: "This link does not exist."}, null);
                }
            }
        ], function (err, data) {
            if (err) {
                return callback({message: err.message}, null);
            } else {
                return callback(null, res, message);
            }
        });
    },

    uploadMedia: function(data, callback) {
        var userData  = TokenService.decode(data.headers.authorization);
        var mediaArray = [];
        let medias;
        var type = data.body.media_type;
        async.series([
            function (secCallback) {
                if(type == 1){
                    UploadimageService.uploadImageMultiple(data, 'file', sails.config.s3bucket.artistmediaimage,sails.config.s3bucket.artistmediaresizeimage, function(err, documentUploaded){
                        if(err)
                            return secCallback(err, null);
                        else{
                            for(var i = 0; i < documentUploaded.file.length; i++){
                                mediaArray.push({
                                    user: userData.id,
                                    file_name: documentUploaded.file[i].extra.fd.split('/').pop(),
                                    type : 1
                                });
                            }
                            return secCallback(null, mediaArray);
                        }
                    });
                }else{
                    UploadimageService.uploadVideoMultiple(data, 'file', sails.config.s3bucket.artistmediavideo,sails.config.s3bucket.artistmediavideothumb, function(err, documentUploaded){
                        if(err)
                            return secCallback(err, null);
                        else{
                            for(var i = 0; i < documentUploaded.file.length; i++){
                                mediaArray.push({
                                    user: userData.id,
                                    file_name: documentUploaded.file[i].extra.fd.split('/').pop(),
                                    type : 2
                                });
                            }
                            return secCallback(null, mediaArray);
                        }
                    });
                }
            },
            function (thumbCallback) {
                if(type == 2){
                    async.forEachOf(mediaArray,function(indvVdo,keyVdo, eachCallback) {
                        UploadimageService.videoThumb(sails.config.s3bucket.artistmediavideo,indvVdo.file_name,sails.config.s3bucket.artistmediavideothumb,function(err,thumbname){
                            if (err)
                                return eachCallback({message: "File thumb fail"});
                            else
                            {
                                return eachCallback(null);
                            }
                        });
                        //return eachCallback(null);
                    } ,function(err){
                        if(err)
                            return thumbCallback(err, null);
                        else
                            return thumbCallback(null,mediaArray);
                    });
                }else{
                    async.forEachOf(mediaArray,function(indvImg,keyImg, eachCallback) {
                        UploadimageService.resizeS3Image(sails.config.s3bucket.artistmediaimage,indvImg.file_name,sails.config.s3bucket.artistmediaresizeimage,200,200,function(err,thumbname){
                            if (err)
                                return eachCallback({message: "File thumb fail"});
                            else
                            {
                                return eachCallback(null);
                            }
                        });
                        //return eachCallback(null);
                    } ,function(err){
                        if(err)
                            return thumbCallback(err, null);
                        else
                            return thumbCallback(null,mediaArray);
                    });
                }
            },
            function (thCallback) {
                ArtistMedia.create(mediaArray).exec(function(err, Createmedias){
                    if(err)
                        thCallback({message: err.message}, null);
                    else{
                        medias = Createmedias;
                        thCallback(null, medias);
                    }
                });
            }
        ], function (err, data) {
            if (err) {
                return callback({message: err.message}, null);
            } else {
                return callback(null, medias);
            }
        });
    },
    //backend apis
    fetchData: function(data ,criteria ,callback) {
        User.find(criteria).populate(['tier']).sort('createdAt Desc').exec(function(err, foundUsers){
            if (err) {
                return callback({message: err.message}, null);
            } else {
                // console.log("foundUsers == ", foundUsers);
                return callback(null,foundUsers);
            }
        });
    },

    updateUser: function(req, updateArr, criteria, callback) {
        let dataObj;
        async.series([
            function (firstCallback) {
                User.findOne(criteria).exec(function(err, foundData){
                    if(err)
                    return firstCallback(err, null);
                    else
                    {
                        dataObj = foundData;
                        return firstCallback(null);
                    }
                });
            },
            function (mailCallback) {
                if(dataObj && updateArr.is_active == 1 && dataObj.is_active_mail_sent != 1 && dataObj.user_type == 1)
                {
                    var templateData = {
                        to: dataObj.email,
                        from: sails.config.fromName+' <'+sails.config.fromEmail+'>',
                        subject: sails.config.project.accountActive.subject,
                        userEmail: dataObj.email,
                        siteName: sails.config.project.name,
                        base_url: sails.config.apiUrl,
                        url: sails.config.frontUrl+'/login',
                        name: dataObj.name,
                        curr_year: (new Date()).getFullYear()
                    };
                    EmailService.sendMail(sails.config.project.accountActive.template, templateData, function(err, message) {
                        if (err) {
                            console.log("error mail");
                            return mailCallback({message: err.message}, null);
                        }else {
                            updateArr.is_active_mail_sent = 1;
                            return mailCallback(null,dataObj);
                        }
                    });
                }
                else
                {
                    return mailCallback(null, {});
                }
            },
            function (secCallback) {
                if(dataObj)
                {
                    User.update(criteria, updateArr).exec(function(err, resObj){
                        if(err)
                        return secCallback({message: err.message}, null);
                        else{
                            userObj = resObj;
                            return secCallback(null, resObj);
                        }
                    });
                }
                else
                {
                    return secCallback({message: "User not found."}, null);
                }
            },
        ], function (err, data) {
            if (err) {
                return callback({message: err.message}, null);
            } else {
                return callback(null, userObj);
            }
        });
    },
    /*========== OLD DELETE FUNCTION ==========*/
    /*deleteUser: function(req, criteria, callback) {
        User.destroy(criteria).exec(function (err, resData){
            if(err)
            {
                return callback({message: err.message}, null);
            }
            else
            {
                return callback(null,resData);
            }
        });
    },*/
    /*========== END ==========*/

    deleteUser: function(req, criteria, callback) {
        let currDate = moment_tz().utc().format('YYYY-MM-DD HH:mm:ss');
        let shows;
        async.series([
            function (callback1) {
                Show.find().where({ show_end_date: { '>=': currDate }, artist: criteria.id }).sort('createdAt DESC').exec(function (err, foundShows) {
                    if (err) {
                        return callback1({ message: err.message }, null);
                    } else {
                        shows = foundShows;
                        return callback1(null, shows);
                    }
                });
            },
            function (callback2) {
                if(shows.length < 1){
                    User.update(criteria,{'is_delete':1}).exec(function (err, resData){
                        if(err)
                        {
                            return callback2({message: err.message}, null);
                        }
                        else
                        {
                            return callback2(null,resData);
                        }
                    });
                }else{
                    console.log("shows = ", shows.length);
                    return callback2({message: "You can not delete this user as One of the sessions of this user is not published yet."}, null);
                }
            }
        ], function (err, data) {
            if (err) {
                return callback({message: err.message}, null);
            } else {
                return callback(null);
            }
        });
    },

    deleteMedia: function(req, criteria, callback) {
        ArtistMedia.destroy(criteria).exec(function (err, resData){
            if(err)
            {
                return callback({message: err.message}, null);
            }
            else
            {
                return callback(null,resData);
            }
        });
    },

    followArtist: function(req,callback) {
        let data = req.body;
        let follower;
        let followData;
        var userData  = TokenService.decode(req.headers.authorization);
        criteria = {artist: req.param('id'), follower: userData.id};
        async.series([
            function (firstCallback) {
                Artistfollowers.findOne(criteria).exec(function(err, foundFollower){
                    if(err)
                        return firstCallback(err, null);
                    else
                    {
                        follower = foundFollower;
                        return firstCallback(null);
                    }
                });
            },
            function (secCallback) {
                if(follower) {
                    Artistfollowers.destroy(criteria).exec(function (err, resData){
                        if(err)
                            return secCallback({message: err.message}, null);
                        else{
                            return secCallback(null, {});
                        }
                    });
                } else {
                    insertData = {artist: req.param('id'), follower: userData.id};
                    Artistfollowers.create(insertData).exec(function(err, follow){
                        if (err) {
                            return secCallback({message: err.message}, null);
                        } else {
                            followData = follow;
                            return secCallback(null,follow);
                        }
                    });
                }
            },
        ], function (err, data) {
            if (err) {
                return callback({message: err.message}, null);
            } else {
                return callback(null, followData);
            }
        });
    },

    getFollowStatus: function(req, callback) {
        var userData  = TokenService.decode(req.headers.authorization);
        criteria = {artist: req.param('id'), follower: userData.id};
        Artistfollowers.findOne(criteria).exec(function (err, resData){
            if(err)
            {
                return callback({message: err.message}, null);
            }
            else
            {
                return callback(null,resData);
            }
        });
    },

    getFollowers: function(req ,callback) {
        var userData  = TokenService.decode(req.headers.authorization);
        let deleteUser = [0];
        let resData;
        async.series([
            function (callback1) {
                UserService.deleteuserID(req, function(err, resData){
                    if(err)
                        return callback1(err, null);
                    else
                    {
                        if(resData.length > 0){
                            deleteUser = resData;
                        }
                        return callback1(null, deleteUser);
                    }
                });
            },
            function (callback2) {
                Artistfollowers.find({artist: userData.id}).where({follower:{'!':deleteUser}}).populate(['follower']).sort('createdAt Desc').exec(function(err, foundUsers){
                    if (err) {
                        return callback2({message: err.message}, null);
                    } else {
                        resData = foundUsers;
                        return callback2(null,foundUsers);
                    }
                });
            }
        ],function(err, res) {
            if (err) {
                return callback({message: err.message}, null);
            } else {
                return callback(null,resData);
            }
        });
    },

    getFavorites: function(req ,callback) {
        var userData  = TokenService.decode(req.headers.authorization);
        let deleteUser = [0];
        let resData;
        async.series([
            function (callback1) {
                UserService.deleteuserID(req, function(err, resData){
                    if(err)
                        return callback1(err, null);
                    else
                    {
                        if(resData.length > 0){
                            deleteUser = resData;
                        }
                        return callback1(null, deleteUser);
                    }
                });
            },
            function (callback2) {
                Artistfollowers.find({follower: userData.id}).where({artist:{'!':deleteUser}}).populate(['artist']).sort('createdAt Desc').exec(function(err, foundUsers){
                    if (err) {
                        return callback2({message: err.message}, null);
                    } else {
                        resData = foundUsers;
                        return callback2(null,foundUsers);
                    }
                });
            }
        ], function(err, res) {
            if (err) {
                return callback({message: err.message}, null);
            } else {
                return callback(null,resData);
            }
        });
    },

    getCustomerShows: function(req,callback) {
        // var userData  = TokenService.decode(req.headers.authorization);
        // let shows;
        // let showids = [];
        // let upcomingshows;
        // let pastshows;
        // let currDate = moment_tz().utc().format('YYYY-MM-DD H:mm:ss');
        /*async.series([
          function (firstCallback) {
              ShowTicket.find({user: userData.id}).exec(function(err, foundShows){
                  if(err)
                    return firstCallback(err, null);
                  else
                  {
                    shows = foundShows;
                    return firstCallback(null);
                  }
              });
            },
          function (secCallback) {
            if(shows) {
              async.forEachOf(shows,function(indvShw,keyShw, eachCallback) {
                     showids.push(indvShw.show)
                     return eachCallback(null);
                  } ,function(err){
                  if(err)
                    return secCallback(err, null);
                  else
                    return secCallback(null,showids);
               });
              } else {
                return secCallback(null,showids);
              }
            },
          function (pastBack) {
            if(showids){
              Show.find().where({publish_date_utc:{'<=':currDate}, id: showids}).populate('artist').sort('createdAt DESC').exec(function(err, foundShows){
                  if (err) {
                    return pastBack({message: err.message}, null);
                  } else {
                    pastshows = foundShows;
                    return pastBack(null,foundShows);
                  }
              });
            }else {
              return pastBack(null,{});
            }
          },
          function (upcomeCall) {
            if(showids){
              Show.find().where({publish_date_utc:{'>':currDate}, id: showids}).populate('artist').sort('createdAt DESC').exec(function(err, foundShows){
                if (err) {
                  return upcomeCall({message: err.message}, null);
                } else {
                  upcomingshows = foundShows;
                  return upcomeCall(null,foundShows);
                }
              });
            } else {
              return upcomeCall(null,{});
            }
          }
        ], function (err, data) {
            if (err) {
                return callback({message: err.message}, null);
            } else {
                let res = {previousShow:pastshows, upcomingShow:upcomingshows};
                return callback(null, res);
            }
        });*/

        var userData  = TokenService.decode(req.headers.authorization);
        let upcomingshows = [];
        let pastshows = [];
        let currDate = moment_tz().utc().format('YYYY-MM-DD H:mm:ss');
        let shows;
        async.series([
            function (firstCallback) {
                ShowTicket.find().where({user: userData.id}).populate('show').sort('createdAt DESC').exec(function(err, foundShows){
                    if(err)
                    {
                        return firstCallback(err, null);
                    }
                    else
                    {
                        shows = foundShows;
                        return firstCallback(null);
                    }
                });
            },
            function (secCallback) {
                if(shows) {
                    async.forEachOf(shows,function(indvShw,keyShw, eachCallback) {
                        // console.log('indvShw.show',indvShw);
                        User.findOne({id: indvShw.show.artist}).exec(function(err, foundUser){
                            if (err)
                                return eachCallback({message: err.message});
                            else
                            {
                                ArtistTip.findOne({show:indvShw.show.id, tip_type:1}).sum('amount').exec(function(err, tipTotal){
                                    if(err)
                                        return eachCallback(err, null);
                                    else
                                    {

                                        let tip_total = 0.00;
                                        if (tipTotal.amount) {
                                            tip_total = tipTotal.amount;
                                        }
                                        shows[keyShw]['show']['tip_total'] = tip_total.toFixed(2);
                                        shows[keyShw]['show']['artist'] = foundUser;
                                        if(indvShw.show.publish_date_utc <= currDate){
                                            pastshows.push(indvShw);
                                        }else{
                                            upcomingshows.push(indvShw);
                                        }
                                        return eachCallback(null);
                                    }
                                });
                            }
                        });
                    } ,function(err){
                        if(err)
                            return secCallback(err, null);
                        else
                            return secCallback(null,{});
                    });
                } else {
                    return secCallback(null,{});
                }
            },
        ], function (err, data) {
            if (err) {
                return callback({message: err.message}, null);
            } else {
                let res = {previousShowTicket:pastshows, upcomingShowTicket:upcomingshows};
                return callback(null, res);
            }
        });
    },
    //backend apis
    getPurchases: function(data ,callback) {
        UserPurchaseHistory.find().populate(['user']).sort('createdAt Desc').exec(function(err, foundPurchases){
            if (err) {
                return callback({message: err.message}, null);
            } else {
                return callback(null,foundPurchases);
            }
        });
    },

    getMyPurchases: function(req ,callback) {
        var user  = TokenService.decode(req.headers.authorization);
        UserPurchaseHistory.find({user: user.id}).sort('createdAt Desc').exec(function(err, foundPurchases){
            if (err) {
                return callback({message: err.message}, null);
            } else {
                return callback(null,foundPurchases);
            }
        });
    },

    getMyTickets: function(req,callback) {
        var userData  = TokenService.decode(req.headers.authorization);
        let upcomingshows = [];
        let pastshows = [];
        let currDate = moment_tz().utc().format('YYYY-MM-DD H:mm:ss');
        let shows;
        async.series([
            function (firstCallback) {
                ShowTicket.find().where({user: userData.id}).populate('show').sort('createdAt DESC').exec(function(err, foundShows){
                    if(err)
                        return firstCallback(err, null);
                    else
                    {
                        shows = foundShows;
                        return firstCallback(null);
                    }
                });
            },
            function (secCallback) {
                if(shows) {
                    async.forEachOf(shows,function(indvShw,keyShw, eachCallback) {
                        User.findOne({id: indvShw.show.artist}).exec(function(err, foundUser){
                            if (err)
                                return eachCallback({message: "File thumb fail"});
                            else
                            {
                                shows[keyShw]['show']['artist'] = foundUser;
                                if(indvShw.show.publish_date_utc <= currDate){
                                    pastshows.push(indvShw);
                                }else{
                                    upcomingshows.push(indvShw);
                                }
                                return eachCallback(null);
                            }
                        });
                    } ,function(err){
                        if(err)
                            return secCallback(err, null);
                        else
                            return secCallback(null,{});
                    });
                } else {
                    return secCallback(null,{});
                }
            },
        ], function (err, data) {
            if (err) {
                return callback({message: err.message}, null);
            } else {
                let res = {previousShowTicket:pastshows, upcomingShowTicket:upcomingshows};
                return callback(null, res);
            }
        });
    },

    getUserProfile: function(req,callback) {
        var userObj  = TokenService.decode(req.headers.authorization);
        let user_id =  req.param('id');
        let userImages = [];
        let userVideos = [];

        async.parallel({
            userData:function (firstCallback) {
                User.findOne({id:user_id}).populate(['country', 'artistmedia']).exec(function(err, user){
                    if(err)
                        return firstCallback(err, null);
                    else
                    {
                        if (user.artistmedia.length > 0) {
                            _.each(user.artistmedia,function(media, i){
                                if (media.type == 1) {
                                    userImages.push(media);
                                } else {
                                    userVideos.push(media);
                                }
                                if (i+1 == user.artistmedia.length) {
                                    user.userImages = userImages;
                                    user.userVideos = userVideos;
                                    return firstCallback(null, user);
                                }
                            });
                        } else {
                            return firstCallback(null, user);
                        }
                    }
                });
            },
            followerCount:function (firstCallback) {
                Artistfollowers.count({artist:user_id}).exec(function(err, Totalfollower){
                    if(err)
                        return firstCallback(err, null);
                    else
                    {
                        return firstCallback(null,Totalfollower);
                    }
                });
            },
        }, function (err, data) {
            if (err) {
                return callback({message: err.message}, null);
            } else {
                return callback(null, data);
            }
        });
    },

    soundCheckData: function(req ,callback) {
        var user  = TokenService.decode(req.headers.authorization);
        User.findOne({id: user.id}).exec(function(err, userObj){
            if (err) {
                return callback({message: err.message}, null);
            } else {
                var stream = userObj.name.replace(/\s/g,'');
                var str_id = Number(userObj.id)+35;

                stream = stream+str_id;
                userObj.wowza_application = sails.config.wowza_application;
                userObj.wowza_sdp = sails.config.wowza_sdp;
                userObj.stream_name = stream;
                return callback(null,userObj);
            }
        });
    },

    goliveShow: function(req ,callback) {
        let currDate = moment_tz().utc().format('YYYY-MM-DD HH:mm:ss');
        var user  = TokenService.decode(req.headers.authorization);
        Show.findOne({show_end_date:{'>':currDate}, artist: user.id}).sort('publish_date ASC').exec(function(err, foundShow){
            if (err) {
                return callback({message: err.message}, null);
            } else {
                return callback(null,foundShow);
            }
        });
    },

    getPresenters: function(req,criteria,callback) {
        User.find(criteria).sort('createdAt ASC').exec(function(err, foundPresenters){
            if (err) {
                return callback({message: err.message}, null);
            } else {
                return callback(null,foundPresenters);
            }
        });
    },

    deleteuserID: function(req,callback){
        let deleteUser = [];
        criteria = {is_delete: 1};
        User.find({where: criteria,select: ['id']}).sort('createdAt ASC').exec(function(err, deletedId){
            if (err) {
                return callback({message: err.message}, null);
            } else {
                _.each(deletedId,function(user_id){
                    deleteUser.push(user_id.id);
                });
                return callback(null,deleteUser);
            }
        });
    },

    exportData: function(data ,criteria ,callback) {
        let resData;
        User.find({where:criteria, select: ['name','email']}).sort('createdAt Desc').exec(function(err, foundUsers){
            if (err) {
                return callback({message: err.message}, null);
            } else {
                return callback(null,foundUsers);
            }
        });
    },
}
