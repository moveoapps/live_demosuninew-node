// RoleService.js - in api/services
var moment = require('moment');
var moment_tz = require('moment-timezone');
var _ = require('underscore');
var request = require('request');
var generator = require('generate-password');
var Hashids = require('hashids');
const stripe = require("stripe")(sails.config.stripe_secret_key);


module.exports = {

    /***************************************************************************
    * Do not utilize this method unless you are positive that the web token   *
    * has not expired.  To ensure that the web token is valid, add the        *
    * `isAuthorized` policy to any controllers which will utilize this.
    ***************************************************************************/

    createAccount: function(request, userData, callback){
        let userObj;
        let accountObj;
        async.series([
            function (firstCallback) {
                User.findOne( {'id': userData.id}).populate('country').exec(function(err, foundUser){
                    if(err)
                    return firstCallback(err, null);
                    else
                    {
                        userObj = foundUser;
                        return firstCallback(null);
                    }
                });
            },
            function (secondCallback) {
                stripe.accounts.create({
                    type: 'custom',
                    //country: userObj.country.iso_code_2,
                    country: 'AU',
                    email: userData.email,
                    //requested_capabilities: ['card_payments']
                }, function(err, account) {
                    if (err) {
                        return callback({message: err.message}, null);
                    } else {
                        accountObj = account;
                        var postData = {stripe_account_id:account.id};
                        User.update({id:userData.id}, postData).exec(function(error, data){
                            if (error) {
                                return secondCallback({message: error.message}, null);
                            } else {
                                return secondCallback(null);
                            }
                        });
                    }
                });
            },
            function (thCallback) {
                if (accountObj) {
                    stripe.accounts.update(accountObj.id,
                        {
                            tos_acceptance: {
                                date: Math.floor(Date.now() / 1000),
                                ip: request.connection.remoteAddress // Assumes you're not using a proxy
                            }
                        }, function(err, account) {
                            if (err) {
                                return thCallback({message: err.message}, null);
                            } else {
                                thCallback(null);
                            }
                        });
                    } else {
                        thCallback(null);
                    }
                },
            ], function (err, data) {
                if (err) {
                    return callback({message: err.message}, null);
                } else {
                    return callback(null);
                }
            });
        },
        addBank: function(req,callback) {
            let data = req.body;
            let userData  = TokenService.decode(req.headers.authorization);
            let userObj;

            async.series([
                function (firstCallback) {
                    User.findOne( {'id': userData.id}).populate('country').exec(function(err, foundUser){
                        if(err)
                        return firstCallback(err, null);
                        else
                        {
                            userObj = foundUser;
                            return firstCallback(null);
                        }
                    });
                },
                function (secCallback) {
                    let externalObj = {
                        object: 'bank_account',
                        country: 'AU',
                        currency:sails.config.currency,
                        account_number:data.account_number
                    };
                    if (data.routing_number) {
                        externalObj.routing_number = data.routing_number;
                    }
                    stripe.accounts.update(userObj.stripe_account_id,
                        {
                            external_account: externalObj
                        }, function(err, account) {
                            if(err)
                            return secCallback({message: err.message}, null);
                            else
                            {
                                User.update({id:userData.id}, {stripe_bank_connected:1}).exec(function(error, data){
                                    if (error) {
                                        return secCallback({message: error.message}, null);
                                    } else {
                                        return secCallback(null);
                                    }
                                });
                            }
                        }
                    );

                }
            ], function (err, data) {
                if (err) {
                    return callback({message: err.message}, null);
                } else {
                    return callback(null);
                }
            });
        },
        createCharge: function(req, orderType, showData, callback) {
            let data = req.body;
            let userData  = TokenService.decode(req.headers.authorization);
            let userObj;
            let orderObj;
            let chargeObj;
            var subTotal = 0;
            var emoji_id;
            var description, item_name, payment_descriptor, return_url;
            if (orderType == 1 || orderType == 4) {
                subTotal = showData.price;

            } else if(orderType == 2 || orderType == 3) {
                emoji_id = req.body.emoji_id;
                subTotal = showData.tipping_amount;

            } else if (orderType == 5 || orderType == 6) {
                if (orderType == 5) {
                    subTotal = showData.price;
                } else {
                    subTotal = showData.ondemand_price;
                }
            }

            let tax = (((subTotal*sails.config.constants.stripe_pr_charge)/100)+sails.config.constants.stripe_fix_charge).toFixed(2);
            let subTotal_tax = (Number(subTotal) + Number(tax)).toFixed(2);
            tax = (((subTotal_tax*sails.config.constants.stripe_pr_charge)/100)+sails.config.constants.stripe_fix_charge).toFixed(2);
            let artistShare = (((subTotal*sails.config.constants.artist_share)/100) + Number(tax)).toFixed(2);
            let partnerShare = ((subTotal*sails.config.constants.partner_share)/100).toFixed(2);
            let grandTotal = (Number(subTotal) + Number(tax)).toFixed(2);
            var order_id = generator.generate({
                length: 10,
                numbers: true
            });
            var amount = Math.round(grandTotal*100);
            async.series([
                function (firstCallback) {
                    User.findOne( {'id': userData.id}).populate('country').exec(function(err, foundUser){
                        if(err)
                        return firstCallback(err, null);
                        else
                        {
                            userObj = foundUser;
                            return firstCallback(null);
                        }
                    });
                },
                function (secCallback) {
                    // Create a Charge:
                    stripe.charges.create({
                        amount: amount,
                        currency: sails.config.currency,
                        source: data.stripe_token,
                        transfer_group: order_id,
                    }).then(function(charge) {
                            chargeObj = charge;
                            return secCallback(null);
                    })
                    .catch((error) => {
                        console.log('error',error);
                        return secCallback(error, null);
                    });
                },
                function (thCallback){
                    var data = {order_id:chargeObj.transfer_group,amount:subTotal,
                        total_amount:grandTotal,
                        tax:tax,
                        show:showData.id,
                        user:userObj.id,
                        order_type:orderType,
                        status:chargeObj.status,
                        emoji:emoji_id};
                        if (orderType == 4) {
                            data.gift_email = req.body.email;
                        }
                        if (orderType == 6) {
                            data.ondemand = showData.id;
                            data.show = null;
                        }
                        ShowOrder.create(data).exec(function (err, orderData){
                            if(err)
                            return thCallback({message: err.message}, null);
                            else
                            {
                                orderObj = orderData;
                                return thCallback(null);
                            }
                        });
                }
            ], function (err, data) {
                if (err) {
                    return callback({message: err.message}, null);
                } else {
                    return callback(null,orderObj);
                }
            });
        },
        stripeOnboarding: function(req,callback) {
            let data = req.body;
            let userObj;
            let code = req.param('code');
            let user_encode = req.param('user_encode');
            var hashids = new Hashids(sails.config.project.name,10);
            let userId = hashids.decode(user_encode);
            console.log('userId',userId);
            async.series([
                function (firstCallback) {
                    User.findOne( {'id': userId}).exec(function(err, foundUser){
                        if(err)
                        return firstCallback(err, null);
                        else
                        {
                            userObj = foundUser;
                            return firstCallback(null);
                        }
                    });
                },
                function (secCallback) {
                    if (userObj) {
                        if (userObj.stripe_account_id) {
                            return callback(null);
                        } else {
                            var token_url = 'https://connect.stripe.com/oauth/token';
                            var postData = {'client_secret':sails.config.stripe_secret_key,'code':code, 'grant_type':'authorization_code'};
                            request.post({url:token_url, form: postData}, function(err, httpResponse, stripeBody){
                                if(err) {
                                    return secCallback(err.message, null);
                                }
                                else {
                                    var parseBody = JSON.parse(stripeBody);
                                    if (parseBody.error) {
                                        return secCallback({message: parseBody.error_description}, null);
                                    } else {
                                        User.update({id:userObj.id}, {stripe_account_id:parseBody.stripe_user_id}).exec(function(error, data){
                                            if (error) {
                                                return secCallback({message: error.message}, null);
                                            } else {
                                                return secCallback(null);
                                            }
                                        });
                                    }
                                }
                            });
                        }
                    } else {
                        return callback({message: 'Something went wrong. Please try again later.'}, null);
                    }

                }
            ], function (err, data) {
                if (err) {
                    return callback({message: err.message}, null);
                } else {
                    return callback(null);
                }
            });
        },
    }
