// UserService.js - in api/services
var moment_tz = require('moment-timezone');
var moment = require('moment');
var request = require('request');

module.exports = {

    /***************************************************************************
    * Do not utilize this method unless you are positive that the web token   *
    * has not expired.  To ensure that the web token is valid, add the        *
    * `isAuthorized` policy to any controllers which will utilize this.
    ***************************************************************************/

    getMyOnDemands: function(req, callback) {
        var userData  = TokenService.decode(req.headers.authorization);
        criteria = {artist:userData.id};
        OnDemand.find(criteria).sort('createdAt DESC').exec(function(err, foundOndemands){
            if (err) {
                return callback({message: err.message}, null);
            } else {
                return callback(null,foundOndemands);
            }
        });
    },
    createOnDemand: function(req,callback) {
        let data = req.body;
        var userData  = TokenService.decode(req.headers.authorization);
        let uploadFile = '';

        async.series([
            function (callOne) {
                req.setTimeout(480000);
                return callOne(null);
            },
            function (call1) {
                if (typeof req._fileparser != 'undefined' && req._fileparser.upstreams.length > 0){
                    console.log('media_name');
                    
                    var filename = req.file('media_name')._files[0].stream.filename;
                    var i = filename.lastIndexOf('.');
                    var ext = filename.substr(i);
                    uploadFile = new moment().valueOf()+ext;
                    UploadimageService.uploadVideo(req,"media_name",sails.config.s3bucket.ondemand, uploadFile, function(err, video){
                        if(err){
                            uploadFile = '';
                            return call1(err.message, null);
                        }
                        else{
                            console.log('media_name Done');

                            return call1(null);
                        }
                    });
                } else {
                    uploadFile = '';
                    return call1(null);
                }
            },
            /*function (call2) {
                UploadimageService.videoThumb(sails.config.s3bucket.ondemand,uploadFile,sails.config.s3bucket.ondemandthumb,function(err,thumbname){
                    if (err) {
                        return call2({message: "File thumb fail"});
                    } else {
                        return call2(null);
                    }
                });
            },*/
            function (call3) {
                if (typeof req._fileparser != 'undefined' && req._fileparser.upstreams.length > 0){
                    console.log('thumb_name');
                    
                    UploadimageService.updatePicture(req, 'thumb_name', sails.config.s3bucket.ondemandthumb , function(err,res){
                        if (err) {
                            return call3(err,null);
                        } else {
                            data.thumb_name = '';
                            if (res) {
                                data.thumb_name = res;
                            }
                            return call3(null);
                        }
                    });
                }else{
                    data.thumb_name = '';
                    return call3(null);
                }
            },
            function (secCall) {
                data.media_name = uploadFile;
                data.artist = userData.id;

                OnDemand.create(data).exec(function(err, ondemand){
                    if (err) {
                        return secCall({message: err.message}, null);
                    } else {
                        showData = ondemand;
                        return secCall(null, showData);
                    }
                });
            },
        ], function (err, res) {
            if (err) {
                return callback({message: err.message}, null);
            } else {
                return callback(null, showData)
            }
        });
    },
    updateOnDemand: function(req,callback) {
        let id =  req.param('id');
        let data = req.body;
        var userData  = TokenService.decode(req.headers.authorization);
        let uploadFile = '';
        let uploadThumb = '';
        async.series([
            function (callOne) {
                req.setTimeout(480000);
                return callOne(null);
            },
            function (cBack) {
                if (typeof req._fileparser != 'undefined' && req._fileparser.upstreams.length > 0){
                    if(req.file('media_name')._files[0]){
                        var filename = req.file('media_name')._files[0].stream.filename;
                        var i = filename.lastIndexOf('.');
                        var ext = filename.substr(i);
                        uploadFile = new moment().valueOf()+ext;
                        UploadimageService.uploadVideo(req,"media_name",sails.config.s3bucket.ondemand, uploadFile, function(err, video){
                            if(err){
                                uploadFile = '';
                                return cBack(err.message, null);
                            }
                            else{
                                return cBack(null);
                            }
                        });
                    }else{
                        uploadFile = '';
                        return cBack(null);
                    }
                } else {
                    uploadFile = '';
                    return cBack(null);
                }
            },
            /*function (thumbCallback) {
                if (uploadFile) {
                    UploadimageService.videoThumb(sails.config.s3bucket.ondemand,uploadFile,sails.config.s3bucket.ondemandthumb,function(err,thumbname){
                        if (err) {
                            return thumbCallback({message: "File thumb fail"});
                        } else {
                            return thumbCallback(null);
                        }
                    });
                } else {
                    return thumbCallback(null);
                }

            },*/
            function (call3) {
                if (typeof req._fileparser != 'undefined' && req._fileparser.upstreams.length > 0){
                    UploadimageService.updatePicture(req, 'thumb_name', sails.config.s3bucket.ondemandthumb , function(err,res){
                        if (err) {
                            return call3(err,null);
                        } else {
                            if (res) {
                                uploadThumb = res;
                            }
                            return call3(null);
                        }
                    });
                }else{
                    uploadThumb = '';
                    return call3(null);
                }
            },
            function (secCall) {
                if (uploadFile) {
                    data.media_name = uploadFile;
                }
                if (uploadThumb) {
                    console.log("Hii");
                    data.thumb_name = uploadThumb;
                }

                data.artist = userData.id;
                //console.log('data.publish_date',data.publish_date);
                OnDemand.update({id:id}, data).exec(function(err, ondemand){
                    if (err) {
                        return secCall({message: err.message}, null);
                    } else {
                        showData = ondemand;
                        return secCall(null, showData);
                    }
                });
            },
        ], function (err, res) {
            if (err) {
                return callback({message: err.message}, null);
            } else {
                return callback(null, showData)
            }
        });
    },
    deleteOnDemand: function(req, criteria, callback) {
      OnDemand.destroy(criteria).exec(function (err, resData){
        if(err)
            return callback({message: err.message}, null);
        else
        {
          return callback(null,resData);
        }
      });
    },

    getVods: function(req, data_limit = null, callback) {
        var userData  = TokenService.decode(req.headers.authorization);
        let currDate = moment_tz().utc().format('YYYY-MM-DD HH:mm:ss');
        let vod_start_date = '2019-02-20 12:00:00'; // Implemented VOD on this date
        let deleteUser = [0];
        let showvod;
        let staticvod;
        let show_vod_len;
        // let genre_id = req.param('genre_id');
        let genre_id;
        let artist_id = req.param('artist_id');

        async.series([
            function (call1){
                UserService.deleteuserID(req, function(err, resData){
                    if(err)
                        return call1(err, null);
                    else
                    {
                        if(resData.length > 0){
                            deleteUser = resData;
                        }
                        return call1(null, deleteUser);
                    }
                });
            },
            function (call2) {
                if(req.param('genre_id')){
                    Genre.findOne({slug: req.param('genre_id')}).exec(function(err, slugGenre){
                        if (err) {
                            return call2({message: err.message}, null);
                        } else {
                            genre_id = slugGenre.id;
                            return call2(null,genre_id);
                        }
                    });
                }else{
                    return call2(null,genre_id);
                }
            },
            function (cBack) {
                let criteria = {show_end_date:{'<':currDate},publish_date:{'>':vod_start_date}, status: 'Published'};

                sqlQuery = 'SELECT S.*,U.name as artist_name, U.profile_picture,U.id as user_id FROM shows as S \
                INNER JOIN users as U ON U.id = S.artist \
                INNER JOIN showgenre as SG ON SG.show = S.id \
                WHERE S.show_end_date < "'+currDate+'" AND S.publish_date > "'+vod_start_date+'" AND S.status = "Published"';

                if (genre_id) {
                    sqlQuery +=' AND SG.genre = "'+genre_id+'"';
                }
                if (artist_id) {
                    sqlQuery +=' AND S.artist = "'+artist_id+'"';
                }

                sqlQuery +='AND S.artist NOT IN ('+deleteUser+')';
                sqlQuery +=' GROUP BY S.id ORDER BY publish_date DESC ';

                if (data_limit > 0) {
                    sqlQuery +=' LIMIT ' + data_limit;
                    //var query = Show.find(criteria).populate(['artist']).sort('publish_date ASC').limit(5);
                } else {
                    //var query = Show.find(criteria).populate(['artist']).sort('publish_date ASC');
                }

                //query.exec(function(err, resShows){
                Show.query(sqlQuery ,function(err, resShows) {
                    if(err){
                        return cBack(err, null);
                    }else {
                        async.forEachOf(resShows,function(indvShw,keyShw, eachCallback) {
                            let vod_pay = 0;
                            let vod_free = 1;
                            if (userData) {
                                VodPayment.findOne({show:indvShw.id, user:userData.id}).exec(function(err, vodPay){
                                    if (err) {
                                        return eachCallback({message: err.message}, null);
                                    } else {

                                        if (vodPay) {
                                            vod_pay = 1;
                                        }

                                        if(indvShw.artist == userData.id)
                                        {
                                            indvShw.vod_pay = vod_free;
                                        }else{
                                            if(indvShw.is_paid == 1){
                                                indvShw.vod_pay = vod_pay;
                                            }else {
                                                indvShw.vod_pay = vod_free;
                                            }
                                            // indvShw.vod_pay = vod_pay;
                                        }
                                        indvShw.uid = 'show_'+indvShw.id;
                                        User.findOne({id:indvShw.artist}).exec(function(err, userObj){
                                            if (err) {
                                                return eachCallback({message: err.message}, null);
                                            } else {
                                                indvShw.artist = userObj;
                                                return eachCallback(null);
                                            }
                                        });
                                    }
                                });
                            } else {
                                if(indvShw.is_paid == 1){
                                    indvShw.vod_pay = vod_pay;
                                }else {
                                    indvShw.vod_pay = vod_free;
                                }
                                // indvShw.vod_pay = vod_pay;
                                User.findOne({id:indvShw.artist}).exec(function(err, userObj){
                                    if (err) {
                                        return eachCallback({message: err.message}, null);
                                    } else {
                                        indvShw.artist = userObj;
                                        return eachCallback(null);
                                    }
                                });
                                // return eachCallback(null);
                            }
                            indvShw.vod_type = 1;
                            indvShw.vod_price = indvShw.ondemand_price;

                        } ,function(err){
                            if(err){
                                return cBack(err, null);
                            } else {
                                showvod = resShows;
                                show_vod_len = showvod.length;
                                return cBack(null);
                            }
                        });

                    }
                });
            },
            function (thumbCallback) {
                criteria = {artist:{'!':deleteUser}};
                if (genre_id) {
                    criteria.genre = genre_id;
                }
                if (artist_id) {
                    criteria.artist = artist_id;
                }
                var query = OnDemand.find(criteria).sort('createdAt DESC').populate(['artist']);
                if (show_vod_len < data_limit) {
                    var len = data_limit - show_vod_len;
                    var query = OnDemand.find(criteria).sort('createdAt DESC').populate(['artist']).limit(len);
                }
                // console.log("query = ", criteria);
                query.exec(function(err, foundOndemands){
                    if (err) {
                        return thumbCallback({message: err.message}, null);
                    } else {
                        async.forEachOf(foundOndemands,function(ondemand,keyondemand, eachCallback) {
                            let vod_pay = 0;
                            let vod_free = 1;
                            ondemand.vod_type = 2;
                            ondemand.vod_price = ondemand.ondemand_price;
                            if (userData) {
                                VodPayment.findOne({ondemand:ondemand.id, user:userData.id}).exec(function(err, vodPay){
                                    if (err) {
                                        return eachCallback({message: err.message}, null);
                                    }
                                    else
                                    {
                                        if (vodPay) {
                                            vod_pay = 1;
                                        }

                                        if(ondemand.artist.id == userData.id)
                                        {
                                            // Remove the payment for a presenters original content
                                            ondemand.vod_pay = vod_free;
                                        }
                                        else
                                        {
                                            if(ondemand.is_paid == 1){
                                                ondemand.vod_pay = vod_pay;
                                            }else {
                                                ondemand.vod_pay = vod_free;
                                            }
                                        }

                                        ondemand.artist_name = ondemand.artist.name;
                                        ondemand.uid = 'ondemand_'+ondemand.id;
                                        return eachCallback(null);
                                    }
                                });
                            } else {
                                if(ondemand.is_paid == 1){
                                    ondemand.vod_pay = vod_pay;
                                }else {
                                    ondemand.vod_pay = vod_free;
                                }
                                return eachCallback(null);
                            }
                        } ,function(err){
                            if(err){
                                return thumbCallback(err, null);
                            } else {
                                staticvod = foundOndemands;
                                return thumbCallback(null);
                            }
                        });
                    }
                });
            },
        ], function (err, res) {
            if (err) {
                return callback({message: err.message}, null);
            } else {
                var vod_link = 'https://streaming.rendezvous.live/Demosuni_VOD';
                if (data_limit > 0) {
                    if (show_vod_len == data_limit) {
                        var finalArr = showvod;
                    } else {
                        var finalArr = showvod.concat(staticvod);
                    }
                } else {
                    var finalArr = showvod.concat(staticvod);
                }

                var fullArry = {'shows':finalArr, 'vod_link':vod_link};

                return callback(null, fullArry);
            }
        });
    },
    getFeaturedVods: function(req, data_limit = null, callback) {
        var userData  = TokenService.decode(req.headers.authorization);
        let currDate = moment_tz().utc().format('YYYY-MM-DD HH:mm:ss');
        let vod_start_date = '2019-02-20 12:00:00'; // Implemented VOD on this date
        let deleteUser = [0];
        let show_vod_len;
        let staticvod;
        let showvod;

        async.series([
            function (call1){
                UserService.deleteuserID(req, function(err, resData){
                    if(err)
                        return call1(err, null);
                    else
                    {
                        if(resData.length > 0){
                            deleteUser = resData;
                        }
                        return call1(null, deleteUser);
                    }
                });
            },
            function (call2) {

                let sqlQuery = 'SELECT S.*,U.name as artist_name, U.profile_picture,U.id as user_id FROM shows as S \
                    INNER JOIN users as U ON U.id = S.artist \
                    INNER JOIN showgenre as SG ON SG.show = S.id \
                    WHERE S.show_end_date < "'+currDate+'" AND S.publish_date > "'+vod_start_date+'" AND S.is_featured = "1" AND S.status = "Published"';

                sqlQuery +='AND S.artist NOT IN ('+deleteUser+')';
                sqlQuery +=' GROUP BY S.id ORDER BY publish_date DESC ';
                if (data_limit > 0) {
                    sqlQuery +=' LIMIT ' + data_limit;
                }

                Show.query(sqlQuery ,function(err, resShows) {
                    if(err){
                        return call2(err, null);
                    }else {
                        async.forEachOf(resShows,function(indvShw,keyShw, eachCallback) {
                            let vod_pay = 0;
                            let vod_free = 1;
                            if (userData) {
                                VodPayment.findOne({show:indvShw.id, user:userData.id}).exec(function(err, vodPay){
                                    if (err) {
                                        return eachCallback({message: err.message}, null);
                                    } else {

                                        if (vodPay) {
                                            vod_pay = 1;
                                        }
                                        if(indvShw.artist == userData.id)
                                        {
                                            indvShw.vod_pay = vod_free;
                                        }else{
                                            if(indvShw.is_paid == 1){
                                                indvShw.vod_pay = vod_pay;
                                            }else {
                                                indvShw.vod_pay = vod_free;
                                            }
                                        }
                                        indvShw.uid = 'show_'+indvShw.id;
                                        User.findOne({id:indvShw.artist}).exec(function(err, userObj){
                                            if (err) {
                                                return eachCallback({message: err.message}, null);
                                            } else {
                                                indvShw.artist = userObj;
                                                return eachCallback(null);
                                            }
                                        });
                                    }
                                });
                            } else {
                                if(indvShw.is_paid == 1){
                                    indvShw.vod_pay = vod_pay;
                                }else {
                                    indvShw.vod_pay = vod_free;
                                }
                                User.findOne({id:indvShw.artist}).exec(function(err, userObj){
                                    if (err) {
                                        return eachCallback({message: err.message}, null);
                                    } else {
                                        indvShw.artist = userObj;
                                        return eachCallback(null);
                                    }
                                });
                            }
                            indvShw.vod_type = 1;
                            indvShw.vod_price = indvShw.ondemand_price;
                        } ,function(err){
                            if(err){
                                return call2(err, null);
                            } else {
                                showvod = resShows;
                                show_vod_len = showvod.length;
                                return call2(null);
                            }
                        });
                    }
                });
            },
            function (call3) {
                if (show_vod_len < data_limit) {
                    var len = data_limit - show_vod_len;
                    var query = OnDemand.find({is_featured: 1}).where({artist:{'!':deleteUser}}).populate(['artist', 'genre']).sort('createdAt ASC').limit(len);
                }
                var query = OnDemand.find({is_featured: 1}).where({artist:{'!':deleteUser}}).populate(['artist', 'genre']).sort('createdAt ASC');
                query.exec(function(err, foundOndemands){
                    if (err) {
                        return call3({message: err.message}, null);
                    } else {
                        async.forEachOf(foundOndemands,function(ondemand,keyondemand, eachCallback) {
                            let vod_pay = 0;
                            let vod_free = 1;
                            ondemand.vod_type = 2;
                            ondemand.vod_price = ondemand.ondemand_price;
                            if (userData) {
                                VodPayment.findOne({ondemand:ondemand.id, user:userData.id}).exec(function(err, vodPay){
                                    if (err) {
                                        return eachCallback({message: err.message}, null);
                                    }
                                    else
                                    {
                                        if (vodPay) {
                                            vod_pay = 1;
                                        }
                                        if(ondemand.artist.id == userData.id){
                                            ondemand.vod_pay = vod_free;
                                        }else{
                                            if(ondemand.is_paid == 1){
                                                ondemand.vod_pay = vod_pay;
                                            }else {
                                                ondemand.vod_pay = vod_free;
                                            }
                                        }
                                        ondemand.artist_name = ondemand.artist.name;
                                        ondemand.uid = 'ondemand_'+ondemand.id;
                                        return eachCallback(null);
                                    }
                                });
                            } else {
                                if(ondemand.is_paid == 1){
                                    ondemand.vod_pay = vod_pay;
                                }else {
                                    ondemand.vod_pay = vod_free;
                                }
                                return eachCallback(null);
                            }
                        } ,function(err){
                            if(err){
                                return call3(err, null);
                            } else {
                                staticvod = foundOndemands;
                                return call3(null);
                            }
                        });
                    }
                });
            },
        ], function (err, res) {
            if (err) {
                return callback({message: err.message}, null);
            } else {
                var vod_link = 'https://streaming.rendezvous.live/Demosuni_VOD';
                if (data_limit > 0) {
                    if (show_vod_len == data_limit) {
                        var finalArr = showvod;
                    } else {
                        var finalArr = showvod.concat(staticvod);
                    }
                } else {
                    var finalArr = showvod.concat(staticvod);
                }
                var fullArry = {'vods':finalArr, 'vod_link':vod_link};
                return callback(null, fullArry);
            }
        });
    },
    /*getFeaturedSearch: function(req, callback) {
        var userData  = TokenService.decode(req.headers.authorization);
        let currDate = moment_tz().utc().format('YYYY-MM-DD HH:mm:ss');
        let vod_start_date = '2019-02-20 12:00:00';
        let staticvod;
        let deleteUser = [0];

        let ss = req.param('ss');
        var sort = req.param('sort');

        let order_by = '';
        if(sort)
        {
            if(sort == '1'){
                order_by = { title: 'ASC' };
            }else if(sort == '2'){
                order_by = { title: 'DESC' };
            }else if(sort == '3'){
                order_by = { id: 'DESC' };
            }else{
                order_by = { id: 'DESC' };
            }
        }

        async.series([
            function (call1){
                UserService.deleteuserID(req, function(err, resData){
                    if(err)
                        return call1(err, null);
                    else
                    {
                        if(resData.length > 0){
                            deleteUser = resData;
                        }
                        return call1(null, deleteUser);
                    }
                });
            },
            function (call2) {
                criteria = {artist:{'!':deleteUser}, is_featured: 1};

                var query = OnDemand.find(criteria).where({ 'title' : { contains : ss } }).sort(order_by).populate(['artist','genre']);

                query.exec(function(err, foundOndemands){
                    if (err) {
                        return call2({message: err.message}, null);
                    } else {
                        async.forEachOf(foundOndemands,function(ondemand,keyondemand, eachCallback) {
                            let vod_pay = 0;
                            let vod_free = 1;
                            ondemand.vod_type = 2;
                            ondemand.vod_price = ondemand.ondemand_price;
                            if (userData) {
                                VodPayment.findOne({ondemand:ondemand.id, user:userData.id}).exec(function(err, vodPay){
                                    if (err) {
                                        return eachCallback({message: err.message}, null);
                                    } else {
                                        if (vodPay) {
                                            vod_pay = 1;
                                        }

                                        if(ondemand.artist.id == userData.id)
                                        {
                                            ondemand.vod_pay = vod_free;
                                        }else {
                                            if(ondemand.is_paid == 1){
                                                ondemand.vod_pay = vod_pay;
                                            }else {
                                                ondemand.vod_pay = vod_free;
                                            }
                                        }

                                        ondemand.artist_name = ondemand.artist.name;
                                        ondemand.uid = 'ondemand_'+ondemand.id;
                                        return eachCallback(null);
                                    }
                                });
                            } else {
                                if(ondemand.is_paid == 1){
                                    ondemand.vod_pay = vod_pay;
                                }else {
                                    ondemand.vod_pay = vod_free;
                                }
                                return eachCallback(null);
                            }
                        } ,function(err){
                            if(err){
                                return call2(err, null);
                            } else {
                                staticvod = foundOndemands;
                                return call2(null);
                            }
                        });
                    }
                });
            },
        ], function (err, res) {
            if (err) {
                return callback({message: err.message}, null);
            } else {
                var vod_link = 'https://streaming.rendezvous.live/Demosuni_VOD';
                var finalArr = staticvod;
                var fullArry = {'vods':finalArr, 'vod_link':vod_link};
                return callback(null, fullArry);
            }
        });
    },*/

    vodPayment: function(req, callback) {
        let data = req.body;
        var userObj  = TokenService.decode(req.headers.authorization);
        let order_type;
        let resData;
        let parseBody;

        async.series([
            /*function (firstCallbackBack) {
                User.findOne({id:userObj.id}).exec(function(err, foundUser){
                    if (err) {
                        return firstCallbackBack({message: err.message}, null);
                    } else {
                        userData = foundUser;
                        return firstCallbackBack(null);
                    }
                });
            },*/
            function (firstCallbackBack) {
                var query;
                if (data.vod_type == 1) {
                    query = Show.findOne({id:data.id}).populate(['timezone','artist']);
                    order_type = 5;
                } else {
                    query = OnDemand.findOne({id:data.id}).populate(['artist']);
                    order_type = 6;
                }

                query.exec(function(err, showObj){
                    if (err) {
                        return firstCallbackBack({message: err.message}, null);
                    } else {
                        resData = showObj;
                        return firstCallbackBack(null);
                    }
                });
            },
            function (secCall) {
                var postData = {grant_type:'client_credentials'};
                var token_url = sails.config.api_url+'/v1/oauth2/token';
                var auth = "Basic " +new Buffer(sails.config.client_id+":"+sails.config.secret_key).toString('base64');

                request.post({url:token_url, form: postData, headers : {"Authorization" : auth}}, function(err, httpResponse, paypalBody){
                    if(err){
                      return secCall({message: err.message}, null);
                    }
                    else{
                      parseBody = JSON.parse(paypalBody);
                      //console.log('tokeon==',parseBody.access_token);
                      return secCall(null);
                    }
                });
            },
            function (thCallback) {
                PaypalService.createPayPalOrder(req, parseBody, resData, userObj, order_type, function(err, orderData){
                    if(err){
                        return thCallback({message: err.message}, null);
                    } else {
                        console.log('PayPal Order Created');
                        orderObj = orderData;
                        return thCallback(null, orderObj);
                    }
                });
            }
        ], function (err, res) {
            if (err) {
              return callback({message: err.message}, null);
            } else {
              return callback(null, orderObj)
            }
        });
    },
    vodPaymentFinal: function(req, callback) {
      let data = req.body;
      payment_type  = 4;
      var userObj  = TokenService.decode(req.headers.authorization);
      if (data.vod_type == 1) {
        order_type = 5;
      } else {
        order_type = 6;
      }

      let userData;
      let showData;
      let tipping_amount;
      let parseBody;
      let orderData;

      async.series([
        function (firstCallbackBack) {
            var criteria = {order_id:data.order_id, user:userObj.id, order_type:order_type};
            if (order_type == 5) {
                criteria.show = data.id;
            } else {
                criteria.ondemand = data.id;
            }
          ShowOrder.findOne(criteria).exec(function(err, orderObj){
            if (err) {
              return firstCallbackBack({message: err.message}, null);
            } else {
              if (!orderObj || orderObj == 'undefined') {
                return callback({message: 'Something went wrong. Please try again later.'}, null); // Stop the process  and directly return to the main function
              } else {
                orderData = orderObj;
                return firstCallbackBack(null);
              }
            }
          });
        },
        function (firstCallbackBack) {
            var criteria = {order_id:orderData.id, user:userObj.id}
            if (order_type == 5) {
                criteria.show = data.id;
            } else {
                criteria.ondemand = data.id;
            }
          VodPayment.findOne(criteria).exec(function(err, ticketObj){
            if (err) {
              return firstCallbackBack({message: err.message}, null);
            } else {
              if (!ticketObj || ticketObj == 'undefined') {
                return firstCallbackBack(null);
              } else {
                return callback({message: 'Something went wrong. Please try again later.'}, null); // Stop the process  and directly return to the main function
              }
            }
          });
        },
        function (firstCallbackBack) {
          User.findOne({id:userObj.id}).exec(function(err, foundUser){
            if (err) {
              return firstCallbackBack({message: err.message}, null);
            } else {
              userData = foundUser;
              return firstCallbackBack(null);
            }
          });
        },
        // function (firstCallbackBack) {
        //   Show.findOne({id:data.show_id}).populate(['timezone','artist']).exec(function(err, showObj){
        //     if (err) {
        //       return firstCallbackBack({message: err.message}, null);
        //     } else {
        //       showData = showObj;
        //       return firstCallbackBack(null);
        //     }
        //   });
        // },
        function (secCall) {
          var postData = {grant_type:'client_credentials'};
          var token_url = sails.config.api_url+'/v1/oauth2/token';
          var auth = "Basic " +new Buffer(sails.config.client_id+":"+sails.config.secret_key).toString('base64');

          request.post({url:token_url, form: postData, headers : {"Authorization" : auth}}, function(err, httpResponse, paypalBody){
            if(err){
              return secCall({message: err.message}, null);
            }
            else{
              parseBody = JSON.parse(paypalBody);
              return secCall(null);
            }
          });
        },
        function (thCallback) {
          PaypalService.payPayPalOrder(req, parseBody, data.order_id, function(err, body){
            if(err){
              return thCallback({message: err.message}, null);
            } else {

              var post_data = {user: userObj.id,amount: orderData.total_amount, order_id: data.order_id, status: body.status, payment_date: body.create_time, payment_type: payment_type};

              UserPurchaseHistory.create(post_data).exec(function(err, show){
              });

              var post_data = {user: userData.id,order_id:orderData.id, amount: orderData.total_amount,'vod_type':data.vod_type};
              if (order_type == 5) {
                  post_data.show = data.id;
              } else {
                  post_data.ondemand = data.id;
              }
              VodPayment.create(post_data).exec(function(err, vodPayment){
                if (err) {
                  return thCallback({message: err.message}, null);
                } else {
                  return thCallback(null);
                }
              });
            }
          });
        }
      ], function (err, res) {
        if (err) {
          return callback({message: err.message}, null);
        } else {
          return callback(null, orderData)
        }
      });
    },
    vodPaymentStripe: function(req, callback) {
      let data = req.body;
      payment_type  = 4;
      var userObj  = TokenService.decode(req.headers.authorization);
      if (data.vod_type == 1) {
        order_type = 5;
      } else {
        order_type = 6;
      }

      let userData;
      let showData;
      let tipping_amount;
      let parseBody;
      let orderData;

      async.series([

        function (firstCallbackBack) {
            var criteria = { user:userObj.id}
            if (order_type == 5) {
                criteria.show = data.id;
            } else {
                criteria.ondemand = data.id;
            }
          VodPayment.findOne(criteria).exec(function(err, ticketObj){
            if (err) {
              return firstCallbackBack({message: err.message}, null);
            } else {
              if (!ticketObj || ticketObj == 'undefined') {
                return firstCallbackBack(null);
              } else {
                return callback({message: 'Something went wrong. Please try again later.'}, null); // Stop the process  and directly return to the main function
              }
            }
          });
        },
        function (firstCallbackBack) {
            var query;
            if (data.vod_type == 1) {
                query = Show.findOne({id:data.id}).populate(['timezone','artist']);
                order_type = 5;
            } else {
                query = OnDemand.findOne({id:data.id}).populate(['artist']);
                order_type = 6;
            }

          query.exec(function(err, showObj){
            if (err) {
              return firstCallbackBack({message: err.message}, null);
            } else {
              showData = showObj;
              return firstCallbackBack(null);
            }
          });
        },
        function (firstCallbackBack) {
          User.findOne({id:userObj.id}).exec(function(err, foundUser){
            if (err) {
              return firstCallbackBack({message: err.message}, null);
            } else {
              userData = foundUser;
              return firstCallbackBack(null);
            }
          });
        },
        function (thCallback) {
            StripeService.createCharge(req, order_type, showData, function (err, res) {
                if (err) {
                    return thCallback({ message: err.message }, null);
                } else {
                    orderData = res;
                    return thCallback(null);
                }
            });
        },
        function (thCallback) {
            var post_data = {user: userObj.id,amount: orderData.total_amount, order_id: orderData.order_id, status: orderData.status, payment_date: orderData.createdAt, payment_type: payment_type};

            UserPurchaseHistory.create(post_data).exec(function (err, show) {
                if (err) {
                    return thCallback({ message: err.message }, null);
                } else {
                    return thCallback(null);
                }
            });
        },
        function (thCallback) {

            var post_data = {user: userData.id,order_id:orderData.id, amount: orderData.total_amount,'vod_type':data.vod_type};
            if (order_type == 5) {
                post_data.show = data.id;
            } else {
                post_data.ondemand = data.id;
            }
            VodPayment.create(post_data).exec(function(err, vodPayment){
              if (err) {
                return thCallback({message: err.message}, null);
              } else {
                return thCallback(null);
              }
            });
        },
      ], function (err, res) {
        if (err) {
          return callback({message: err.message}, null);
        } else {
          return callback(null, orderData)
        }
      });
    },
    getOnDemand: function(req,callback) {
        let deleteUser = [0];
        let resData;

        async.series([
            function (call1){
                UserService.deleteuserID(req, function(err, resData){
                    if(err)
                        return call1(err, null);
                    else
                    {
                        if(resData.length > 0){
                            deleteUser = resData;
                        }
                        return call1(null, deleteUser);
                    }
                });
            },
            function (call2) {
                criteria = {artist:{'!':deleteUser}};

                OnDemand.find(criteria).sort('createdAt DESC').populate(['artist','genre']).exec(function(err, foundOndemands){
                    if (err) {
                        return call2({message: err.message}, null);
                    } else {
                        resData = foundOndemands;
                        return call2(null,foundOndemands);
                    }
                });
            },
        ], function (err, res) {
            if (err) {
                return callback({message: err.message}, null);
            } else {
                return callback(null, resData);
            }
        });
    },
    updateFeatured: function(req,callback) {
        let id =  req.param('id');
        let data = req.body;
        var userData  = TokenService.decode(req.headers.authorization);

        OnDemand.update({id:id}, data).exec(function(err, ondemand){
            if (err) {
                return callback({message: err.message}, null);
            } else {
                return callback(null, ondemand);
            }
        });
    },
    admingetVod: function(req,criteria,callback) {
        OnDemand.findOne(criteria).exec(function(err, foundOndemands){
            if (err) {
                return callback({message: err.message}, null);
            } else {
                return callback(null,foundOndemands);
            }
        });
    },
    addVod: function(req,callback) {
        let data = req.body;
        var userData  = TokenService.decode(req.headers.authorization);
        let uploadFile = '';
        let uploadThumb = '';

        async.series([
            function (callOne) {
                req.setTimeout(480000);
                return callOne(null);
            },
            function (cBack) {
                if (typeof req._fileparser != 'undefined' && req._fileparser.upstreams.length > 0){
                    if(req.file('media_name')._files[0]){
                        var filename = req.file('media_name')._files[0].stream.filename;
                        var i = filename.lastIndexOf('.');
                        var ext = filename.substr(i);
                        uploadFile = new moment().valueOf()+ext;
                        UploadimageService.uploadVideo(req,"media_name",sails.config.s3bucket.ondemand, uploadFile, function(err, video){
                            if(err){
                                uploadFile = '';
                                return cBack(err.message, null);
                            }
                            else{
                                return cBack(null);
                            }
                        });
                    }else{
                        uploadFile = '';
                        return cBack(null);
                    }
                } else {
                    uploadFile = '';
                    return cBack(null);
                }
            },
            function (thumbCallback) {
                if (typeof req._fileparser != 'undefined' && req._fileparser.upstreams.length > 0){
                    if(req.file('thumb_name')._files[0]){
                        /*UploadimageService.updatePicture(req, 'thumb_name', sails.config.s3bucket.ondemandthumb , function(err,res){
                            if (err) {
                                return thumbCallback(err,null);
                            } else {
                                if (res) {
                                    uploadThumb = res;
                                }
                                return thumbCallback(null);
                            }
                        });*/
                        var filename = req.file('thumb_name')._files[0].stream.filename;
    					var i = filename.lastIndexOf('.');
    					var ext = filename.substr(i);

    					uploadThumb = new moment().valueOf()+ext;
    					UploadimageService.uploadImage(req, 'thumb_name', sails.config.s3bucket.ondemandthumb, uploadThumb, function(err, image){
    						if(err){
    							return thumbCallback(err.message, null);
    						}
    						else{
    							return thumbCallback(null);
    						}
    					});
                    }else{
                        uploadThumb = '';
                        return thumbCallback(null);
                    }
                }else{
                    uploadThumb = '';
                    return thumbCallback(null);
                }
            },
            function (secCall) {
                data.media_name = uploadFile;
                data.thumb_name = uploadThumb;
                OnDemand.create(data).exec(function(err, ondemand){
                    if (err) {
                        return secCall({message: err.message}, null);
                    } else {
                        showData = ondemand;
                        return secCall(null, showData);
                    }
                });
            },
        ], function (err, res) {
            if (err) {
                return callback({message: err.message}, null);
            } else {
                return callback(null, showData)
            }
        });
    },
    updateVod: function(req,callback) {
        let id =  req.param('id');
        let data = req.body;
        var userData  = TokenService.decode(req.headers.authorization);
        let uploadFile = '';
        let uploadThumb = '';

        async.series([
            function (callOne) {
                req.setTimeout(480000);
                return callOne(null);
            },
            function (cBack) {
                if (typeof req._fileparser != 'undefined' && req._fileparser.upstreams.length > 0){
                    if(req.file('media_name')._files[0]){
                        var filename = req.file('media_name')._files[0].stream.filename;
                        var i = filename.lastIndexOf('.');
                        var ext = filename.substr(i);
                        uploadFile = new moment().valueOf()+ext;
                        UploadimageService.uploadVideo(req,"media_name",sails.config.s3bucket.ondemand, uploadFile, function(err, video){
                            if(err){
                                uploadFile = '';
                                return cBack(err.message, null);
                            }
                            else{
                                return cBack(null);
                            }
                        });
                    } else {
                        uploadFile = '';
                        return cBack(null);
                    }
                } else {
                    uploadFile = '';
                    return cBack(null);
                }
            },
            function (thumbCallback) {
                /*if (uploadFile) {
                    UploadimageService.videoThumb(sails.config.s3bucket.ondemand,uploadFile,sails.config.s3bucket.ondemandthumb,function(err,thumbname){
                        if (err) {
                            return thumbCallback({message: "File thumb fail"});
                        } else {
                            return thumbCallback(null);
                        }
                    });
                } else {
                    return thumbCallback(null);
                }*/
                if (typeof req._fileparser != 'undefined' && req._fileparser.upstreams.length > 0){
                    if(req.file('thumb_name')._files[0]){
                        var filename = req.file('thumb_name')._files[0].stream.filename;
    					var i = filename.lastIndexOf('.');
    					var ext = filename.substr(i);

    					uploadThumb = new moment().valueOf()+ext;
    					UploadimageService.uploadImage(req, 'thumb_name', sails.config.s3bucket.ondemandthumb, uploadThumb, function(err, image){
    						if(err){
    							console.log("error =1= ", err);
    							return thumbCallback(err.message, null);
    						}
    						else{
    							return thumbCallback(null);
    						}
    					});
                    }else{
                        uploadThumb = '';
                        return thumbCallback(null);
                    }
                }else{
                    uploadThumb = '';
                    return thumbCallback(null);
                }
            },
            function (secCall) {
                if (uploadFile) {
                    data.media_name = uploadFile;
                }
                if (uploadThumb) {
                    data.thumb_name = uploadThumb;
                }

                OnDemand.update({id:id}, data).exec(function(err, ondemand){
                    if (err) {
                        return secCall({message: err.message}, null);
                    } else {
                        showData = ondemand;
                        return secCall(null, showData);
                    }
                });
            },
        ], function (err, res) {
            if (err) {
                return callback({message: err.message}, null);
            } else {
                return callback(null, showData)
            }
        });
    },
    getFeaturedOld: function(req, data_limit = null, callback) {
        var userData  = TokenService.decode(req.headers.authorization);
        let currDate = moment_tz().utc().format('YYYY-MM-DD HH:mm:ss');
        let vod_start_date = '2019-02-20 12:00:00'; // Implemented VOD on this date
        let deleteUser = [0];
        let staticvod;
        let show_vod_len;

        async.series([
            function (call1){
                UserService.deleteuserID(req, function(err, resData){
                    if(err)
                        return call1(err, null);
                    else
                    {
                        if(resData.length > 0){
                            deleteUser = resData;
                        }
                        return call1(null, deleteUser);
                    }
                });
            },
            function (call3) {
                if(data_limit > 0){
                    var query = OnDemand.find({is_featured: 1}).where({artist:{'!':deleteUser}}).populate(['artist', 'genre']).sort('createdAt ASC').limit(data_limit);
                }
                var query = OnDemand.find({is_featured: 1}).where({artist:{'!':deleteUser}}).populate(['artist', 'genre']).sort('createdAt ASC');
                query.exec(function(err, foundOndemands){
                    if (err) {
                        return call3({message: err.message}, null);
                    } else {
                        async.forEachOf(foundOndemands,function(ondemand,keyondemand, eachCallback) {
                            let vod_pay = 0;
                            let vod_free = 1;
                            ondemand.vod_type = 2;
                            ondemand.vod_price = ondemand.ondemand_price;
                            if (userData) {
                                VodPayment.findOne({ondemand:ondemand.id, user:userData.id}).exec(function(err, vodPay){
                                    if (err) {
                                        return eachCallback({message: err.message}, null);
                                    }
                                    else
                                    {
                                        if (vodPay) {
                                            vod_pay = 1;
                                        }

                                        if(ondemand.artist.id == userData.id)
                                        {
                                            // Remove the payment for a presenters original content
                                            ondemand.vod_pay = vod_free;
                                        }
                                        else
                                        {
                                            if(ondemand.is_paid == 1){
                                                ondemand.vod_pay = vod_pay;
                                            }else {
                                                ondemand.vod_pay = vod_free;
                                            }
                                        }

                                        ondemand.artist_name = ondemand.artist.name;
                                        ondemand.uid = 'ondemand_'+ondemand.id;
                                        return eachCallback(null);
                                    }
                                });
                            } else {
                                if(ondemand.is_paid == 1){
                                    ondemand.vod_pay = vod_pay;
                                }else {
                                    ondemand.vod_pay = vod_free;
                                }
                                return eachCallback(null);
                            }
                        } ,function(err){
                            if(err){
                                return call3(err, null);
                            } else {
                                staticvod = foundOndemands;
                                return call3(null);
                            }
                        });
                    }
                });
            },
        ], function (err, res) {
            if (err) {
                return callback({message: err.message}, null);
            } else {
                var vod_link = 'https://streaming.rendezvous.live/Demosuni_VOD';
                var finalArr = staticvod;
                var fullArry = {'vods':finalArr, 'vod_link':vod_link};
                return callback(null, fullArry);
            }
        });
    },
}
