module.exports = function (message, res){

  //var res = this.res;
  var response = {
    status: false,
    code: 404,
    message: message,
    data : {}
  }

  return res.jsonx(response);
}
