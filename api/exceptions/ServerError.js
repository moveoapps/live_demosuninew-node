module.exports = function (message, res){

  //var res = this.res;
  var response = {
    status: false,
    code: 500,
    message: message,
    data : {}
  }

  return res.jsonx(response);
}