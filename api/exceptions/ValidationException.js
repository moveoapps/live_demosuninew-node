module.exports = function (message, res){

  var response = {
    status: false,
    code: 422,
    message: message,
    data : {}
  }

  return res.jsonx(response);
}