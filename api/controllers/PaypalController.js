/**
* RoleController
*
* @description :: Server-side logic for managing roles
* @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
*/

var NotFoundException = require('../exceptions/NotFoundException');
var ValidationException = require('../exceptions/ValidationException');
var ServerError = require('../exceptions/ServerError.js');
const Joi = require('joi');

module.exports = {
	onboardingComplete: function (request, response) {

		PaypalService.onboardingComplete(request, function(error, data){
			if (!error) {
				response.ok(data);
			} else {
				return ServerError(error.message, response);
			}
		});
	},
	webhookOnboardingComplete: function (request, response) {
		PaypalService.webhookOnboardingComplete(request, function(error, data){
			if (!error) {
				response.ok(data);
			} else {
				return ServerError(error.message, response);
			}
		});
	},
	webhookPaymentCompletePost: function (request, response) {
		PaypalService.webhookPaymentCompletePost(request, function(error, data){
			if (!error) {
				response.ok(data);
			} else {
				return ServerError(error.message, response);
			}
		});
	},
	setCronPayout: function (req, res) {
		PaypalService.setCronPayout(req, function(err, resData){
		  if(err)
		    return ServerError(err.message,res);
		  else
		    res.ok(resData);
		});
	},
	setCronOnDemandPayout: function (req, res) {
		PaypalService.setCronOnDemandPayout(req, function(err, resData){
		  if(err)
		    return ServerError(err.message,res);
		  else
		    res.ok(resData);
		});
  },
	setCronCaptureID: function(req, res){
		PaypalService.setCronCaptureID(req, function(err, orderData){
			if(err) {
				return ServerError(err.message, res);
			} else {
				res.ok(orderData);
			}
		});
	},
	getPartnerReferelDetail: function(req, res){
		partner_referral_id = req.param('partner_referral_id');
		console.log('order_id',partner_referral_id);
		PaypalService.getPartnerReferelDetail(partner_referral_id, function(err, orderData){
			if(err) {
				return ServerError(err.message, res);
			} else {
				res.ok(orderData);
			}
		});
	}
};
