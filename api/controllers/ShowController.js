/**
 * GenreController
 *
 * @description :: Server-side logic for managing categories
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */
var NotFoundException = require('../exceptions/NotFoundException');
var ValidationException = require('../exceptions/ValidationException');
var ServerError = require('../exceptions/ServerError');
const Joi = require('joi');
var moment_tz = require('moment-timezone');


module.exports = {
	createShow: function (req, res) {
		const schema = Joi.object().keys({
			title: Joi.string().required(),
			publish_date: Joi.string().required(),
			timezone: Joi.number().required(),
			duration_hour: Joi.number().allow(''),
			duration_minute: Joi.number().allow(''),
			genre: Joi.string().required(),
			// tipprize: Joi.string().required(),
			tipprize: Joi.string().allow(''),
			audiance_capacity: Joi.number().required(),
			price: Joi.number().required(),
			ondemand_price: Joi.number().required(),
			description: Joi.string().allow(''),
			is_reminder: Joi.boolean().required(),
			tags: Joi.string().allow(''),
			city: Joi.string().required(''),
			classification_category: Joi.number().required(),
			top_tip_prize: Joi.string().allow(''),
			is_paid: Joi.string().required(),
			is_password: Joi.string().required(),
			password: Joi.string().allow('')
		});
		const result = Joi.validate(req.body, schema);
		if(result.error===null){
		  	ShowService.createShow(req, function(err, resData){
			  	if(err)
			    	return ServerError(err.message,res);
			  	else
			    	res.ok(resData);
			});
		} else {
			return ValidationException(result.error.details[0].message,res);
		}
  	},

	getShows: function (req, res) {
		ShowService.getShows(req, function(err, resData){
			if(err)
				return ServerError(err.message,res);
			else
				res.ok(resData);
		});
	},

	getFeaturedShows: function (req, res) {
		ShowService.getFeaturedShows(req, function(err, resData){
			if(err)
				return ServerError(err.message,res);
			else
				res.ok(resData);
		});
	},

	/*getFeaturedShows: function (req, res) {
		ShowService.getFeaturedShows(req, function(err, resData){
			if(err)
				return ServerError(err.message,res);
			else
				res.ok(resData);
		});
	},*/

	searchShows: function (req, res) {
		ShowService.searchShows(req, function(err, resData){
			if(err)
				return ServerError(err.message,res);
			else
				res.ok(resData);
		});
	},

	getShowDetail: function (req, res) {
	    ShowService.getShowDetail(req, function(err, resData){
	      	if(err)
	        	return ServerError(err.message,res);
	      	else
	        	res.ok(resData);
	    });
  	},

  	giftAFriendFreeShow: function (req, res) {
		const schema = Joi.object().keys({
				//email: Joi.string().required(),
				email: Joi.string().email({ minDomainAtoms: 2 }),
				show_id: Joi.number().required(),
		});
		const result = Joi.validate(req.body, schema);
		if(result.error===null){
			ShowService.giftAFriendFreeShow(req, function(err, resData){
				if(err)
					return ServerError(err.message,res);
				else
					res.ok(resData);
			});
		} else {
			return ValidationException(result.error.details[0].message,res);
		}
	},

	giftAFriend: function (req, res) {
		const schema = Joi.object().keys({
				//email: Joi.string().required(),
				email: Joi.string().email({ minDomainAtoms: 2 }),
				show_id: Joi.number().required(),
		});
		const result = Joi.validate(req.body, schema);
		if(result.error===null){
			ShowService.giftAFriend(req, function(err, resData){
				if(err)
					return ServerError(err.message,res);
				else
					res.ok(resData);
			});
		} else {
			return ValidationException(result.error.details[0].message,res);
		}
	},

	giftAFriendFinal: function (req, res) {
		ShowService.giftAFriendFinal(req, function(err, resData){
			if(err)
				return ServerError(err.message,res);
			else
				res.ok(resData,'You have successfully gift a show ticket.');
		});
	},

	giftAFriendStripe: function (req, res) {
		const schema = Joi.object().keys({
				//email: Joi.string().required(),
				email: Joi.string().email({ minDomainAtoms: 2 }),
				show_id: Joi.number().required(),
				stripe_token: Joi.string().required(),
		});
		const result = Joi.validate(req.body, schema);
		if(result.error===null){
			ShowService.giftAFriendStripe(req, function(err, resData){
				if(err)
					return ServerError(err.message,res);
				else
					res.ok(resData,'You have successfully gift a show ticket.');
			});
		} else {
			return ValidationException(result.error.details[0].message,res);
		}
	},

	purchaseTip: function (req, res) {
		ShowService.purchaseTip(req, function(err, resData){
			if(err)
				return ServerError(err.message,res);
			else {
				console.log('con',resData);
				res.ok(resData, 'Your tipping dollar has been added to your account');
			}
		});
	},

	getFreeShowAccess: function (req, res) {
		ShowService.getFreeShowAccess(req, function(err, resData){
			if(err)
				return ServerError(err.message,res);
			else
				res.ok(resData, 'Congratulations! You’ve Got a free access!.');
		});
	},
	getShowAccess: function (req, res) {
		ShowService.getShowAccess(req, function(err, resData){
			if(err)
				return ServerError(err.message,res);
			else
				res.ok(resData, 'Congratulations! Order Created.');
		});
	},

	getShowAccessFinal: function (req, res) {
		ShowService.getShowAccessFinal(req, function(err, resData){
			if(err)
				return ServerError(err.message,res);
			else
				res.ok(resData, 'Congratulations! You get access.');
		});
	},

	getShowAccessStripe: function (req, res) {
		ShowService.getShowAccessStripe(req, function(err, resData){
			if(err)
				return ServerError(err.message,res);
			else
				res.ok(resData, 'Congratulations! You get access.');
		});
	},

	tipToArtist: function (req, res) {
		const schema = Joi.object().keys({
				tipping_amount: Joi.number().required(),
				show_user: Joi.number().required(),
				show_id: Joi.number().required(),
				tip_type: Joi.number().allow(''),
				emoji_id: Joi.number().allow(''),
		});
		const result = Joi.validate(req.body, schema);
		if(result.error===null){
			if (req.body.tip_type == 1) {
				ShowService.tipToArtist(req, function(err, resData){
					if(err)
						return ServerError(err.message,res);
					else
						res.ok(resData, 'You give a tip to an presenter!');
				});
			} else {
				//ShowService.tipEmojiToArtist(req, function(err, resData){
				ShowService.tipToArtist(req, function(err, resData){
					if(err)
						return ServerError(err.message,res);
					else
						res.ok(resData, 'You give a tip to an presenter!');
				});
			}

		} else {
			return ValidationException(result.error.details[0].message,res);
		}
	},

	tipToArtistFinal: function (req, res) {
		const schema = Joi.object().keys({
				order_id: Joi.string().required(),
				show_id: Joi.number().required(),
				tip_type: Joi.number().allow(''),
				emoji_id: Joi.number().allow(''),
		});
		const result = Joi.validate(req.body, schema);
		if(result.error===null){
			if (req.body.tip_type == 1) {
				ShowService.tipToArtistFinal(req, function(err, resData){
					if(err)
						return ServerError(err.message,res);
					else
						res.ok(resData, 'You give a tip to an presenter!');
				});
			} else {
				//ShowService.tipEmojiToArtist(req, function(err, resData){
				ShowService.tipToArtistFinal(req, function(err, resData){
					if(err)
						return ServerError(err.message,res);
					else
						res.ok(resData, 'You give a tip to an presenter!');
				});
			}

		} else {
			return ValidationException(result.error.details[0].message,res);
		}
	},

	tipToArtistStripe: function (req, res) {
		const schema = Joi.object().keys({
				show_id: Joi.number().required(),
				tip_type: Joi.number().allow(''),
				emoji_id: Joi.number().allow(''),
				stripe_token: Joi.string().required(''),
				tipping_amount: Joi.number().required(),
				show_user: Joi.number().required(),
		});
		const result = Joi.validate(req.body, schema);
		if(result.error===null){
			ShowService.tipToArtistStripe(req, function(err, resData){
				if(err)
					return ServerError(err.message,res);
				else
					res.ok(resData, 'You give a tip to an presenter!');
			});
		} else {
			return ValidationException(result.error.details[0].message,res);
		}
	},

	tipToArtistByAdmin: function (req, res) {
		const schema = Joi.object().keys({
				tipping_amount: Joi.number().required(),
				show_user: Joi.number().required(),
				tip_type: Joi.number().allow(''),
		});
		const result = Joi.validate(req.body, schema);
		if(result.error===null){
				ShowService.tipToArtistByAdmin(req, function(err, resData){
					if(err)
						return ServerError(err.message,res);
					else
						res.ok(resData, 'You give a tip to an presenter!');
				});
		} else {
			return ValidationException(result.error.details[0].message,res);
		}
	},

	getUser: function (req, res) {
		var userObj  = TokenService.decode(req.headers.authorization);

		User.findOne({id: userObj.id}).exec(function(err, foundUser){
			if (err) {
				return ServerError(err.message,res);
			} else {
				res.ok(foundUser);
			}
		});
	},

	checkTicketStatus: function (req, res) {
		ShowService.checkTicketStatus(req, function(err, resData){
			if(err)
				return ServerError(err.message,res);
			else
				res.ok(resData);
		});
	},

	checkShowOwner: function (req, res) {
		ShowService.checkShowOwner(req, function(err, resData){
			if(err)
				return ServerError(err.message,res);
			else
				res.ok(resData);
		});
	},

	checkSessionPassword: function (req, res) {
		ShowService.checkSessionPassword(req, function(err, resData){
			if(err)
				return ServerError(err.message,res);
			else
				res.ok(resData);
		});
	},

	//backend apis
  	getShowList: function (req, res) {
    	criteria = {};
	    ShowService.fetchData(req, criteria, function(err, resData){
		  	if(err)
		    	return ServerError(err.message,res);
		  	else
		    	res.ok(resData);
		});
  	},

  	addShow: function (req, res) {
		const schema = Joi.object().keys({
			title: Joi.string().required(),
			publish_date: Joi.string().required(),
			artist: Joi.number().required(),
			duration_hour: Joi.number().allow(''),
			duration_minute: Joi.number().allow(''),
			genre: Joi.string().required(),
			// tipprize: Joi.string().required(),
			tipprize: Joi.string().allow(''),
			audiance_capacity: Joi.number().required(),
			price: Joi.number().required(),
			ondemand_price: Joi.number().required(),
			description: Joi.string().allow(''),
			is_reminder: Joi.boolean().required(),
			status: Joi.string().required(),
			meta_title: Joi.string().allow(''),
			meta_keys: Joi.string().allow(''),
			meta_description: Joi.string().allow(''),
			classification_category: Joi.number().required(),
			timezone: Joi.number().required(),
			is_featured: Joi.boolean().required(),
			is_comment: Joi.boolean().required(),
			is_reminder: Joi.boolean().required(),
			tags: Joi.string().allow(''),
			city: Joi.string().required(''),
			top_tip_prize: Joi.string().allow(''),
			is_paid: Joi.string().required(),
			is_password: Joi.string().required(),
			password: Joi.string().allow('')
		});
		const result = Joi.validate(req.body, schema);
		if(result.error===null){
		  	ShowService.createShow(req, function(err, resData){
			  	if(err)
			    	return ServerError(err.message,res);
			  	else
				    res.ok(resData);
			});
		} else {
			return ValidationException(result.error.details[0].message,res);
		}
  	},

  	updateShow: function (req, res) {
  		criteria = {id: req.param('id')};
	    ShowService.updateShow(req, req.body, criteria, function(err, resData){
	  		if(err)
		    	return ServerError(err.message,res);
		  	else
		    	res.ok(resData);
		});
  	},

	updateShowStatus: function (req, res) {
  		criteria = {id: req.param('id')};
	    ShowService.updateShowStatus(req, req.body, criteria, function(err, resData){
		  	if(err)
		    	return ServerError(err.message,res);
		  	else
		    	res.ok(resData);
		});
  	},

  	updateShowBulk: function (req, res) {
  		criteria = {id: req.body.showArray};
	    ShowService.updateShowBulk(req, req.body.showData, criteria, function(err, resData){
		  	if(err)
		    	return ServerError(err.message,res);
		  	else
		    	res.ok(resData);
		});
  	},

  	getShow: function (req, res) {
		criteria = {id: req.param('id')};
	    ShowService.getShow(req, criteria, function(err, resData){
		  	if(err)
		    	return ServerError(err.message,res);
		  	else
		    	res.ok(resData[0]);
		});
  	},

	getShowDetails: function (req, res) {
		criteria = {id: req.param('id')};
	    ShowService.getShowDetails(req, criteria, function(err, resData){
		  	if(err)
		    	return ServerError(err.message,res);
		  	else
		    	res.ok(resData[0]);
		});
  	},

	getShowData: function (req, res) {
		criteria = {id: req.param('id')};
	    ShowService.getShowData(req, criteria, function(err, resData){
		  	if(err)
		    	return ServerError(err.message,res);
		  	else
		    	res.ok(resData);
		});
  	},

  	deleteShow: function (req, res) {
  		criteria = {id: req.body.showArray};
	    ShowService.deleteShow(req, criteria, function(err, resData){
		  	if(err)
		    	return ServerError(err.message,res);
		  	else{
				res.ok(resData);
				console.log('sssss');
			}
		});
  	},

	getActiveClassificationCategory: function (req, res) {
		criteria = {is_active:1};
		ShowService.getActiveClassificationCategory(req, criteria, function(err, resData){
			if(err)
				return ServerError(err.message,res);
			else
				res.ok(resData);
		});
	},

	getUserShows: function (req, res) {
		var user  = TokenService.decode(req.headers.authorization);
		//let currDate = new Date();
		let currDate = moment_tz().utc().format('YYYY-MM-DD HH:mm:ss');
		//criteria = {artist: user.id, status: 'Published', publish_date:{'>':currDate}, is_finalize_list:{"!=":1}};
		criteria = {artist: user.id, status: 'Published', show_end_date:{'<':currDate},or : [{is_finalize_list: null}, {is_finalize_list: { not: 1 }}]};
	    ShowService.getUserShows(req, criteria, function(err, resData){
		  	if(err)
		    	return ServerError(err.message,res);
		  	else
		    	res.ok(resData);
		});
  	},

	getShowSongList: function (req, res) {
		criteria = {show: req.param('id')};
	    ShowService.getShowSongList(req, criteria, function(err, resData){
		  	if(err)
		    	return ServerError(err.message,res);
		  	else
		    	res.ok(resData);
		});
  	},

	addShowSong: function (req, res) {
		const schema = Joi.object().keys({
			show: Joi.number().required(),
			song_name: Joi.string().required(),
			song_writers: Joi.string().required(),
			ori_perfomer: Joi.string().required(),
			record_label: Joi.string().required(),
			duration_second: Joi.number().allow(''),
			duration_minute: Joi.number().allow('')
		});
		const result = Joi.validate(req.body, schema);
		if(result.error===null){
		  	ShowService.createShowSong(req, function(err, resData){
			  	if(err)
			    	return ServerError(err.message,res);
			  	else
			    	res.ok(resData);
			});
		} else {
			return ValidationException(result.error.details[0].message,res);
		}
  	},

	deleteShowSong: function (req, res) {
  		criteria = {id: req.body.songArray};
	    ShowService.deleteShowSong(req, criteria, function(err, resData){
		  	if(err)
		    	return ServerError(err.message,res);
		  	else
		    	res.ok(resData);
		});
  	},

	finalizeList: function (req, res) {
		criteria = {show: req.body.show_id};
    	ShowService.finalizeList(req, criteria, function(err, resData){
	  		if(err)
	    		return ServerError(err.message,res);
	  		else
	    		res.ok(resData);
		});
  	},

	sendShowEmailSms: function (req, res) {
	  ShowService.sendShowEmailSms(req, function(err, resData){
		  if(err)
		    return ServerError(err.message,res);
		  else
		    res.ok(resData);
		});
  	},

	accountHistoryList: function (req, res) {
		var user  = TokenService.decode(req.headers.authorization);
		ShowService.accountHistoryList(req, user, function(err, resData){
			if(err) {
				return ServerError(err.message,res);
			} else {
				res.ok(resData);
			}
		});
	},

	shareFacebook: function(req, res){
		ShowService.shareFacebook(req, function(err, showData){
			if(err) {
				return ServerError(err.message, res);
			} else {
				res.ok(showData);
			}
		});
	},

	getCaptureID: function(req, res){
		order_id = req.param('order_id');
		ShowService.getCaptureID(order_id, function(err, orderData){
			if(err) {
				return ServerError(err.message, res);
			} else {
				res.ok(orderData);
			}
		});
	},

	getOrderDetail: function(req, res){
		order_id = req.param('order_id');
		console.log('order_id',order_id);
		ShowService.getOrderDetail(order_id, function(err, orderData){
			if(err) {
				return ServerError(err.message, res);
			} else {
				res.ok(orderData);
			}
		});
	},


};
