/**
 * YouTubeController
 *
 * @description :: Server-side actions for handling incoming requests.
 * @help        :: See https://sailsjs.com/docs/concepts/actions
 */

var NotFoundException = require('../exceptions/NotFoundException');
var ValidationException = require('../exceptions/ValidationException');
var ServerError = require('../exceptions/ServerError');
const Joi = require('joi');

module.exports = {

    // Admin Function
        youTubeList: function (req, res)
        {
            var userData = TokenService.decode(req.headers.authorization);
            criteria = {};
            YouTubeService.youTubeList(req,criteria, function(err, resData){
                if(err)
                {
                    return ServerError(err.message,res);
                }
                else
                {
                    res.ok(resData);
                }
            });
        },

        getyouTube: function (req, res)
        {
            criteria = { id: req.param('id') };
            YouTubeService.getyouTube(req, criteria, function (err, resData) {
                if (err)
                {
                    return ServerError(err.message, res);
                }
                else
                {
                    res.ok(resData);
                }
            });
        },

        addyouTube: function (req, res)
        {
            const schema = Joi.object().keys({
                title: Joi.string().required(),
                user: Joi.string().required(),
                genre: Joi.string().required(),
                youtube_link: Joi.string().required(),
            });
            const result = Joi.validate(req.body, schema);

            if(result.error===null){
                YouTubeService.addyouTube(req, function(err, resData){
                    if(err)
                    {
                        return ServerError(err.message,res);
                    }
                    else
                    {
                        res.ok(resData, 'Youtube video created successfully.');
                    }
                });
            }
            else
            {
                return ValidationException(result.error.details[0].message,res);
            }
        },

        updateyouTube: function (req, res)
        {
            const schema = Joi.object().keys({
                title: Joi.string().required(),
                user: Joi.string().required(),
                genre: Joi.string().required(),
                youtube_link: Joi.string().required(),
            });
            const result = Joi.validate(req.body, schema);
            criteria = { id: req.param('id') };
            if(result.error===null){
                YouTubeService.updateyouTube(req,criteria, function(err, resData){
                    if(err)
                    {
                        return ServerError(err.message,res);
                    }
                    else
                    {
                        res.ok(resData, 'Youtube video created successfully.');
                    }
                });
            }
            else
            {
                return ValidationException(result.error.details[0].message,res);
            }
        },

        deleteyouTube: function (req, res) {
            criteria = {id: req.body.youtubeArray};
            YouTubeService.deleteyouTube(req, criteria, function(err, resData){
                if(err)
                {
                    return ServerError(err.message,res);
                }
                else
                {
                    res.ok(resData);
                    console.log('sssss');
                }
            });
        },

    // User Function

        createData: function (req, res)
        {
    		const schema = Joi.object().keys({
                title: Joi.string().required(),
                genre: Joi.string().required(),
    			youtube_link: Joi.string().required(),
    		});
    		const result = Joi.validate(req.body, schema);

    		if(result.error===null){
                YouTubeService.createData(req, function(err, resData){
                    if(err)
                    {
                        return ServerError(err.message,res);
                    }
                    else
                    {
                        res.ok(resData, 'Youtube video created successfully.');
                    }
    			});
    		}
            else
            {
    			return ValidationException(result.error.details[0].message,res);
    		}
        },

        allData: function (req, res)
        {
            var userData = TokenService.decode(req.headers.authorization);
            criteria = {};
    		YouTubeService.allData(req,criteria, function(err, resData){
    			if(err)
                {
                    return ServerError(err.message,res);
                }
    			else
                {
                    res.ok(resData);
                }
    		});
    	},

        getData: function (req, res)
        {
            criteria = { id: req.param('id') };
    		YouTubeService.getData(req, criteria, function (err, resData) {
    			if (err)
                {
                    return ServerError(err.message, res);
                }
    			else
                {
                    res.ok(resData);
                }
    		});
    	},


};
