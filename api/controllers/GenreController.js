/**
 * GenreController
 *
 * @description :: Server-side logic for managing categories
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */
var NotFoundException = require('../exceptions/NotFoundException');
var ValidationException = require('../exceptions/ValidationException');
var ServerError = require('../exceptions/ServerError');


module.exports = {
	getCategories: function (req, res) {
		criteria = {};
	    GenreService.fetchData(req, criteria, function(err, resData){
		  	if(err)
		    	return ServerError(err.message,res);
		  	else
		    	res.ok(resData);
		});
  	},
  	getActiveCategories: function (req, res) {
		criteria = {is_active: 1};
	    GenreService.fetchData(req, criteria, function(err, resData){
		  	if(err)
		    	return ServerError(err.message,res);
		  	else
		    	res.ok(resData);
		});
  	},

  	saveGenre: function (req, res) {
	    GenreService.addMyGenre(req, function(err, resData){
		  	if(err)
		    	return NotFoundException(err.message, res);
		  	else
		    	res.ok(resData);
		});
  	},
  	unsaveGenre: function (req, res) {
	    GenreService.removeMyGenre(req, function(err, resData){
		  	if(err)
		    	return NotFoundException(err.message, res);
	  		else
		    	res.ok(resData);
		});
  	},

  	updateGenre: function (req, res) {
  		criteria = {id: req.param('id')};
	    GenreService.updateGenre(req, req.body, criteria, function(err, resData){
		  	if(err)
			    return ServerError(err.message,res);
		  	else
			    res.ok(resData);
		});
  	},
  	updateGenreBulk: function (req, res) {
  		criteria = {id: req.body.genreArray};
	    GenreService.updateBulkGenre(req.body.postData, criteria, function(err, resData){
		  	if(err)
			    return ServerError(err.message,res);
		  	else
			    res.ok(resData);
		});
  	},
  	addGenre: function (req, res) {
	    GenreService.addGenre(req, function(err, resData){
		  	if(err)
		    	return ServerError(err.message,res);
		  	else
		    	res.ok(resData);
		});
  	},
  	getGenre: function (req, res) {
		criteria = {id: req.param('id')};
	    GenreService.fetchData(req, criteria, function(err, resData){
		  	if(err)
		    	return ServerError(err.message,res);
		  	else
		    	res.ok(resData[0]);
		});
  	},
	getGenreDetail: function (req, res) {
		criteria = {slug: req.param('id')};
	  	GenreService.getGenreDetail(req, criteria, function(err, resData){
		  	if(err)
		    	return ServerError(err.message,res);
		  	else
		    	res.ok(resData);
		});
  	},
	getGenreShows: function (req, res) {
	  	GenreService.getGenreShows(req, function(err, resData){
		  	if(err)
		    	return ServerError(err.message,res);
		  	else
		    	res.ok(resData);
		});
  	},
	getGenreOnDemand: function(req, res){
		GenreService.getGenreOnDemand(req, '', function(err, resData){
		  	if(err)
		    	return ServerError(err.message,res);
		  	else
		    	res.ok(resData);
		});
	},
	getGenreYouTube: function (req, res) {
	  	GenreService.getGenreYouTube(req, function(err, resData){
		  	if(err)
		    	return ServerError(err.message,res);
		  	else
		    	res.ok(resData);
		});
  	},

  	deleteGenre: function (req, res) {
  		criteria = {id: req.body.genreArray};
	    GenreService.deleteGenre(req, criteria, function(err, resData){
		  	if(err)
		    	return ServerError(err.message,res);
		  	else
		    	res.ok(resData);
		});
  	},
};
