/**
* RoleController
*
* @description :: Server-side logic for managing roles
* @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
*/

var NotFoundException = require('../exceptions/NotFoundException');
var ValidationException = require('../exceptions/ValidationException');
var ServerError = require('../exceptions/ServerError.js');
const Joi = require('joi');

module.exports = {
	getRoleList: function (request, response) {
		let criteria = (request.body)?request.body:{};
		RoleService.getList(request, response, criteria, function(error, data){
			if (!error) {
				response.ok(data);
			} else {
				return ServerError(error.message, response);
			}
		});
	},
	createRole: function(request, response) {
		let data = {
			name: request.param('name'),
			is_active: request.param('is_active'),
		}
		RoleService.createRole(request, response, data, function(error, data){
			if(!error) {
				response.ok(data);
			} else {
				return ServerError(error.message, response);
			}
		});
	},
	getSingleRole: function(request, response) {
		let criteria = {
			id: request.param('id'),
		};
		RoleService.getRole(request, response, criteria, function(error, data){
			if(!error) {
				response.ok(data);
			} else {
				return ServerError(error.message, response);
			}
		});
	},
	updateRole: function(request, response) {
		let criteria = {
			id: request.param('id'),
		};
		let data = {
			name: request.param('name'),
			is_active: request.param('is_active'),
		}
		return this.update(request, response, criteria, data);
	},
	updateRoleStatus: function(request, response) {
		let criteria = {
			id: request.param('id'),
		};
		let data = {
			is_active: request.param('is_active'),
		}
		return this.update(request, response, criteria, data);
	},
	updateRoleBulk: function(request, response) {
		let criteria = {
			'id': request.body.Ids
		};
		let data = {
			is_active: request.param('is_active'),
		}
		RoleService.bulkUpdateRole(request, response, criteria, data, function(error, data){
			if(error) {
				return ServerError(error.message, response);
			} else {
				response.ok(data);
			}
		});
	},
	deleteRole: function(request, response) {
		let criteria = {
			'id': request.body.Ids
		};

		RoleService.deleteRole(request, response, criteria, function(error, data){
			if(error) {
				return ServerError(error.message, response);
			} else {
				response.ok(data);
			}
		});
	},

	/**
	 * Name : update
	 * Desc : private function for update role.
	 */
	update: function(request, response, criteria, data) {
		RoleService.updateRole(request, response, criteria, data, function(error, data){
			if(!error) {
				response.ok(data);
			} else {
				return ServerError(error.message, response);
			}
		});
	},
	getModuleList: function(request, response) {
		RoleService.getModuleList(request, response, function(error, modules){
			if(!error) {
				response.ok(modules);
			} else {
				return ServerError(error.message, response);
			}
		});
	},
	getModulePermission: function(request, response) {
		var userData  = TokenService.decode(request.headers.authorization);
		RoleService.getModulePermission(request, userData, function(error, permission){
			if(!error) {
				response.ok(permission);
			} else {
				return ServerError(error.message, response);
			}
		});
	},
};
