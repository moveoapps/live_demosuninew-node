/**
 * TierController
 *
 * @description :: Server-side logic for managing users
 * @help        :: See http://links.sailsjs.org/docs/controllers
 */
var NotFoundException = require('../exceptions/NotFoundException');
var ValidationException = require('../exceptions/ValidationException');
var ServerError = require('../exceptions/ServerError.js');

const Joi = require('joi');

module.exports = {

  //backend apis
  getTiers: function (req, res) {
    criteria = {};
	    TierService.fetchData(req, criteria, function(err, resData){
		  if(err)
		    return ServerError(err.message,res);
		  else
		    res.ok(resData);
		});
  },
  addTier: function (req, res) {
	    TierService.addTier(req, function(err, resData){
		  if(err)
		    return NotFoundException(err.message, res);
		  else
		    res.ok(resData);
		});
  },
  updateTier: function (req, res) {
  		criteria = {id: req.param('id')};
	    TierService.updateTier(req, req.body, criteria, function(err, resData){
		  if(err)
		    return ServerError(err.message,res);
		  else
		    res.ok(resData);
		});
  },
  updateTierBulk: function (req, res) {
  		criteria = {id: req.body.tierArray};
	    TierService.updateTier(req, req.body.tierData, criteria, function(err, resData){
		  if(err)
		    return ServerError(err.message,res);
		  else
		    res.ok(resData);
		});
  },
  getTier: function (req, res) {
		criteria = {id: req.param('id')};
	    TierService.fetchData(req, criteria, function(err, resData){
		  if(err)
		    return ServerError(err.message,res);
		  else
		    res.ok(resData[0]);
		});
  },
  deleteTier: function (req, res) {
  		criteria = {id: req.body.tierArray};
	    TierService.deleteTier(req, criteria, function(err, resData){
		  if(err)
		    return ServerError(err.message,res);
		  else
		    res.ok(resData);
		});
  },
}
