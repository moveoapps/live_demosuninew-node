/**
* OnDemandController
*
* @description :: Server-side logic for managing categories
* @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
*/
var NotFoundException = require('../exceptions/NotFoundException');
var ValidationException = require('../exceptions/ValidationException');
var ServerError = require('../exceptions/ServerError');
const Joi = require('joi');


module.exports = {
	getMyOnDemands: function (req, res) {
		OnDemandService.getMyOnDemands(req, function(err, resData){
			if(err)
			return ServerError(err.message,res);
			else
			res.ok(resData);
		});
	},
	createOnDemand: function (req, res) {
		const schema = Joi.object().keys({
			genre: Joi.string().required(),
			title: Joi.string().required(),
			ondemand_price: Joi.number().required(),
			is_paid: Joi.string().required()
		});
		const result = Joi.validate(req.body, schema);
		if(result.error===null){
			OnDemandService.createOnDemand(req, function(err, resData){
				if(err)
				return ServerError(err.message,res);
				else
				res.ok(resData,'On-demand created successfully.');
			});
		} else {
			return ValidationException(result.error.details[0].message,res);
		}
	},
	updateOnDemand: function (req, res) {
		const schema = Joi.object().keys({
			genre: Joi.string().required(),
			title: Joi.string().required(),
			ondemand_price: Joi.number().required(),
			is_paid: Joi.string().required()
		});
		const result = Joi.validate(req.body, schema);
		if(result.error===null){
			OnDemandService.updateOnDemand(req, function(err, resData){
				if(err)
				return ServerError(err.message,res);
				else
				res.ok(resData[0],'On-demand updated successfully.');
			});
		} else {
			return ValidationException(result.error.details[0].message,res);
		}
	},
	deleteOnDemand: function (req, res) {
		criteria = {id: req.body.id};
		OnDemandService.deleteOnDemand(req, criteria, function(err, resData){
			if(err)
			return ServerError(err.message,res);
			else
			res.ok(resData);
		});
	},
	getVods: function (req, res) {
  	  OnDemandService.getVods(req, 0, function(err, resData){
  		if(err)
  		  return ServerError(err.message,res);
  		else
  		  res.ok(resData);
  	  });
    },
	getArtistVods: function (req, res) {
  	  	OnDemandService.getVods(req, 10, function(err, resData){
	  		if(err)
	  		  	return ServerError(err.message,res);
	  		else
	  		  	res.ok(resData);
  	  	});
    },
	vodPayment: function (req, res) {
		OnDemandService.vodPayment(req, function(err, resData){
			if(err){
				return ServerError(err.message,res);
			} else {
				res.ok(resData, 'Congratulations! Order Created.');
			}
		});
	},
	vodPaymentFinal: function (req, res) {
		OnDemandService.vodPaymentFinal(req, function(err, resData){
			if(err)
				return ServerError(err.message,res);
			else
				res.ok(resData, 'Congratulations! You get On-Demand Access.');
		});
	},
	vodPaymentStripe: function (req, res) {
		OnDemandService.vodPaymentStripe(req, function(err, resData){
			if(err)
				return ServerError(err.message,res);
			else
				res.ok(resData, 'Congratulations! You get On-Demand Access.');
		});
	},
	getFeatured: function (req, res) {
		OnDemandService.getFeaturedVods(req, 0, function(err, resData){
			if(err)
	  		  	return ServerError(err.message,res);
	  		else
	  		  	res.ok(resData);
		});
	},

	//Admin api
	adminVods: function (req, res) {
  	  	OnDemandService.getOnDemand(req, function(err, resData){
	  		if(err)
	  		  	return ServerError(err.message,res);
	  		else
	  		  	res.ok(resData);
  	  	});
    },
	updateFeatured:function (req, res) {
		OnDemandService.updateFeatured(req, function(err, resData){
			if(err)
				return ServerError(err.message,res);
			else
				res.ok(resData[0],'On-demand updated successfully.');
		});
	},
	deleteVod: function (req, res) {
		criteria = {id: req.body.vodArray};
		OnDemandService.deleteOnDemand(req, criteria, function(err, resData){
			if(err)
			return ServerError(err.message,res);
			else
			res.ok(resData);
		});
	},
	admingetVod: function (req, res) {
		criteria = {id: req.param('id')};
		OnDemandService.admingetVod(req, criteria, function(err, resData){
			if(err)
	  		  	return ServerError(err.message,res);
	  		else
	  		  	res.ok(resData);
		});
	},
	addVod: function (req, res) {
		const schema = Joi.object().keys({
			artist: Joi.string().required(),
			genre: Joi.string().required(),
			title: Joi.string().required(),
			ondemand_price: Joi.number().required(),
			is_paid: Joi.string().required(),
			is_featured: Joi.string().required()
		});
		const result = Joi.validate(req.body, schema);
		if(result.error===null){
			OnDemandService.addVod(req, function(err, resData){
				if(err)
				return ServerError(err.message,res);
				else
				res.ok(resData,'On-demand created successfully.');
			});
		} else {
			return ValidationException(result.error.details[0].message,res);
		}
	},
	updateVod: function (req, res) {
		const schema = Joi.object().keys({
			artist: Joi.string().required(),
			genre: Joi.string().required(),
			title: Joi.string().required(),
			ondemand_price: Joi.number().required(),
			is_paid: Joi.string().required(),
			is_featured: Joi.string().required()
		});
		const result = Joi.validate(req.body, schema);
		if(result.error===null){
			OnDemandService.updateVod(req, function(err, resData){
				if(err)
				return ServerError(err.message,res);
				else
				res.ok(resData[0],'On-demand updated successfully.');
			});
		} else {
			return ValidationException(result.error.details[0].message,res);
		}
	},
};
