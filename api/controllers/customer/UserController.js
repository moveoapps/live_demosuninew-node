/**
 * UserController
 *
 * @description :: Server-side logic for managing users
 * @help        :: See http://links.sailsjs.org/docs/controllers
 */
const Joi = require('joi');
var bcrypt = require('bcrypt');
var NotFoundException = require('../../exceptions/NotFoundException');
var ValidationException = require('../../exceptions/ValidationException');
var ServerError = require('../../exceptions/ServerError.js');
var generator = require('generate-password');
module.exports = {

    login: function (req, res) {
        // User.findOne({'email':req.body.email, gifted_user:0}).populate('tier').exec(function (err, user) {
        User.findOne({'email':req.body.email, gifted_user:0, is_delete: {'!':1}}).populate('tier').exec(function (err, user) {
            // if (err) res.serverError(err);

            if (user) {
                User.isValidPassword(req.body.password, user, function(err, match) {
                    if (match) {
                        // password match
                        if (user.is_active == 1) {
                            if(user.is_verified == 1){
                                var token = TokenService.sign({id: user.id,user_type: user.user_type}, sails.config.project.tokenTTL);
                                if (user.user_type == 2 && user.name && user.name != null && user.email && user.email != null && user.country && user.country != null) {
                                    is_profile_complete = 1;
                                } else if (user.user_type == 1 && user.name && user.name != null && user.email && user.email != null && user.country && user.country != null && user.bio && user.bio != null) {
                                    is_profile_complete = 1;
                                } else {
                                    is_profile_complete = 0;
                                }

                                user.is_profile_complete = is_profile_complete;

                                User.update({id:user.id},{firsttime_login:0}).exec(function(err, userData){
                                    res.ok({user: user, token: token});
                                });

                            }else{
                                return NotFoundException('Sorry! your email is not verified. Please verify your email first.', res);
                            }
                        } else {
                            return NotFoundException('Sorry! your account is not active. Please try again later.', res);
                        }
                    } else {
                        //res.unauthorized();
                        return NotFoundException('Email and Password does not match', res);
                    }
                });

            } else {
                return NotFoundException('You are not registered user. Please sign up.', res);
            }
        });
    },

    fbLogin: function (req, res) {
        const schema = Joi.object().keys({
            name: Joi.string().required(),
            email: Joi.string().required().email(),
            social_id: Joi.string().required(),
            user_type: Joi.number().allow(''),
            social_type: Joi.number().required(),
            is_verified: Joi.boolean().allow(''),
            is_active: Joi.boolean().allow('')
        });

        const result = Joi.validate(req.body, schema);
        if(result.error===null) {
            UserService.fbLogin(req, function(err, resData){
                if(err)
                return NotFoundException(err.message, res);
                else{
                    res.ok(resData);
                }
            });
        } else {
            return ValidationException(result.error.details[0].message,res);
        }
    },

    requestPasswordReset: function (req, res) {
        User.findOneByEmail(req.body.email).exec(function (err, user) {
            if (err) res.serverError(err);

            if (user) {
                // Create a verification token, which will expire in 2 hours
                token = TokenService.sign({id: user.id,user_type: user.user_type}, sails.config.project.passwordReset.tokenTTL);

                var templateData = {
                    to: user.email,
                    from: sails.config.fromName+' <'+sails.config.fromEmail+'>',
                    subject: sails.config.project.passwordReset.subject,
                    userEmail: user.email,
                    siteName: sails.config.project.name,
                    url: sails.config.frontUrl + sails.config.project.passwordReset.uri + token,
                    base_url: sails.config.apiUrl,
                    name: user.name,
                    curr_year: (new Date()).getFullYear()
                };
                EmailService.sendMail(sails.config.project.passwordReset.template, templateData, function(err, message) {
                    if (err){
                        return sails.log.error('> error sending password reset email', err);
                        console.log('error',err);
                    }

                    sails.log.verbose('> sending password reset email to:', user.email);
                });

                res.ok({},'Mail send');
            } else {
                return NotFoundException('This email does not exist.', res);
            }

        });
    },

    validatePasswordReset: function (req, res) {
        // Verify that the reset token has not expired
        token = req.param('token');
        TokenService.verify(token, function(err, data) {
            if(err) {
                res.serverError(err);
            } else {
                User.findOne({id: data.id}).exec(function (err, user) {
                    if (err){
                        res.serverError(err);
                    } else {
                        if (user) {
                            User.encryptPassword(req.body.password, function(err, encryptedPassword) {
                                user.password = encryptedPassword;
                                user.save();
                                res.ok({});
                            })
                        } else {
                            res.notFound({ error: 'User not found' });
                        }
                    }
                });
            }
        });
    },

    changePassword: function (req, res) {
        UserService.changePassword(req, function(err, user){
            if(err)
            return NotFoundException(err.message, res);
            else{
                res.ok(user);
            }
        });
    },

    logout: function (req, res) {
        UserService.logout(req, function(err, response){
            if(err)
            return NotFoundException(err.message, response);
            else{
                res.ok(response);
            }
        });
    },

    updateProfile: function (req, res) {
        UserService.updateProfile(req, function(err, user){
            if(err)
            return ServerError(err.message, res);
            else
            res.ok(user);
        });
    },

    getTimezone: function (req, res) {
        Timezone.find().sort('order_number ASC').exec(function(err, resData){
            if(err)
            return ServerError(err.message,res);
            else
            res.ok(resData);
        });
    },

    signup: function (req, res) {
        //Update user profile
        const schema = Joi.object().keys({
            name: Joi.string().required(),
            email: Joi.string().required().email(),
            password: Joi.string().required(),
            confirmPassword: Joi.string().allow(''),
            //timezone: Joi.number().required(),
            country: Joi.number().required(),
            user_type: Joi.number().required(),
            agreeTearms: Joi.boolean().allow(''),
            is_verified: Joi.boolean().allow(''),
            is_active: Joi.boolean().allow(''),
            bank_name: Joi.string().allow(''),
            account_number: Joi.string().allow(''),
            //artist_type: Joi.number().allow(''),
            payment_type: Joi.number().allow(''),
        });

        const result = Joi.validate(req.body, schema);
        if(result.error===null) {
            UserService.registration(req, function(err, user){
                if(err)
                return ServerError(err.message, res);
                else
                res.ok(user);
            });
        } else {
            return ValidationException(result.error.details[0].message,res);
        }
        //NotFoundException("called", res);
    },

    uploadMedia: function (req, res) {
        req.setTimeout(0);
        UserService.uploadMedia(req, function(err, user){
            if(err)
                return ServerError(err.message, res);
            else
                res.ok(user);
        });
    },

    deleteMedia: function (req, res) {
        criteria = {id: req.body.id};
        UserService.deleteMedia(req, criteria, function(err, resData){
            if(err)
            return ServerError(err.message,res);
            else
            res.ok(resData);
        });
    },

    verifyAccount: function (req, res) {
        const schema = Joi.object().keys({
            id: Joi.string().required(),
        });

        const result = Joi.validate(req.body, schema);
        if(result.error===null) {
            UserService.verifyAccount(req, function(err, resData, message){
                if(err)
                return NotFoundException(err.message, res);
                else{
                    res.ok(resData, message);
                }
            });
        } else {
            return ValidationException(result.error.details[0].message,res);
        }
    },

    checkFrontToken: function (req, res) {
        let data = req.body;
        var userData  = TokenService.decode(data.token);
        var user = data.user;
        if (userData) {
            User.findOne({id:userData.id}).populate(['artistmedia']).exec(function(err, userObj){
                if(err)
                return ServerError(err.message,res);
                else {
                    if(userData.id == user.id && userObj && userObj != 'undefined') {
                        res.ok({isMatch:true, user: userObj});
                    } else {
                        res.ok({isMatch:false});
                    }
                }
            });
        } else {
            res.ok({isMatch:false});
        }
    },
    //backend apis
    getUsers: function (req, res) {
        criteria = {is_delete: {'!':1}};
        UserService.fetchData(req, criteria, function(err, resData){
            if(err){
                return ServerError(err.message,res);
            }
            else{
                res.ok(resData);
            }
        });
    },

    getUser: function (req, res) {
        criteria = {id: req.param('id')};
        UserService.fetchData(req, criteria, function(err, resData){
            if(err)
                return ServerError(err.message,res);
            else
                res.ok(resData[0]);
        });
    },

    getUserByEmail: function (req, res) {
        User.findOne({select: ['profile_picture','country']}).populate('country').where({email:req.body.email}).exec(function(err, userData){
            if(err)
                return ServerError(err.message,res);
            else
                res.ok(userData);
        })
    },

    updateUser: function (req, res) {
        criteria = {id: req.param('id')};
        UserService.updateUser(req, req.body, criteria, function(err, resData){
            if(err)
                return ServerError(err.message,res);
            else
                res.ok(resData);
        });
    },

    updateVerifyStatus: function (req, res) {
        criteria = {id: req.param('id')};
        UserService.updateUser(req, req.body, criteria, function(err, resData){
            if(err)
                return ServerError(err.message,res);
            else
                res.ok(resData);
        });
    },

    updateUserBulk: function (req, res) {
        criteria = {id: req.body.userArray};
        UserService.updateUser(req, req.body.userData, criteria, function(err, resData){
            if(err)
                return ServerError(err.message,res);
            else
                res.ok(resData);
        });
    },

    deleteUser: function (req, res) {
        criteria = {id: req.body.userArray};
        UserService.deleteUser(req, criteria, function(err, resData){
            if(err)
                return ServerError(err.message,res);
            else
                res.ok(resData);
        });
    },

    followArtist: function (req, res) {
	    UserService.followArtist(req, function(err, resData){
		  if(err)
		    return NotFoundException(err.message, res);
		  else
		    res.ok(resData);
		});
    },

    getFollowStatus: function (req, res) {
	    UserService.getFollowStatus(req, function(err, resData){
		  if(err)
		    return NotFoundException(err.message, res);
		  else
		    res.ok(resData);
		});
    },

    getFollowers: function (req, res) {
        UserService.getFollowers(req, function(err, resData){
            if(err)
                return ServerError(err.message,res);
            else
                res.ok(resData);
        });
    },

    getFavorites: function (req, res) {
        UserService.getFavorites(req, function(err, resData){
            if(err)
                return ServerError(err.message,res);
            else
                res.ok(resData);
        });
    },

    getCustomerShows: function (req, res) {
	    UserService.getCustomerShows(req, function(err, resData){
            if(err)
                return ServerError(err.message,res);
            else
                res.ok(resData);
		});
    },
  //backend apis
    getPurchases: function (req, res) {
        UserService.getPurchases(req, function(err, resData){
            if(err)
                return ServerError(err.message,res);
            else
                res.ok(resData);
		});
    },

    getMyPurchases: function (req, res) {
        UserService.getMyPurchases(req, function(err, resData){
            if(err)
                return ServerError(err.message,res);
            else
                res.ok(resData);
        });
    },

    getMyTickets: function (req, res) {
        UserService.getMyTickets(req, function(err, resData){
            if(err)
                return ServerError(err.message,res);
            else
                res.ok(resData);
        });
    },

    getUserProfile: function (req, res) {
        UserService.getUserProfile(req, function(err, resData){
            if(err)
                return ServerError(err.message,res);
            else
                res.ok(resData);
        });
    },

    soundCheckData: function (req, res) {
        UserService.soundCheckData(req, function(err, resData){
            if(err)
                return ServerError(err.message,res);
            else
                res.ok(resData);
        });
    },

    goliveShow: function (req, res) {
        UserService.goliveShow(req, function(err, resData){
            if(err){
                return ServerError(err.message,res);
            }
            else{
                // console.log('resData',resData);
                res.ok(resData);
            }

        });
    },

    getPresenters: function (req, res) {
        criteria = {user_type: '1'};
        UserService.getPresenters(req, criteria, function(err, resData){
            if(err){
                return ServerError(err.message,res);
            }
            else{
                // console.log('resData',resData);
                res.ok(resData);
            }

        });
    },

    exportUser: function (req, res) {
        criteria = {is_delete: {'!':1}};
        UserService.exportData(req, criteria, function(err, resData){
            if(err){
                return ServerError(err.message,res);
            }
            else{
                res.ok(resData);
            }
        });
    },

}
