/**
 * EmojiController
 *
 * @description :: Server-side logic for managing users
 * @help        :: See http://links.sailsjs.org/docs/controllers
 */
var NotFoundException = require('../exceptions/NotFoundException');
var ValidationException = require('../exceptions/ValidationException');
var ServerError = require('../exceptions/ServerError.js');

const Joi = require('joi');

module.exports = {

  //backend apis
  getEmojis: function (req, res) {
    criteria = {};
	    EmojiService.fetchData(req, criteria, function(err, resData){
		  if(err)
		    return ServerError(err.message,res);
		  else
		    res.ok(resData);
		});
  },
  addEmoji: function (req, res) {
	    EmojiService.addEmoji(req, function(err, resData){
		  if(err)
		    return NotFoundException(err.message, res);
		  else
		    res.ok(resData);
		});
  },
  updateEmoji: function (req, res) {
  		criteria = {id: req.param('id')};
	    EmojiService.updateEmoji(req, req.body, criteria, function(err, resData){
		  if(err)
		    return ServerError(err.message,res);
		  else
		    res.ok(resData);
		});
  },
  updateEmojiBulk: function (req, res) {
  		criteria = {id: req.body.emojiArray};
	    EmojiService.updateEmoji(req, req.body.emojiData, criteria, function(err, resData){
		  if(err)
		    return ServerError(err.message,res);
		  else
		    res.ok(resData);
		});
  },
  getEmoji: function (req, res) {
		criteria = {id: req.param('id')};
	    EmojiService.fetchData(req, criteria, function(err, resData){
		  if(err)
		    return ServerError(err.message,res);
		  else
		    res.ok(resData[0]);
		});
  },
  deleteEmoji: function (req, res) {
  		criteria = {id: req.body.emojiArray};
	    EmojiService.deleteEmoji(req, criteria, function(err, resData){
		  if(err)
		    return ServerError(err.message,res);
		  else
		    res.ok(resData);
		});
  },
}
