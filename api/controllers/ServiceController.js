/**
 * ServiceController
 *
 * @description :: Server-side actions for handling incoming requests.
 * @help        :: See https://sailsjs.com/docs/concepts/actions
 */

module.exports = {
  

  /**
   * `ServiceController.YouTube()`
   */
  YouTube: async function (req, res) {
    return res.json({
      todo: 'YouTube() is not implemented yet!'
    });
  }

};

