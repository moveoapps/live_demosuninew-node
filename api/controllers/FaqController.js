/**
 * FaqController
 *
 * @description :: Server-side logic for managing users
 * @help        :: See http://links.sailsjs.org/docs/controllers
 */
var NotFoundException = require('../exceptions/NotFoundException');
var ValidationException = require('../exceptions/ValidationException');
var ServerError = require('../exceptions/ServerError.js');

const Joi = require('joi');

module.exports = {

  //backend apis
  getFaqs: function (req, res) {
    criteria = {};
	    FaqService.fetchData(req, criteria, function(err, resData){
		  if(err)
		    return ServerError(err.message,res);
		  else
		    res.ok(resData);
		});
  },
  addFaq: function (req, res) {
	    FaqService.addFaq(req, function(err, resData){
		  if(err)
		    return NotFoundException(err.message, res);
		  else
		    res.ok(resData);
		});
  },
  updateFaq: function (req, res) {
  		criteria = {id: req.param('id')};
	    FaqService.updateFaq(req, req.body, criteria, function(err, resData){
		  if(err)
		    return ServerError(err.message,res);
		  else
		    res.ok(resData);
		});
  },
  updateFaqBulk: function (req, res) {
  		criteria = {id: req.body.faqArray};
	    FaqService.updateFaq(req, req.body.faqData, criteria, function(err, resData){
		  if(err)
		    return ServerError(err.message,res);
		  else
		    res.ok(resData);
		});
  },
  getFaq: function (req, res) {
		criteria = {id: req.param('id')};
	    FaqService.fetchData(req, criteria, function(err, resData){
		  if(err)
		    return ServerError(err.message,res);
		  else
		    res.ok(resData[0]);
		});
  },
  deleteFaq: function (req, res) {
  		criteria = {id: req.body.faqArray};
	    FaqService.deleteFaq(req, criteria, function(err, resData){
		  if(err)
		    return ServerError(err.message,res);
		  else
		    res.ok(resData);
		});
  },
}
