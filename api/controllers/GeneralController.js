/**
* GenreController
*
* @description :: Server-side logic for managing categories
* @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
*/
var NotFoundException = require('../exceptions/NotFoundException');
var ValidationException = require('../exceptions/ValidationException');
var ServerError = require('../exceptions/ServerError');


module.exports = {
	getCMSBySlug: function (req, res) {
		criteria = {slug:req.param('slug')};
		GeneralService.getCMS(req, criteria, function(err, resData){
			if(err)
			return ServerError(err.message,res);
			else
			res.ok(resData[0]);
		});
	},
	getActiveFaqs: function (req, res) {
		criteria = {is_active:1};
		Faq.find(criteria).exec(function(err, resData){
			if(err)
			return ServerError(err.message,res);
			else
			res.ok(resData);
		});
	},
	getActiveArtist: function (req, res) {
		criteria = {user_type:1, is_active:1, is_verified:1};
		User.find(criteria).exec(function(err, resData){
			if(err)
			return ServerError(err.message,res);
			else
			res.ok(resData);
		});
	},
	getTipPrize: function (req, res) {
		TipPrize.find().exec(function(err, resData){
			if(err)
			return ServerError(err.message,res);
			else
			res.ok(resData);
		});
	},
	getArtist: function (req, res) {
		criteria = {id: req.param('id')};
		User.find(criteria).exec(function(err, resData){
			if(err)
			return ServerError(err.message,res);
			else
			res.ok(resData);
		});
	},
	getActiveGenre: function (req, res) {
		criteria = {is_active:1};
		Genre.find(criteria).exec(function(err, resData){
			if(err)
			return ServerError(err.message,res);
			else
			res.ok(resData);
		});
	},
	getHomeData: function (req, res) {
		GeneralService.getHomeData(req, function(err, resData){
			if(err)
			return ServerError(err.message,res);
			else
			res.ok(resData);
		});
	},
	getTimezones: function (req, res) {
		GeneralService.getTimezones(req, function(err, resData){
			if(err)
			return ServerError(err.message,res);
			else
			res.ok(resData);
		});
	},
	getCountries: function(req, res){
		Country.find().exec(function(err, resData){
			if(err)
			return ServerError(err.message,res);
			else
			res.ok(resData);
		});
	},
	updateGeneralInfo: function (req, res) {
		GeneralService.updateGeneralInfo(req, function(err, resData){
			if(err)
			return NotFoundException(err.message, res);
			else
			res.ok(resData);
		});
	},
	getGeneralInfoByKey: function (req, res) {
		GeneralService.getGeneralInfoByKey(req, function(err, resData){
			if(err)
			return ServerError(err.message,res);
			else
			res.ok(resData);
		});
	},
	grandPermission: function (req, res) {
		GeneralService.grandPermission(req, function(err, resData){
			if(err)
			return ServerError(err.message,res);
			else
			res.ok(resData);
		});
	},
	grandStripePermission: function (req, res) {
		GeneralService.grandStripePermission(req, function(err, resData){
			if(err)
			return ServerError(err.message,res);
			else
			res.ok(resData);
		});
	},
	setCron: function (req, res) {
		GeneralService.setCron(req, function(err, resData){
			if(err)
			return ServerError(err.message,res);
			else
			res.ok(resData);
		});
	},
	addBank: function (req, res) {
		StripeService.addBank(req, function(err, resData){
			if(err)
			return ServerError(err.message,res);
			else
			res.ok(resData);
		});
	},
	stripeOnboarding: function (req, res) {
		StripeService.stripeOnboarding(req, function(err, resData){
			if(err)
			return ServerError(err.message,res);
			else
			res.ok(resData);
		});
	},

};
