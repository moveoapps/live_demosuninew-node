/**
 * UserController
 *
 * @description :: Server-side logic for managing users
 * @help        :: See http://links.sailsjs.org/docs/controllers
 */
var bcrypt = require('bcrypt');
var NotFoundException = require('../exceptions/NotFoundException');
var ValidationException = require('../exceptions/ValidationException');
var ServerError = require('../exceptions/ServerError.js');
var generator = require('generate-password');

const Joi = require('joi');

module.exports = {
  adminLogin: function (req, res) {
    AdminUser.findOne({'email':req.body.email}).exec(function (err, user) {
     // if (err) res.serverError(err);

      if (user) {
        AdminUser.isValidPassword(req.body.password, user, function(err, match) {
          if (match) {
            // password match
            if (user.is_active == 1) {
              var token = TokenService.sign({id: user.id,user_type: 5}, sails.config.project.tokenTTL);

              res.ok({user: user, token: token});
            } else {
                return NotFoundException('Sorry! your account is not active. PLease try again later.', res);
            }

          } else {
            //res.unauthorized();
            return NotFoundException('Email and Password does not match', res);
          }
        });

      } else {
        return NotFoundException('Email and Password does not match', res);
      }
    });
  },
  requestPasswordReset: function (req, res) {
    AdminUser.findOneByEmail(req.body.email).exec(function (err, user) {
      if (err) res.serverError(err);

      if (user) {
        // Create a verification token, which will expire in 2 hours
        token = TokenService.sign({id: user.id,user_type: 5}, sails.config.project.passwordReset.tokenTTL);

        var templateData = {
          to: user.email,
          from: sails.config.fromName+' <'+sails.config.fromEmail+'>',
          subject: sails.config.project.passwordReset.subject,
          userEmail: user.email,
          siteName: sails.config.project.name,
          url: sails.config .webUrl + sails.config.project.passwordReset.uri + token,
          base_url: sails.config.apiUrl,
          name: user.first_name+' '+user.last_name,
          curr_year: (new Date()).getFullYear()
        };

        EmailService.sendMail(sails.config.project.passwordReset.template, templateData, function(err, message) {
          if (err){
              return sails.log.error('> error sending password reset email', err);

          }

          sails.log.verbose('> sending password reset email to:', user.email);
        });

        res.ok({},'Mail send');
      } else {
        //res.notFound({ error: 'User not found' });
        return NotFoundException('This email does not exist.', res);
      }

    });
  },
  validatePasswordReset: function (req, res) {
    // Verify that the reset token has not expired
    token = req.param('token');
    TokenService.verify(token, function(err, data) {
        if(err) {
            res.serverError(err);
        } else {

            AdminUser.findOne({id: data.id}).exec(function (err, user) {
                if (err){
                    res.serverError(err);
                } else {
                    if (user) {
                        AdminUser.encryptPassword(req.body.password, function(err, encryptedPassword) {
                            user.password = encryptedPassword;
                            user.save();
                            res.ok({});
                        })
                    } else {
                        res.notFound({ error: 'User not found' });
                    }
                }
            });
        }
    });
  },
  forgotPassword: function (req, res) {
    User.findOneByEmail(req.body.email).exec(function (err, user) {
      if (err) res.serverError(err);

      if (user) {
        // Create a verification token, which will expire in 2 hours
        token = TokenService.sign({id: user.id,user_type: 5}, sails.config.project.ForgotPassword.tokenTTL);

        var password = generator.generate({
            length: 10,
            numbers: true
        });

        User.encryptPassword(password, function(err, encryptedPassword) {
            user.password = encryptedPassword;
            user.save();

             var templateData = {
              to: user.email,
              from: sails.config.fromName+' <'+sails.config.fromEmail+'>',
              subject: sails.config.project.ForgotPassword.subject,
              new_password: password,
              siteName: sails.config.project.name,
              base_url: sails.config.project.appUrl,
              name: user.first_name+' '+user.last_name,
              curr_year: (new Date()).getFullYear()
            };
            EmailService.sendMail(sails.config.project.ForgotPassword.template, templateData, function(err, message) {
              if (err) return sails.log.error('> error in sending email', err);
              sails.log.verbose('> sending forgot password email to:', user.email);
            });

            res.ok({});
        })


      } else {
        return NotFoundException('User not found', res);
      }
    });
  },
  changePassword: function (req, res) {
      // Create a user record in the database.
      const schema = Joi.object().keys({
          old_password: Joi.string().required(),
          new_password: Joi.string().required(),
      });

      const result = Joi.validate(req.body, schema);
      if(result.error===null){

        UserService.changePassword(req, function(err, user){
          if(err)
            return NotFoundException(err.message, res);
          else{
            res.ok(user);
          }
        });
      } else {
        return ValidationException(result.error.details[0].message,res);
      }
  },

  logout: function (req, res) {
    AdminUserService.logout(req, function(err, response){
      if(err)
        return NotFoundException(err.message, response);
      else{
        res.ok(response);
      }
    });
  },

  // updateAdminUser: function (req, res) {
  //   //Update user profile
  //   const schema = Joi.object().keys({
  //       first_name: Joi.string().required(),
  //       last_name: Joi.string().required(),
  //       email: Joi.string().required().email(),
  //       is_active: Joi.string().required().allow(''),
  //       avatar: Joi.string().allow('')
  //   });
  //
  //   const result = Joi.validate(req.body, schema);
  //   if(result.error===null){
  //     criteria = {id: req.param('id')};
  //     AdminUserService.updateAdminUser(req, criteria, function(err, user){
  //       if(err)
  //         return ServerError(err.message, res);
  //       else
  //         res.ok(user[0]);
  //     });
  //   }else{
  //     return ValidationException(result.error.details[0].message,res);
  //   }
  //   //NotFoundException("called", res);
  // },

  updateAdminProfile: function (req, res) {
    //Update user profile
    const schema = Joi.object().keys({
        first_name: Joi.string().required(),
        last_name: Joi.string().required(),
        email: Joi.string().required().email(),
        password: Joi.string().allow(''),
        avatar: Joi.string().allow('')
    });

    const result = Joi.validate(req.body, schema);
    if(result.error===null){

      AdminUserService.updateAdminProfile(req, function(err, user){

        if(err)
          return ServerError(err.message, res);
        else
          res.ok(user);
      });
    }else{
      return ValidationException(result.error.details[0].message,res);
    }
    //NotFoundException("called", res);
  },

  fetch: function (req, res) {
    criteria = {};
    AdminUserService.fetchData(req, criteria, function(err, userData){
      if(err)
        return NotFoundException(err.message, res);
      else{
        res.ok(userData);
      }
    });
  },
  getUser: function (req, res) {
      criteria = {id: req.param('id'), is_admin: 0};
      UserService.fetchData(req, criteria, function(err, resData){
      if(err)
        return ServerError(err.message,res);
      else
        res.ok(resData[0]);
    });
  },

  //backend apis
  getAdminUsers: function (req, res) {
    var userData  = TokenService.decode(req.headers.authorization);

		criteria = {id:{'!' : userData.id}};
	    AdminUserService.fetchData(req, criteria, function(err, resData){
		  if(err)
		    return ServerError(err.message,res);
		  else
		    res.ok(resData);
		});
  	},
  getActiveAdminUsers: function (req, res) {
		criteria = {is_active: 1};
	    AdminUserService.fetchData(req, criteria, function(err, resData){
		  if(err)
		    return ServerError(err.message,res);
		  else
		    res.ok(resData);
		});
  	},
  getActiveRoles: function (req, res) {
  		criteria = {is_active: 1};
  	    AdminUserService.getActiveRoles(req, criteria, function(err, resData){
  		  if(err)
  		    return ServerError(err.message,res);
  		  else
  		    res.ok(resData);
  		});
    },
  addAdminUser: function (req, res) {
	    AdminUserService.addAdminUser(req, function(err, resData){
		  if(err)
		    return NotFoundException(err.message, res);
		  else
		    res.ok(resData);
		});
  	},
  updateAdminUser: function (req, res) {
  		criteria = {id: req.param('id')};
	    AdminUserService.updateAdminUser(req, req.body, criteria, function(err, resData){
		  if(err)
		    return ServerError(err.message,res);
		  else
		    res.ok(resData);
		});
  	},
  updateAdminUserBulk: function (req, res) {
  		criteria = {id: req.body.adminuserArray};
	    AdminUserService.updateAdminUser(req, req.body.adminuserData, criteria, function(err, resData){
		  if(err)
		    return ServerError(err.message,res);
		  else
		    res.ok(resData);
		});
  	},
  getAdminUser: function (req, res) {
		criteria = {id: req.param('id')};
	    AdminUserService.fetchData(req, criteria, function(err, resData){
		  if(err)
		    return ServerError(err.message,res);
		  else
		    res.ok(resData[0]);
		});
  	},
  deleteAdminUser: function (req, res) {
  		criteria = {id: req.body.adminuserArray};
	    AdminUserService.deleteAdminUser(req, criteria, function(err, resData){
		  if(err)
		    return ServerError(err.message,res);
		  else
		    res.ok(resData);
		});
  	},
  adminUserPasswordSet: function (req, res) {
      // Verify that the reset token has not expired
      token = req.param('token');

      TokenService.verify(token, function(err, data) {
          if(err) {
              res.serverError(err);
          } else {

              AdminUser.findOne({id: data.id}).exec(function (err, adminuser) {
                  if (err){
                      res.serverError(err);
                  } else {
                      if (adminuser) {
                          AdminUser.encryptPassword(req.body.password, function(err, encryptedPassword) {
                              adminuser.password = encryptedPassword;
                              adminuser.save();
                              res.ok({});
                          })
                      } else {
                          res.notFound({ error: 'Admin User not found' });
                      }
                  }
              });
          }
      });
    },
    routeAllow: function (req, res) {
        var userData  = TokenService.decode(req.headers.authorization);
        AdminUserService.routeAllow(req, userData, function(err, resData){
  		  if(err)
  		    return ServerError(err.message,res);
  		  else
  		    res.ok(resData);
  		});
    },
}
