/**
 * CmsController
 *
 * @description :: Server-side logic for managing users
 * @help        :: See http://links.sailsjs.org/docs/controllers
 */
var NotFoundException = require('../exceptions/NotFoundException');
var ValidationException = require('../exceptions/ValidationException');
var ServerError = require('../exceptions/ServerError.js');

const Joi = require('joi');

module.exports = {

  //backend apis
  getCmses: function (req, res) {
    criteria = {};
	    CmsService.fetchData(req, criteria, function(err, resData){
		  if(err)
		    return ServerError(err.message,res);
		  else
		    res.ok(resData);
		});
  },
  addCms: function (req, res) {
	    CmsService.addCms(req, function(err, resData){
		  if(err)
		    return NotFoundException(err.message, res);
		  else
		    res.ok(resData);
		});
  },
  updateCms: function (req, res) {
  		criteria = {id: req.param('id')};
	    CmsService.updateCms(req, req.body, criteria, function(err, resData){
		  if(err)
		    return ServerError(err.message,res);
		  else
		    res.ok(resData);
		});
  },
  updateCmsBulk: function (req, res) {
  		criteria = {id: req.body.cmsArray};
	    CmsService.updateCms(req, req.body.cmsData, criteria, function(err, resData){
		  if(err)
		    return ServerError(err.message,res);
		  else
		    res.ok(resData);
		});
  },
  getCms: function (req, res) {
		criteria = {id: req.param('id')};
	    CmsService.fetchData(req, criteria, function(err, resData){
		  if(err)
		    return ServerError(err.message,res);
		  else
		    res.ok(resData[0]);
		});
  },
  deleteCms: function (req, res) {
  		criteria = {id: req.body.cmsArray};
	    CmsService.deleteCms(req, criteria, function(err, resData){
		  if(err)
		    return ServerError(err.message,res);
		  else
		    res.ok(resData);
		});
  },
}
