/**
 * isAuthenticated
 *
 * @module      :: Policy
 * @description :: Simple policy to allow any authenticated
 *                 Assumes that the request has a valid JWT token in `req.headers.Authorization`
 * @docs        :: http://sailsjs.org/#!documentation/policies
 *
 */

module.exports = function(req, res, next) {

  if(req.headers.authorization) {
    InvalidtokenService.check(req.headers.authorization, function (err, token) {
      if (err || token.length > 0) {
        return res.forbidden('Your token is not valid.');
      } else {
        next()
      }
    });
  }
};
