/**
* User.js
*
* @description :: This is a basic user model which enables supports user registration and password resets.
* @docs        :: http://sailsjs.org/#!documentation/models
*/

module.exports = {
  tableName: 'cms',
  migrate: 'safe',
  autoPk: true,

  attributes: {

    title: {
      type: 'string',
      required: true
    },
    slug: {
      type: 'string',
      required: true
    },
    description: {
      type: 'longtext',
      required: false
    },


    toJSON: function() {
      var obj = this.toObject();
      obj = Utils.cleanObj(obj);

      return obj;
    }
  },


};
