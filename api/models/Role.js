/**
* User.js
*
* @description :: This is a basic user model which enables supports user registration and password resets.
* @docs        :: http://sailsjs.org/#!documentation/models
*/


module.exports = {
    autoPk: true,
    migrate: 'alter',
    tableName: 'role',
    attributes: {
        name: {
            type: 'string',
            required: true
        },
        is_active: {
            type: 'boolean',
            defaultsTo: true
        },

        // REFERENCES
        modules: {
            collection: 'module',
            via: 'role',
            through: 'permission'
        },
        toJSON: function() {
            var obj = this.toObject();
            return obj;
        }
    },
};
