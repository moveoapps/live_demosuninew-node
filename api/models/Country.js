/**
 * Country.js
 *
 * @description :: TODO: You might write a short summary of how this model works and what it represents here.
 * @docs        :: http://sailsjs.org/documentation/concepts/models-and-orm/models
 */

module.exports = {

  autoPk: true,
  migrate: 'safe',
  tableName: 'countries',

  attributes: {
    name: {
      type: 'string',
      required: true
    },
    iso_code_2: {
      type: 'string',
      required: true
    },
    iso_code_3: {
      type: 'email',
      required: true,
      unique: true
    },
    country_code: {
      required: true,
      type: 'string'
    },
    is_active: {
      type: 'boolean',
      defaultsTo: false
    },

    //Add a reference to user
    users: {
      collection: 'user',
      via: 'country'
    },


    toJSON: function() {
      var obj = this.toObject();
      delete obj.is_active;
      return obj;
    }
  },
};
