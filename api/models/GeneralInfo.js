/**
* GeneralInfo.js
*
* @description :: This is a basic user model which enables supports user registration and password resets.
* @docs        :: http://sailsjs.org/#!documentation/models
*/

module.exports = {
  tableName: 'general_info',
  migrate: 'safe',
  autoPk: true,
  autoCreatedAt: false,
  autoUpdatedAt: false,

  attributes: {

    option: {
      type: 'string',
      required: true
    },
    value: { // minimum price per view
      type: 'string',
      required: true
    },

    toJSON: function() {
      var obj = this.toObject();
      obj = Utils.cleanObj(obj);

      return obj;
    }
  },


};
