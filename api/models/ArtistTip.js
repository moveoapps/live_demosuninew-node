/**
* User.js
*
* @description :: This is a basic user model which enables supports user registration and password resets.
* @docs        :: http://sailsjs.org/#!documentation/models
*/

module.exports = {
  tableName: 'artist_tip',
  migrate: 'alter',
  autoPk: true,

  attributes: {

    artist: {
      model: 'user',
    },
    show: {
      model: 'show',
    },
    user: {
      model: 'user',
    },
    emoji_id: {
      type: 'integer',
    },
    tip_type: {
      type: 'integer',
      enum: [1, 2, 3], // 1 = Dollar Tip, 2 = Emoji Tip, 3 = Tip From Admin
    },
    amount: {
      type: 'float',
    },
    order_id: {
      model: 'showorder',
    },
    payout_done: {
        type: 'boolean',
        defaultsTo: false
    },

    toJSON: function() {
      var obj = this.toObject();
      obj = Utils.cleanObj(obj);

      return obj;
    }
  },


};
