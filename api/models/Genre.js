/**
* User.js
*
* @description :: This is a basic user model which enables supports user registration and password resets.
* @docs        :: http://sailsjs.org/#!documentation/models
*/

module.exports = {
  tableName: 'genre',
  migrate: 'safe',
  autoPk: true,

  attributes: {

    name: {
      type: 'string',
      required: true
    },
    slug: {
      type: 'string',
      required: true
    },
    cover_image: {
      type: 'string',
      required: false
    },
    is_active: {
      type: 'boolean',
      defaultsTo: true
    },

    // reference
    shows: {
      collection: 'show',
      via: 'show',
      through: 'showgenre'
    },
    toJSON: function() {
      var obj = this.toObject();
      obj = Utils.cleanObj(obj);

      if(obj.cover_image == "" || typeof obj.cover_image == 'undefined')
        obj.cover_image="";
      else
        obj.cover_image = sails.config.s3bucket.bucketurl+sails.config.s3bucket.genremedia+'/'+obj.cover_image;

      return obj;
    }
  },

  afterDestroy: function (destroyedRecords, next) {

    const options = { key: sails.config.s3bucket.accesskey, secret: sails.config.s3bucket.secretkey, bucket: sails.config.s3bucket.bucketname}

    // Create and configure an adapter
    const adapter = require('skipper-better-s3')(options);
    _.each(destroyedRecords,function(indvGenre){

      adapter.rm('genre/'+indvGenre.cover_image, (err, res) => {
      });

    });
    next(null, true);
  },


};
