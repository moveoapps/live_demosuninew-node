/**
* User.js
*
* @description :: This is a basic user model which enables supports user registration and password resets.
* @docs        :: http://sailsjs.org/#!documentation/models
*/

module.exports = {
  tableName: 'user_purchase_history',
  migrate: 'alter',
  autoPk: true,

  attributes: {

    user: {
      model: 'user',
    },
    amount: {
      type: 'float',
    },
    order_id: {
      type: 'string',
    },
    status: {
      type: 'string',
    },
    payment_date: {
      type: 'datetime',
    },
    payment_type: {
      type: 'integer',
      enum: [1, 2, 3, 4], // 1 = Tipping Dollar, 2 = Show Ticket, 3 = Gift Ticket, 4 = On-demand Payment
    },

    toJSON: function() {
      var obj = this.toObject();
      obj = Utils.cleanObj(obj);

      return obj;
    }
  },

};
