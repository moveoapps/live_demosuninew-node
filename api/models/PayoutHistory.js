/**
* ShowList.js
*
* @description :: This is a basic user model which enables supports user registration and password resets.
* @docs        :: http://sailsjs.org/#!documentation/models
*/

module.exports = {
  tableName: 'payout_history',
  migrate: 'alter',
  autoPk: true,

  attributes: {

    show: {
      model: 'show',
    },
    response: {
      type: 'mediumtext',
    },

    toJSON: function() {
      var obj = this.toObject();
      obj = Utils.cleanObj(obj);

      return obj;
    }
  },


};
