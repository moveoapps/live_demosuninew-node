/**
* Tier.js
*
* @description :: This is a basic user model which enables supports user registration and password resets.
* @docs        :: http://sailsjs.org/#!documentation/models
*/

module.exports = {
  tableName: 'setlist',
  migrate: 'alter',
  autoPk: true,

  attributes: {

    show:{
      model: 'show',
    },
    show_url: {
      type: 'string',
    },
    licence: {
      type: 'string',
    },
    qtr_end: {
      type: 'string',
    },
    music_artist: {
      type: 'string',
    },

    toJSON: function() {
      var obj = this.toObject();
      obj = Utils.cleanObj(obj);

      return obj;
    }
  },


};
