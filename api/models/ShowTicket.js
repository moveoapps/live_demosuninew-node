/**
* ShowList.js
*
* @description :: This is a basic user model which enables supports user registration and password resets.
* @docs        :: http://sailsjs.org/#!documentation/models
*/

module.exports = {
  tableName: 'show_ticket',
  migrate: 'alter',
  autoPk: true,

  attributes: {
    show: {
      model: 'show',
    },
    user: {
      model: 'user',
    },
    amount: {
      type: 'float',
    },
    booking_id: {
      type: 'string',
    },
    order_id: {
      model: 'showorder',
    },
    payout_done: {
        type: 'boolean',
        defaultsTo: false
    },
    is_free: {
      type: 'boolean',
      defaultsTo: false
  },
    toJSON: function() {
      var obj = this.toObject();
      obj = Utils.cleanObj(obj);

      return obj;
    }
  },


};
