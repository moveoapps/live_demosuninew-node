/**
* User.js
*
* @description :: This is a basic user model which enables supports user registration and password resets.
* @docs        :: http://sailsjs.org/#!documentation/models
*/

module.exports = {
    tableName: 'ondemand',
    migrate: 'alter',
    autoPk: true,

    attributes: {
        artist: {
            model: 'user',
        },
        genre: {
            model: 'genre',
        },
        title: {
            type: 'string',
            required: false
        },
        media_name: {
            type: 'string',
        },
        thumb_name: {
            type: 'string',
        },
        ondemand_price: {
            type: 'float',
        },
        is_paid: {
          type: 'boolean',
          defaultsTo: true
        },
        is_featured: {
          type: 'boolean',
          defaultsTo: false
        },
        vodpayment: {
          collection: 'vodpayment',
          via: 'ondemand',
        },
        showorder: {
          collection: 'showorder',
          via: 'ondemand',
        },
        toJSON: function() {
            var obj = this.toObject();
            obj = Utils.cleanObj(obj);

            //obj.vod_type = 2;
            if(obj.media_name == "" || typeof obj.media_name == 'undefined') {
                obj.media_name="";
            } else {
                var filename = obj.media_name;
                // var thumbFileName = filename.split('.');
                // var thumbname = thumbFileName[0]+'.png';
                // obj.thumb = sails.config.s3bucket.bucketurl+sails.config.s3bucket.ondemandthumb+'/'+thumbname;
                obj.media_name = sails.config.s3bucket.bucketurl+sails.config.s3bucket.ondemand+'/'+obj.media_name;
            }

            if(obj.thumb_name == "" || typeof obj.thumb_name == 'undefined'){
                obj.thumb_name = "";
            }else{
                var thumbname = obj.thumb_name;
                obj.thumb = sails.config.s3bucket.bucketurl+sails.config.s3bucket.ondemandthumb+'/'+thumbname;
            }
            //delete obj.user_type;
            delete obj.createdAt;
            delete obj.updatedAt;
            //console.log('obj',obj);
            return obj;
        }
    },
    afterDestroy: function (destroyedRecords, next) {

      const options = { key: sails.config.s3bucket.accesskey, secret: sails.config.s3bucket.secretkey, bucket: sails.config.s3bucket.bucketname}

      // Create and configure an adapter
      const adapter = require('skipper-better-s3')(options);
      _.each(destroyedRecords,function(indvUser){

            adapter.rm(sails.config.s3bucket.ondemand+'/'+indvUser.media_name, (err, res) => {
            });
            var filename = indvUser.media_name;
            var thumbFileName = filename.split('.');
            // var thumbname = thumbFileName[0]+'.png';
            var thumbname = indvUser.thumb_name;
            adapter.rm(sails.config.s3bucket.ondemandthumb+'/'+thumbname, (err, res) => {
            });

      });
      next(null, true);
    },
};
