/**
* User.js
*
* @description :: This is a basic user model which enables supports user registration and password resets.
* @docs        :: http://sailsjs.org/#!documentation/models
*/


module.exports = {

    autoPk: true,
    migrate: 'alter',
    tableName: 'permission',

    attributes: {
        role: {
            model: 'role'
        },
        module: {
            model: 'module'
        },
        add: {
            type: 'boolean',
            defaultsTo: true
        },
        edit: {
            type: 'boolean',
            defaultsTo: true
        },
        delete: {
            type: 'boolean',
            defaultsTo: true
        },
        toJSON: function() {
            var obj = this.toObject();
            return obj;
        }
    },
};
