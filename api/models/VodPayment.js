/**
* ShowList.js
*
* @description :: This is a basic user model which enables supports user registration and password resets.
* @docs        :: http://sailsjs.org/#!documentation/models
*/

module.exports = {
  tableName: 'vod_payment',
  migrate: 'alter',
  autoPk: true,

  attributes: {
    show: {
      model: 'show',
    },
    ondemand: {
      model: 'ondemand',
    },
    vod_type: {
      type: 'integer',
      enum: [1, 2], // 1 = Show VOD, 2 = Static Vod
    },
    user: {
      model: 'user',
    },
    amount: {
      type: 'float',
    },
    booking_id: {
      type: 'string',
    },
    order_id: {
      model: 'showorder',
    },
    payout_done: {
        type: 'boolean',
        defaultsTo: false
    },
    toJSON: function() {
      var obj = this.toObject();
      obj = Utils.cleanObj(obj);

      return obj;
    }
  },


};
