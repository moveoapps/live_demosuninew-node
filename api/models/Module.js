/**
* User.js
*
* @description :: This is a basic user model which enables supports user registration and password resets.
* @docs        :: http://sailsjs.org/#!documentation/models
*/


module.exports = {

    autoPk: true,
    migrate: 'alter',
    tableName: 'modules',

    attributes: {
        name: {
            type: 'string',
            required: true
        },
        add_data: {
            type: 'boolean',
            defaultsTo: true
        },
        edit_data: {
            type: 'boolean',
            defaultsTo: true
        },
        delete_data: {
            type: 'boolean',
            defaultsTo: true
        },
        is_active: {
            type: 'boolean',
            defaultsTo: true
        },

        // REFERENCES
        roles: {
            collection: 'role',
            via: 'module',
            through: 'permission'
        },
        toJSON: function() {
            var obj = this.toObject();
            return obj;
        }
    },
};
