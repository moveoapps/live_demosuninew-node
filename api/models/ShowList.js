/**
* ShowList.js
*
* @description :: This is a basic user model which enables supports user registration and password resets.
* @docs        :: http://sailsjs.org/#!documentation/models
*/

module.exports = {
  tableName: 'showlist',
  migrate: 'alter',
  autoPk: true,

  attributes: {

    show: {
      model: 'show',
    },
    song_name: {
      type: 'string',
      required: true
    },
    song_writers: {
      type: 'text',
      required: true
    },
    ori_perfomer: {
      type: 'string',
      required: true
    },
    record_label: {
      type: 'string',
      required: true
    },
    duration_second: {
      type: 'integer',
      defaultsTo: 0
    },
    duration_minute: {
      type: 'integer',
      defaultsTo: 0
    },
    is_active: {
      type: 'boolean',
      defaultsTo: true
    },
    toJSON: function() {
      var obj = this.toObject();
      obj = Utils.cleanObj(obj);

      return obj;
    }
  },


};
