/**
* User.js
*
* @description :: This is a basic user model which enables supports user registration and password resets.
* @docs        :: http://sailsjs.org/#!documentation/models
*/

module.exports = {
    tableName: 'artist_balance',
    migrate: 'alter',
    autoPk: true,

    attributes: {

        user: {
            model: 'user',
        },
        amount: {
            type: 'integer',
        },
        current_balance: {
            type: 'integer',
        },
        balance_type: {
            type: 'integer',
            enum: [1, 2], // 1 = Add, 2 = Substract
        },
        description: {
            type: 'string',
        },


        toJSON: function() {
            var obj = this.toObject();
            obj = Utils.cleanObj(obj);

            return obj;
        }
    },


};
