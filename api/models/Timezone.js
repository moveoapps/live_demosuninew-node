/**
 * Country.js
 *
 * @description :: TODO: You might write a short summary of how this model works and what it represents here.
 * @docs        :: http://sailsjs.org/documentation/concepts/models-and-orm/models
 */

module.exports = {

  autoPk: true,
  migrate: 'safe',
  tableName: 'timezone',
  autoCreateAt: false,
  autoUpdateAt: false,
  attributes: {
    country_code: {
      type: 'string',
      required: true
    },
    zone_name: {
      type: 'string',
      required: true
    },
    code: {
      type: 'string'
    },
    order_number: {
      type: 'integer',
    },

    // Add a reference to user
    users: {
      collection: 'user',
      via: 'timezone'
    },
    shows: {
      collection: 'user',
      via: 'timezone'
    },

    toJSON: function() {
      var obj = this.toObject();
      delete obj.is_active;
      return obj;
    }
  },
};
