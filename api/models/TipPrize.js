/**
* Faq.js
*
* @description :: This is a basic user model which enables supports user registration and password resets.
* @docs        :: http://sailsjs.org/#!documentation/models
*/

module.exports = {
  tableName: 'tip_prize',
  migrate: 'alter',
  autoPk: true,

  attributes: {

    prize: {
      type: 'string',
      required: true
    },

    toJSON: function() {
      var obj = this.toObject();
      obj = Utils.cleanObj(obj);

      return obj;
    }
  },


};
