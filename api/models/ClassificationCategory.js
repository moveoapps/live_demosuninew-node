/**
* ClassificationCategory.js
*
* @description :: This is a basic user model which enables supports user registration and password resets.
* @docs        :: http://sailsjs.org/#!documentation/models
*/

module.exports = {
  tableName: 'classification_category',
  migrate: 'alter',
  autoPk: true,

  attributes: {

    name: {
      type: 'string',
      required: true
    },
    is_active: {
      type: 'boolean',
      defaultsTo: true
    },

    // Add a reference to user
    // show: {
    //   collection: 'show',
    //   via: 'classificationcategory'
    // },

    toJSON: function() {
      var obj = this.toObject();
      obj = Utils.cleanObj(obj);

      return obj;
    }
  },


};
