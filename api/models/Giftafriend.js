/**
* Tier.js
*
* @description :: This is a basic user model which enables supports user registration and password resets.
* @docs        :: http://sailsjs.org/#!documentation/models
*/

module.exports = {
  tableName: 'giftafriend',
  migrate: 'alter',
  autoPk: true,

  attributes: {

    to_user: {
      model: 'user',
    },
    user: {
      model: 'user',
    },
    show_id:{
      model: 'show',
    },

    toJSON: function() {
      var obj = this.toObject();
      obj = Utils.cleanObj(obj);

      return obj;
    }
  },


};
