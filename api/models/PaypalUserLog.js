/**
* User.js
*
* @description :: This is a basic user model which enables supports user registration and password resets.
* @docs        :: http://sailsjs.org/#!documentation/models
*/


module.exports = {
    autoPk: true,
    migrate: 'alter',
    tableName: 'paypal_user_log',
    attributes: {
        merchantId: {
            type: 'string',
            required: true
        },
        merchantIdInPayPal: {
            type: 'string',
        },
        isEmailConfirmed: {
            type: 'string',
        },
        permissionsGranted: {
            type: 'string',
        },
        accountStatus: {
            type: 'string',
        },
        productIntentID: {
            type: 'string',
        },
        consentStatus: {
            type: 'string',
        },
        returnMessage: {
            type: 'string',
        },


        toJSON: function() {
            var obj = this.toObject();
            return obj;
        }
    },
};
