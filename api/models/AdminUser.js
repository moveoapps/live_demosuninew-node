/**
* User.js
*
* @description :: This is a basic user model which enables supports user registration and password resets.
* @docs        :: http://sailsjs.org/#!documentation/models
*/

var bcrypt = require('bcrypt');

module.exports = {

  autoPk: true,
  migrate: 'safe',
  tableName: 'admin_users',

  attributes: {
    first_name: {
      type: 'string',
      required: true
    },
    last_name: {
      type: 'string',
      required: false
    },
    email: {
      type: 'email',
      required: true,
      unique: true
    },
    password: {
      required: false,
      type: 'string'
    },
    mobile: {
      type: 'string',
      required: false,
      //unique: true,
      defaultsTo: ''
    },
    avatar: {
      required: false,
      type: 'string',
      defaultsTo: ''
    },
    role: {
        model: 'role'
    },
    is_active: {
      type: 'boolean',
      defaultsTo: true
    },



    toJSON: function() {
      var obj = this.toObject();

      //console.log('obj.bio=='+typeof obj.bio);
      if(obj.avatar == "" || typeof obj.avatar == 'undefined')
        obj.avatar="";
      else
        obj.avatar = sails.config.s3bucket.bucketurl+sails.config.s3bucket.userprofile+obj.avatar;


      //delete obj.password;
      delete obj.verification_code;
      delete obj.email_verification_code;

      return obj;
    }
  },

  // Lifecycle Callbacks
  beforeCreate: function (properties, next) {
    if (properties.password) {
      this.encryptPassword(properties.password, function(err, encryptedPassword) {
        if (err) return next(err);
        properties.password = encryptedPassword;
        next()
      });
    } else {
      next()
    }

  },

  encryptPassword: function(password, next) {
    bcrypt.genSalt(10, function(err, salt) {
      if (err) return next(err);

      bcrypt.hash(password, salt, function(err, hash) {
        if (err) return next(err);

        next(null, hash)
      });
    });
  },

  isValidPassword: function(password, user, next) {
    bcrypt.compare(password, user.password, function(err, match) {
      if (err) {
        next(err);
      }

      if (match) {
        next(null, true);
      } else {
        next(err);
      }
    });
  },
  afterDestroy: function (destroyedRecords, next) {

    const options = { key: sails.config.s3bucket.accesskey, secret: sails.config.s3bucket.secretkey, bucket: sails.config.s3bucket.bucketname}

    // Create and configure an adapter
    const adapter = require('skipper-better-s3')(options);
    _.each(destroyedRecords,function(indvUser){

      adapter.rm('avatar/'+indvUser.avatar, (err, res) => {
      });

    });
    next(null, true);
  },

};
