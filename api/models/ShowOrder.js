/**
* ShowList.js
*
* @description :: This is a basic user model which enables supports user registration and password resets.
* @docs        :: http://sailsjs.org/#!documentation/models
*/

module.exports = {
  tableName: 'show_order',
  migrate: 'alter',
  autoPk: true,

  attributes: {
    order_id: {
      type: 'string',
    },
    payment_id: {
      type: 'string',
    },
    amount: {
      type: 'float',
    },
    tax: {
      type: 'float',
    },
    total_amount: {
      type: 'float',
    },
    show: {
      model: 'show',
    },
    ondemand: {
      model: 'ondemand',
    },
    user: {
      model: 'user',
    },
    order_type: {
      type: 'integer',
      enum: [1, 2, 3, 4, 5, 6], // 1 = Show Ticket, 2 = Tip Amount, 3 = Tip Emoji, 4 = Gift a friend, 5 = show Ondemand, 6 = Static ondemand
    },
    approval_url: {
      type: 'string',
    },
    status: {
      type: 'string',
    },
    emoji: {
      model: 'emoji',
    },
    gift_email: {
      type: 'string',
    },
    capture_id: {
      type: 'string',
    },
    // Add a reference to user
    artistips: {
      collection: 'artisttip',
      via: 'order_id'
    },
    showtickets: {
      collection: 'showticket',
      via: 'order_id'
    },
    vodpayments: {
      collection: 'vodpayment',
      via: 'order_id'
    },
    toJSON: function() {
      var obj = this.toObject();
      obj = Utils.cleanObj(obj);

      return obj;
    }
  },


};
