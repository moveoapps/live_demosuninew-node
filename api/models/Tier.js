/**
* Tier.js
*
* @description :: This is a basic user model which enables supports user registration and password resets.
* @docs        :: http://sailsjs.org/#!documentation/models
*/

module.exports = {
  tableName: 'tiers',
  migrate: 'safe',
  autoPk: true,

  attributes: {

    name: {
      type: 'string',
      required: true
    },
    min_ppv: { // minimum price per view
      type: 'string',
      required: true
    },
    max_ppv: { // maximum price per view
      type: 'string',
      required: true
    },
    is_active: {
      type: 'boolean',
      defaultsTo: true
    },
    // Add a reference to user
    users: {
      collection: 'user',
      via: 'tier'
    },

    toJSON: function() {
      var obj = this.toObject();
      obj = Utils.cleanObj(obj);

      return obj;
    }
  },


};
