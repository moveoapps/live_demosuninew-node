/**
* Faq.js
*
* @description :: This is a basic user model which enables supports user registration and password resets.
* @docs        :: http://sailsjs.org/#!documentation/models
*/

module.exports = {
  tableName: 'faqs',
  migrate: 'alter',
  autoPk: true,

  attributes: {

    question: {
      type: 'string',
      required: true
    },
    answer: { // minimum price per view
      type: 'text',
      required: true
    },
    is_active: {
      type: 'boolean',
      defaultsTo: true
    },

    toJSON: function() {
      var obj = this.toObject();
      obj = Utils.cleanObj(obj);

      return obj;
    }
  },


};
