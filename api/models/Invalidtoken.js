/**
 * Invalidtoken.js
 *
 * @description :: TODO: You might write a short summary of how this model works and what it represents here.
 * @docs        :: http://sailsjs.org/documentation/concepts/models-and-orm/models
 */

module.exports = {

  autoPk: true,
  migrate: 'safe',
  tableName: 'Invalidtoken',

  attributes: {
    token: {
      type: 'string',
      required: true
    },
    toJSON: function() {
      var obj = this.toObject();
      return obj;
    }
  },
};
