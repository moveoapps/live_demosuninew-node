/**
* User.js
*
* @description :: This is a basic user model which enables supports user registration and password resets.
* @docs        :: http://sailsjs.org/#!documentation/models
*/

module.exports = {
  tableName: 'show_tip_prize',
  migrate: 'alter',
  autoPk: true,

  attributes: {

    show: {
      model: 'show',
    },
    tipprize: {
      model: 'tipprize',
    },
    toJSON: function() {
      var obj = this.toObject();
      obj = Utils.cleanObj(obj);

      return obj;
    }
  },


};
