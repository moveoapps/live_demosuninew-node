/**
* User.js
*
* @description :: This is a basic user model which enables supports user registration and password resets.
* @docs        :: http://sailsjs.org/#!documentation/models
*/

var bcrypt = require('bcrypt');

module.exports = {
    tableName: 'users',
    migrate: 'alter',
    autoPk: true,

    attributes: {
        name: {
            type: 'string',
            required: false
        },
        email: {
            type: 'string',
            required: false
        },
        password: {
            required: false,
            type: 'string'
        },
        balance: {
            type: 'integer',
            defaultsTo: 0
        },
        user_type: {
            type: 'string',
            enum: [1, 2], // 1 = artist, 2 = member
        },
        country: {
            model: 'country',
        },
        timezone: {
            model: 'timezone',
        },
        tier: {
            model: 'tier',
            defaultsTo: 1
        },
        social_type: {
            type: 'string',
            enum: [1, 2], // 1 = Facebook, 2 = Google
        },
        social_id: {
            type: 'string',
            required: false
        },
        date_of_birth: {
            type: 'date',
            required: false
        },
        phone: {
            type: 'string',
            required: false
        },
        profile_picture: {
            required: false,
            type: 'string',
            defaultsTo: ''
        },
        bio: {
            required: false,
            type: 'text'
        },
        website_url: {
            required: false,
            type: 'string'
        },
        facebook_url: {
            required: false,
            type: 'string'
        },
        twitter_url: {
            required: false,
            type: 'string'
        },
        instagram_url: {
            required: false,
            type: 'string'
        },
        other_url: {
            required: false,
            type: 'string'
        },
        paypal_action_url: {
            required: false,
            type: 'string'
        },
        partner_referral_id: {
            required: false,
            type: 'string'
        },
        partner_client_id: {
            required: false,
            type: 'string'
        },
        merchantIdInPayPal: {
            required: false,
            type: 'string'
        },
        stripe_account_id: {
            required: false,
            type: 'string'
        },
        stripe_bank_connected: {
            type: 'boolean',
            defaultsTo: false
        },
        is_verified: {
            type: 'boolean',
            defaultsTo: false
        },
        is_active: {
            type: 'boolean',
            defaultsTo: true
        },
        is_delete: {
            type: 'boolean',
            defaultsTo: false
        },
        is_active_mail_sent: {
            type: 'boolean',
            defaultsTo: false
        },
        artist_type: {
            type: 'string',
            enum: [1, 2], // 1 = emerging, 2 = established
        },
        payment_type: {
            type: 'string',
            enum: [1, 2], // 1 = PayPal, 2 = Stripe
        },
        gifted_user: {
            type: 'boolean',
            defaultsTo: false
        },
        firsttime_login: {
            type: 'boolean',
            defaultsTo: true
        },

        // reference
        followers: {
            collection : 'artistfollowers',
            via : 'artist'
        },
        following: {
            collection : 'artistfollowers',
            via : 'follower'
        },
        artistmedia: {
            collection : 'artistmedia',
            via : 'user'
        },
        show: {
            collection : 'show',
            via : 'artist'
        },
        friends: {
            collection: 'giftafriend',
            via: 'user'
        },
        mygift: {
            collection: 'giftafriend',
            via: 'to_user'
        },
        artistbalancehistory: {
            collection: 'artistbalance',
            via: 'user'
        },
        tickets: {
            collection : 'showticket',
            via : 'user'
        },
        youtube: {
            collection : 'youtubevideo',
            via : 'user'
        },
        toJSON: function() {
            var obj = this.toObject();
            obj = Utils.cleanObj(obj);

            if(obj.profile_picture == "" || typeof obj.profile_picture == 'undefined'){
                obj.profile_picture="";
            }
            else{
                obj.thumb = sails.config.s3bucket.bucketurl+sails.config.s3bucket.userProfileThumb+'/'+obj.profile_picture;
                obj.profile_picture = sails.config.s3bucket.bucketurl+sails.config.s3bucket.userprofile+obj.profile_picture;
            }

            obj.tippingDollarValue = sails.config.project.tippingDollarValue;
            //delete obj.user_type;
            //delete obj.createdAt;
            delete obj.updatedAt;

            return obj;
        }
    },

    // Lifecycle Callbacks
    beforeCreate: function (properties, next) {
        if (properties.password) {
            this.encryptPassword(properties.password, function(err, encryptedPassword) {
                if (err) return next(err);
                properties.password = encryptedPassword;
                next()
            });
        } else {
            next()
        }

    },

    encryptPassword: function(password, next) {
        bcrypt.genSalt(10, function(err, salt) {
            if (err) return next(err);

            bcrypt.hash(password, salt, function(err, hash) {
                if (err) return next(err);

                next(null, hash)
            });
        });
    },

    isValidPassword: function(password, user, next) {
        bcrypt.compare(password, user.password, function(err, match) {
            if (err) {
                next(err);
            }

            if (match) {
                next(null, true);
            } else {
                next(err);
            }
        });
    },
    afterDestroy: function (destroyedRecords, next) {

        const options = { key: sails.config.s3bucket.accesskey, secret: sails.config.s3bucket.secretkey, bucket: sails.config.s3bucket.bucketname}

        // Create and configure an adapter
        const adapter = require('skipper-better-s3')(options);
        _.each(destroyedRecords,function(indvUser){

            adapter.rm('avatar/'+indvUser.avatar, (err, res) => {});
            adapter.rm(sails.config.s3bucket.userProfileThumb+'/'+indvUser.avatar, (err, res) => {});

            /*Artistfollowers.destroy({artist:indvUser.id}).exec(function (err, resData){ });
            ArtistBalance.destroy({user:indvUser.id}).exec(function (err, resData){ });
            ArtistMedia.destroy({user:indvUser.id}).exec(function (err, resData){ });

            OnDemand.destroy({artist:indvUser.id}).exec(function (err, resData){ });
            YoutubeVideo.destroy({user:indvUser.id}).exec(function (err, resData){ });

            Show.destroy({artist:indvUser.id}).exec(function (err, resData){ });
            ShowOrder.destroy({user:indvUser.id}).exec(function (err, resData){ });
            ShowTicket.destroy({user:indvUser.id}).exec(function (err, resData){ });*/

        });
        next(null, true);
    },


};
