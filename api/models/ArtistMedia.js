/**
* ArtistMedia.js
*
* @description :: This is a basic user model which enables supports user registration and password resets.
* @docs        :: http://sailsjs.org/#!documentation/models
*/


module.exports = {

  autoPk: true,
  migrate: 'alter',
  tableName: 'artist_media',

  attributes: {
    user: {
      model: 'user',
    },
    file_name: {
      type: 'string',
      required: true,
    },
    type: {
      type: 'string',
      enum: [1, 2] // 1 = Image, 2 = Video
    },

    toJSON: function() {
      var obj = this.toObject();
      obj = Utils.cleanObj(obj);

      if(obj.file_name == "" || typeof obj.file_name == 'undefined')
        obj.file_name="";
      else
        if(obj.type == 1){
          obj.thumb = sails.config.s3bucket.bucketurl+sails.config.s3bucket.artistmediaresizeimage+'/'+obj.file_name;
          obj.file_name = sails.config.s3bucket.bucketurl+sails.config.s3bucket.artistmediaimage+'/'+obj.file_name;
        }
        if(obj.type == 2){
          var filename = obj.file_name;
          var thumbFileName = filename.split('.');
          var thumbname = thumbFileName[0]+'.png';
          obj.thumb = sails.config.s3bucket.bucketurl+sails.config.s3bucket.artistmediavideothumb+'/'+thumbname;
          obj.file_name = sails.config.s3bucket.bucketurl+sails.config.s3bucket.artistmediavideo+'/'+obj.file_name;
        }

      //delete obj.user_type;
      delete obj.createdAt;
      delete obj.updatedAt;

      return obj;
    }
  },
  afterDestroy: function (destroyedRecords, next) {

    const options = { key: sails.config.s3bucket.accesskey, secret: sails.config.s3bucket.secretkey, bucket: sails.config.s3bucket.bucketname}

    // Create and configure an adapter
    const adapter = require('skipper-better-s3')(options);
    _.each(destroyedRecords,function(indvUser){
      if(indvUser.type == 1){
          adapter.rm(sails.config.s3bucket.artistmediaimage+'/'+indvUser.file_name, (err, res) => {
          });
          adapter.rm(sails.config.s3bucket.artistmediaresizeimage+'/'+indvUser.file_name, (err, res) => {
          });
      }else{
          adapter.rm(sails.config.s3bucket.artistmediavideo+'/'+indvUser.file_name, (err, res) => {
          });
          var filename = indvUser.file_name;
          var thumbFileName = filename.split('.');
          var thumbname = thumbFileName[0]+'.png';
          adapter.rm(sails.config.s3bucket.artistmediavideothumb+'/'+thumbname, (err, res) => {
          });
      }

    });
    next(null, true);
  },
};
