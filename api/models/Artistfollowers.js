/**
* User.js
*
* @description :: This is a basic user model which enables supports user registration and password resets.
* @docs        :: http://sailsjs.org/#!documentation/models
*/

module.exports = {
  tableName: 'artist_followers',
  migrate: 'safe',
  autoPk: true,

  attributes: {
    artist: {
      model: 'user',
    },
    follower: {
      model: 'user',
    },

    toJSON: function() {
      var obj = this.toObject();
      obj = Utils.cleanObj(obj);

      return obj;
    }
  },


};
