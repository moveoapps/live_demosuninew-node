/**
* YoutubeVideo.js
*
* @description :: A model definition represents a database table/collection.
* @docs        :: https://sailsjs.com/docs/concepts/models-and-orm/models
*/

module.exports = {
    tableName: 'youtube_video',
    migrate: 'alter',
    autoPk: true,

    attributes: {
        user: {
            model: 'user',
        },
        genre: {
            model: 'genre',
        },
        title: {
            type: 'text',
            required: false
        },
        youtube_link: {
            type: 'longtext',
            required: false
        },
        toJSON: function() {
          var obj = this.toObject();
          obj = Utils.cleanObj(obj);

          return obj;
        }
    },

};
