/**
* Emoji.js
*
* @description :: This is a basic user model which enables supports user registration and password resets.
* @docs        :: http://sailsjs.org/#!documentation/models
*/

module.exports = {
  tableName: 'emojis',
  migrate: 'alter',
  autoPk: true,

  attributes: {

    image: {
      type: 'string',
      required: true
    },
    value: { // value per Emoji
      type: 'integer',
      required: true
    },
    is_active: {
      type: 'boolean',
      defaultsTo: true
    },

    // Add a reference to user
    showemoji: {
      collection: 'showorder',
      via: 'emoji'
    },

    toJSON: function() {
      var obj = this.toObject();
      obj = Utils.cleanObj(obj);

      if(obj.image == "" || typeof obj.image == 'undefined')
        obj.image="";
      else
        obj.image = sails.config.s3bucket.bucketurl+sails.config.s3bucket.emojiimage+'/'+obj.image;

      return obj;
    }
  },

  afterDestroy: function (destroyedRecords, next) {

    const options = { key: sails.config.s3bucket.accesskey, secret: sails.config.s3bucket.secretkey, bucket: sails.config.s3bucket.bucketname}

    // Create and configure an adapter
    const adapter = require('skipper-better-s3')(options);
    _.each(destroyedRecords,function(indvEmoji){

      adapter.rm(sails.config.s3bucket.emojiimage+'/'+indvEmoji.image, (err, res) => {
      });

    });
    next(null, true);
  },


};
