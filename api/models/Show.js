/**
* User.js
*
* @description :: This is a basic user model which enables supports user registration and password resets.
* @docs        :: http://sailsjs.org/#!documentation/models
*/


module.exports = {
    tableName: 'shows',
    migrate: 'alter',
    autoPk: true,
    attributes: {
        title: {
            type: 'string',
            //required: true
        },
        media_name: {
          type: 'string',
        },
        // show_type: {
        //     type: 'string',
        //     enum: [1, 2, 3], // 1 = Free, 2 = member only, 3 = Premium
        //     defaultsTo: 1
        // },
        duration_hour: {
          type: 'integer',
          defaultsTo: 0
        },
        duration_minute: {
          type: 'integer',
          defaultsTo: 0
        },
        description: {
          type: 'mediumtext',
        },
        // synopsis: {
        //   type: 'longtext',
        // },
        artist: {
          model: 'user',
        },
        price: {
          type: 'float',
        },
        ondemand_price: {
          type: 'float',
        },
        // point_value: {
        //   type: 'integer',
        // },
        audiance_capacity: {
          type: 'integer',
        },
        publish_date: {
          type: 'string',
        },
        publish_date_utc: {
          type: 'string',
        },
        show_end_date: {
          type: 'string',
        },
        stream_name: {
          type: 'string',
        },
        status: {
          type: 'string',
          enum: ['Draft', 'Pending', 'Published', 'Archived'],
          defaultsTo: 'Published'
        },
        tags: {
          type: 'longtext',
        },
        meta_title: {
          type: 'string',
        },
        meta_keys: {
          type: 'string',
        },
        meta_description: {
          type: 'string',
        },
        classification_category: {
            model: 'classificationcategory',
        },
        city: {
          type: 'string',
        },
        is_featured: {
          type: 'boolean',
          defaultsTo: false
        },
        is_reminder: {
          type: 'boolean',
          defaultsTo: false
        },
        is_comment: {
            type: 'boolean',
            defaultsTo: true
        },
        is_paid: {
          type: 'boolean',
          defaultsTo: true
        },
        is_finalize_list: {
            type: 'boolean',
            defaultsTo: false
        },
        payout_done: {
            type: 'boolean',
            defaultsTo: false
        },
        setlistemail_done: {
            type: 'boolean',
            defaultsTo: false
        },
        timezone: {
          model: 'timezone',
        },
        toptipperemail_done: {
            type: 'boolean',
            defaultsTo: false
        },
        is_password: {
          type: 'boolean',
          defaultsTo: false
        },
        password: {
          type: 'string'
        },

        // reference
        genres: {
          collection: 'genre',
          via: 'show',
          through: 'showgenre'
        },
        tipprizes: {
          collection: 'tipprize',
          via: 'show',
          through: 'showtipprize'
        },
        showlist: {
          collection: 'showlist',
          via: 'show',
        },
        setlist: {
          collection: 'setlist',
          via: 'show',
        },
        showtickets: {
          collection: 'showticket',
          via: 'show',
        },
        showtips: {
          collection: 'artisttip',
          via: 'show',
        },
        vodpayment: {
          collection: 'vodpayment',
          via: 'show',
        },
        toJSON: function() {
          var obj = this.toObject();
          obj = Utils.cleanObj(obj);

          //obj.vod_type = 1;
          if(obj.media_name == "" || typeof obj.media_name == 'undefined')
            obj.media_name="";
          else
            obj.media_name = sails.config.s3bucket.bucketurl+sails.config.s3bucket.showmedia+'/'+obj.media_name;

            obj.wowza_link = 'https://'+sails.config.wowza_server_ip+'/'+sails.config.wowza_application+'/'+obj.stream_name+'/playlist.m3u8';
          return obj;
        }
      },

      afterDestroy: function (destroyedRecords, next) {

        const options = { key: sails.config.s3bucket.accesskey, secret: sails.config.s3bucket.secretkey, bucket: sails.config.s3bucket.bucketname}

        // Create and configure an adapter
        const adapter = require('skipper-better-s3')(options);
        _.each(destroyedRecords,function(indvShow){

          adapter.rm('show/'+indvShow.media_name, (err, res) => {
          });

        });

        ShowGenre.destroy({show: _.pluck(destroyedRecords, 'id')}).exec(next);
        ShowList.destroy({show: _.pluck(destroyedRecords, 'id')}).exec(next);

        //next(null, true);
      },

};
